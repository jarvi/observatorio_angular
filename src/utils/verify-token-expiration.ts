export const verifyTokenExpiration = (token: string) => {
    try {
        const payload = JSON.parse(window.atob(token.split('.')[1]));
        const expirationDate = new Date(payload.exp * 1000);
        const currentDate = new Date();
        return expirationDate > currentDate;
    } catch (error) {
        return false;
    }
}