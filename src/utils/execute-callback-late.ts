export const executeCallbackLate = (callback: () => void) => {
    setTimeout(() => { callback(); });
}