export const numberToWeekDay = (dayNumber: number) => {
    const weekDays = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
    return weekDays[dayNumber];
}

export const weekDayToNumber = (day: string) => {
    const weekDays = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
    const dayIndex = weekDays.indexOf(day);

    return dayIndex;
}

export const numberToMonth = (monthNumber: number) => {
    const months = [
        "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
    return months[monthNumber - 1];
}

export const monthToNumber = (month: string) => {
    const months = [
        "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
    const monthIndex = months.indexOf(month);
    
    return monthIndex + 1;
}

export const createDateFromTimeString = (timeString: string) => {
    const [hours, minutes, seconds] = timeString.split(':').map(Number);
    const currentDate = new Date();
    currentDate.setHours(hours, minutes, seconds);
    return currentDate;
}

export const getFirstAndLast = (array: number[]) => {
    if (array.length === 0) return [];
    if (array.length === 1) return [array[0], array[0]];

    return [array[0], array[array.length - 1]];
}

export const formatDateToTimeString = (date: Date): string => {
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');

    return `${hours}:${minutes}:${seconds}`;
}