export const switchedFormStructure = () => {
    return {
        consultUsers: [''],
        createUsers: [''],
        editUsers: [''],
        inactivateUsers: [''],
        consultRoles: [''],
        createRoles: [''],
        editRoles: [''],
        inactivateRoles: [''],
        manageDataSources: [''],
        viewBoards: [''],
        viewNotifications: [''],
        editNotifications: [''],
        generateReports: [''],
        viewReports: [''],
    }
}