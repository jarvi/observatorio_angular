import { Validators } from "@angular/forms";
import { ValidatorService } from "src/app/shared/services/validator.service";

const validatorService = new ValidatorService();

const passwordValidators = [
    Validators.required,
    Validators.minLength(7),
    validatorService.whiteSpacesValidator(),
    validatorService.upperCaseLowerCaseValidator(),
    validatorService.specialCharacterValidator(),
];

export const personalInformationFormStructure = (isEdit: boolean) => {
    return {
        identificationType: [{ value: null, disabled: isEdit}, [Validators.required]],
        identificationNumber: [{ value: null, disabled: isEdit}, [Validators.required, 
            validatorService.minMaxLengthValidator(0, 20),
            validatorService.whiteSpacesStartValidator(),
        ]],
        name: ['', [
            Validators.required,
            Validators.maxLength(30),
            validatorService.whiteSpacesStartValidator()
        ]],
        surnames: ['', [
            Validators.required,
            Validators.maxLength(30),
            validatorService.whiteSpacesStartValidator()
        ]],
        email: ['', [
            Validators.required,
            Validators.maxLength(40),
            validatorService.emailValidator(),
            validatorService.medellinEmailValidator(),
            validatorService.whiteSpacesStartValidator()
        ]],
        gender: [null, [Validators.required]],
        phone: [null, [
            Validators.required, 
            validatorService.minMaxLengthValidator(0, 15),
            validatorService.whiteSpacesStartValidator()
        ]],
        contactNumber: [null, [
            validatorService.minMaxLengthValidator(0, 15),
            validatorService.whiteSpacesStartValidator()
        ]],
        role: [null, [Validators.required]],
        address: ['', [
            Validators.maxLength(50),
            validatorService.whiteSpacesStartValidator()
        ]],
    }
}

export const passwordFormStructure = {
    password: ['', passwordValidators],
    passwordConfirmation: ['', passwordValidators],
    passwordMatch: [false, [Validators.requiredTrue]],
    sendEmail: [true]
}