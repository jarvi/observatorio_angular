import { FormGroup } from "@angular/forms";
import { PERMISSION_TYPE } from "src/app/shared/enums/permission-type.enum";

export const getPermissionsFromSwitches = (form: FormGroup): string => {

    const defaultPermissions = Object.values(PERMISSION_TYPE);

    let permissions: string[] = defaultPermissions.map((permission) => {
        return form.controls[permission.formName].value;
    });

    permissions = permissions.filter((permission: string) => permission !== '' && permission !== null);

    return permissions.join(',');
}

export const preloadPermissions = (form: FormGroup, permissions: string[]) => {
    
    const defaultPermissions = Object.values(PERMISSION_TYPE);

    defaultPermissions.forEach((permission) => {
        return form.controls[permission.formName]?.setValue('');
    });

    permissions.forEach((permission: string) => {
        const permissionFound = defaultPermissions.find((defaultPermission) => defaultPermission.name === permission);
        permissionFound && form.controls[permissionFound.formName]?.setValue(permissionFound.name);
    });
}

export const getLabelsFromPermissions = (permissions: string[]): string[] => {
    return permissions.map((permission) => {
        const permissionFound = Object.values(PERMISSION_TYPE).find((defaultPermission) => defaultPermission.name === permission);
        return permissionFound?.labeledValue ?? '';
    });
}

export const getPermissionsFromLabels = (labels: string[]): string[] => {
    return labels.map((label) => {
        const permissionFound = Object.values(PERMISSION_TYPE).find((defaultPermission) => defaultPermission.labeledValue === label);
        return permissionFound?.name ?? '';
    });
}

export const getPermissionsFromForm = (form: FormGroup): string[] => {

    const defaultPermissions = Object.values(PERMISSION_TYPE);

    const permissionsFormControls = defaultPermissions.map((permission) => {
        return form.controls[permission.formName];
    });

    const permissions = permissionsFormControls.filter((control) => control.value !== '' && control.value !== null).map((control) => {
        return control.value;
    });

    return permissions;
}