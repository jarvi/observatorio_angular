export const generateRandomPassword = () => {
    const uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
    const specialCharacters = '!@#$%^&*()';

    const passwordLength = 7;
    const password = [
        uppercaseLetters[getRandomIndex(uppercaseLetters.length)],
        lowercaseLetters[getRandomIndex(lowercaseLetters.length)],
        specialCharacters[getRandomIndex(specialCharacters.length)]
    ];

    const remainingLength = passwordLength - password.length;
    const allCharacters = uppercaseLetters + lowercaseLetters + specialCharacters;

    for (let i = 0; i < remainingLength; i++) {
        password.push(allCharacters[getRandomIndex(allCharacters.length)]);
    }

    return password.join('');
}

const getRandomIndex = (max: number): number => {
    const array = new Uint32Array(1);
    window.crypto.getRandomValues(array);
    const randomValue = array[0] / (Math.pow(2, 32) - 1);
    return Math.floor(randomValue * max);
}