import { AbstractControl, FormGroup } from "@angular/forms";

export const formHasSomeValue = (formGroup: FormGroup, excludeControl: string[]): boolean => {
    for (const controlName in formGroup.controls) {
        if (!excludeControl.includes(controlName)) {
            const control: AbstractControl = formGroup.get(controlName)!;
            if (control?.value) {
                return true;
            }
        }
    }
    return false;
}
