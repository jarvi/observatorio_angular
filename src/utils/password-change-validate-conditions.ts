import { PasswordConditions } from "src/app/modules/administration/modules/user/interfaces/password-conditions.interface";

export const onPasswordChangeValidateConditions = (password: string, conditions: PasswordConditions[]) => {
    const trimmedPassword = password?.trim();
    const upperCase = trimmedPassword?.match(/[A-Z]/g);
    const lowerCase = trimmedPassword?.match(/[a-z]/g);
    const specialCharacter = trimmedPassword?.match(/[^a-zA-Z0-9\s]/g);

    conditions.forEach((condition) => {
        switch (condition.id) {
            case 'minLength':
                condition.isValid = trimmedPassword?.length >= 7 || false;
                break;
            case 'upperCaseLowerCase':
                condition.isValid = !!upperCase && !!lowerCase;
                break;
            case 'specialCharacter':
                condition.isValid = !!specialCharacter;
                break;
        }
    });
}