export const addLeadingZero = (input: string): string => {
    return input.toString().length === 1 ? '0' + input : input;
}