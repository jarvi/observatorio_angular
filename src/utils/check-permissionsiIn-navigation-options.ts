import { navigationLinkOptions } from "src/app/modules/dashboard/constants/navitation-link-options";
import { NavigationLink } from "src/app/modules/dashboard/interfaces/navigation-link.interface";
import { PermissionType } from "src/app/shared/interfaces/permission-type.interface";

const checkPermissionsInNavigationOptions = (options: any[], permissions: string[]): any[] => {
    const filteredOptions = options.filter((option) => {
        const hasPermission = option.route === '/home' || option.permissions?.some((permission: any) => permissions.includes(permission));

        if (hasPermission) {
            return true;
        }

        if (option.subOptions && option.subOptions.length > 0) {
            const filteredSubOptions = checkPermissionsInNavigationOptions(option.subOptions, permissions);
            return filteredSubOptions.length > 0;
        }

        return false;
    });

    return filteredOptions.map((option) => ({
        ...option,
        subOptions: option.subOptions
            ? checkPermissionsInNavigationOptions(option.subOptions, permissions)
            : [],
    }));
};

export const filterNavigationOptions = (permissions: PermissionType[]): NavigationLink[] => {
    const permissionsList = permissions.map((permission) => permission.codename);

    if (permissionsList.length > 0) {
        const filteredOptions = checkPermissionsInNavigationOptions(navigationLinkOptions, permissionsList);
        return filteredOptions;
    }
    return [];
}