import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BiBoardsComponent } from './pages/bi-boards/bi-boards.component';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';
import { BiBoardComponent } from './pages/bi-board/bi-board.component';

const routes: Routes = [
  {
    path: '',
    component: BiBoardsComponent,
    data: {
      breadcrumb: '',
      permissions: [
        PERMISSION_TYPE.viewBoard.name,
      ]
    }
  },
  {
    path: 'board/:name/:reportId',
    component: BiBoardComponent,
    data: {
      breadcrumb: 'Tablero',
      permissions: [
        PERMISSION_TYPE.viewBoard.name,
      ]
    },
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BiBoardsRoutingModule { }
