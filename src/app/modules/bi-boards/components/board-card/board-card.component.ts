import { Component, Input } from '@angular/core';

@Component({
  selector: 'bi-boards-board-card',
  templateUrl: './board-card.component.html',
  styleUrls: ['./board-card.component.scss']
})
export class BoardCardComponent {

  @Input() reportId: string = '';
  @Input() class: string = '';
  @Input() name: string = '';
  @Input() image: string = '';
}
