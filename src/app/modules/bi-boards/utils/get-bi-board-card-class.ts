import { BI_BOARD_NAME } from "../enums/bi-board-name.enum";

export const getBiBoardCardClass = (name: string): string => {
    let className: string = ''

    switch (name) {
        case BI_BOARD_NAME.educativeTransformation:
            className = 'educative-transformation'
            break;
        case BI_BOARD_NAME.indicators:
            className = 'indicators'
            break;
        case BI_BOARD_NAME.enrollmentReport:
            className = 'enrollment-weekly-report'
            break;
        case BI_BOARD_NAME.reportAnnexes:
            className = 'report-annexes'
            break;
        case BI_BOARD_NAME.schoolEnvironment:
            className = 'school-environment'
            break;
        case BI_BOARD_NAME.characterizationOfEducationalInstitutions:
            className = 'characterization'
            break;
        case BI_BOARD_NAME.reportCommunes:
            className = 'report-communes'
            break;
        case BI_BOARD_NAME.ieReport:
            className = 'ie-report'
            break;
        case BI_BOARD_NAME.coreReport:
            className = 'core-report'
            break;
        case BI_BOARD_NAME.saber11Icfes:
            className = 'saber-11-icfes'
            break;
        case BI_BOARD_NAME.indicatorManagement:
            className = 'indicator-management'
            break;
        case BI_BOARD_NAME.teachersCharacterization:
            className = 'teachers-characterization'
            break;
        case BI_BOARD_NAME.futureComputers:
            className = 'future-computers'
            break;
        default:
            className = 'default'
            break;
    }

    return className;
}