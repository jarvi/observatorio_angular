import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BiBoardsRoutingModule } from './bi-boards-routing.module';

import { SharedModule } from 'src/app/shared/shared.module';

import { BiBoardsComponent } from './pages/bi-boards/bi-boards.component';
import { BiBoardComponent } from './pages/bi-board/bi-board.component';
import { BoardCardComponent } from './components/board-card/board-card.component';


@NgModule({
  declarations: [
    BiBoardsComponent,
    BiBoardComponent,
    BoardCardComponent
  ],
  imports: [
    CommonModule,
    BiBoardsRoutingModule,

    SharedModule
  ]
})
export class BiBoardsModule { }
