import { TestBed } from '@angular/core/testing';

import { BiBoardService } from './bi-board.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { of, take, throwError } from 'rxjs';
import { BIBoardCard } from '../interfaces/bi-board-card.interfaces';
import { GetBoardResponse } from '../interfaces/get-board-response.interface';

describe('BiBoardService', () => {
  let service: BiBoardService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService
      ]
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    service = TestBed.inject(BiBoardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the bi board cards expected', () => {

    const responseExpected: BIBoardCard[] = [
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-01', class: 'educative-transformation', name: 'Transformación Educativa', image: './../../../../assets/images/biBoardImages/1.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-02', class: 'indicators', name: 'Indicadores', image: './../../../../assets/images/biBoardImages/2.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-03', class: 'enrollment-weekly-report', name: 'Reporte de Matrículas', image: './../../../../assets/images/biBoardImages/3.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-04', class: 'report-annexes', name: 'Reporte Anexos', image: './../../../../assets/images/biBoardImages/4.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-05', class: 'school-environment', name: 'Ambiente Escolar', image: './../../../../assets/images/biBoardImages/5.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-06', class: 'characterization', name: 'Caracterización de Instituciones Educativas', image: './../../../../assets/images/biBoardImages/6.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-07', class: 'report-communes', name: 'Reporte Comunas', image: './../../../../assets/images/biBoardImages/7.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-08', class: 'ie-report', name: 'Reporte IE', image: './../../../../assets/images/biBoardImages/8.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-09', class: 'core-report', name: 'Reporte Núcleos', image: './../../../../assets/images/biBoardImages/9.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-10', class: 'saber-11-icfes', name: 'Comportamiento histórico Pruebas Saber', image: './../../../../assets/images/biBoardImages/10.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-11', class: 'indicator-management', name: 'Gestión indicadores', image: './../../../../assets/images/biBoardImages/11.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-12', class: 'teachers-characterization', name: 'Caracterización Docentes', image: './../../../../assets/images/biBoardImages/12.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-13', class: 'future-computers', name: 'Computadores Futuro', image: './../../../../assets/images/biBoardImages/13.webp' },
      { workSpaceId: 'abc-123-workspace', reportId: 'abc-123-14', class: 'default', name: 'Computadores', image: './../../../../assets/images/biBoardImages/14.webp' },
    ]
    const responseService: GetBoardResponse[] = [
      { id: 1, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/1.webp', name: 'Transformación Educativa', report_id: 'abc-123-01', updated_at: '', web_iframe: '' },
      { id: 2, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/2.webp', name: 'Indicadores', report_id: 'abc-123-02', updated_at: '', web_iframe: '' },
      { id: 3, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/3.webp', name: 'Reporte de Matrículas', report_id: 'abc-123-03', updated_at: '', web_iframe: '' },
      { id: 4, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/4.webp', name: 'Reporte Anexos', report_id: 'abc-123-04', updated_at: '', web_iframe: '' },
      { id: 5, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/5.webp', name: 'Ambiente Escolar', report_id: 'abc-123-05', updated_at: '', web_iframe: '' },
      { id: 6, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/6.webp', name: 'Caracterización de Instituciones Educativas', report_id: 'abc-123-06', updated_at: '', web_iframe: '' },
      { id: 7, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/7.webp', name: 'Reporte Comunas', report_id: 'abc-123-07', updated_at: '', web_iframe: '' },
      { id: 8, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/8.webp', name: 'Reporte IE', report_id: 'abc-123-08', updated_at: '', web_iframe: '' },
      { id: 9, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/9.webp', name: 'Reporte Núcleos', report_id: 'abc-123-09', updated_at: '', web_iframe: '' },
      { id: 10, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/10.webp', name: 'Comportamiento histórico Pruebas Saber', report_id: 'abc-123-10', updated_at: '', web_iframe: '' },
      { id: 11, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/11.webp', name: 'Gestión indicadores', report_id: 'abc-123-11', updated_at: '', web_iframe: '' },
      { id: 12, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/12.webp', name: 'Caracterización Docentes', report_id: 'abc-123-12', updated_at: '', web_iframe: '' },
      { id: 13, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/13.webp', name: 'Computadores Futuro', report_id: 'abc-123-13', updated_at: '', web_iframe: '' },
      { id: 14, group_id: 'abc-123-workspace', embeded_url: 'url', created_at: '', image_url: './../../../../assets/images/biBoardImages/14.webp', name: 'Computadores', report_id: 'abc-123-14', updated_at: '', web_iframe: '' },
    ]

    spyOn(httpRequestService, 'get').and.returnValue(of(responseService));

    service.getBoards()
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should does not get the bi board cards expected if there an error', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.getBoards()
      .pipe(take(1))
      .subscribe(
        () => { },
        (err) => {
          expect(err).toEqual('No fue posible obtener los tableros');
        }
      );
  });

  it('should get the bi board expected', () => {
    const responseService: GetBoardResponse = {
      id: 1,
      group_id: 'abc-123-workspace',
      embeded_url: 'url',
      created_at: '',
      image_url: './../../../../assets/images/biBoardImages/1.webp',
      name: 'Transformación Educativa',
      report_id: 'abc-123-01',
      updated_at: '',
      web_iframe: ''
    }

    spyOn(httpRequestService, 'get').and.returnValue(of(responseService));

    service.getBoard('abc-123-01')
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseService);
      });
  });

  it('should does not get the bi board expected if there an error', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.getBoard('abc-123-01')
      .pipe(take(1))
      .subscribe(
        () => { },
        (err) => {
          expect(err).toEqual('No fue posible obtener el tablero');
        }
      );
  });
});
