import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { getBiBoardCardClass } from '../utils/get-bi-board-card-class';
import { environment } from 'src/environments/environment';
import { BIBoardCard } from '../interfaces/bi-board-card.interfaces';
import { GetBoardResponse } from '../interfaces/get-board-response.interface';

@Injectable({
  providedIn: 'root'
})
export class BiBoardService {

  private _httpRequestService = inject(HttpRequestService);

  private _pathBoard: string = environment.pathBoard;

  public getBoards(): Observable<BIBoardCard[]> {
    return this._httpRequestService.get<{}, GetBoardResponse[]>(`${this._pathBoard}/list/`)
      .pipe(
        map((response: GetBoardResponse[]) => {
          const biBoardCards: BIBoardCard[] = response.map((board) => {
            return {
              workSpaceId: board.group_id,
              reportId: board.report_id,
              name: board.name,
              image: board.image_url,
              class: getBiBoardCardClass(board.name)
            }
          })
          return biBoardCards;
        }),
        catchError((error) => {
          return throwError(() => 'No fue posible obtener los tableros');
        })
      );
  }

  public getBoard(reportId: string): Observable<GetBoardResponse> {
    return this._httpRequestService.get<{}, GetBoardResponse>(`${this._pathBoard}/${reportId}/`)
      .pipe(
        map((response: GetBoardResponse) => {
          return response;
        }),
        catchError((error) => {
          return throwError(() => 'No fue posible obtener el tablero');
        })
      );
  }
}
