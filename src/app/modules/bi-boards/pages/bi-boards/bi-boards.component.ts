import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { BIBoardCard } from '../../interfaces/bi-board-card.interfaces';
import { BiBoardService } from '../../services/bi-board.service';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { ModalAlert } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Component({
  templateUrl: './bi-boards.component.html',
  styleUrls: ['./bi-boards.component.scss']
})
export class BiBoardsComponent implements OnInit, OnDestroy {

  private _router = inject(Router);
  private _biBoardService = inject(BiBoardService);
  private _mobileWidthService = inject(MobileWidthService);
  private _modalAlertService = inject(ModalAlertService)
  private _spinnerService = inject(SpinnerService);

  private _widthSubscription: Subscription = new Subscription();
  private _boardsSubscription: Subscription = new Subscription();

  public isScreenSmall: boolean = false;
  public cards: BIBoardCard[] = [];

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });

    this._spinnerService.showSpinner = true;
    this._boardsSubscription = this._biBoardService.getBoards().subscribe({
      next: (boardCards) => {
        this.cards = boardCards;
        this._spinnerService.showSpinner = false;
      },
      error: (error) => {
        this._spinnerService.showSpinner = false;
        this.showErrorAlert(error);
        this._router.navigateByUrl('/');
      }
    })
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
    this._boardsSubscription.unsubscribe();
  }

  private showErrorAlert(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Continuar',
        primary: true,
      },
      message
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }
}
