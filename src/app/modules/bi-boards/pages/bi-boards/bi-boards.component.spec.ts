import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiBoardsComponent } from './bi-boards.component';
import { BoardCardComponent } from '../../components/board-card/board-card.component';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BiBoardService } from '../../services/bi-board.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, throwError } from 'rxjs';

describe('BiBoardsComponent', () => {
  let component: BiBoardsComponent;
  let fixture: ComponentFixture<BiBoardsComponent>;
  let biBoardService: BiBoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        BiBoardsComponent,
        BoardCardComponent,
      ],
      providers: [
        BiBoardService,
        HttpRequestService
      ],
      imports: [
        RouterModule,
        RouterTestingModule,
        HttpClientTestingModule
      ]
    });
    fixture = TestBed.createComponent(BiBoardsComponent);
    biBoardService = TestBed.inject(BiBoardService);

    spyOn(biBoardService, 'getBoards').and.returnValue(of([]));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('BiBoardsComponent with getBoards request error', () => {
  let component: BiBoardsComponent;
  let fixture: ComponentFixture<BiBoardsComponent>;
  let biBoardService: BiBoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        BiBoardsComponent,
        BoardCardComponent,
      ],
      providers: [
        BiBoardService,
        HttpRequestService
      ],
      imports: [
        RouterModule,
        RouterTestingModule,
        HttpClientTestingModule
      ]
    });
    fixture = TestBed.createComponent(BiBoardsComponent);
    biBoardService = TestBed.inject(BiBoardService);

    spyOn(biBoardService, 'getBoards').and.returnValue(throwError(() => {}));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
