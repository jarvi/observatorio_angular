import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Subscription, mergeMap } from 'rxjs';
import { BiBoardService } from '../../services/bi-board.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { ModalAlert } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';

@Component({
  templateUrl: './bi-board.component.html',
  styleUrls: ['./bi-board.component.scss']
})
export class BiBoardComponent implements OnInit, OnDestroy {

  private _router = inject(Router);
  private _activatedRoute = inject(ActivatedRoute);
  private _biBoardService = inject(BiBoardService)
  private _spinnerService = inject(SpinnerService);
  private _modalAlertService = inject(ModalAlertService);
  private _mobileWidthService = inject(MobileWidthService);
  private _sanitizer = inject(DomSanitizer);

  private _paramsSubscription: Subscription = new Subscription();
  private _widthSubscription: Subscription = new Subscription();

  public isScreenSmall: boolean = false;
  public iframeURL: string = '';
  public safeIframeURL?: SafeResourceUrl;

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
  
    this._spinnerService.showSpinner = true;

    this._paramsSubscription = this._activatedRoute.paramMap.pipe(
      mergeMap((params) => {
        const reportId = params.get('reportId') ?? '';
        return this._biBoardService.getBoard(reportId);
      })).subscribe({
        next: board => {
          this.iframeURL = board.embeded_url;
          this._spinnerService.showSpinner = false;
          this.safeIframeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.iframeURL);// NOSONAR
        },
        error: error => {
          error && this.showErrorModal(error);
          this._spinnerService.showSpinner = false;
          this._router.navigateByUrl('/');
        }
      })
  }

  ngOnDestroy(): void {
    this._paramsSubscription.unsubscribe();
    this._widthSubscription.unsubscribe();
  }

  private showErrorModal(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message,
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }
}
