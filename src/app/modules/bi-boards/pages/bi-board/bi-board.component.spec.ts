import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiBoardComponent } from './bi-board.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BiBoardService } from '../../services/bi-board.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { of, throwError } from 'rxjs';

describe('BiBoardComponent', () => {
  let component: BiBoardComponent;
  let fixture: ComponentFixture<BiBoardComponent>;
  let biBoardService: BiBoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BiBoardComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      providers: [
        BiBoardService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ reportId: '123-abc' }))
          },
        },
      ]
    });
    fixture = TestBed.createComponent(BiBoardComponent);
    biBoardService = TestBed.inject(BiBoardService);

    spyOn(biBoardService, 'getBoard').and.returnValue(of({
      id: 1,
      group_id: 'abc-123-workspace',
      embeded_url: 'url',
      created_at: '',
      image_url: './../../../../assets/images/biBoardImages/1.webp',
      name: 'Transformación Educativa',
      report_id: 'abc-123-01',
      updated_at: '',
      web_iframe: ''
    }));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('BiBoardComponent with error in BiBoardService', () => {
  let component: BiBoardComponent;
  let fixture: ComponentFixture<BiBoardComponent>;
  let biBoardService: BiBoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BiBoardComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      providers: [
        BiBoardService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ reportId: '123-abc' }))
          },
        },
      ]
    });
    fixture = TestBed.createComponent(BiBoardComponent);
    biBoardService = TestBed.inject(BiBoardService);

    spyOn(biBoardService, 'getBoard').and.returnValue(throwError(() => 'Error'));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('BiBoardComponent without paramMap', () => {
  let component: BiBoardComponent;
  let fixture: ComponentFixture<BiBoardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BiBoardComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      providers: [
        BiBoardService,
        HttpRequestService,
      ]
    });
    fixture = TestBed.createComponent(BiBoardComponent);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

