export interface GetBoardResponse {
    id: number;
    report_id: string;
    group_id: string;
    embeded_url: string;
    name: string;
    web_iframe: string;
    image_url: string;
    created_at: string;
    updated_at: string;
}