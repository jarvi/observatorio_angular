export interface BIBoardCard {
    workSpaceId: string;
    reportId: string;
    class: string;
    name: string;
    image: string;
}