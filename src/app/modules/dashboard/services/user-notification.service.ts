import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';
import { NotificationResponse } from '../interfaces/notifications.interface';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserNotificationService {

  private _httpRequestService = inject(HttpRequestService);

  private _pathNotifications: string = environment.pathNotifications;

  public getNotifications(): Observable<NotificationResponse[]> {
    return this._httpRequestService.get<{}, NotificationResponse[]>(`${this._pathNotifications}/`)
      .pipe(
        map((response: NotificationResponse[]) => response),
        catchError((error) => {
          return throwError(() => 'No fue posible obtener las notificaciones del usuario');
        })
      );
  }

  public readNotification(notificationId: number): Observable<NotificationResponse> {
    return this._httpRequestService.put<{}, NotificationResponse>(`${this._pathNotifications}/read-unread/${notificationId}/`)
      .pipe(
        map((response: NotificationResponse) => response),
        catchError((error) => {
          return throwError(() => 'No fue posible leer la notificación');
        })
      );
  }

  public deleteNotification(notificationId: number): Observable<number> {
    return this._httpRequestService.delete<number>(`${this._pathNotifications}/delete-notification/${notificationId}/`)
      .pipe(
        map(() =>  notificationId),
        catchError((error) => {
          return throwError(() => 'No fue posible eliminar la notificación');
        })
      );
  }

  public markAllNotificationsAsReaded(): Observable<boolean> {
    return this._httpRequestService.put<{}, {}>(`${this._pathNotifications}/read-all/`)
      .pipe(
        map(() => {
          return true;
        }),
        catchError((error) => {
          return throwError(() => 'No fue posible leer todas las notificaciones');
        })
      );
  }
}
