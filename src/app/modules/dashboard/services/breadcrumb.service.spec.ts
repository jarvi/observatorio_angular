import { TestBed } from '@angular/core/testing';
import { BreadcrumbService } from './breadcrumb.service';
import { Router, RouterEvent, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { DATA_ROUTES } from 'src/app/shared/enums/data-options-types';

describe('BreadcrumbService', () => {
  let service: BreadcrumbService;
  let router: Router;
  let routerEventsSubject: BehaviorSubject<RouterEvent>;

  beforeEach(() => {
    routerEventsSubject = new BehaviorSubject<any>(null);

    TestBed.configureTestingModule({
      providers: [
        BreadcrumbService,
        {
          provide: Router,
          useValue: {
            events: routerEventsSubject.asObservable(),
            routerState: {
              snapshot: {
                root: {
                  data: {
                    breadcrumb: 'Home'
                  },
                  url: [
                    { path: DATA_ROUTES.dataLoading },
                  ],
                  firstChild: {
                    data: {
                      breadcrumb: (data: any) => {
                        return `Breadcrumb for ${data}`;
                      }
                    },
                    url: ['/dashboard']
                  },
                  params: {
                    type: 'PRUEBA'
                  }
                },
                toString: () => ''
              } as unknown as RouterStateSnapshot
            }
          }
        }
      ]
    });

    router = TestBed.inject(Router);
    service = TestBed.inject(BreadcrumbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

describe('BreadcrumbService for board', () => {
  let service: BreadcrumbService;
  let router: Router;
  let routerEventsSubject: BehaviorSubject<RouterEvent>;

  beforeEach(() => {
    routerEventsSubject = new BehaviorSubject<any>(null);

    TestBed.configureTestingModule({
      providers: [
        BreadcrumbService,
        {
          provide: Router,
          useValue: {
            events: routerEventsSubject.asObservable(),
            routerState: {
              snapshot: {
                root: {
                  data: {
                    breadcrumb: 'Boards'
                  },
                  url: [
                    { path: DATA_ROUTES.board },
                  ],
                  firstChild: {
                    data: {
                      breadcrumb: (data: any) => {
                        return `Breadcrumb for ${data}`;
                      }
                    },
                    url: ['/dashboard']
                  },
                  params: {
                    name: 'PRUEBA'
                  }
                },
                toString: () => ''
              } as unknown as RouterStateSnapshot
            }
          }
        }
      ]
    });

    router = TestBed.inject(Router);
    service = TestBed.inject(BreadcrumbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});