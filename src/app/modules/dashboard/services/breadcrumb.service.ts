import { Injectable, OnDestroy } from '@angular/core';
import { ActivatedRouteSnapshot, Data, Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Breadcrumb } from '../interfaces/breadcrumb.interface';
import { DATA_ROUTES } from 'src/app/shared/enums/data-options-types';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService implements OnDestroy {

  private _breadcrumbSubscription: Subscription = new Subscription();
  private readonly _breadcrumbs$ = new BehaviorSubject<Breadcrumb[]>([]);
  public readonly breadcrumbs$ = this._breadcrumbs$.asObservable();

  constructor(private _router: Router) {
    this._breadcrumbSubscription = this._router.events
      .subscribe(() => {
        const root = this._router.routerState.snapshot.root;
        const breadcrumbs: Breadcrumb[] = [];
        
        this.addBreadcrumb(root, [], breadcrumbs);
        this._breadcrumbs$.next(breadcrumbs);
      });
  }

  ngOnDestroy(): void {
    this._breadcrumbSubscription.unsubscribe();
  }

  private addBreadcrumb(route: ActivatedRouteSnapshot, parentUrl: string[], breadcrumbs: Breadcrumb[]) {
    if (route) {
      const routeUrl = parentUrl.concat(route.url.map(url => url.path));

      if (route.data['breadcrumb']) {
        const breadcrumb = {
          label: this.getLabel(route),
          url: '/' + routeUrl.join('/')
        };
        breadcrumbs.push(breadcrumb);
      }

      route.firstChild && this.addBreadcrumb(route.firstChild, routeUrl, breadcrumbs);
    }
  }

  private getLabel(route: ActivatedRouteSnapshot) {
    const data: Data = route.data;

    let label = typeof data['breadcrumb'] === 'function' ? data['breadcrumb'](data) : data['breadcrumb'];
    
    return this.getLabelForSpecialRoute(label, route);
  }

  private getLabelForSpecialRoute(label: string, route: ActivatedRouteSnapshot): string {
    const routes = route.url.map(url => url.path);

    if(routes.includes(DATA_ROUTES.dataLoading)) {
      const type = route.params['type'];
      return `${label} ${type}`
    }
    
    if(routes.includes(DATA_ROUTES.board)) {
      const name = route.params['name'];
      return `${label} ${name}`
    }
    
    return label;
  }
}
