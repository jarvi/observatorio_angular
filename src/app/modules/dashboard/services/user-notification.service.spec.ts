import { TestBed } from '@angular/core/testing';

import { UserNotificationService } from './user-notification.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpRequestService } from 'src/app/shared/services';
import { of, take, throwError } from 'rxjs';
import { NotificationResponse } from '../interfaces/notifications.interface';

describe('UserNotificationService', () => {
  let service: UserNotificationService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      providers: [
        HttpRequestService,
      ]
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    service = TestBed.inject(UserNotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get notifications', () => {
    const responseExpected: NotificationResponse[] = [{
      id: 10,
      data_source: "SIMAT",
      message: "Saludos desde db 1",
      many_days: 1,
      is_readed: true,
      created_at: "2023-09-13T13:22:58.753000-05:00",
      updated_at: "2023-10-30T16:52:36.877805-05:00"
    }]

    spyOn(httpRequestService, 'get').and.returnValue(of(responseExpected));

    service.getNotifications()
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should not get notifications and return an error', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.getNotifications()
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('No fue posible obtener las notificaciones del usuario');
        }
      );
  });

  it('should read a notification', () => {
    const responseExpected: NotificationResponse = {
      id: 10,
      data_source: "SIMAT",
      message: "Saludos desde db 1",
      many_days: 1,
      is_readed: true,
      created_at: "2023-09-13T13:22:58.753000-05:00",
      updated_at: "2023-10-30T16:52:36.877805-05:00"
    }

    spyOn(httpRequestService, 'put').and.returnValue(of(responseExpected));

    service.readNotification(1)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should not read a notification and return an error', () => {
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => {}));

    service.readNotification(1)
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('No fue posible leer la notificación');
        }
      );
  });

  it('should delete a notification', () => {
    const responseExpected: number = 1;

    spyOn(httpRequestService, 'delete').and.returnValue(of(responseExpected));

    service.deleteNotification(1)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should not delete a notification and return an error', () => {
    spyOn(httpRequestService, 'delete').and.returnValue(throwError(() => {}));

    service.deleteNotification(1)
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('No fue posible eliminar la notificación');
        }
      );
  });

  it('should read all notifications', () => {

    spyOn(httpRequestService, 'put').and.returnValue(of({}));

    service.markAllNotificationsAsReaded()
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(true);
      });
  });

  it('should not read all notifications and return an error', () => {
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => {}));

    service.markAllNotificationsAsReaded()
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('No fue posible leer todas las notificaciones');
        }
      );
  });
});
