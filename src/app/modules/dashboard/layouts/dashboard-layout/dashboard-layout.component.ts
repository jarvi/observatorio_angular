import { ChangeDetectorRef, Component, OnDestroy, OnInit, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { UserService } from '../../../../shared/services/user.service';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { User } from 'src/app/shared/interfaces';
import { fadeAnimation } from 'src/app/shared/animations/animations';

@Component({
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss'],
  animations: [fadeAnimation]
})
export class DashboardLayoutComponent implements OnInit, OnDestroy {

  private _authService = inject(AuthService);
  private _mobileWidthService = inject(MobileWidthService);
  private _cdRef = inject(ChangeDetectorRef);
  public userInformationService = inject(UserService);

  private _widthSubscription: Subscription = new Subscription();
  private _userInformationSubscription: Subscription = new Subscription();

  public isScreenSmall: boolean = false;
  public userInformation?: User;
  
  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });

    this._userInformationSubscription = this.userInformationService.getUserInformation().subscribe({
      next: (userInformation) => {
        this.userInformation = userInformation;
      },
      error: () => {
        setTimeout(() => {
          this.onLogout();
        }, 200);
      }
    });

    this._cdRef.detectChanges();
  }

  public onLogout(): void {
    this._authService.logout();
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
    this._userInformationSubscription.unsubscribe();
  }
}
