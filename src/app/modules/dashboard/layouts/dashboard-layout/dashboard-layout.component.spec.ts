import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { Router } from '@angular/router';

import { AuthService } from 'src/app/modules/auth/services/auth.service';

import { HttpRequestService } from 'src/app/shared/services/http-request.service';

import { AuthModule } from 'src/app/modules/auth/auth.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { DashboardLayoutComponent } from './dashboard-layout.component';
import { NavbarComponent } from '../../components/navbar/navbar.component';
import { SidebarComponent } from '../../components/sidebar/sidebar.component';
import { SidebarFixedComponent } from '../../components/sidebar-fixed/sidebar-fixed.component';
import { AccordionMenuComponent } from '../../components/accordion-menu/accordion-menu.component';
import { BreadcrumbComponent } from '../../components/breadcrumb/breadcrumb.component';
import { ChangePasswordModalComponent } from '../../components/change-password-modal/change-password-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { of, take, throwError } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationsModalComponent } from '../../components/notifications-modal/notifications-modal.component';
import { NotificationFiltersComponent } from '../../components/notification-filters/notification-filters.component';

describe('DashboardLayoutComponent', () => {
  let component: DashboardLayoutComponent;
  let fixture: ComponentFixture<DashboardLayoutComponent>;
  let userInformationService: UserService;
  let router: Router;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardLayoutComponent,
        NavbarComponent,
        SidebarComponent,
        SidebarFixedComponent,
        AccordionMenuComponent,
        BreadcrumbComponent,
        ChangePasswordModalComponent,
        NotificationsModalComponent,
        NotificationFiltersComponent
      ],
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        AuthModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        AuthService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(DashboardLayoutComponent);
    userInformationService = TestBed.inject(UserService);

    spyOn(userInformationService, 'getUserInformation').and.returnValue(throwError(() => {}));

    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should go to login route when logout method is exec', () => {
    spyOn(router, 'navigateByUrl');
    component.onLogout();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/auth');
  });
});

describe('DashboardLayoutComponent with nice request', () => {
  let component: DashboardLayoutComponent;
  let fixture: ComponentFixture<DashboardLayoutComponent>;
  let userInformationService: UserService;
  let router: Router;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardLayoutComponent,
        NavbarComponent,
        SidebarComponent,
        SidebarFixedComponent,
        AccordionMenuComponent,
        BreadcrumbComponent,
        ChangePasswordModalComponent,
        NotificationsModalComponent,
        NotificationFiltersComponent
      ],
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        AuthModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        AuthService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(DashboardLayoutComponent);
    userInformationService = TestBed.inject(UserService);

    spyOn(userInformationService, 'getUserInformation').and.returnValue(of({
      id: 1,
      email: '',
      firstName: '',
      lastName: '',
      completeName: '',
      isActive: false,
      documentType: {
        id: 0,
        name: '',
        alias: ''
      },
      documentNumber: '',
      address: '',
      cellPhone: '',
      contactNumber: '',
      gender: '',
      roles: []
    }));

    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});