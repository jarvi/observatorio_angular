import { PERMISSION_TYPE } from "src/app/shared/enums/permission-type.enum";
import { NavigationLink, SubNavigationLink } from "../interfaces/navigation-link.interface";
import { DATA_OPTIONS_TYPES, DATA_ROUTES } from "src/app/shared/enums/data-options-types";

const getSubOptions = (id: string, permission: string): SubNavigationLink[] => {
    return [
        { 
            id: `${id}-simat`, 
            name: DATA_OPTIONS_TYPES.simat, 
            route: `/${id}/${DATA_OPTIONS_TYPES.simat}`,
            permissions: [permission]
        },
        { 
            id: `${id}-due`, 
            name: DATA_OPTIONS_TYPES.due, 
            route: `/${id}/${DATA_OPTIONS_TYPES.due}`,
            permissions: [permission]
        },
        { 
            id: `${id}-saber-tests`, 
            name: DATA_OPTIONS_TYPES.saberTests, 
            route: `/${id}/${DATA_OPTIONS_TYPES.saberTests}`,
            permissions: [permission]
        },
        { 
            id: `${id}-humant-talent`, 
            name: DATA_OPTIONS_TYPES.humanTalent, 
            route: `/${id}/${DATA_OPTIONS_TYPES.humanTalent}`,
            permissions: [permission]
        },
        { 
            id: `${id}-pae`, 
            name: DATA_OPTIONS_TYPES.pae, 
            route: `/${id}/${DATA_OPTIONS_TYPES.pae}`,
            permissions: [permission]
        },
        { 
            id: `${id}-future-computers`, 
            name: DATA_OPTIONS_TYPES.futuresComputers, 
            route: `/${id}/${DATA_OPTIONS_TYPES.futuresComputers}`,
            permissions: [permission]
        },
        { 
            id: `${id}-educative-infraestructure`, 
            name: DATA_OPTIONS_TYPES.educativeInfraestructure, 
            route: `/${id}/${DATA_OPTIONS_TYPES.educativeInfraestructure}`,
            permissions: [permission]
        },
        { 
            id: `${id}-internal-efficiency`, 
            name: DATA_OPTIONS_TYPES.internalEfficiency, 
            route: `/${id}/${DATA_OPTIONS_TYPES.internalEfficiency}`,
            permissions: [permission]
        },
        { 
            id: `${id}-technical-average`, 
            name: DATA_OPTIONS_TYPES.technicalAverage, 
            route: `/${id}/${DATA_OPTIONS_TYPES.technicalAverage}`,
            permissions: [permission]
        },
        { 
            id: `${id}-protector-school-environment`, 
            name: DATA_OPTIONS_TYPES.schoolEnvironmentProtector, 
            route: `/${id}/${DATA_OPTIONS_TYPES.schoolEnvironmentProtector}`,
            permissions: [permission]
        },
        { 
            id: `${id}-quota-projection`, 
            name: DATA_OPTIONS_TYPES.projectionOfQuotas, 
            route: `/${id}/${DATA_OPTIONS_TYPES.projectionOfQuotas}`,
            permissions: [permission]
        },
        { 
            id: `${id}-transport`, 
            name: DATA_OPTIONS_TYPES.transport, 
            route: `/${id}/${DATA_OPTIONS_TYPES.transport}`,
            permissions: [permission]
        },
        { 
            id: `${id}-dropout-rate`, 
            name: DATA_OPTIONS_TYPES.dropoutRate, 
            route: `/${id}/${DATA_OPTIONS_TYPES.dropoutRate}`,
            permissions: [permission]
        },
        { 
            id: `${id}-consolidated-simat`, 
            name: DATA_OPTIONS_TYPES.consolidatedSIMAT, 
            route: `/${id}/${DATA_OPTIONS_TYPES.consolidatedSIMAT}`,
            permissions: [permission]
        },
        { 
            id: `${id}-detailed-strategies`, 
            name: DATA_OPTIONS_TYPES.detailedStrategies, 
            route: `/${id}/${DATA_OPTIONS_TYPES.detailedStrategies}`,
            permissions: [permission]
        },
        { 
            id: `${id}-indicative-plan-and-action-plan`, 
            name: DATA_OPTIONS_TYPES.indicativePlanAndActionPlan, 
            route: `/${id}/${DATA_OPTIONS_TYPES.indicativePlanAndActionPlan}`,
            permissions: [permission]
        },
    ]
}

export const navigationLinkOptions: NavigationLink[] = [
    {
        id: 'principal-page',
        name: 'Página Principal',
        route: '/home',
        subOptions: [],
        permissions: []
    },
    {
        id: 'administration',
        name: 'Adminsitración',
        subOptions: [
            { 
                id: 'administration-systems-users', 
                name: 'Usuarios del sistema', 
                route: '/administration/systems-users',
                permissions: [
                    PERMISSION_TYPE.viewUser.name,
                    PERMISSION_TYPE.addUser.name,
                    PERMISSION_TYPE.changeUser.name,
                    PERMISSION_TYPE.deleteUser.name
                ]
            },
            { 
                id: 'administration-roles', 
                name: 'Roles', 
                route: '/administration/roles',
                permissions: [
                    PERMISSION_TYPE.viewRol.name,
                    PERMISSION_TYPE.addRol.name,
                    PERMISSION_TYPE.changeRol.name,
                    PERMISSION_TYPE.deleteRol.name
                ]
            },
        ],
        permissions: []
    },
    {
        id: 'data-sources',
        name: 'Fuentes de datos',
        subOptions: getSubOptions(`data-sources/${DATA_ROUTES.dataLoading}`, PERMISSION_TYPE.addSource.name),
        permissions: []
    },
    {
        id: 'bi-boards',
        name: 'Tableros BI',
        route: '/bi-boards',
        subOptions: [],
        permissions: [PERMISSION_TYPE.viewBoard.name]
    },
    {
        id: 'reports',
        name: 'Informes',
        subOptions: [
            {
                id: 'report-standardized-tables', 
                name: 'Tablas Estandarizadas', 
                route: '/reports/standardized-tables',
                permissions: [
                    PERMISSION_TYPE.addReport.name,
                    PERMISSION_TYPE.viewReport.name
                ]
            },
            {
                id: 'report-dinamic-reports', 
                name: 'Reportes dinámicos', 
                route: '/reports/dinamic-reports',
                permissions: [
                    PERMISSION_TYPE.addReport.name,
                    PERMISSION_TYPE.viewReport.name
                ]
            }
        ],
        permissions: []
    },
    {
        id: 'configure-notifications',
        name: 'Configurar notificaciones',
        route: '/configure-notifications',
        subOptions: [],
        permissions: [PERMISSION_TYPE.viewNotification.name]
    },
]