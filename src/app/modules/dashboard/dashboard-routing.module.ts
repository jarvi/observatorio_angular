import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { isAuthenticatedGuard } from '../auth/guards';
import { routerPermissionGuard } from 'src/app/shared/guards/router-permission.guard';

const routes: Routes = [
  {
    path: '',
    component: DashboardLayoutComponent,
    canActivateChild: [isAuthenticatedGuard],
    data: {
      breadcrumb: 'Observatorio'
    },
    children: [
      {
        path: 'home',
        component: HomePageComponent,
      },
      {
        path: 'administration',
        loadChildren: () => import('../administration/administration.module').then(m => m.AdministrationModule),
        canActivateChild: [routerPermissionGuard],
        data: {
          breadcrumb: 'Administración'
        }
      },
      {
        path: 'data-sources',
        loadChildren: () => import('../data-sources/data-sources.module').then(m => m.DataSourcesModule),
        canActivateChild: [routerPermissionGuard],
        data: {
          breadcrumb: 'Fuentes de datos'
        }
      },
      {
        path: 'bi-boards',
        loadChildren: () => import('../bi-boards/bi-boards.module').then(m => m.BiBoardsModule),
        canActivateChild: [routerPermissionGuard],
        data: {
          breadcrumb: 'Tableros BI'
        }
      },
      {
        path: 'reports',
        loadChildren: () => import('../reports/reports.module').then(m => m.ReportsModule),
        canActivateChild: [routerPermissionGuard],
        data: {
          breadcrumb: 'Informes'
        }
      },
      {
        path: 'configure-notifications',
        loadChildren: () => import('../configure-notifications/configure-notifications.module').then(m => m.ConfigureNotificationsModule),
        canActivateChild: [routerPermissionGuard],
        data: {
          breadcrumb: 'Configurar notificaciones'
        }
      },
      {
        path: '**',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
