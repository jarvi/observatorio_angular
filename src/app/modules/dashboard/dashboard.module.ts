import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { SidebarFixedComponent } from './components/sidebar-fixed/sidebar-fixed.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ChangePasswordButtonComponent } from './components/change-password-button/change-password-button.component';
import { AccordionMenuComponent } from './components/accordion-menu/accordion-menu.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PanelMenuComponent } from './components/panel-menu/panel-menu.component';
import { ChangePasswordModalComponent } from './components/change-password-modal/change-password-modal.component';
import { NotificationsModalComponent } from './components/notifications-modal/notifications-modal.component';
import { NotificationCardComponent } from './components/notification-card/notification-card.component';
import { NotificationFiltersComponent } from './components/notification-filters/notification-filters.component';

@NgModule({
  declarations: [
    DashboardLayoutComponent,
    SidebarFixedComponent,
    BreadcrumbComponent,
    NavbarComponent,
    SidebarComponent,
    ChangePasswordButtonComponent,
    AccordionMenuComponent,
    HomePageComponent,
    PanelMenuComponent,
    ChangePasswordModalComponent,
    NotificationsModalComponent,
    NotificationCardComponent,
    NotificationFiltersComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DashboardRoutingModule,

    PrimeNgModule,
    SharedModule,
  ]
})
export class DashboardModule { }
