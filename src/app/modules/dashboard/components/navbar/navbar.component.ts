import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { MobileWidthService } from '../../../../shared/services/mobile-width.service';

@Component({
  selector: 'dashboard-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

  @Input() public userName: string = '';
  @Input() public email: string = '';
  @Input() public documentType: string = '';
  @Input() public documentNumber: string = '';
  @Output() public logout: EventEmitter<void> = new EventEmitter<void>();

  private _mobileWidthService = inject(MobileWidthService);

  private _widthSubscription: Subscription = new Subscription();

  public showSidebar: boolean = false;
  public isScreenSmall: boolean = false;
  public visibleNotificationModal: boolean = false;
  public numberUnreadNotifications: number = 0;

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
  }

  public visibleChange(visible: boolean): void {
    this.showSidebar = visible;
  }

  public onLogout(): void { this.logout.emit(); }

  public showNotificationModal(showModal: boolean) { this.visibleNotificationModal = showModal; }

  public getNumberUnreadNotifications(numberNotifications: number) {
    this.numberUnreadNotifications = numberNotifications;
  }
}
