import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

import { NavbarComponent } from './navbar.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from 'src/app/shared/services/user.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { ChangePasswordModalComponent } from '../change-password-modal/change-password-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationsModalComponent } from '../notifications-modal/notifications-modal.component';
import { NotificationFiltersComponent } from '../notification-filters/notification-filters.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavbarComponent,
        SidebarComponent,
        ChangePasswordModalComponent,
        NotificationsModalComponent,
        NotificationFiltersComponent,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule
      ],
      providers: [
        UserService,
        HttpRequestService,
      ]
    });
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should exec the visibleChange method successfully when user information button is clicked', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    const userInformationButton = compiled.querySelector('#user-information-button') as HTMLButtonElement;

    userInformationButton.click();
    
    expect(component.showSidebar).toBeTrue();
  });

  it('should exec the logout method successfully', () => {
    spyOn(component.logout, 'emit');
    component.onLogout();
    expect(component.logout.emit).toHaveBeenCalled();
  });

  it('should change visibleNotificationModal value to true', () => {
    component.showNotificationModal(true);

    expect(component.visibleNotificationModal).toBeTrue();
  });
});
