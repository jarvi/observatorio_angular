import { Component, OnDestroy, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { filterNavigationOptions } from 'src/utils/check-permissionsiIn-navigation-options';
import { NavigationLink } from '../../interfaces/navigation-link.interface';

@Component({
  selector: 'dashboard-sidebar-fixed',
  templateUrl: './sidebar-fixed.component.html',
  styleUrls: ['./sidebar-fixed.component.scss']
})
export class SidebarFixedComponent implements OnDestroy {
  
  private _userPermissionsService = inject(UserPermissionsService);

  private _subscription: Subscription = new Subscription();

  public options: NavigationLink[] = [];

  constructor() {
    this._subscription = this._userPermissionsService.getPermissions().subscribe((permissions) => {

      if (!permissions) return;
      this.options = [...filterNavigationOptions(permissions)];
    });
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
