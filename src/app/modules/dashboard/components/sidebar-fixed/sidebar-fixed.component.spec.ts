import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';

import { SharedModule } from 'src/app/shared/shared.module';

import { SidebarFixedComponent } from './sidebar-fixed.component';
import { AccordionMenuComponent } from '../accordion-menu/accordion-menu.component';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

describe('SidebarFixedComponent', () => {
  let component: SidebarFixedComponent;
  let fixture: ComponentFixture<SidebarFixedComponent>;
  let mockUserPermissionsService: jasmine.SpyObj<UserPermissionsService>;

  beforeEach(() => {
    mockUserPermissionsService = jasmine.createSpyObj('UserPermissionsService', ['getPermissions']);
    mockUserPermissionsService.getPermissions.and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.addRol.name,
    }]));

    TestBed.configureTestingModule({
      declarations: [
        SidebarFixedComponent,
        AccordionMenuComponent
      ],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService,
        { provide: UserPermissionsService, useValue: mockUserPermissionsService }
      ]
    });
    fixture = TestBed.createComponent(SidebarFixedComponent);

    component = fixture.componentInstance;
    
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('SidebarFixedComponent without permissions returned', () => {
  let component: SidebarFixedComponent;
  let fixture: ComponentFixture<SidebarFixedComponent>;
  let mockUserPermissionsService: jasmine.SpyObj<UserPermissionsService>;

  beforeEach(() => {
    mockUserPermissionsService = jasmine.createSpyObj('UserPermissionsService', ['getPermissions']);
    mockUserPermissionsService.getPermissions.and.returnValue(of(undefined as any));

    TestBed.configureTestingModule({
      declarations: [
        SidebarFixedComponent,
        AccordionMenuComponent
      ],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService,
        { provide: UserPermissionsService, useValue: mockUserPermissionsService }
      ]
    });
    fixture = TestBed.createComponent(SidebarFixedComponent);

    component = fixture.componentInstance;
    
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
