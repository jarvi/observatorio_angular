import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'dashboard-change-password-button',
  templateUrl: './change-password-button.component.html',
  styleUrls: ['./change-password-button.component.scss']
})
export class ChangePasswordButtonComponent {

  @Output() openChangePasswordModal: EventEmitter<void> = new EventEmitter();

  public onOpenChangePasswordModal(): void { this.openChangePasswordModal.emit(); }
}
