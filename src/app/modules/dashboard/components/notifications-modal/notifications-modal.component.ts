import { Component, OnInit, inject, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { BehaviorSubject, Observable, Subscription, map, of, switchMap, tap } from 'rxjs';
import { UserNotificationService } from '../../services/user-notification.service';
import { UserNotification } from '../../interfaces/notifications.interface';
import { ModalAlertService, UserPermissionsService } from 'src/app/shared/services';
import { ModalAlert } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

@Component({
  selector: 'dashboard-notifications-modal',
  templateUrl: './notifications-modal.component.html',
  styleUrls: ['./notifications-modal.component.scss']
})
export class NotificationsModalComponent implements OnInit, OnDestroy {

  @Input() public visibleNotificationModal: boolean = false;
  @Output() visibleNotificationModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public numberUnreadNotifications: EventEmitter<number> = new EventEmitter<number>();

  private _listNotificationsService = inject(UserNotificationService);
  private _modalAlertService = inject(ModalAlertService);
  private _userPermissionsService = inject(UserPermissionsService);

  private _getNotificationsSubscription: Subscription = new Subscription();
  private _setNotificationsSubscription: Subscription = new Subscription();
  private _readNotificationSubscription: Subscription = new Subscription();
  private _deleteNotificationSubscription: Subscription = new Subscription();
  private _readAllNotificationsSubscription: Subscription = new Subscription();
  private _currentFilterSubject = new BehaviorSubject<boolean | null>(false);
  private _notificationsBaseListSubject = new BehaviorSubject<UserNotification[]>([]);
  private _notificationsBaseListObservable: Observable<UserNotification[]> = this._notificationsBaseListSubject.asObservable();

  public currentFilterObservable: Observable<boolean | null> = this._currentFilterSubject.asObservable();
  public notificationsList: UserNotification[] = [];
  public isLoading: boolean = false;
  public hasAccess: boolean = false;
  public notificationPermission: string = PERMISSION_TYPE.viewNotification.name;

  ngOnInit(): void {
    this._setNotificationsSubscription = this.setCurrentNotifications().pipe(
      switchMap(() => this.filterNotifications())
    ).subscribe();

    this.isLoading = true;

    this._getNotificationsSubscription = this.getNotifications().subscribe({
      next: () => { this.isLoading = false; },
      error: (error) => {
        this.isLoading = false;
        this.showErrorModal(error);
      }
    });
  }

  ngOnDestroy(): void {
    this._getNotificationsSubscription.unsubscribe();
    this._setNotificationsSubscription.unsubscribe();
    this._readNotificationSubscription.unsubscribe();
    this._deleteNotificationSubscription.unsubscribe();
    this._readAllNotificationsSubscription.unsubscribe();

    this.hasAccess = false;
  }

  private showErrorModal(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private getNotifications(): Observable<void> {
    return this._userPermissionsService.permissions$
      .pipe(
        switchMap(permissions => {
          this.hasAccess = permissions.some((permission) => permission.codename === this.notificationPermission);

          if (!this.hasAccess) return of();

          return this._listNotificationsService.getNotifications()
            .pipe(
              map(notifications => {
                return notifications.map(notification => ({
                  id: notification.id,
                  description: notification.message,
                  date: new Date(notification.created_at),
                  title: `${notification.many_days} ${notification.many_days === 1 ? 'día' : 'días'} para realizar la carga de datos ${notification.data_source}`,
                  isReaded: notification.is_readed
                }))
              }),
              tap(notifications => {
                this._notificationsBaseListSubject.next(notifications);
              }),
              map(() => { }),
            );
        })
      )
  }

  private setCurrentNotifications(): Observable<void> {
    return this._notificationsBaseListObservable.pipe(
      tap(notifications => { 
        this.notificationsList = notifications;

        const numberUnreadNotifications = notifications
          .filter(notification => !notification.isReaded).length;

        this.numberUnreadNotifications.emit(numberUnreadNotifications);
      }),
      map(() => { })
    )
  }

  private filterNotifications(): Observable<void> {
    return this.currentFilterObservable.pipe(
      tap((filterValue) => {
        if (filterValue === null) {
          this.notificationsList = [...this._notificationsBaseListSubject.value];
          return;
        }

        const notificationListFiltered = this._notificationsBaseListSubject.value
          .filter(notification => notification.isReaded === filterValue);

        this.notificationsList = [...notificationListFiltered];
      }),
      map(() => { })
    )
  }

  public showNotificationModal(showModal: boolean) {
    this.visibleNotificationModalChange.emit(showModal);
  }

  public hideReadAllNotifications(): boolean {
    const allNotificationsAreReaded = this._notificationsBaseListSubject.value
      .filter(notification => !notification.isReaded).length === 0;

    const currentFilterIsNotReaded = this._currentFilterSubject.value === false;
    
    return !allNotificationsAreReaded && currentFilterIsNotReaded;
  }

  public onChangeFilter(filterValue: boolean | null) {
    this._currentFilterSubject.next(filterValue);
  }

  public onReadNotification(id: number) {
    this.isLoading = true;

    this._readNotificationSubscription = this._listNotificationsService.readNotification(id).subscribe({
      next: ({ id, is_readed }) => {
        const newNotifications = this._notificationsBaseListSubject.value.map(notificationBase => {
          if (notificationBase.id === id) {
            return { ...notificationBase, isReaded: is_readed };
          }
    
          return notificationBase;
        });
        
        this._notificationsBaseListSubject.next(newNotifications);
        this.isLoading = false;
      },
      error: (error) => {
        this.isLoading = false;
        this.showErrorModal(error);
      }
    });
  }

  public onDeleteNotification(id: number) {
    this.isLoading = true;
    
    this._deleteNotificationSubscription = this._listNotificationsService.deleteNotification(id).subscribe({
      next: (notificationId) => {
        const newNotifications = this._notificationsBaseListSubject.value
          .filter(notification => notification.id !== notificationId);
        
        this._notificationsBaseListSubject.next(newNotifications);
        this.isLoading = false;
      },
      error: (error) => {
        this.isLoading = false;
        this.showErrorModal(error);
      }
    })
  }
  
  public onReadAllNotifications() {
    this.isLoading = true;

    this._readAllNotificationsSubscription = this._listNotificationsService.markAllNotificationsAsReaded().subscribe({
      next: () => {
        const newNotifications = this._notificationsBaseListSubject.value.map(notificationBase => ({
          ...notificationBase,
          isReaded: true
        }));
        
        this._notificationsBaseListSubject.next(newNotifications);
        this.isLoading = false;
      },
      error: (error) => {
        this.isLoading = false;
        this.showErrorModal(error);
      }
    });
  }
}
