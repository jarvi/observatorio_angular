import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsModalComponent } from './notifications-modal.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserNotificationService } from '../../services/user-notification.service';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { NotificationFiltersComponent } from '../notification-filters/notification-filters.component';
import { of, throwError } from 'rxjs';
import { NotificationCardComponent } from '../notification-card/notification-card.component';
import { UserPermissionsService } from 'src/app/shared/services';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

describe('NotificationsModalComponent', () => {
  let component: NotificationsModalComponent;
  let fixture: ComponentFixture<NotificationsModalComponent>;
  let userNotificationService: UserNotificationService;
  let userPermissionsService: UserPermissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        NotificationsModalComponent,
        NotificationFiltersComponent,
        NotificationCardComponent
      ],
      imports: [
        HttpClientTestingModule,
        PrimeNgModule,
      ],
    });
    fixture = TestBed.createComponent(NotificationsModalComponent);
    userNotificationService = TestBed.inject(UserNotificationService);
    userPermissionsService = TestBed.inject(UserPermissionsService);

    spyOn(userNotificationService, 'getNotifications').and.returnValue(of([
      {
        id: 1,
        data_source: 'SIMAT',
        message: 'Prueba',
        many_days: 3,
        is_readed: true,
        created_at: '01-01-2023',
        updated_at: '01-01-2023',
      },
      {
        id: 2,
        data_source: 'SIMAT',
        message: 'Prueba',
        many_days: 1,
        is_readed: false,
        created_at: '01-01-2023',
        updated_at: '01-01-2023',
      }
    ]));
    spyOnProperty(userPermissionsService, 'permissions$', 'get').and.returnValue(of([
      { id: 1, codename: PERMISSION_TYPE.viewBoard.name },
      { id: 2, codename: PERMISSION_TYPE.addSource.name },
      { id: 3, codename: PERMISSION_TYPE.viewNotification.name },
    ]));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change filter to null', () => {
    component.onChangeFilter(null);
    expect(component.notificationsList.length > 1).toBeTrue()
  });

  it('should emit a value if showNotificationModal is executed', () => {
    spyOn(component.visibleNotificationModalChange, 'emit');

    component.showNotificationModal(true);
    expect(component.visibleNotificationModalChange.emit).toHaveBeenCalledWith(true);
  });

  it('should read the notification', () => {
    spyOn(userNotificationService, 'readNotification').and.returnValue(of({
      id: 2,
      data_source: 'SIMAT',
      message: 'Prueba',
      many_days: 1,
      is_readed: true,
      created_at: '01-01-2023',
      updated_at: '01-01-2023',
    }));

    component.onReadNotification(2);

    expect(component.isLoading).toBeFalse();
  });

  it('should not read the notification if readNotification response with an error', () => {
    spyOn(userNotificationService, 'readNotification').and.returnValue(throwError(() => {}));

    component.onReadNotification(2);

    expect(component.isLoading).toBeFalse();
  });

  it('should delete the notification', () => {
    spyOn(userNotificationService, 'deleteNotification').and.returnValue(of(2));

    component.onDeleteNotification(2);

    expect(component.isLoading).toBeFalse();
  });

  it('should not delete the notification if deleteNotification response with an error', () => {
    spyOn(userNotificationService, 'deleteNotification').and.returnValue(throwError(() => {}));

    component.onDeleteNotification(2);

    expect(component.isLoading).toBeFalse();
  });

  it('should read all notifications the notification', () => {
    spyOn(userNotificationService, 'markAllNotificationsAsReaded').and.returnValue(of(true));

    component.onReadAllNotifications();

    expect(component.isLoading).toBeFalse();
  });

  it('should not read all notifications the notification if markAllNotificationsAsReaded response with an error', () => {
    spyOn(userNotificationService, 'markAllNotificationsAsReaded').and.returnValue(throwError(() => {}));

    component.onReadAllNotifications();

    expect(component.isLoading).toBeFalse();
  });
});

describe('NotificationsModalComponent with getNotification bad request', () => {
  let component: NotificationsModalComponent;
  let fixture: ComponentFixture<NotificationsModalComponent>;
  let userNotificationService: UserNotificationService;
  let userPermissionsService: UserPermissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        NotificationsModalComponent,
        NotificationFiltersComponent,
        NotificationCardComponent
      ],
      imports: [
        HttpClientTestingModule,
        PrimeNgModule,
      ],
    });
    fixture = TestBed.createComponent(NotificationsModalComponent);
    userNotificationService = TestBed.inject(UserNotificationService);
    userPermissionsService = TestBed.inject(UserPermissionsService);

    spyOnProperty(userPermissionsService, 'permissions$', 'get').and.returnValue(of([
      { id: 1, codename: PERMISSION_TYPE.viewBoard.name },
      { id: 2, codename: PERMISSION_TYPE.addSource.name },
      { id: 3, codename: PERMISSION_TYPE.viewNotification.name },
    ]));

    spyOn(userNotificationService, 'getNotifications').and.returnValue(throwError(() => { }));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});