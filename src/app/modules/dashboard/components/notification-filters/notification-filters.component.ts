import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dashboard-notification-filters',
  templateUrl: './notification-filters.component.html',
  styleUrls: ['./notification-filters.component.scss']
})
export class NotificationFiltersComponent {

  @Output() public changeFilter: EventEmitter<boolean | null> = new EventEmitter<boolean | null>();

  public filters = [
    { id: 'readed', label: 'No leídas', value: false, selected: true },
    { id: 'notReaded', label: 'Leídas', value: true, selected: false },
    { id: 'all', label: 'Todas', value: null, selected: false },
  ]

  public onChangeFilter(value: boolean | null) {
    this.filters = this.filters.map(filter => ({
      ...filter,
      selected: value === filter.value
    }));

    const filterValue = this.filters.find(filter => filter.value === value)!.value;

    this.changeFilter.emit(filterValue);
  }
}
