import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationFiltersComponent } from './notification-filters.component';

describe('NotificationFiltersComponent', () => {
  let component: NotificationFiltersComponent;
  let fixture: ComponentFixture<NotificationFiltersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationFiltersComponent]
    });
    fixture = TestBed.createComponent(NotificationFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the changeFilter value', () => {
    spyOn(component.changeFilter, 'emit');
    
    component.onChangeFilter(true);

    expect(component.changeFilter.emit).toHaveBeenCalledWith(true);
  });
});
