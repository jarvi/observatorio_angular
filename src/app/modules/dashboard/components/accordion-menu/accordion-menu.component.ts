import { Component, Input } from '@angular/core';
import { NavigationLink } from '../../interfaces/navigation-link.interface';

@Component({
  selector: 'dashboard-accordion-menu',
  templateUrl: './accordion-menu.component.html',
  styleUrls: ['./accordion-menu.component.scss']
})
export class AccordionMenuComponent {

  @Input() public options: NavigationLink[] = [];

  private clearStyles(div: HTMLElement, subList: HTMLElement) {
    div.removeAttribute('style');
    subList.removeAttribute('style');
  }

  private setStyles(divSubList: HTMLElement, subList: HTMLElement) {
    if (divSubList.offsetTop >= 0) return;
    
    divSubList.style.top = '0';
    divSubList.style.height = '100%';

    subList.style.overflowY = 'overlay';
    subList.style.maxHeight = '100%';
  }

  public assignStyle(id: string, remove: boolean) {
    const divSubList = document.getElementById(`div-sublist-${id}`);
    const subList = document.getElementById(`sublist-${id}`);

    if (!divSubList || !subList) return;

    if (remove) {
      this.clearStyles(divSubList, subList);
    } else {
      this.setStyles(divSubList, subList);
    }
  }
}
