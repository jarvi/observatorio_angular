import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionMenuComponent } from './accordion-menu.component';

describe('AccordionMenuComponent', () => {
  let component: AccordionMenuComponent;
  let fixture: ComponentFixture<AccordionMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccordionMenuComponent]
    });
    fixture = TestBed.createComponent(AccordionMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should just call assignStyle', () => {
    spyOn(component, 'assignStyle');
    component.assignStyle('aa', false);

    expect(component.assignStyle).toHaveBeenCalled();
  });

  it('should change the div style when offsetTop is less than 0', () => {
    const mockedDivElement = document.createElement('div');
    mockedDivElement.setAttribute('id', 'id-1');

    Object.defineProperty(mockedDivElement, 'offsetTop', { value: -4000 });

    spyOn(document, 'getElementById').and.returnValue(mockedDivElement);
    component.assignStyle('id-1', false);

    expect(mockedDivElement.style.top).toEqual('0px');
    expect(mockedDivElement.style.height).toEqual('100%');
  });

  it('should not change the div style when offsetTop is equal to 0', () => {
    const mockedDivElement = document.createElement('div');
    mockedDivElement.setAttribute('id', 'id-1');

    Object.defineProperty(mockedDivElement, 'offsetTop', { value: 0 });

    spyOn(document, 'getElementById').and.returnValue(mockedDivElement);
    component.assignStyle('id-1', false);

    expect(mockedDivElement.style.top).toEqual('');
  });

  it('should remove the div styles', () => {
    const mockedDivElement = document.createElement('div');
    mockedDivElement.setAttribute('id', 'id-1');

    Object.defineProperty(mockedDivElement, 'offsetTop', { value: -4000 });

    spyOn(document, 'getElementById').and.returnValue(mockedDivElement);
    component.assignStyle('id-1', true);

    expect(mockedDivElement.style.top).toEqual('');
  });

  it('should not do anything if there are not element in the document', () => {
    spyOn(document, 'getElementById');
    component.assignStyle('id-1', false);

    expect(document.getElementById).toHaveBeenCalled();
  });
});
