import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { UserService } from 'src/app/shared/services/user.service';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { filterNavigationOptions } from 'src/utils/check-permissionsiIn-navigation-options';
import { ModalAlert, ChangePasswordRequest } from 'src/app/shared/interfaces';
import { NavigationLink } from '../../interfaces/navigation-link.interface';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';

@Component({
  selector: 'dashboard-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnChanges, OnDestroy {

  @Input() public userName: string = '';
  @Input() public email: string = '';
  @Input() public documentType: string = '';
  @Input() public documentNumber: string = '';
  @Input() public visible: boolean = false;
  @Output() public visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public logout: EventEmitter<void> = new EventEmitter<void>();

  private _fb = inject(FormBuilder);
  private _mobileWidthService = inject(MobileWidthService);
  private _modalAlertService = inject(ModalAlertService);
  private _validatorService = inject(ValidatorService);
  private _userService = inject(UserService);
  private _userPermissionsService = inject(UserPermissionsService);

  private _widthSubscription: Subscription = new Subscription();
  private _changePasswordSubscription: Subscription = new Subscription();
  private _subscriptionPermissions: Subscription = new Subscription();

  public firtsNamePart: string = '';
  public lastNamePart: string = '';
  public isScreenSmall: boolean = false;
  public options: NavigationLink[] = [];
  public showChangePasswordModal: boolean = false;
  public isLoadingChangePassword: boolean = false;

  public changePasswordForm: FormGroup = this._fb.group({
    currentPassword: ['', [
      Validators.required,
      Validators.minLength(7),
      this._validatorService.whiteSpacesValidator()
    ]],
    password: ['', [
      Validators.required,
      Validators.minLength(7),
      this._validatorService.whiteSpacesValidator(),
      this._validatorService.upperCaseLowerCaseValidator(),
      this._validatorService.specialCharacterValidator(),
    ]],
    passwordConfirmation: ['', [
      Validators.required,
      Validators.minLength(7),
      this._validatorService.whiteSpacesValidator(),
      this._validatorService.upperCaseLowerCaseValidator(),
      this._validatorService.specialCharacterValidator(),
    ]],
    passwordMatch: [false, [Validators.requiredTrue]]
  });

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      if (isScreenSmall && this.isScreenSmall !== isScreenSmall) {
        this._subscriptionPermissions = this.getPermissions();
      }

      this.isScreenSmall = isScreenSmall;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['userName']) {
      const nameParts = this.userName.split(' ');
      this.firtsNamePart = nameParts[0];
      this.lastNamePart = nameParts.slice(1).join(' ');
    }
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
    this._changePasswordSubscription.unsubscribe();
    this._subscriptionPermissions.unsubscribe();
  }

  public getPermissions(): Subscription {
    this._subscriptionPermissions.unsubscribe();
    
    return this._userPermissionsService.getPermissions().subscribe((permissions) => {
      if (!permissions) return;
      this.options = [...filterNavigationOptions(permissions)];
    });
  }

  public onVisibleChange(visible: boolean): void { this.visibleChange.emit(visible); }

  public onVisibleChangeChangePasswordModal(visible: boolean) {
    if (visible) return;

    this.onCancelChangePassword();
  }

  public onLogout(): void {
    this.onVisibleChange(false);

    const warinigAlert: ModalAlert = {
      title: 'Cerrar sesión',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cerrar sesión',
        primary: true,
        action: () => this.logout.emit()
      },
      cancelButton: {
        label: 'Cancelar',
        primary: false,
      },
      highlightText: '¿Está seguro que desea cerrar la sesión?',
    }

    this._modalAlertService.showModalAlert(warinigAlert);
  }

  public openChangePasswordModal(): void {
    this.onVisibleChange(false);

    this.showChangePasswordModal = true;
  }

  public onChangePassword(): void {
    if (this.changePasswordForm.invalid) {
      this._validatorService.isValidField(this.changePasswordForm);
      return;
    }

    const newPassword: ChangePasswordRequest = {
      email: '',
      old_password: this.changePasswordForm.value.currentPassword,
      new_password: this.changePasswordForm.value.password,
      confirm_new_password: this.changePasswordForm.value.passwordConfirmation,
    }

    this.isLoadingChangePassword = true;

    this._changePasswordSubscription = this._userService.changePassword(newPassword).subscribe({
      next: () => {
        this.isLoadingChangePassword = false;
        this.showChangePasswordModal = false;
        this.showChangePasswordSuccessAlert();
      },
      error: (error) => {
        this.isLoadingChangePassword = false;
        this.showChangePasswordModal = false;
        this.changePasswordForm.reset();
        error && this.showErrorAlert(error);
      }
    });

  }

  public onCancelChangePassword(): void {
    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this.changePasswordForm.reset();
          this.showChangePasswordModal = false;
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
        action: () => {
          this.showChangePasswordModal = true;
        }
      },
      highlightText: '¿Está seguro que desea cancelar el cambio de contraseña?'
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }

  public showChangePasswordSuccessAlert(): void {
    const successAlert: ModalAlert = {
      title: 'Contraseña actualizada',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Continuar',
        primary: true,
        action: () => {
          this.changePasswordForm.reset();
          this.showChangePasswordModal = false;
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.changePasswordForm.reset();
          this.showChangePasswordModal = false;
        }
      },
      message: 'Su contraseña ha sido actualizada correctamente.',
      highlightText: 'Haga click para continuar'
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  public showErrorAlert(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Continuar',
        primary: true,
        action: () => {
          this.showChangePasswordModal = true;
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.showChangePasswordModal = true;
        }
      },
      message
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }
}
