import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

import { SidebarComponent } from './sidebar.component';
import { ChangePasswordButtonComponent } from '../change-password-button/change-password-button.component';
import Swal from 'sweetalert2';
import { UserService } from 'src/app/shared/services/user.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChangePasswordModalComponent } from '../change-password-modal/change-password-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { PanelMenuComponent } from '../panel-menu/panel-menu.component';
import { RouterTestingModule } from '@angular/router/testing';
import { UserPermissionsService } from '../../../../shared/services/user-permissions.service';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  let userService: UserService;
  let httpRequestService: HttpRequestService;
  let userPermissionsService: UserPermissionsService;
  let mobileWidthService: MobileWidthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SidebarComponent,
        ChangePasswordButtonComponent,
        ChangePasswordModalComponent,
        PanelMenuComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        PrimeNgModule,
        SharedModule
      ],
      providers: [
        UserService,
        HttpRequestService,
      ]
    });
    fixture = TestBed.createComponent(SidebarComponent);
    httpRequestService = TestBed.inject(HttpRequestService);
    userPermissionsService = TestBed.inject(UserPermissionsService);
    mobileWidthService = TestBed.inject(MobileWidthService);

    spyOn(mobileWidthService, 'getIsScreenSmall').and.returnValue(of(true));

    userService = TestBed.inject(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    Swal.close();
  })

  it('should not get the navigation options if getPermissions return undefined', () => {
    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of(undefined as any));
    component.getPermissions();
    expect(component.options).toEqual([]);
  });

  it('should get the navigation options', () => {
    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of([]));
    component.getPermissions();
    expect(component.options).toEqual([]);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should exec the visibleChange method successfully when close button is clicked', () => {
    component.visible = true;
    fixture.detectChanges();
    spyOn(component.visibleChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    const closeButton = compiled.querySelector('#close-button-sidebar') as HTMLButtonElement;

    closeButton.click();
    
    expect(component.visibleChange.emit).toHaveBeenCalled();
  });

  it('should exec the logout method successfully when logout button is clicked', () => {
    component.visible = true;
    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    const logoutButton = compiled.querySelector('#logout-button-sidebar') as HTMLButtonElement;

    logoutButton.click();

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton();

    confirmButton?.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should call ngOnChanges when userName changes', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newUserName = 'Nombre Apellido';
    component.userName = newUserName;
    fixture.detectChanges();
    component.ngOnChanges({ userName: {
      previousValue: '',
      currentValue: newUserName,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.firtsNamePart).toEqual('Nombre');
    expect(component.lastNamePart).toEqual('Apellido');
  });

  it('should does not show the cancel modal alert', () => {
    component.onVisibleChangeChangePasswordModal(true);

    const title = Swal.getHtmlContainer()?.querySelector('.modal-title')?.innerHTML;
    
    expect(title).not.toEqual('Cancelar');
  });

  it('should showes the cancel modal alert and press confirm button', () => {
    component.onVisibleChangeChangePasswordModal(false);
    
    expect(Swal.isVisible()).toBeTruthy();
    const confirmButton = Swal.getConfirmButton();
    confirmButton?.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should showes the cancel modal alert and press cancel button', () => {
    component.onVisibleChangeChangePasswordModal(false);
    
    expect(Swal.isVisible()).toBeTruthy();
    const cancelButton = Swal.getCancelButton();
    cancelButton?.click();
    expect(cancelButton).toBeTruthy();
  });

  it('should showes the success modal alert and press confirm button', () => {
    component.showChangePasswordSuccessAlert();
    
    expect(Swal.isVisible()).toBeTruthy();
    const confirmButton = Swal.getConfirmButton();
    confirmButton?.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should showes the success modal alert and press cancel button', () => {
    component.showChangePasswordSuccessAlert();
    
    expect(Swal.isVisible()).toBeTruthy();
    const cancelButton = Swal.getCancelButton();
    cancelButton?.click();
    expect(cancelButton).toBeTruthy();
  });

  it('should emit false with visibleChange when openChangePasswordModal method is executed', () => {
    spyOn(component.visibleChange, 'emit');
    component.openChangePasswordModal();
    
    expect(component.visibleChange.emit).toHaveBeenCalledWith(false);
  });

  it('should be invalid the change password form', () => {
    component.onChangePassword();

    expect(component.changePasswordForm.valid).toBeFalsy();
  });

  it('should be valid the change password form and change to false if changePassword methed return an error and press confirm button', () => {

    component.changePasswordForm.controls['currentPassword'].setValue('123456Ab@');
    component.changePasswordForm.controls['password'].setValue('123456Ab@');
    component.changePasswordForm.controls['passwordConfirmation'].setValue('123456Ab@');

    expect(component.changePasswordForm.valid).toBeTrue();

    spyOn(userService, 'changePassword').and.returnValue(throwError(() => 'error'))

    component.onChangePassword();
    expect(component.changePasswordForm.invalid).toBeTrue();
    expect(Swal.isVisible());
    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should be valid the change password form and change to false if changePassword methed return an error and press cancel button', () => {

    component.changePasswordForm.controls['currentPassword'].setValue('123456Ab@');
    component.changePasswordForm.controls['password'].setValue('123456Ab@');
    component.changePasswordForm.controls['passwordConfirmation'].setValue('123456Ab@');

    expect(component.changePasswordForm.valid).toBeTrue();

    spyOn(userService, 'changePassword').and.returnValue(throwError(() => 'error'))

    component.onChangePassword();
    expect(component.changePasswordForm.invalid).toBeTrue();
    expect(Swal.isVisible());
    const cancelButton = Swal.getCancelButton()!;
    cancelButton.click();
    expect(cancelButton).toBeTruthy();
  });

  it('should be valid the change password form', () => {

    component.changePasswordForm.controls['currentPassword'].setValue('123456Ab@');
    component.changePasswordForm.controls['password'].setValue('123456Ab@');
    component.changePasswordForm.controls['passwordConfirmation'].setValue('123456Ab@');

    expect(component.changePasswordForm.valid).toBeTrue();

    spyOn(userService, 'changePassword').and.returnValue(of(true));

    component.onChangePassword();
  });
});
