import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordModalComponent } from './change-password-modal.component';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

describe('ChangePasswordModalComponent', () => {
  let component: ChangePasswordModalComponent;
  let fixture: ComponentFixture<ChangePasswordModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChangePasswordModalComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        PrimeNgModule,
        SharedModule
      ],
    });
    fixture = TestBed.createComponent(ChangePasswordModalComponent);
    component = fixture.componentInstance;
    component.changePasswordForm = new FormGroup({
      currentPassword: new FormControl(''),
      password: new FormControl(''),
      passwordConfirmation: new FormControl(''),
      passwordMatch: new FormControl(false),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit visibleChange with false when onVisibleChange method is executed', () => {
    spyOn(component.visibleChange, 'emit');
    component.onVisibleChange(false);
    
    expect(component.visibleChange.emit).toHaveBeenCalledWith(false);
  });

  it('should change hideCurrentPassword value when togglePasswordVisibility is executed with currentPassword param', () => {
    component.togglePasswordVisibility('currentPassword');
    expect(component.hideCurrentPassword).toBeTrue();
  });

  it('should change hidePassword value when togglePasswordVisibility is executed with password param', () => {
    component.togglePasswordVisibility('password');
    expect(component.hidePassword).toBeTrue();
  });

  it('should change hidePasswordConfirmation value when togglePasswordVisibility is executed with passwordConfirmation param', () => {
    component.togglePasswordVisibility('passwordConfirmation');
    expect(component.hidePasswordConfirmation).toBeTrue();
  });

  it('should emit changePassword when onChangePassword method is executed', () => {
    spyOn(component.changePassword, 'emit');
    component.onChangePassword();

    expect(component.changePassword.emit).toHaveBeenCalled();
  });
});
