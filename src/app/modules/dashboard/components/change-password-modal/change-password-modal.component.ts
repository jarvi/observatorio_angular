import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { onPasswordChangeValidateConditions } from 'src/utils/password-change-validate-conditions';
import { PasswordConditions } from 'src/app/modules/administration/modules/user/interfaces/password-conditions.interface';

@Component({
  selector: 'dashboard-change-password-modal',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.scss']
})
export class ChangePasswordModalComponent implements OnInit, OnDestroy {

  @Input() public visible: boolean = false;
  @Input() public changePasswordForm: FormGroup = new FormGroup({});
  @Input() public isLoading: boolean = false;
  @Output() public visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public changePassword: EventEmitter<void> = new EventEmitter();

  private _passwordSubscription: Subscription = new Subscription();
  private _passwordConfirmationSubscription: Subscription = new Subscription();

  public hideCurrentPassword: boolean = false;
  public hidePassword: boolean = false;
  public hidePasswordConfirmation: boolean = false;
  public passwordConditions: PasswordConditions[] = [
    {
      id: 'minLength',
      description: 'Debe ingresar al menos 7 caracteres',
      isValid: false
    },
    {
      id: 'upperCaseLowerCase',
      description: 'Combine mayúsculas y minúsculas',
      isValid: false
    },
    {
      id: 'specialCharacter',
      description: 'Incluya al menos 1 carácter especial',
      isValid: false
    },
  ]
  public passwordConfirmationConditions: PasswordConditions[] = [
    {
      id: 'matching',
      description: 'Las contraseñas coinciden',
      isValid: false
    },
  ]

  ngOnInit(): void {
    this._passwordSubscription = this.changePasswordForm.controls['password'].valueChanges.subscribe((value) => {
      this.onPasswordChangeValidation(value);
      this.onPasswordConfirmationChange();
    });

    this._passwordConfirmationSubscription = this.changePasswordForm.controls['passwordConfirmation'].valueChanges.subscribe((value) => {
      this.onPasswordConfirmationChange();
    });
  }

  ngOnDestroy(): void {
    this._passwordSubscription.unsubscribe();
    this._passwordConfirmationSubscription.unsubscribe();
  }

  public onVisibleChange(visible: boolean): void {
    this.visibleChange.emit(visible);
  }

  public enabledShowPasswordButton(formControlName: string): boolean {
    const control = this.changePasswordForm.controls[formControlName];
    const enabled = !!control?.value;
  
    if (!enabled) {
      switch (formControlName) {
        case 'currentPassword':
          this.hideCurrentPassword = false;
          break;
        case 'password':
          this.hidePassword = false;
          break;
        case 'passwordConfirmation':
          this.hidePasswordConfirmation = false;
          break;
      }
    }
  
    return !enabled;
  }

  public togglePasswordVisibility(formControlName: string): void {
    switch (formControlName) {
      case 'currentPassword':
        this.hideCurrentPassword = !this.hideCurrentPassword;
        break;
      case 'password':
        this.hidePassword = !this.hidePassword;
        break;
      case 'passwordConfirmation':
        this.hidePasswordConfirmation = !this.hidePasswordConfirmation;
        break;
    }
  }

  public onPasswordChangeValidation(password: string): void {
    onPasswordChangeValidateConditions(password, this.passwordConditions);
  }

  public onPasswordConfirmationChange(): void {
    const passwordValue = this.changePasswordForm.controls['password'].value;
    const passwordConfirmationValue = this.changePasswordForm.controls['passwordConfirmation'].value;
    const passwordMatch = this.changePasswordForm.controls['passwordMatch'];
    const isValid = passwordConfirmationValue && passwordValue && passwordConfirmationValue === passwordValue;
    passwordMatch.setValue(isValid);

    this.passwordConfirmationConditions = this.passwordConfirmationConditions.map((condition) => {
      if (condition.id === 'matching') {
        condition.isValid = isValid;
      }
      return condition;
    })
  }

  public onChangePassword(): void {
    this.changePassword.emit();
  }
}
