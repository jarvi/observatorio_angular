import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

import { PanelMenuComponent } from './panel-menu.component';
import { NavigationLink } from '../../interfaces/navigation-link.interface';
import { MenuItem } from 'primeng/api';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PanelMenuComponent', () => {
  let component: PanelMenuComponent;
  let fixture: ComponentFixture<PanelMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PanelMenuComponent],
      imports: [
        PrimeNgModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
    });
    fixture = TestBed.createComponent(PanelMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnChanges when userName changes', fakeAsync(() => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newOptions: NavigationLink[] = [
      {
        id: '1',
        name: 'Test',
        route: 'test',
        subOptions: [{
          id: '1',
          name: 'Test 1',
          route: 'test-1',
          permissions: []
        }],
        permissions: []
      },
      {
        id: '2',
        name: 'Test 2',
        route: 'test-2',
        subOptions: [],
        permissions: []
      }
    ];

    const itemsExpected: MenuItem[] = [
      {
        id: '1',
        label: 'Test',
        routerLink: 'test',
        items: [
          {
            id: '1',
            label: 'Test 1',
            routerLink: 'test-1'
          }
        ]
      },
      {
        id: '2',
        label: 'Test 2',
        routerLink: 'test-2',
        items: undefined
      }
    ]

    component.options = newOptions;
    fixture.detectChanges();
    component.ngOnChanges({ options: {
      previousValue: '',
      currentValue: newOptions,
      firstChange: true,
      isFirstChange: () => true
    }});
    tick(500);
    fixture.detectChanges();

    expect(component.ngOnChanges).toHaveBeenCalled();
    
    expect(component.items).toEqual(itemsExpected);
  }));
});
