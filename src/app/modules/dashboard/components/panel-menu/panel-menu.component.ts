import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { NavigationLink, SubNavigationLink } from '../../interfaces/navigation-link.interface';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'dashboard-panel-menu',
  templateUrl: './panel-menu.component.html',
  styleUrls: ['./panel-menu.component.scss']
})
export class PanelMenuComponent implements OnChanges {

  @Input() public options: NavigationLink[] = [];

  public items: MenuItem[] = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['options']) {
      executeCallbackLate(() => {
        this.items = this.options.map((option: NavigationLink) => {
          return {
            id: option.id,
            label: option.name,
            routerLink: option.route,
            items: option.subOptions.length > 0 ? option.subOptions.map((subOption: SubNavigationLink) => {
              return {
                id: subOption.id,
                label: subOption.name,
                routerLink: subOption.route
              }
            }) : undefined,
          }
        })
      });
    }
  }
}
