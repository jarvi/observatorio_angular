import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationCardComponent } from './notification-card.component';
import { UserNotification } from '../../interfaces/notifications.interface';

describe('NotificationCardComponent', () => {
  let component: NotificationCardComponent;
  let fixture: ComponentFixture<NotificationCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationCardComponent]
    });
    fixture = TestBed.createComponent(NotificationCardComponent);
    component = fixture.componentInstance;
    component.notification = {
      id: 1,
      title: 'Title',
      description: 'Description',
      date: new Date(),
      isReaded: true
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return Fecha no válida when formatDate is executed with a bad date', () => {
    const date = new Date('asdasdasasdasd');
    const result = component.formatDate(date);

    expect(result).toEqual('Fecha no válida');
  });

  it('should emit if onReadNotification is executed with a unreaded notification', () => {
    spyOn(component.readNotification, 'emit');

    const notification: UserNotification ={
      id: 1,
      title: 'Title',
      description: 'Description',
      date: new Date(),
      isReaded: false
    }
    component.onReadNotification(notification.id);

    expect(component.readNotification.emit).toHaveBeenCalled();
  });

  it('should emit if onDeleteNotification is executed', () => {
    spyOn(component.deleteNotification, 'emit');

    component.onDeleteNotification(1);

    expect(component.deleteNotification.emit).toHaveBeenCalledWith(1);
  });
});
