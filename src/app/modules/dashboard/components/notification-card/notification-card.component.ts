import { Component, Input, EventEmitter, Output } from '@angular/core';
import * as moment from 'moment';
import { UserNotification } from '../../interfaces/notifications.interface';

@Component({
  selector: 'dashboard-notification-card',
  templateUrl: './notification-card.component.html',
  styleUrls: ['./notification-card.component.scss']
})
export class NotificationCardComponent {

  @Input() public notification!: UserNotification;
  @Output() public readNotification: EventEmitter<number> = new EventEmitter<number>();
  @Output() public deleteNotification: EventEmitter<number> = new EventEmitter<number>();

  public formatDate(date: Date) {
    moment.locale('es');

    return moment(date).isValid() ?
      moment(date).format('LL' + ' | ' + 'hh:mm A') :
        'Fecha no válida';
  }

  public onReadNotification(id: number) {
    this.readNotification.emit(id);
  }

  public onDeleteNotification(id: number) { this.deleteNotification.emit(id); }
}
