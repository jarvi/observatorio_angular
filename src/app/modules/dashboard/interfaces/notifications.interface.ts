export interface NotificationResponse {
    id: number;
    data_source: string;
    message: string;
    many_days: number;
    is_readed: boolean;
    created_at: string;
    updated_at: string;
}

export interface UserNotification {
    id: number;
    title: string;
    description: string;
    date: Date;
    isReaded: boolean;
}