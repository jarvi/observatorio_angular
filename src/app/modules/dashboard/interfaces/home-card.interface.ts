export interface HomeCard {
    id: string;
    icon: string;
    title: string;
    description?: string;
    route: string;
    access: boolean;
}