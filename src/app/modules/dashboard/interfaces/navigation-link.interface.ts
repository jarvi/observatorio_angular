export interface NavigationLink {
    id: string;
    name: string;
    route?: string;
    subOptions: SubNavigationLink[];
    permissions: string[];
}

export interface SubNavigationLink {
    id: string;
    name: string;
    route: string;
    permissions: string[];
}