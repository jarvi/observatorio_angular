import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';

import { HomePageComponent } from './home-page.component';
import { VersionAppComponent } from 'src/app/shared/components/version-app/version-app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { of } from 'rxjs';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

describe('HomeComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  let userPermissionsService: UserPermissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomePageComponent,
        VersionAppComponent
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        UserPermissionsService,
        HttpRequestService
      ],
    });
    fixture = TestBed.createComponent(HomePageComponent);
    userPermissionsService = TestBed.inject(UserPermissionsService);

    spyOnProperty(userPermissionsService, 'permissions$', 'get').and.returnValue(of([
      { id: 1, codename: PERMISSION_TYPE.viewBoard.name },
      { id: 2, codename: PERMISSION_TYPE.addSource.name },
      { id: 3, codename: PERMISSION_TYPE.addReport.name },
    ]))

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('HomeComponent with viewReport permission', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  let userPermissionsService: UserPermissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomePageComponent,
        VersionAppComponent
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        UserPermissionsService,
        HttpRequestService
      ],
    });
    fixture = TestBed.createComponent(HomePageComponent);
    userPermissionsService = TestBed.inject(UserPermissionsService);

    spyOnProperty(userPermissionsService, 'permissions$', 'get').and.returnValue(of([
      { id: 1, codename: PERMISSION_TYPE.viewBoard.name },
      { id: 2, codename: PERMISSION_TYPE.addSource.name },
      { id: 3, codename: PERMISSION_TYPE.viewReport.name },
    ]))

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
