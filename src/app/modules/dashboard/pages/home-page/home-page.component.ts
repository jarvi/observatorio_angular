import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { PermissionType } from 'src/app/shared/interfaces/permission-type.interface';
import { HomeCard } from '../../interfaces/home-card.interface';
import { DATA_OPTIONS_TYPES } from 'src/app/shared/enums/data-options-types';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

@Component({
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {

  public cards: HomeCard[] = []

  private _mobileWidthService = inject(MobileWidthService);
  private _userPermissionsService = inject(UserPermissionsService);

  private _widthSubscription: Subscription = new Subscription();
  private _userPermissionsSubscription: Subscription = new Subscription();
  
  public isScreenSmall: boolean = false;

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
    
    this._userPermissionsSubscription = this._userPermissionsService.permissions$.subscribe((permissions) => {
      this.verifyRolePermissions(permissions);
    });
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
    this._userPermissionsSubscription.unsubscribe();
  }

  private verifyRolePermissions(permissions: PermissionType[]) {
    const userPermissionsList = permissions.map((permission) => permission.codename);
    const homePermissions = {
      viewBoard: PERMISSION_TYPE.viewBoard.name,
      addSource: PERMISSION_TYPE.addSource.name,
      addReport: PERMISSION_TYPE.addReport.name,
      viewReport: PERMISSION_TYPE.viewReport.name,
    }

    const dataSourcesAccess: boolean = userPermissionsList.some(permission => permission === homePermissions.addSource);
    const biBoardsAccess: boolean = userPermissionsList.some(permission => permission === homePermissions.viewBoard);
    const addReportAccess: boolean = userPermissionsList.some(permission => permission === homePermissions.addReport);
    const viewReportAccess: boolean = userPermissionsList.some(permission => permission === homePermissions.viewReport);

    this.cards = [
      {
        id: 'manage',
        icon: '../../../../../assets/images/icons/manageHomeIcon.svg',
        title: 'Gestionar',
        description: 'fuentes de datos',
        route: `/data-sources/data-loading/${DATA_OPTIONS_TYPES.simat}`,
        access: dataSourcesAccess
      },
      {
        id: 'boards',
        icon: '../../../../../assets/images/icons/boardHomeIcon.svg',
        title: 'Tableros',
        description: 'BI',
        route: '/bi-boards',
        access: biBoardsAccess
      },
      {
        id: 'reports',
        icon: '../../../../../assets/images/icons/reportHomeIcon.svg',
        title: 'Informes',
        route: '/reports/standardized-tables',
        access: addReportAccess || viewReportAccess
      }
    ]
  }
}
