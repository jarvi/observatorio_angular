import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatagridNotificationsComponent } from './datagrid-notifications.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DataSourcesNotificationsService } from '../../services/data-sources-notifications.service';
import { HttpRequestService } from 'src/app/shared/services';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { LoadingPeriodComponent } from 'src/app/modules/data-sources/components/loading-period/loading-period.component';
import { EditLoadingPeriodNotificationComponent } from '../edit-loading-period-notification/edit-loading-period-notification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { DataSourceNotificacion, NotificationTime } from '../../interfaces/data-source-notification.interface';
import { TIME_TYPE } from 'src/app/modules/data-sources/enums';
import { LoadingPeriodFrecuency } from 'src/app/modules/data-sources/interfaces/loading-period-frecuency.interface';
import Swal from 'sweetalert2';
import { of, throwError } from 'rxjs';

describe('DatagridNotificationsComponent', () => {
  let component: DatagridNotificationsComponent;
  let fixture: ComponentFixture<DatagridNotificationsComponent>;
  let dataSourcesNotificationsService: DataSourcesNotificationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DatagridNotificationsComponent,
        LoadingPeriodComponent,
        EditLoadingPeriodNotificationComponent
      ],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        PrimeNgModule,
        SharedModule
      ],
      providers: [
        HttpRequestService,
        DataSourcesNotificationsService,
      ]
    });
    fixture = TestBed.createComponent(DatagridNotificationsComponent);
    dataSourcesNotificationsService = TestBed.inject(DataSourcesNotificationsService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change the selected notification', () => {
    const notification: DataSourceNotificacion = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.week,
        weekDays: ['Lunes', 'Martes']
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        weekDays: ['Lunes']
      },
    };

    component.toggleModal(notification);

    expect(component.notificationSelected).toEqual(notification);
  });

  it('should not change the selected notification if time type is undefined', () => {
    const notification: DataSourceNotificacion = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.undefined,
        weekDays: []
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        weekDays: []
      },
    };

    component.toggleModal(notification);

    expect(component.notificationSelected).toEqual(undefined);
  });

  it('should convert frecuency to string frecuency for week time', () => {
    const frecuency: LoadingPeriodFrecuency = {
      eachTime: 2,
      time: TIME_TYPE.week,
      weekDays: ['Lunes', 'Martes']
    };
    const notificationTime: NotificationTime = {
      time: new Date(),
      weekDays: ['Lunes']
    }
    const frecuencyText = component.getDataSourceTimeInString(frecuency, notificationTime);
    expect(frecuencyText).toEqual('Cada 2 semanas, Lunes')
  });

  it('should convert frecuency to string frecuency for month time', () => {
    const frecuency: LoadingPeriodFrecuency = {
      eachTime: 2,
      time: TIME_TYPE.month,
      monthDays: [1, 2]
    };
    const notificationTime: NotificationTime = {
      time: new Date(),
      monthDays: [1, 2]
    }
    const frecuencyText = component.getDataSourceTimeInString(frecuency, notificationTime);
    expect(frecuencyText).toEqual('Cada 2 meses, del día 1 al día 2')
  });

  it('should convert frecuency to string frecuency for year time', () => {
    const frecuency: LoadingPeriodFrecuency = {
      eachTime: 2,
      time: TIME_TYPE.year,
      yearMonths: ['Enero'],
      yearDays: [1, 2]
    };
    const notificationTime: NotificationTime = {
      time: new Date(),
      monthDays: [1, 2]
    }
    const frecuencyText = component.getDataSourceTimeInString(frecuency, notificationTime);
    expect(frecuencyText).toEqual('Cada 2 años, Enero del día 1 al día 2')
  });

  it('should convert frecuency to string frecuency for year time with same notification day', () => {
    const frecuency: LoadingPeriodFrecuency = {
      eachTime: 2,
      time: TIME_TYPE.year,
      yearMonths: ['Enero'],
      yearDays: [1, 2]
    };
    const notificationTime: NotificationTime = {
      time: new Date(),
      monthDays: [1, 1]
    }
    const frecuencyText = component.getDataSourceTimeInString(frecuency, notificationTime);
    expect(frecuencyText).toEqual('Cada 2 años, Enero día 1')
  });

  it('should exec the dataSourcesNotificationsService service to inactivate and show the success modal', () => {
    spyOn(dataSourcesNotificationsService, 'activateDataSourceNotification').and.returnValue(of(true));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ]

    component.activateNotification(false, 1, 'Name 1');
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should exec the dataSourcesNotificationsService service to activate and show the success modal', () => {
    spyOn(dataSourcesNotificationsService, 'activateDataSourceNotification').and.returnValue(of(true));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: false,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ]

    component.activateNotification(true, 1, 'Name 1');
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should exec the dataSourcesNotificationsService service, return false, and not show the success modal', () => {
    spyOn(dataSourcesNotificationsService, 'activateDataSourceNotification').and.returnValue(of(false));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ]

    component.activateNotification(false, 1, 'Name 1');
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show the inactivate modal and confirm', () => {
    spyOn(dataSourcesNotificationsService, 'activateDataSourceNotification').and.returnValue(of(true));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ]

    component.onSwitchClick(true, 1, 'Name 1');
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show the activate modal and confirm,', () => {
    spyOn(dataSourcesNotificationsService, 'activateDataSourceNotification').and.returnValue(of(true));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: false,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ]

    component.onSwitchClick(false, 1, 'Name 1');
    const confirmButton = Swal.getConfirmButton();

    expect(confirmButton).toBeTruthy();
    confirmButton!.click();
  });

  it('should edit notification week selected if editDataSourceNotification is called successfully', () => {
    spyOn(dataSourcesNotificationsService, 'editDataSourceNotification').and.returnValue(of('Test'));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ];

    component.notificationSelected = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.week,
        weekDays: ['Lunes', 'Martes']
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        weekDays: ['Lunes']
      },
    };

    component.configurationLoadingPeriodNotificationForm.controls['weekDays'].setValue(['Lunes', 'Martes']);

    component.onEditLoadingPeriodNotification();
    expect(component.modalVisible).toEqual(false);
    expect(Swal.isVisible()).toBeTrue();
  });

  it('should edit notification month selected if editDataSourceNotification is called successfully', () => {
    spyOn(dataSourcesNotificationsService, 'editDataSourceNotification').and.returnValue(of('Test'));

    component.configurationLoadingPeriodNotificationForm.controls['monthDays'].setValue(undefined);

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.month,
          monthDays: [1,3]
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          monthDays: [1,2]
        },
      },
      {
        id: 2,
        name: 'Name 2',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.month,
          monthDays: [1,3]
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          monthDays: [1,2]
        },
      }
    ];

    component.notificationSelected = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.month,
        monthDays: [1,3]
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        monthDays: [1,2]
      },
    };

    component.configurationLoadingPeriodNotificationForm.controls['monthDays'].setValue([1,2]);

    component.onEditLoadingPeriodNotification();
    expect(component.modalVisible).toEqual(false);
    expect(Swal.isVisible()).toBeTrue();
  });

  it('should not edit notification week selected if editDataSourceNotification is called with error and press confirm button', () => {
    spyOn(dataSourcesNotificationsService, 'editDataSourceNotification').and.returnValue(throwError(() => 'Error'));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ];

    component.notificationSelected = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.week,
        weekDays: ['Lunes', 'Martes']
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        weekDays: ['Lunes']
      },
    };

    component.onEditLoadingPeriodNotification();
    expect(component.modalVisible).toEqual(false);
    expect(Swal.isVisible()).toBeTrue();

    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();

    expect(confirmButton).toBeTruthy();
  });

  it('should not edit notification week selected if editDataSourceNotification is called with error and press cancel button', () => {
    spyOn(dataSourcesNotificationsService, 'editDataSourceNotification').and.returnValue(throwError(() => 'Error'));

    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes']
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          weekDays: ['Lunes']
        },
      }
    ];

    component.notificationSelected = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.week,
        weekDays: ['Lunes', 'Martes']
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        weekDays: ['Lunes']
      },
    };

    component.onEditLoadingPeriodNotification();
    expect(component.modalVisible).toEqual(false);
    expect(Swal.isVisible()).toBeTrue();

    const cancelButton = Swal.getCancelButton()!;
    cancelButton.click();

    expect(cancelButton).toBeTruthy();
  });

  it('should cancel the notification modify and reset all', () => {
    component.notificationSelected = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.month,
        monthDays: [1,3]
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        monthDays: [1,2]
      },
    };

    component.onCancelLoadingPeriodNotification();
    
    const confirmationButton = Swal.getConfirmButton()!;
    confirmationButton.click();
    
    expect(component.modalVisible).toEqual(false);
  });

  it('should not cancel the notification modify', () => {
    component.notifications = [
      {
        id: 1,
        name: 'Name 1',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.month,
          monthDays: [1,3]
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          monthDays: [1,2]
        },
      },
      {
        id: 2,
        name: 'Name 2',
        frecuency: {
          eachTime: 2,
          time: TIME_TYPE.month,
          monthDays: [1,3]
        },
        isActive: true,
        notificationTime: {
          time: new Date(),
          monthDays: [1,2]
        },
      }
    ];

    component.notificationSelected = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.month,
        monthDays: [1,3]
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        monthDays: [1,2]
      },
    };

    component.configurationLoadingPeriodNotificationForm.controls['monthDays'].setValue(undefined);
    component.configurationLoadingPeriodNotificationForm.controls['weekDays'].setValue(undefined);

    component.onCancelLoadingPeriodNotification();
    
    const cancelButton = Swal.getCancelButton()!;
    cancelButton.click();

    expect(component.modalVisible).toEqual(false);
  });
});
