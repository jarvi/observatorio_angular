import { Component, Input, inject, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TooltipOptions } from 'primeng/api';
import { ModalAlertService, SpinnerService } from 'src/app/shared/services';
import { DataSourcesNotificationsService } from '../../services/data-sources-notifications.service';
import { ModalAlert } from 'src/app/shared/interfaces';
import { DataSourceNotificacion, NotificationTime } from '../../interfaces/data-source-notification.interface';
import { LoadingPeriodFrecuency } from 'src/app/modules/data-sources/interfaces/loading-period-frecuency.interface';
import { ActivateDataSourceNotificationRequest } from '../../interfaces/activate-data-source-notification-request.interface';
import { EditDataSourceNotificationRequest } from '../../interfaces/edit-data-source-notification-request.interface';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { TIME_TYPE } from 'src/app/modules/data-sources/enums';
import { dataSourceTimeToString } from 'src/app/modules/data-sources/utils/data-source-time-to-string';
import { executeCallbackLate } from 'src/utils/execute-callback-late';
import { getFirstAndLast, weekDayToNumber } from 'src/utils/time-conversions';

@Component({
  selector: 'configure-notifications-datagrid-notifications',
  templateUrl: './datagrid-notifications.component.html',
  styleUrls: ['./datagrid-notifications.component.scss']
})
export class DatagridNotificationsComponent implements OnDestroy {

  @Input() public notifications: DataSourceNotificacion[] = [];

  private _fb = inject(FormBuilder);
  private _modalAlertService = inject(ModalAlertService);
  private _spinnerService = inject(SpinnerService);
  private _dataSourcesNotificationsService = inject(DataSourcesNotificationsService);

  private _activateDataSourceNotificationSubscription: Subscription = new Subscription();
  private _editDataSourceNotificationSubscription: Subscription = new Subscription();

  public configurationLoadingPeriodNotificationForm: FormGroup = this._fb.group({
    weekDays: [[]],
    monthDays: [[]],
  });
  public notificationSelected?: DataSourceNotificacion;
  public tooltipOptionsEditButton: TooltipOptions = {
    tooltipPosition: 'top',
    tooltipEvent: 'hover',
      positionTop: -5,
      positionLeft: -30,
    tooltipLabel: 'Clic aquí para cambiar el tiempo de envió de notificaciones',
    tooltipStyleClass: 'configure-notifications-datagrid-notifications-edit-button'
  }
  public modalVisible: boolean = false;

  ngOnDestroy(): void {
    this._activateDataSourceNotificationSubscription.unsubscribe();
    this._editDataSourceNotificationSubscription.unsubscribe();
  }

  private assignValueToConfigurationLoadingPeriodNotificationForm(weekDays?: string[], monthDays?: number[]) {
    this.configurationLoadingPeriodNotificationForm.controls['weekDays'].setValue(weekDays);
    this.configurationLoadingPeriodNotificationForm.controls['monthDays'].setValue(monthDays);
  }
  
  private replaceNotificationTime(notificationSelected: DataSourceNotificacion) {
    const formControlMonthDays = this.configurationLoadingPeriodNotificationForm.controls['monthDays'];
    const formControlWeekDays = this.configurationLoadingPeriodNotificationForm.controls['weekDays'];

    this.notifications = this.notifications.map(notification => {
      if (notification.id === notificationSelected?.id) {
        return {
          ...notification,
          notificationTime: {
            ...notification.notificationTime,
            monthDays: getFirstAndLast(formControlMonthDays.value),
            weekDays: formControlWeekDays.value,
          }
        }
      }
      return notification;
    });
  }

  private showActivateInactivateNotificationAlert(isActivate: boolean, notificationId: number, dataSourceName: string): void {
    const optionString = isActivate ? 'Inactivar' : 'Activar';

    const warinigAlert: ModalAlert = {
      title: `${optionString} notificación`,
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: `Si, ${optionString.toLowerCase()}`,
        primary: true,
        action: () => {
          this.activateNotification(!isActivate, notificationId, dataSourceName);
        }
      },
      cancelButton: {
        label: 'Cancelar',
        primary: false,
      },
      highlightText: `¿Está seguro que desea ${optionString.toLowerCase()} la notificación de ${dataSourceName}?`,
    }

    this._modalAlertService.showModalAlert(warinigAlert);
  }
  
  private showCancelAlert() {
    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this.configurationLoadingPeriodNotificationForm.reset();

          executeCallbackLate(() => {
            this.assignValueToConfigurationLoadingPeriodNotificationForm(
              this.notificationSelected?.notificationTime.weekDays,
              this.notificationSelected?.notificationTime.monthDays
            );

            this.notificationSelected = undefined;
          })
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
        action: () => this.returnFormToCurrentValue()
      },
      message: 'Si hace clic en “Si, cancelar” perderá toda la información diligenciada hasta el momento.',
      highlightText: `¿Está seguro que desea cancelar la configuración de frecuencia de notificaciones de ${this.notificationSelected!.name}?`,
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }

  private showSuccessAlert(title: string, message: string) {
    const successAlert: ModalAlert = {
      title,
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Aceptar',
        primary: true
      },
      message,
      highlightText: 'Haga click para continuar',
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  private showErrorAlert(error: string): void {
    const resetAll = () => {
      this.notificationSelected = undefined;
      this.configurationLoadingPeriodNotificationForm.reset();
    }

    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
        action: () => resetAll()
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => resetAll()
      },
      highlightText: error
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private returnFormToCurrentValue() {
    const monthDaysValue = this.configurationLoadingPeriodNotificationForm.controls['monthDays'].value || [];
    const weekDaysValue = this.configurationLoadingPeriodNotificationForm.controls['weekDays'].value || [];

    const notificationSelectedReference: DataSourceNotificacion = { 
      ...this.notificationSelected!,
      notificationTime: {
        ...this.notificationSelected!.notificationTime,
        monthDays: getFirstAndLast(monthDaysValue),
        weekDays: weekDaysValue
      }
    };

    this.toggleModal(notificationSelectedReference);

    this.notificationSelected = undefined;

    executeCallbackLate(() => {
      this.notificationSelected = notificationSelectedReference;
    })
  }

  private editLoadingPeriodNotification() {
    this._spinnerService.showSpinner = true;

    const formControlMonthDaysValue: number[] = this.configurationLoadingPeriodNotificationForm.controls['monthDays'].value;
    const formControlWeekDays: string[] = this.configurationLoadingPeriodNotificationForm.controls['weekDays'].value;

    const request: EditDataSourceNotificationRequest = {
      id: this.notificationSelected!.id,
      notification_days: formControlMonthDaysValue && formControlMonthDaysValue.length > 0 ? 
        formControlMonthDaysValue : 
        formControlWeekDays.map(day => weekDayToNumber(day))
    }
    
    this._editDataSourceNotificationSubscription = this._dataSourcesNotificationsService.editDataSourceNotification(request).subscribe({
      next: (notificationName) => {
        this._spinnerService.showSpinner = false;
        
        this.replaceNotificationTime(this.notificationSelected!);
        this.showSuccessAlert(
          'Configuración de notificaciones actualizada', 
          `La frecuencia de notificaciones de ${notificationName} se ha modificado exitosamente.`
          );
      },
      error: (error) => {
          this._spinnerService.showSpinner = false;
          this.showErrorAlert(error);
      },
    });
  }
  
  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
  }

  public activateNotification(activate: boolean, notificationId: number, dataSourceName: string): void {
    this._spinnerService.showSpinner = true;

    const request: ActivateDataSourceNotificationRequest = {
      id: notificationId,
    }

    this._activateDataSourceNotificationSubscription = this._dataSourcesNotificationsService.activateDataSourceNotification(request).subscribe(response => {
      if (response) {
        this._spinnerService.showSpinner = false;

        this.notifications = this.notifications.map((notification) => {
          if (notification.id === notificationId) notification.isActive = activate;
          return notification;
        });

        const optionInactivatedString = !activate ? 'inactivada' : 'activada';
        const secondOptionInactivatedString = !activate ? 'inactivado' : 'activado';
        
        this.showSuccessAlert(
          `Notificación ${optionInactivatedString}`, 
          `La notificación de ${dataSourceName} se ha ${secondOptionInactivatedString} exitosamente.`
        );
      } else {
        this._spinnerService.showSpinner = false;
      }
    })
  }

  public toggleModal(notification: DataSourceNotificacion) {
    if(!notification.frecuency.time || notification.frecuency.time === TIME_TYPE.undefined) return;

    this.notificationSelected = notification;
    this.assignValueToConfigurationLoadingPeriodNotificationForm(
      notification.notificationTime.weekDays,
      notification.notificationTime.monthDays
    );

    this.onVisibleChange(true);
  }

  public getDataSourceTimeInString(loadingPeriodFrecuency: LoadingPeriodFrecuency, notificationTime: NotificationTime): string {
    return dataSourceTimeToString(loadingPeriodFrecuency, notificationTime);
  }

  public onSwitchClick(isActivate: boolean, notificationId: number, notificationName: string): void {
    this.showActivateInactivateNotificationAlert(isActivate, notificationId, notificationName);
  }

  public onEditLoadingPeriodNotification() {
    this.onVisibleChange(false);
    this.editLoadingPeriodNotification();
  }

  public onCancelLoadingPeriodNotification() {
    this.onVisibleChange(false);
    this.showCancelAlert();
  }
}
