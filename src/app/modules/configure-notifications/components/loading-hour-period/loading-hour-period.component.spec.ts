import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingHourPeriodComponent } from './loading-hour-period.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

describe('LoadingHourPeriodComponent', () => {
  let component: LoadingHourPeriodComponent;
  let fixture: ComponentFixture<LoadingHourPeriodComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingHourPeriodComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        PrimeNgModule
      ]
    });
    fixture = TestBed.createComponent(LoadingHourPeriodComponent);
    component = fixture.componentInstance;

    component.configurationForm = new FormGroup({
      time: new FormControl(null, [Validators.required]),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should hide overlay component', () => {
    component.onVisibleChange(false);
    expect(component.modalVisible).toEqual(false);
  });

  it('should show the overmay element when toggleModalTime method is executed', () => {
    component.toggleModalTime();
    expect(component.modalVisible).toEqual(true);
  });

  it('should emit value when onEditTimeLoadingPeriod method is executed', () => {
    spyOn(component.editTimeLoadingPeriod, 'emit');

    component.configurationForm.controls['time'].setValue(2);
    component.onEditTimeLoadingPeriod();
    expect(component.editTimeLoadingPeriod.emit).toHaveBeenCalled();
  });

  it('should not emit value when onEditTimeLoadingPeriod method is executed', () => {
    spyOn(component.editTimeLoadingPeriod, 'emit');

    component.onEditTimeLoadingPeriod();
    expect(component.editTimeLoadingPeriod.emit).not.toHaveBeenCalled();
  });

  it('should emit value when onCancelTimeLoadingPeriod method is executed', () => {
    spyOn(component.cancelTimeLoadingPeriod, 'emit');
    component.onCancelTimeLoadingPeriod();
    expect(component.cancelTimeLoadingPeriod.emit).toHaveBeenCalled();
  });
  
  it('should call closeOverlayTime and emit cancelTimeLoadingPeriod with a function', () => {
    
    const mockEmitFunction = jasmine.createSpy('mockEmitFunction');
    
    component.cancelTimeLoadingPeriod.emit = mockEmitFunction;
  
    component.onCancelTimeLoadingPeriod();
  
    expect(mockEmitFunction).toHaveBeenCalled();
  
    expect(mockEmitFunction).toHaveBeenCalledWith(jasmine.any(Function));
    
    const simulatedEvent = new PointerEvent('click', { pointerType: 'mouse' });
    const emittedFunction = mockEmitFunction.calls.mostRecent().args[0];
    emittedFunction(simulatedEvent);
  });
});
