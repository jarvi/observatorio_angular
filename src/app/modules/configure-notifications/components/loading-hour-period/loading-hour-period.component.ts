import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'configure-notifications-loading-hour-period',
  templateUrl: './loading-hour-period.component.html',
  styleUrls: ['./loading-hour-period.component.scss']
})
export class LoadingHourPeriodComponent {

  @Input() public configurationForm: FormGroup = new FormGroup({});
  @Input() public time: Date = new Date();
  @Output() public editTimeLoadingPeriod: EventEmitter<void> = new EventEmitter<void>();
  @Output() public cancelTimeLoadingPeriod: EventEmitter<() => void> = new EventEmitter<() => void>();

  public isUpdating: Subject<boolean> = new Subject<boolean>();
  public modalVisible: boolean = false;

  public toggleModalTime() {
    this.onVisibleChange(true);
    this.isUpdating.next(true);

    executeCallbackLate(() => { this.isUpdating.next(false); });
  }

  public onEditTimeLoadingPeriod() {
    if (this.configurationForm.invalid) return;

    this.onVisibleChange(false);
    this.editTimeLoadingPeriod.emit();
  }

  public onCancelTimeLoadingPeriod() {
    this.onVisibleChange(false);
    this.cancelTimeLoadingPeriod.emit(() => {
      this.toggleModalTime();
    });
  }
  
  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
  }
}
