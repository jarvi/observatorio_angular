import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLoadingPeriodNotificationComponent } from './edit-loading-period-notification.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { DataSourceNotificacion } from '../../interfaces/data-source-notification.interface';
import { TIME_TYPE } from 'src/app/modules/data-sources/enums';

describe('EditLoadingPeriodNotificationComponent', () => {
  let component: EditLoadingPeriodNotificationComponent;
  let fixture: ComponentFixture<EditLoadingPeriodNotificationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditLoadingPeriodNotificationComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        PrimeNgModule,
        SharedModule
      ]
    });
    fixture = TestBed.createComponent(EditLoadingPeriodNotificationComponent);
    component = fixture.componentInstance;
    component.configurationLoadingPeriodNotificationForm = new FormGroup({
      time: new FormControl(null),
      weekDays: new FormControl([]),
      monthDays: new FormControl([]),
    })
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnChanges when notificationSelected changes with week time', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newNotificationSelected: DataSourceNotificacion = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.week,
        weekDays: ['Lunes', 'Martes']
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        weekDays: ['Lunes']
      },
    };
    component.notificationSelected = newNotificationSelected;
    fixture.detectChanges();
    component.ngOnChanges({ notificationSelected: {
      previousValue: '',
      currentValue: newNotificationSelected,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
  });

  it('should call ngOnChanges when notificationSelected changes with month time', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newNotificationSelected: DataSourceNotificacion = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.month,
        monthDays: [1, 2, 3]
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        monthDays: [1, 2]
      },
    };
    component.notificationSelected = newNotificationSelected;
    fixture.detectChanges();
    component.ngOnChanges({ notificationSelected: {
      previousValue: '',
      currentValue: newNotificationSelected,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
  });

  it('should emit value when onEditLoadingPeriodNotification is called', () => {
    spyOn(component.editLoadingPeriodNotification, 'emit');

    component.onEditLoadingPeriodNotification();
    expect(component.editLoadingPeriodNotification.emit).toHaveBeenCalled();
  });

  it('should not emit value when onEditLoadingPeriodNotification is called', () => {
    spyOn(component.editLoadingPeriodNotification, 'emit');

    const newNotificationSelected: DataSourceNotificacion = {
      id: 1,
      frecuency: {
        eachTime: 2,
        time: TIME_TYPE.month,
        monthDays: [1, 2, 3]
      },
      isActive: true,
      name: 'Test',
      notificationTime: {
        time: new Date(),
        monthDays: [1, 2]
      },
    };
    component.notificationSelected = newNotificationSelected;
    fixture.detectChanges();
    component.ngOnChanges({ notificationSelected: {
      previousValue: '',
      currentValue: newNotificationSelected,
      firstChange: true,
      isFirstChange: () => true
    }});

    component.onEditLoadingPeriodNotification();
    expect(component.editLoadingPeriodNotification.emit).not.toHaveBeenCalled();
  });

  it('should emit value when onCancelLoadingPeriodNotification is called', () => {
    spyOn(component.cancelLoadingPeriodNotification, 'emit');

    component.onCancelLoadingPeriodNotification();
    expect(component.cancelLoadingPeriodNotification.emit).toHaveBeenCalled();
  });
});
