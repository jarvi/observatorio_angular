import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, inject } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { DataSourceNotificacion } from '../../interfaces/data-source-notification.interface';
import { ChipButton } from 'src/app/shared/interfaces/chip-button.interface';
import { TIME_TYPE } from 'src/app/modules/data-sources/enums';
import { weekDays } from 'src/app/modules/data-sources/constants/loading-period-times';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'configure-notifications-edit-loading-period-notification',
  templateUrl: './edit-loading-period-notification.component.html',
  styleUrls: ['./edit-loading-period-notification.component.scss']
})
export class EditLoadingPeriodNotificationComponent implements OnChanges {

  @Input() public configurationLoadingPeriodNotificationForm: FormGroup = new FormGroup({});
  @Input() public notificationSelected?: DataSourceNotificacion;
  @Output() public editLoadingPeriodNotification: EventEmitter<void> = new EventEmitter<void>();
  @Output() public cancelLoadingPeriodNotification: EventEmitter<() => void> = new EventEmitter<() => void>();

  private _cdr = inject(ChangeDetectorRef);

  public isUpdating: Subject<boolean> = new Subject<boolean>();
  public timeObject = { week: TIME_TYPE.week, month: TIME_TYPE.month, year: TIME_TYPE.year }
  public weekDaysList: ChipButton[] = weekDays;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['notificationSelected']) {
      this.isUpdating.next(true);
      this.determinateTimeInputType();
    }
  }

  private clearWeekValidators() {
    const weekDays = this.configurationLoadingPeriodNotificationForm.controls['weekDays'];
    weekDays.setValidators([]);
    weekDays.updateValueAndValidity();
    weekDays.setValue([])
  }

  private clearMonthValidators() {
    const monthDays = this.configurationLoadingPeriodNotificationForm.controls['monthDays'];
    monthDays.setValidators([]);
    monthDays.updateValueAndValidity();
    monthDays.setValue([])
  }

  private determinateTimeInputType() {
    switch (this.notificationSelected?.frecuency.time) {
      case TIME_TYPE.week:
        this.configurationLoadingPeriodNotificationForm.controls['weekDays'].setValidators([Validators.required]);
        this.configurationLoadingPeriodNotificationForm.controls['weekDays']?.updateValueAndValidity();

        this.clearMonthValidators();
        break;
      case TIME_TYPE.month:
      case TIME_TYPE.year:
        this.configurationLoadingPeriodNotificationForm.controls['monthDays'].setValidators([Validators.required]);
        this.configurationLoadingPeriodNotificationForm.controls['monthDays']?.updateValueAndValidity();

        this.clearWeekValidators();
        break;
    }

    executeCallbackLate(() => {
      this.isUpdating.next(false);
      this._cdr.detectChanges();
    });
  }

  public onEditLoadingPeriodNotification() {
    if (this.configurationLoadingPeriodNotificationForm.invalid) return;

    this.editLoadingPeriodNotification.emit();
  }

  public onCancelLoadingPeriodNotification() {
    this.cancelLoadingPeriodNotification.emit();
  }
}
