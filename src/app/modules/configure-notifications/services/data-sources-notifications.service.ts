import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, of, throwError } from 'rxjs';
import { HttpRequestService } from 'src/app/shared/services';
import { GetDataSourceNotificationsResponse } from '../interfaces/get-data-source-notifications-response.interface';
import { ActivateDataSourceNotificationRequest } from '../interfaces/activate-data-source-notification-request.interface';
import { EditDataSourceNotificationRequest } from '../interfaces/edit-data-source-notification-request.interface';
import { EditDataSourceNotificationResponse } from '../interfaces/edit-data-source-notification-response.interface';
import { EditDataSourceNotificationTimeRequest } from '../interfaces/edit-data-source-notification-time-request.interface';
import { DataSourceNotificacion } from '../interfaces/data-source-notification.interface';
import { TIME_TYPE } from '../../data-sources/enums';
import { environment } from 'src/environments/environment';
import { createDateFromTimeString, getFirstAndLast, numberToMonth, numberToWeekDay } from '../../../../utils/time-conversions';

@Injectable({
  providedIn: 'root'
})
export class DataSourcesNotificationsService {

  private _httpRequestService$ = inject(HttpRequestService);

  private _pathNotifications: string = environment.pathNotifications;

  public getDataSourceNotifications(): Observable<DataSourceNotificacion[]> {
    return this._httpRequestService$.get<{}, GetDataSourceNotificationsResponse[]>(`${this._pathNotifications}/periods/`)
      .pipe(
        map((response) => response.map(notification => ({
          id: notification.id,
          name: notification.data_source,
          isActive: !notification.is_paused,
          frecuency: {
            eachTime: notification.eachtime,
            time: notification.period as TIME_TYPE,
            weekDays: notification.week_days.map(day => numberToWeekDay(day)),
            monthDays: getFirstAndLast(notification.month_days),
            yearMonths: notification.year_months.map(month => numberToMonth(month)),
            yearDays: getFirstAndLast(notification.year_days),
          },
          notificationTime: {
            time: createDateFromTimeString(notification.notification_hour),
            monthDays: [TIME_TYPE.month, TIME_TYPE.year].includes(notification.period as TIME_TYPE) && notification.notification_days ?
              getFirstAndLast(notification.notification_days) :
              undefined,
            weekDays: notification.period === TIME_TYPE.week ? notification.notification_days.map(day => numberToWeekDay(day)) : undefined,
          }
        }))),
        catchError(() => {
          return throwError(() => '¡No fue posible obtener las fuentes de datos!');
        })
      );
  }

  public activateDataSourceNotification({ id }: ActivateDataSourceNotificationRequest): Observable<boolean> {
    return this._httpRequestService$.put<ActivateDataSourceNotificationRequest, {}>(`${this._pathNotifications}/pause-unpause/${id}/`)
      .pipe(
        map(() => true),
        catchError((error) => {
          return of(false);
        })
      );
  }

  public editDataSourceNotification({ id, notification_days }: EditDataSourceNotificationRequest): Observable<string> {
    return this._httpRequestService$.put<EditDataSourceNotificationRequest, EditDataSourceNotificationResponse>(`${this._pathNotifications}/notification-period/${id}/`, { notification_days })
      .pipe(
        map(({ data_source }: EditDataSourceNotificationResponse) => data_source),
        catchError(() => {
          return throwError(() => '¡No fue posible modificar las notificaciones para la fuente de datos!');
        })
      );
  }

  public editDataSourceNotificationTime({ notification_hour }: EditDataSourceNotificationTimeRequest): Observable<boolean> {
    return this._httpRequestService$.put<EditDataSourceNotificationTimeRequest, {}>(`${this._pathNotifications}/change-hour/`, { notification_hour })
      .pipe(
        map(() => true),
        catchError(() => {
          return throwError(() => '¡No fue posible modificar la hora para las notificaciones de las fuentes de datos!');
        })
      );
  } 
}
