import { TestBed } from '@angular/core/testing';

import { DataSourcesNotificationsService } from './data-sources-notifications.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpRequestService } from 'src/app/shared/services';
import { of, take, throwError } from 'rxjs';
import { EditDataSourceNotificationRequest } from '../interfaces/edit-data-source-notification-request.interface';
import { EditDataSourceNotificationResponse } from '../interfaces/edit-data-source-notification-response.interface';
import { ActivateDataSourceNotificationRequest } from '../interfaces/activate-data-source-notification-request.interface';
import { GetDataSourceNotificationsResponse } from '../interfaces/get-data-source-notifications-response.interface';
import { TIME_TYPE } from '../../data-sources/enums';
import { DataSourceNotificacion } from '../interfaces/data-source-notification.interface';
import { EditDataSourceNotificationTimeRequest } from '../interfaces/edit-data-source-notification-time-request.interface';

describe('DataSourcesNotificationsService', () => {
  let service: DataSourcesNotificationsService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        DataSourcesNotificationsService,
        HttpRequestService
      ]
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    service = TestBed.inject(DataSourcesNotificationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get response when editDataSourceNotification is executed', () => {
    const response: EditDataSourceNotificationResponse = {
      data_source: 'Name'
    } 
    spyOn(httpRequestService, 'put').and.returnValue(of(response));

    const request: EditDataSourceNotificationRequest = {
      id: 1,
      notification_days: [0, 1]
    }

    service.editDataSourceNotification(request)
    .subscribe((response) => {
      expect(response).toEqual('Name');
    });
  });

  it('should not get response when editDataSourceNotification is executed with a bad request', () => {
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => 'Error'));

    const request: EditDataSourceNotificationRequest = {
      id: 1,
      notification_days: [0, 1]
    }

    service.editDataSourceNotification(request)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {expect(err).toEqual('¡No fue posible modificar las notificaciones para la fuente de datos!');}
    );
  });

  it('should get response when activateDataSourceNotification is executed', () => {
    const response: EditDataSourceNotificationResponse = {
      data_source: 'Name'
    } 
    spyOn(httpRequestService, 'put').and.returnValue(of(response));

    const request: ActivateDataSourceNotificationRequest = {
      id: 1
    }

    service.activateDataSourceNotification(request)
    .subscribe((response) => {
      expect(response).toEqual(true);
    });
  });

  it('should not get response when activateDataSourceNotification is executed with a bad request', () => {
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => 'Error'));

    const request: ActivateDataSourceNotificationRequest = {
      id: 1
    }

    service.activateDataSourceNotification(request)
    .subscribe((response) => {
      expect(response).toEqual(false);
    });
  });

  it('should get response when getDataSourceNotifications is executed', () => {
    const responseExpected: DataSourceNotificacion[] = [{
        id: 1,
        frecuency: {
          eachTime: 1,
          time: TIME_TYPE.week,
          weekDays: ['Lunes', 'Martes'],
          monthDays: [],
          yearDays: [],
          yearMonths: []
        },
        isActive: true,
        name: 'Name',
        notificationTime: {
          time: new Date(new Date().setHours(12, 30, 0, 0)),
          weekDays: ['Martes'],
          monthDays: undefined
        }
    }]
    const response: GetDataSourceNotificationsResponse[] = [{
      id: 1,
      data_source: 'Name',
      is_paused: false,
      eachtime: 1,
      notification_hour: '12:30:00',
      period: TIME_TYPE.week,
      week_days: [0,1],
      month_days: [],
      year_days: [],
      year_months: [],
      notification_days: [1]
    }]
    spyOn(httpRequestService, 'get').and.returnValue(of(response));

    service.getDataSourceNotifications()
    .subscribe((response) => {
      expect(response[0].name).toEqual(responseExpected[0].name);
    });
  });

  it('should get response when getDataSourceNotifications is executed for month', () => {
    const responseExpected: DataSourceNotificacion[] = [{
        id: 1,
        frecuency: {
          eachTime: 1,
          time: TIME_TYPE.month,
          weekDays: [],
          monthDays: [1,2,3],
          yearDays: [],
          yearMonths: []
        },
        isActive: true,
        name: 'Name',
        notificationTime: {
          time: new Date(new Date().setHours(12, 30, 0, 0)),
          monthDays: [1,2]
        }
    }]
    const response: GetDataSourceNotificationsResponse[] = [{
      id: 1,
      data_source: 'Name',
      is_paused: false,
      eachtime: 1,
      notification_hour: '12:30:00',
      period: TIME_TYPE.month,
      week_days: [],
      month_days: [1,2,3],
      year_days: [],
      year_months: [],
      notification_days: [1,2]
    }]
    spyOn(httpRequestService, 'get').and.returnValue(of(response));

    service.getDataSourceNotifications()
    .subscribe((response) => {
      expect(response[0].name).toEqual(responseExpected[0].name);
    });
  });

  it('should get response when getDataSourceNotifications is executed for month with only one day', () => {
    const responseExpected: DataSourceNotificacion[] = [{
        id: 1,
        frecuency: {
          eachTime: 1,
          time: TIME_TYPE.month,
          weekDays: [],
          monthDays: [1],
          yearDays: [],
          yearMonths: []
        },
        isActive: true,
        name: 'Name',
        notificationTime: {
          time: new Date(new Date().setHours(12, 30, 0, 0)),
          monthDays: [1]
        }
    }]
    const response: GetDataSourceNotificationsResponse[] = [{
      id: 1,
      data_source: 'Name',
      is_paused: false,
      eachtime: 1,
      notification_hour: '12:30:00',
      period: TIME_TYPE.month,
      week_days: [],
      month_days: [1],
      year_days: [],
      year_months: [],
      notification_days: [1]
    }]
    spyOn(httpRequestService, 'get').and.returnValue(of(response));

    service.getDataSourceNotifications()
    .subscribe((response) => {
      expect(response[0].name).toEqual(responseExpected[0].name);
    });
  });

  it('should get response when getDataSourceNotifications is executed for year', () => {
    const responseExpected: DataSourceNotificacion[] = [{
        id: 1,
        frecuency: {
          eachTime: 1,
          time: TIME_TYPE.year,
          weekDays: [],
          monthDays: [],
          yearDays: [1,2,3],
          yearMonths: ['Enero']
        },
        isActive: true,
        name: 'Name',
        notificationTime: {
          time: new Date(new Date().setHours(12, 30, 0, 0)),
          monthDays: [1,2]
        }
    }]
    const response: GetDataSourceNotificationsResponse[] = [{
      id: 1,
      data_source: 'Name',
      is_paused: false,
      eachtime: 1,
      notification_hour: '12:30:00',
      period: TIME_TYPE.year,
      week_days: [],
      month_days: [],
      year_days: [1,2,3],
      year_months: [1],
      notification_days: [1,2]
    }]
    spyOn(httpRequestService, 'get').and.returnValue(of(response));

    service.getDataSourceNotifications()
    .subscribe((response) => {
      expect(response[0].name).toEqual(responseExpected[0].name);
    });
  });

  it('should not get response when getDataSourceNotifications is executed with a bad request', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => 'Error'));

    service.getDataSourceNotifications()
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {expect(err).toEqual('¡No fue posible obtener las fuentes de datos!');}
    );
  });

  it('should edit all time on notifications', () => {
    const response = { data_source: 'Name' };
    spyOn(httpRequestService, 'put').and.returnValue(of(response));

    const request: EditDataSourceNotificationTimeRequest = {
      notification_hour: '12:30:00'
    }

    service.editDataSourceNotificationTime(request)
    .subscribe((response) => {
      expect(response).toEqual(true);
    });
  });

  it('should not edit all time on notifications when is executed with a bad request', () => {
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => 'Error'));

    const request: EditDataSourceNotificationTimeRequest = {
      notification_hour: '12:30:00'
    }

    service.editDataSourceNotificationTime(request)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {expect(err).toEqual('¡No fue posible modificar la hora para las notificaciones de las fuentes de datos!');}
    );
  });
});
