import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigureNotificationsRoutingModule } from './configure-notifications-routing.module';
import { ConfigureNotificationsComponent } from './pages/configure-notifications/configure-notifications.component';
import { DatagridNotificationsComponent } from './components/datagrid-notifications/datagrid-notifications.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EditLoadingPeriodNotificationComponent } from './components/edit-loading-period-notification/edit-loading-period-notification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingHourPeriodComponent } from './components/loading-hour-period/loading-hour-period.component';


@NgModule({
  declarations: [
    ConfigureNotificationsComponent,
    DatagridNotificationsComponent,
    EditLoadingPeriodNotificationComponent,
    LoadingHourPeriodComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConfigureNotificationsRoutingModule,

    PrimeNgModule,
    SharedModule
  ]
})
export class ConfigureNotificationsModule { }
