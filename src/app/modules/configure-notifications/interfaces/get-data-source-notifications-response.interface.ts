export interface GetDataSourceNotificationsResponse {
    id: number;
    data_source: string;
    is_paused: boolean;
    eachtime: number;
    notification_hour: string;
    period: string;
    week_days: number[];
    month_days: number[];
    year_days: number[];
    year_months: number[];
    notification_days: number[];
}
