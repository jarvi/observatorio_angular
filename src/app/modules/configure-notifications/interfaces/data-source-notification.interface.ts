import { LoadingPeriodFrecuency } from "../../data-sources/interfaces/loading-period-frecuency.interface";

export interface DataSourceNotificacion {
    id: number;
    name: string;
    frecuency: LoadingPeriodFrecuency;
    isActive: boolean;
    notificationTime: NotificationTime;
}

export interface NotificationTime {
    time: Date;
    weekDays?: string[];
    monthDays?: number[];
}