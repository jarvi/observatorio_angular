export interface EditDataSourceNotificationTimeRequest {
    notification_hour: string;
}