export interface EditDataSourceNotificationResponse {
    data_source: string;
}