export interface EditDataSourceNotificationRequest {
    id: number;
    notification_days: number[];
}