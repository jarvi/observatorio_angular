import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigureNotificationsComponent } from './pages/configure-notifications/configure-notifications.component';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

const routes: Routes = [
  {
    path: '',
    component: ConfigureNotificationsComponent,
    data: {
      breadcrumb: '',
      permissions: [
        PERMISSION_TYPE.changeNotification.name,
        PERMISSION_TYPE.viewNotification.name,
      ]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigureNotificationsRoutingModule { }
