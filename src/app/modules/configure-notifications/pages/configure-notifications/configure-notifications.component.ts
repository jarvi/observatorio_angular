import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MobileWidthService, ModalAlertService, SpinnerService, ValidatorService } from 'src/app/shared/services';
import { DataSourcesNotificationsService } from '../../services/data-sources-notifications.service';
import { DataSourceNotificacion } from '../../interfaces/data-source-notification.interface';
import { EditDataSourceNotificationTimeRequest } from '../../interfaces/edit-data-source-notification-time-request.interface';
import { ModalAlert } from 'src/app/shared/interfaces';
import { slideInOutAnimation } from 'src/app/shared/animations/animations';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { executeCallbackLate } from 'src/utils/execute-callback-late';
import { formatDateToTimeString } from '../../../../../utils/time-conversions';

@Component({
  templateUrl: './configure-notifications.component.html',
  styleUrls: ['./configure-notifications.component.scss'],
  animations: [slideInOutAnimation]
})
export class ConfigureNotificationsComponent implements OnInit, OnDestroy {

  private _dataSourcesNotificationsService = inject(DataSourcesNotificationsService);
  private _spinnerService = inject(SpinnerService);
  private _validatorService = inject(ValidatorService);
  private _modalAlertService = inject(ModalAlertService);
  private _mobileWidthService = inject(MobileWidthService);
  private _fb = inject(FormBuilder);

  private _getNotificationSubscription: Subscription = new Subscription();
  private _widthSubscription: Subscription = new Subscription();
  private _editDataSourceNotificationTimeSubscription: Subscription = new Subscription();

  public notifications: DataSourceNotificacion[] = [];
  public hideConfiguration: boolean = false;
  public isScreenSmall: boolean = false;
  public timeLoadingPeriod: Date | null = null;
  public configurationLoadingHourPeriodNotificationForm: FormGroup = this._fb.group({
    time: [null, [Validators.required, this._validatorService.dateIsLessThan(7, 30)]]
  });

  ngOnInit(): void {
    this.getNotifications();

    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
  }

  ngOnDestroy(): void {
    this._getNotificationSubscription.unsubscribe();
    this._widthSubscription.unsubscribe();
    this._editDataSourceNotificationTimeSubscription.unsubscribe();
  }

  private getNotifications() {
    this._spinnerService.showSpinner = true;
    this._getNotificationSubscription = this._dataSourcesNotificationsService.getDataSourceNotifications().subscribe({
      next: (notificationsResponse) => {
        this.notifications = notificationsResponse;

        if (this.notifications.length > 0) {
          this.timeLoadingPeriod = this.notifications[0].notificationTime.time;
          this.configurationLoadingHourPeriodNotificationForm.controls['time'].setValue(this.notifications[0].notificationTime.time);
        }

        this._spinnerService.showSpinner = false;
      },
      error: () => {
        this.notifications = [];
        this._spinnerService.showSpinner = false;
      }
    })
  }

  private showModalAlertError(message: string) {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      message,
      highlightText: 'Haga click para continuar',
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private showModalAlertSuccess() {
    const successAlert: ModalAlert = {
      title: 'Periodo de carga actualizado',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Aceptar',
        primary: true
      },
      message: 'La hora de notificaciones de carga de datos se ha modificado exitosamente.',
      highlightText: 'Haga click para continuar',
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  private replaceTimeInNotifications(newDate: Date) {
    this.notifications = this.notifications.map(notification => ({
      ...notification,
      notificationTime: {
        ...notification.notificationTime,
        time: newDate
      }
    }))
  }

  private resetForm() {
    this.configurationLoadingHourPeriodNotificationForm.reset();

    executeCallbackLate(() => {
      this.configurationLoadingHourPeriodNotificationForm.controls['time'].setValue(this.timeLoadingPeriod);
    });
  }

  public showHideConfiguration() {
    this.hideConfiguration = !this.hideConfiguration;
  }

  public onEditTimeLoadingPeriod() {
    this._spinnerService.showSpinner = true;

    const request: EditDataSourceNotificationTimeRequest = {
      notification_hour: formatDateToTimeString(this.configurationLoadingHourPeriodNotificationForm.controls['time'].value),
    }

    this._editDataSourceNotificationTimeSubscription = this._dataSourcesNotificationsService.editDataSourceNotificationTime(request).subscribe({
      next: () => {
        this._spinnerService.showSpinner = false;
        this.showModalAlertSuccess();

        this.timeLoadingPeriod = this.configurationLoadingHourPeriodNotificationForm.controls['time'].value;
        this.replaceTimeInNotifications(this.timeLoadingPeriod!);
      },
      error: (error) => {
        this._spinnerService.showSpinner = false;
        this.showModalAlertError(error);
        this.resetForm();
      }
    })
  }

  public onCancelTimeLoadingPeriod(continueAction: () => void) {
    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => { this.resetForm(); }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
        action: () => {
          continueAction();
        }
      },
      message: 'Si hace clic en “Si, cancelar” perderá toda la información diligenciada hasta el momento.',
      highlightText: '¿Está seguro que desea cancelar la configuración de hora de notificaciones de carga de datos?',
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }
}
