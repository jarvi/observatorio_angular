import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureNotificationsComponent } from './configure-notifications.component';
import { DataSourcesNotificationsService } from '../../services/data-sources-notifications.service';
import { HttpRequestService } from 'src/app/shared/services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { TIME_TYPE } from 'src/app/modules/data-sources/enums';
import { DatagridNotificationsComponent } from '../../components/datagrid-notifications/datagrid-notifications.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { EditLoadingPeriodNotificationComponent } from '../../components/edit-loading-period-notification/edit-loading-period-notification.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('ConfigureNotificationsComponent', () => {
  let component: ConfigureNotificationsComponent;
  let fixture: ComponentFixture<ConfigureNotificationsComponent>;
  let dataSourcesNotificationsService: DataSourcesNotificationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConfigureNotificationsComponent],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        DataSourcesNotificationsService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(ConfigureNotificationsComponent);
    dataSourcesNotificationsService = TestBed.inject(DataSourcesNotificationsService);

    spyOn(dataSourcesNotificationsService, 'getDataSourceNotifications').and.returnValue(of([]));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change hideConfiguration to true', () => {
      component.showHideConfiguration();
      expect(component.hideConfiguration).toBeTrue();
  });

  it('should show loading period modal alert when try to cancel and press confirm button', () => {
      const callBack = () => { };
      component.onCancelTimeLoadingPeriod(callBack);

      expect(Swal.isVisible()).toBeTrue();

      const confirmButton = Swal.getConfirmButton()!;
      confirmButton.click();

      expect(confirmButton).toBeTruthy();
  });

  it('should show loading period modal alert when try to cancel and press cancel button', () => {
      const callBack = () => { };
      component.onCancelTimeLoadingPeriod(callBack);

      expect(Swal.isVisible()).toBeTrue();

      const cancelButton = Swal.getCancelButton()!;
      cancelButton.click();

      expect(cancelButton).toBeTruthy();
  });

  it('should change time of all notification when editDataSourceNotificationTime method is executed', () => {
    component.notifications = [{
      id: 1,
      name: 'Name',
      isActive: true,
      frecuency: {
        time: TIME_TYPE.week,
        eachTime: 1,
        weekDays: ['Lunes']
      },
      notificationTime: {
        time: new Date(),
        weekDays: ['Lunes']
      }
    }]

    const newTime = new Date(new Date().setHours(12, 30, 0, 0));

    component.configurationLoadingHourPeriodNotificationForm.controls['time'].setValue(newTime);
    spyOn(dataSourcesNotificationsService, 'editDataSourceNotificationTime').and.returnValue(of(true));

    component.onEditTimeLoadingPeriod();
    expect(component.notifications[0].notificationTime.time).toEqual(newTime);
  });

  it('should not change time of all notification when editDataSourceNotificationTime method is executed with a bad request', () => {
    const newTime = new Date(new Date().setHours(12, 30, 0, 0));
    spyOn(component.configurationLoadingHourPeriodNotificationForm, 'reset');

    component.configurationLoadingHourPeriodNotificationForm.controls['time'].setValue(newTime);
    spyOn(dataSourcesNotificationsService, 'editDataSourceNotificationTime').and.returnValue(throwError(() => {}));

    component.onEditTimeLoadingPeriod();
    expect(component.configurationLoadingHourPeriodNotificationForm.reset).toHaveBeenCalled();
  });
});

describe('ConfigureNotificationsComponent', () => {
  let component: ConfigureNotificationsComponent;
  let fixture: ComponentFixture<ConfigureNotificationsComponent>;
  let dataSourcesNotificationsService: DataSourcesNotificationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ConfigureNotificationsComponent,
        DatagridNotificationsComponent,
        EditLoadingPeriodNotificationComponent,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        DataSourcesNotificationsService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(ConfigureNotificationsComponent);
    dataSourcesNotificationsService = TestBed.inject(DataSourcesNotificationsService);

    spyOn(dataSourcesNotificationsService, 'getDataSourceNotifications').and.returnValue(of([{
      id: 1,
      name: 'Name',
      isActive: true,
      frecuency: {
        time: TIME_TYPE.week,
        eachTime: 1,
        weekDays: ['Lunes']
      },
      notificationTime: {
        time: new Date(),
        weekDays: ['Lunes']
      }
    }]));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('ConfigureNotificationsComponent with bad request', () => {
  let component: ConfigureNotificationsComponent;
  let fixture: ComponentFixture<ConfigureNotificationsComponent>;
  let dataSourcesNotificationsService: DataSourcesNotificationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConfigureNotificationsComponent],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        DataSourcesNotificationsService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(ConfigureNotificationsComponent);
    dataSourcesNotificationsService = TestBed.inject(DataSourcesNotificationsService);

    spyOn(dataSourcesNotificationsService, 'getDataSourceNotifications').and.returnValue(throwError(() => ''));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});