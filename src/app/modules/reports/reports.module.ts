import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ReportsRoutingModule } from './reports-routing.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { StandardizedTablesFormComponent } from './components/standardized-tables-form/standardized-tables-form.component';
import { StandardizedTablesComponent } from './pages/standardized-tables/standardized-tables.component';
import { DinamicReportsComponent } from './pages/dinamic-reports/dinamic-reports.component';
import { LoadingPeriodModalComponent } from './components/loading-period-modal/loading-period-modal.component';


@NgModule({
  declarations: [
    StandardizedTablesComponent,
    StandardizedTablesFormComponent,
    DinamicReportsComponent,
    LoadingPeriodModalComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ReportsRoutingModule,

    PrimeNgModule,
    SharedModule
  ]
})
export class ReportsModule { }
