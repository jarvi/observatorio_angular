import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StandardizedTablesComponent } from './standardized-tables.component';
import { ReportsService } from '../../services/reports.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StandardizedTablesFormComponent } from '../../components/standardized-tables-form/standardized-tables-form.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { of, throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingPeriodModalComponent } from '../../components/loading-period-modal/loading-period-modal.component';

describe('StandardizedTablesComponent', () => {
  let component: StandardizedTablesComponent;
  let fixture: ComponentFixture<StandardizedTablesComponent>;
  let reportsServices: ReportsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        StandardizedTablesComponent,
        StandardizedTablesFormComponent,
        LoadingPeriodModalComponent
      ],
      imports: [
        HttpClientTestingModule,
        PrimeNgModule,
        ReactiveFormsModule,
        SharedModule,
        BrowserAnimationsModule
      ],
      providers: [
        ReportsService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(StandardizedTablesComponent);
    reportsServices = TestBed.inject(ReportsService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should does not do anything if generateReportForm is invalid', () => {
    component.generateReport();
    expect(component).toBeTruthy();
  });

  it('should get the report if generateReportForm is ok', () => {
    const response: Blob = new Blob();

    spyOn(reportsServices, 'getReport').and.returnValue(of(response));
    spyOn(document.body, 'appendChild');
    const spyObj = jasmine.createSpyObj('a', ['click']);
    spyOn(document, 'createElement').and.returnValue(spyObj);

    component.generateReportForm.patchValue({
      systemReport: 1,
      generatedTime: {
        year: 2023,
        months: ['Marzo']
      }
    })

    component.generateReport();
    expect(component.isLoading).toBeFalse();
  });

  it('should not get the report if generateReportForm is ok but request response with a bad request', () => {
    spyOn(reportsServices, 'getReport').and.returnValue(throwError(() => {}));
    
    component.generateReportForm.patchValue({
      systemReport: 1,
      generatedTime: {
        year: 2023,
        months: ['Marzo']
      }
    })

    component.generateReport();
    expect(component.isLoading).toBeFalse();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should set empty value to info message', () => {
    component.onCloseInfoMessage();
    expect(component.infoMessage.message).toEqual('')
  });
});

describe('StandardizedTablesComponent with reports error', () => {
  let component: StandardizedTablesComponent;
  let fixture: ComponentFixture<StandardizedTablesComponent>;
  let reportsServices: ReportsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        StandardizedTablesComponent,
        StandardizedTablesFormComponent,
        LoadingPeriodModalComponent
      ],
      imports: [
        HttpClientTestingModule,
        PrimeNgModule,
        ReactiveFormsModule,
        SharedModule,
        BrowserAnimationsModule
      ],
      providers: [
        ReportsService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(StandardizedTablesComponent);
    reportsServices = TestBed.inject(ReportsService);
    component = fixture.componentInstance;

    spyOn(reportsServices, 'getSystemReports').and.returnValue(throwError(() => {}));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});