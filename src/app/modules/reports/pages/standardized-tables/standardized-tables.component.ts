import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TooltipOptions } from 'primeng/api';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { InfoMessage, ModalAlert } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { INFO_MESSAGE_TYPE } from 'src/app/shared/enums/info-message-type.enum';
import { ReportsService } from '../../services/reports.service';
import { StandardizedTablesRequest } from '../../interfaces/standardized-tables-request.interfaces';
import { slideInOutAnimation } from 'src/app/shared/animations/animations';
import { monthToNumber } from 'src/utils/time-conversions';

@Component({
  templateUrl: './standardized-tables.component.html',
  styleUrls: ['./standardized-tables.component.scss'],
  animations: [slideInOutAnimation]
})
export class StandardizedTablesComponent implements OnInit, OnDestroy {

  private _fb = inject(FormBuilder);
  private _mobileWidthService = inject(MobileWidthService);
  private _modalAlertService = inject(ModalAlertService);
  private _reportsServices = inject(ReportsService);

  private _widthSubscription: Subscription = new Subscription();
  private _getReportSubscription: Subscription = new Subscription();

  public isScreenSmall: boolean = false;
  public isLoading: boolean = false;
  public generateReportForm: FormGroup = this._fb.group({
    systemReport: [null, [Validators.required]],
    generatedTime: [null, [Validators.required]],
  });
  public infoMessage: InfoMessage = {
    message: 'Recuerde que el botón "Generar" se habilitará al completar todos los campos obligatorios.',
    type: INFO_MESSAGE_TYPE.warning
  }
  public tooltipOptions: TooltipOptions = {
    tooltipPosition: 'top',
    tooltipEvent: 'hover',
    positionTop: -10,
    positionLeft: -50,
    tooltipLabel: 'Complete todos los campos',
    tooltipStyleClass: 'standardized-tables-generate-button'
  }

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
    this._getReportSubscription.unsubscribe();
  }

  private showErrorModal(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private resetForm() {
    this.generateReportForm.reset();
  }

  private getReport() {
    const request: StandardizedTablesRequest = {
      report: this.generateReportForm.controls['systemReport'].value,
      fecha_inicial: `${this.generateReportForm.controls['generatedTime'].value.year}-${monthToNumber(this.generateReportForm.controls['generatedTime'].value.months[0])}`,
      fecha_final: `${this.generateReportForm.controls['generatedTime'].value.year}-${monthToNumber(this.generateReportForm.controls['generatedTime'].value.months[0])}`
    }

    this.isLoading = true;
    
    this._getReportSubscription = this._reportsServices.getReport(request).subscribe({
      next: (file: Blob) => {
        const url = window.URL.createObjectURL(file);
        const a = document.createElement('a');
        a.href = url;
        a.download = `${request.report}.xlsx`;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        
        this.isLoading = false;
        this.resetForm();
      },
      error: (error) => {
        this.isLoading = false;
        this.resetForm();
        this.showErrorModal(error);
      },
    })
  }

  public generateReport() {
    if (this.generateReportForm.invalid) return;
    this.getReport();
  }

  public onCloseInfoMessage() { this.infoMessage = { ...this.infoMessage, message: '' } }
}
