import { Component, OnInit, inject } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './dinamic-reports.component.html'
})
export class DinamicReportsComponent implements OnInit {

  private _sanitizer = inject(DomSanitizer);
  private url: string = environment.urlPowerBIDinamicReports;
  public safeIframeURL?: SafeResourceUrl;

  ngOnInit(): void {
    this.safeIframeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.url);// NOSONAR
  }
}
