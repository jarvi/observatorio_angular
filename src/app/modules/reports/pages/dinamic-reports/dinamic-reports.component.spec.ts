import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DinamicReportsComponent } from './dinamic-reports.component';

describe('DinamicReportsComponent', () => {
  let component: DinamicReportsComponent;
  let fixture: ComponentFixture<DinamicReportsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DinamicReportsComponent]
    });
    fixture = TestBed.createComponent(DinamicReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
