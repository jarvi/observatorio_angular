import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StandardizedTablesComponent } from './pages/standardized-tables/standardized-tables.component';
import { DinamicReportsComponent } from './pages/dinamic-reports/dinamic-reports.component';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'standardized-tables',
    component: StandardizedTablesComponent,
    data: {
      breadcrumb: 'Tablas Estandarizadas',
      permissions: [
        PERMISSION_TYPE.addReport.name,
        PERMISSION_TYPE.viewReport.name,
      ]
    }
  },
  {
    path: 'dinamic-reports',
    component: DinamicReportsComponent,
    data: {
      breadcrumb: 'Reportes dinámicos',
      permissions: [
        PERMISSION_TYPE.addReport.name,
        PERMISSION_TYPE.viewReport.name,
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
