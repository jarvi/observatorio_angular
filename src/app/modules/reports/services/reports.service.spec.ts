import { TestBed } from '@angular/core/testing';

import { ReportsService } from './reports.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, take, throwError } from 'rxjs';
import { StandardizedTablesRequest } from '../interfaces/standardized-tables-request.interfaces';
import { SystemReportResponse } from '../interfaces/system-report-response.interface';

describe('ReportsService', () => {
  let service: ReportsService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService,
      ]
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    service = TestBed.inject(ReportsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get reports', () => {
    const response: SystemReportResponse = {
      dates: {
        fecha_inicial: ['2023-10'],
        fecha_final: ['2023-10'],
      },
      reports: ['Report 1']
    }

    spyOn(httpRequestService, 'get').and.returnValue(of(response));

    service.getSystemReports()
    .pipe(take(1))
    .subscribe((response) => {
      expect(response).toEqual(response);
    });
  });

  it('should get reports if it is already in the variable', () => {
    const response: SystemReportResponse = {
      dates: {
        fecha_inicial: ['2023-10'],
        fecha_final: ['2023-10'],
      },
      reports: ['Report 1']
    }

    spyOn(httpRequestService, 'get').and.returnValue(of(response));

    service.getSystemReports()
    .pipe(take(1))
    .subscribe((response) => {
      expect(response).toEqual(response);
    });

    service.getSystemReports()
    .pipe(take(1))
    .subscribe((response) => {
      expect(response).toEqual(response);
    });
  });

  it('should not get reports', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.getSystemReports()
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('¡La lista de reportes no pudo ser obtenida!');
      }
    );
  });

  it('should get the report', () => {
    const response: string = 'Ok'

    spyOn(httpRequestService, 'get').and.returnValue(of(response));
    
    const request: StandardizedTablesRequest = {
      report: 'Report',
      fecha_inicial: '2023-01',
      fecha_final: '2023-01'
    }

    service.getReport(request)
    .pipe(take(1))
    .subscribe((response) => {
      expect(response).toEqual(response);
    });
  });

  it('should not get the report', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));
    
    const request: StandardizedTablesRequest = {
      report: 'Report',
      fecha_inicial: '2023-01',
      fecha_final: '2023-01'
    }

    service.getReport(request)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('¡El reporte no pudo ser generado!');
      }
    );
  });
});
