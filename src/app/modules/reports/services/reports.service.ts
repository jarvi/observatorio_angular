import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, of, throwError } from 'rxjs';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { StandardizedTablesRequest } from '../interfaces/standardized-tables-request.interfaces';
import { environment } from 'src/environments/environment';
import { SystemReportResponse } from '../interfaces/system-report-response.interface';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  private _httpRequestService$ = inject(HttpRequestService);

  private _pathReport: string = environment.pathReport;

  public systemReports: SystemReportResponse = {
    dates: {
      fecha_inicial: [],
      fecha_final: []
    },
    reports: []
  };

  public getSystemReports(): Observable<SystemReportResponse> {
    if (this.systemReports.reports.length > 0) {
      return of(this.systemReports);
    }

    return this._httpRequestService$.get<{}, SystemReportResponse>(`${this._pathReport}/sem/`)
      .pipe(
        map((response: SystemReportResponse) => {
          this.systemReports = response;
          return response;
        }),
        catchError(() => {
          return throwError(() => '¡La lista de reportes no pudo ser obtenida!');
        })
      );
  }

  public getReport(request: StandardizedTablesRequest): Observable<Blob> {
    return this._httpRequestService$.get<StandardizedTablesRequest, Blob>(`${this._pathReport}/create_excel/${request.report}/${request.fecha_inicial}/${request.fecha_final}/`, {}, true)
      .pipe(
        map((response: Blob) => response),
        catchError(() => {
          return throwError(() => '¡El reporte no pudo ser generado!');
        })
      );
  }
}
