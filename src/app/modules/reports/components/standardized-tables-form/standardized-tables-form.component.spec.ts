import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardizedTablesFormComponent } from './standardized-tables-form.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportsService } from '../../services/reports.service';
import { HttpRequestService } from 'src/app/shared/services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoadingPeriodModalComponent } from '../loading-period-modal/loading-period-modal.component';
import { of } from 'rxjs';
import { SystemReportResponse } from '../../interfaces/system-report-response.interface';

describe('StandardizedTablesFormComponent', () => {
  let component: StandardizedTablesFormComponent;
  let fixture: ComponentFixture<StandardizedTablesFormComponent>;
  let reportsService: ReportsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        StandardizedTablesFormComponent,
        LoadingPeriodModalComponent
      ],
      imports: [
        PrimeNgModule,
        ReactiveFormsModule,
        SharedModule,
        HttpClientTestingModule
      ],
      providers: [
        ReportsService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(StandardizedTablesFormComponent);
    reportsService = TestBed.inject(ReportsService)
    component = fixture.componentInstance;
    component.generateReportForm = new FormGroup({
      systemReport: new FormControl(''),
      generatedTime: new FormControl(null),
    });

    const response: SystemReportResponse = {
      dates: {
        fecha_inicial: ['2023-10'],
        fecha_final: ['2023-10']
      },
      reports: [
        'Report 1',
        'Report 2',
      ]
    }

    spyOn(reportsService, 'getSystemReports').and.returnValue(of(response))

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change showLoadingPeriodModal value to true', () => {
    component.onShowLoadingPeriodModal();
    expect(component.showLoadingPeriodModal).toBeTrue();
  });

  it('should change showLoadingPeriodModal value to true with no monthList', () => {
    component.monthList = [];

    component.onShowLoadingPeriodModal();
    expect(component.showLoadingPeriodModal).toBeFalse();
  });

  it('should change generatedTime value and set a new value to buttonTimeLabel', () => {
    component.generateReportForm.controls['generatedTime'].setValue({
      months: ['Marzo'],
      year: 2023
    })
    expect(component.buttonTimeLabel).toEqual('Marzo de 2023')
  });

  it('should change generatedTime value and set a empty value to buttonTimeLabel', () => {
    component.generateReportForm.controls['generatedTime'].setValue({
      months: null,
      year: null
    })
    expect(component.buttonTimeLabel).toEqual('')
  });
});

describe('StandardizedTablesFormComponent with differtent dates', () => {
  let component: StandardizedTablesFormComponent;
  let fixture: ComponentFixture<StandardizedTablesFormComponent>;
  let reportsService: ReportsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        StandardizedTablesFormComponent,
        LoadingPeriodModalComponent
      ],
      imports: [
        PrimeNgModule,
        ReactiveFormsModule,
        SharedModule,
        HttpClientTestingModule
      ],
      providers: [
        ReportsService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(StandardizedTablesFormComponent);
    reportsService = TestBed.inject(ReportsService)
    component = fixture.componentInstance;
    component.generateReportForm = new FormGroup({
      systemReport: new FormControl(''),
      generatedTime: new FormControl(null),
    });

    const response: SystemReportResponse = {
      dates: {
        fecha_inicial: ['2023-10'],
        fecha_final: ['2024-01']
      },
      reports: [
        'Report 1',
        'Report 2',
      ]
    }

    spyOn(reportsService, 'getSystemReports').and.returnValue(of(response))

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});