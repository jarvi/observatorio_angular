import { Component, Input, OnDestroy, OnInit, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { ReportsService } from '../../services/reports.service';
import { SystemReport } from '../../interfaces/system-report.interface';
import { ModalAlertService, SpinnerService } from 'src/app/shared/services';
import { ModalAlert } from 'src/app/shared/interfaces';
import { ReportMonth } from '../../interfaces/report-month.interface';
import { ReportDates } from '../../interfaces/report-dates.interface';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { numberToMonth } from 'src/utils/time-conversions';

@Component({
  selector: 'reports-standardized-tables-form',
  templateUrl: './standardized-tables-form.component.html',
  styleUrls: ['./standardized-tables-form.component.scss']
})
export class StandardizedTablesFormComponent implements OnInit, OnDestroy {

  @Input() public generateReportForm: FormGroup = new FormGroup({});

  private _mobileWidthService = inject(MobileWidthService);
  private _reportsService = inject(ReportsService);
  private _spinnerService = inject(SpinnerService);
  private _modalAlertService = inject(ModalAlertService);

  private _widthSubscription: Subscription = new Subscription();
  private _generatedTimeSubscription: Subscription = new Subscription();
  private _systemReportsSubscription: Subscription = new Subscription();

  public showLoadingPeriodModal: boolean = false;
  public isScreenSmall: boolean = false;
  public systemReports: SystemReport[] = [];
  public buttonTimeLabel: string = '';
  public yearList: string[] = [];
  public monthList: ReportMonth[] = [];

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });

    this.generateButtonTimeLabel();
    this.getSystemReports();
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
    this._generatedTimeSubscription.unsubscribe();
    this._systemReportsSubscription.unsubscribe();
  }

  private getSystemReports() {
    this._spinnerService.showSpinner = true;

    this._systemReportsSubscription = this._reportsService.getSystemReports().subscribe({
      next: response => {
        this.systemReports = response.reports.map((report, index) => ({
          id: index + 1,
          name: report
        }));
        this._spinnerService.showSpinner = false;

        const initialDate = response.dates.fecha_inicial[0].split('-');
        const finalDate = response.dates.fecha_final[0].split('-');

        const dates: ReportDates = {
          initialDate: {
            year: initialDate[0],
            month: initialDate[1]
          },
          finalDate: {
            year: finalDate[0],
            month: finalDate[1]
          },
        }

        this.setYearList(dates.initialDate.year, dates.finalDate.year);
        this.setMonths(dates);
      },
      error: error => {
        this._spinnerService.showSpinner = false;
        this.showErrorModal(error);
      } 
    });
  }

  private setYearList(initialYear: string, finalYear: string) {
    this.yearList = (initialYear === finalYear) 
      ? [initialYear] : [initialYear, finalYear];
  }

  private generateMonthArrayNumber(initialMonth: number, finalMonth: number) {
    return Array.from(
      { length: finalMonth - initialMonth + 1 }, 
      (_, index) => initialMonth + index
    );
  }

  private setMonths(dates: ReportDates) {
    const { initialDate, finalDate } = dates;

    if (initialDate.year === finalDate.year) {
      const monthsNumberArray = this.generateMonthArrayNumber(
        parseInt(initialDate.month),
        parseInt(finalDate.month)
      ).map(month => numberToMonth(month));

      this.monthList = [
        { year: initialDate.year, months: monthsNumberArray },
      ];
      
      return;
    }

    const initialMonthsNumberArray = this.generateMonthArrayNumber(
      parseInt(initialDate.month),
      12
    ).map(month => numberToMonth(month));
    const finalMonthsNumberArray = this.generateMonthArrayNumber(
      1,
      parseInt(finalDate.month)
    ).map(month => numberToMonth(month));

    this.monthList = [
      { year: initialDate.year, months: initialMonthsNumberArray },
      { year: finalDate.year, months: finalMonthsNumberArray },
    ]
  }

  private showErrorModal(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private generateButtonTimeLabel() {
    this._generatedTimeSubscription = this.generateReportForm.controls['generatedTime'].valueChanges.subscribe(time => {
      const months = time?.months;
      const year = time?.year;
      this.buttonTimeLabel = months && year ? `${months} de ${year}` : '';
    });
  }

  public onShowLoadingPeriodModal() { 
    if (this.monthList.length === 0) return;
    
    this.showLoadingPeriodModal = true;
  }
}
