import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingPeriodModalComponent } from './loading-period-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

describe('LoadingPeriodModalComponent', () => {
  let component: LoadingPeriodModalComponent;
  let fixture: ComponentFixture<LoadingPeriodModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingPeriodModalComponent],
      imports: [
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        PrimeNgModule
      ]
    });
    fixture = TestBed.createComponent(LoadingPeriodModalComponent);
    component = fixture.componentInstance;

    component.monthList = [
      { year: '2023', months: ['Enero', 'Febrero'] }
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit value when onCancel is executed', () => {
    spyOn(component.visibleChange, 'emit');

    component.onCancel(false);

    expect(component.visibleChange.emit).toHaveBeenCalled();
  });

  it('should change the generatedTimeFormControl value when onSubmit is executed', () => {
    component.timeForm.controls['year'].setValue('2023');
    component.timeForm.controls['yearMonths'].setValue(['Marzo']);
    component.onSubmit();

    const formValue = component.generatedTimeFormControl.value;

    expect(formValue).toEqual({ year: '2023', months: [ 'Marzo' ]});
  });

  it('should not change the generatedTimeFormControl value when onSubmit is executed with an invalid value', () => {
    component.timeForm.controls['year'].setValue(2023);
    component.timeForm.controls['yearMonths'].setValue(null);
    component.onSubmit();

    const formValue = component.generatedTimeFormControl.value;

    expect(formValue).not.toEqual({ year: 2023, months: [ 'Marzo' ]});
  });
});
