import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ChipButton } from 'src/app/shared/interfaces/chip-button.interface';
import { ReportMonth } from '../../interfaces/report-month.interface';

@Component({
  selector: 'reports-loading-period-modal',
  templateUrl: './loading-period-modal.component.html',
  styleUrls: ['./loading-period-modal.component.scss']
})
export class LoadingPeriodModalComponent implements OnInit, OnDestroy {

  @Input() public visible: boolean = false;
  @Input() public generatedTimeFormControl: AbstractControl = new FormControl();
  @Input() public yearList: string[] = [];
  @Input() public monthList: ReportMonth[] = [];
  @Output() public visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  private _fb = inject(FormBuilder);

  private _generatedTimeFormControlSubscription: Subscription = new Subscription();
  private _yearFormControlSubscription: Subscription = new Subscription();

  public yearMonthsList: ChipButton[] = [];
  public timeForm: FormGroup = this._fb.group({
    year: [[Validators.required]],
    yearMonths: [[], [Validators.required]]
  });

  ngOnInit(): void {
    this._generatedTimeFormControlSubscription = this.generatedTimeFormControl.statusChanges.subscribe(status => {
      if (status === 'INVALID') this.timeForm.reset();
    });

    this._yearFormControlSubscription = this.timeForm.controls['year'].valueChanges.subscribe(value => {
      this.yearMonthsList = this.monthList.find(month => month.year === value)?.months
        .map((month, index) => ({
          id: index + 1,
          name: month,
          selected: false,
          blocked: false
        })) || [];

      this.timeForm.controls['yearMonths'].setValue([]);
    })
  }

  ngOnDestroy(): void {
    this._generatedTimeFormControlSubscription.unsubscribe();
    this._yearFormControlSubscription.unsubscribe();
  }

  private onVisibleChange(visible: boolean): void {
    this.visibleChange.emit(visible);
  }

  public onCancel(visible: boolean) {
    this.onVisibleChange(visible);
    this.timeForm.reset();
  }

  public onSubmit() {
    if (this.timeForm.invalid) return;

    const yearFormControlValue = this.timeForm.controls['year'].value;
    const yearMonthsFormControlValue = this.timeForm.controls['yearMonths'].value;
    
    this.generatedTimeFormControl.setValue({
      year: yearFormControlValue,
      months: yearMonthsFormControlValue
    });
    
    this.onVisibleChange(false);
  }
}
