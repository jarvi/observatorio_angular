export interface ReportDates {
    initialDate: ReportDate;
    finalDate: ReportDate;
}

interface ReportDate {
    year: string;
    month: string;
}