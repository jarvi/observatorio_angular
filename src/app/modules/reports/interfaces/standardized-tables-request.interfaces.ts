export interface StandardizedTablesRequest {
    report: string;
    fecha_inicial: string;
    fecha_final: string;
}