export interface SystemReportResponse {
    dates: Dates;
    reports: string[];
}

export interface Dates {
    fecha_inicial: string[];
    fecha_final:   string[];
}
