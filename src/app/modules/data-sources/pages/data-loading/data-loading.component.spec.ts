import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DataLoadingComponent } from './data-loading.component';
import { RouterTestingModule } from '@angular/router/testing';
import { DataSourcesService } from '../../services/data-sources.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { DatagridFilesComponent } from '../../components/datagrid-files/datagrid-files.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { CheckValidationFilesPipe } from '../../pipes/check-validation-files.pipe';
import { CheckValidationSomeFilePipe } from '../../pipes/check-validation-some-file.pipe';
import { FileDetailsComponent } from '../../components/file-details/file-details.component';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { FILE_STATUS } from '../../enums/file-status.enum';
import { of, throwError } from 'rxjs';
import { fileRules } from '../../constants/file-rules';
import { CheckStatusPipe } from '../../pipes/check-status.pipe';
import { FileValidationResponse } from '../../interfaces/file-validation-response.interface';
import { DATA_OPTIONS_TYPES } from 'src/app/shared/enums/data-options-types';
import { SIMAT_FILE } from '../../enums/simat-files.enum';
import { DUE_FILE } from '../../enums/due-files.enum';
import { SABER_TESTS_FILE } from '../../enums/saber-tests-files.enum';
import { CONSOLIDATED_SIMAT_FILE, DETAILED_STRATEGIES_FILE, DROPOUT_RATE_FILE, EDUCATIVE_INFRAESTRUCTURE_FILE, FUTURES_COMPUTERS_FILE, HUMAN_TALENT_FILE, INDICATIVE_PLAN_AND_ACTION_PLAN_FILE, INTERNAL_EFFICIENCY_FILE, PAE_FILE, PROJECTION_OF_QUOTAS_FILE, SCHOOL_ENVIRONMENT_PROTECTOR_FILE, TECHNICAL_AVERANGE_FILE, TRANSPORT_FILE } from '../../enums';
import { LoadFilesResponse } from '../../interfaces/file-load-response.interface';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileSheetsValidationResponse } from '../../interfaces/file-sheets-validation-response.interface';

describe('DataLoadingComponent', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show a modal warning alert when onCalcel method is called and reset all', (done) => {
    fixture.ngZone!.run(() => {
      spyOn(router, 'navigateByUrl');

      component.files = [{
        id: '1',
        fileName: 'TEST',
        status: FILE_STATUS.pending,
      }]

      component.onCancel();

      expect(Swal.isVisible()).toBeTruthy();
      const confirmButton = Swal.getConfirmButton();
      confirmButton!.click();
      expect(component.files[0].status).toEqual(FILE_STATUS.pending);
      done();
    });
  });

  it('should not do anything if some file has pending status', () => {
    component.files = [{
      id: '1',
      fileName: 'TEST',
      status: FILE_STATUS.pending,
    }]
    component.dataSourceId = DATA_OPTIONS_TYPES.simat;

    component.onLoadFiles();
    expect(component).toBeTruthy();
  });

  it('should call loadFiles method if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: SIMAT_FILE.anexo5a,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: SIMAT_FILE.anexo5b,
        status: FILE_STATUS.complete,
      },
      {
        id: '3',
        fileName: SIMAT_FILE.anexo5o,
        status: FILE_STATUS.complete,
      },
      {
        id: '4',
        fileName: SIMAT_FILE.anexo6a,
        status: FILE_STATUS.complete,
      },
      {
        id: '5',
        fileName: SIMAT_FILE.anexo6b,
        status: FILE_STATUS.complete,
      },
      {
        id: '6',
        fileName: SIMAT_FILE.anexo6o,
        status: FILE_STATUS.complete,
      }
    ]    
    component.dataSourceId = DATA_OPTIONS_TYPES.simat;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(loadFilesResponse.detail).toEqual('ok');
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for due if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: DUE_FILE.due,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.due;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for future computers if all is ok', () => {
    component.files = [
      {
        id: '1',
        fileName: FUTURES_COMPUTERS_FILE.cruzan,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: FUTURES_COMPUTERS_FILE.computersDelivered,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.futuresComputers;

    const response: LoadFilesResponse = {
      detail: 'Ok',
      failed_anexos: []
    }

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(response));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for educative infraestructure if all is ok', () => {
    component.files = [
      {
        id: '1',
        fileName: EDUCATIVE_INFRAESTRUCTURE_FILE.matrix,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: EDUCATIVE_INFRAESTRUCTURE_FILE.dotation,
        status: FILE_STATUS.complete,
      },
      {
        id: '3',
        fileName: EDUCATIVE_INFRAESTRUCTURE_FILE.sem,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.educativeInfraestructure;

    const response: LoadFilesResponse = {
      detail: 'Ok',
      failed_anexos: []
    }

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(response));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for transport if all is ok', () => {
    component.files = [
      {
        id: '1',
        fileName: TRANSPORT_FILE.ocem,
        status: FILE_STATUS.complete,
      },
      {
        id: '1',
        fileName: TRANSPORT_FILE.ocemProfile,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.transport;

    const response: LoadFilesResponse = {
      detail: 'Ok',
      failed_anexos: []
    }

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(response));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal error alert if loadFiles return an error', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: SIMAT_FILE.anexo5a,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: SIMAT_FILE.anexo5b,
        status: FILE_STATUS.complete,
      },
      {
        id: '3',
        fileName: SIMAT_FILE.anexo5o,
        status: FILE_STATUS.complete,
      },
      {
        id: '4',
        fileName: SIMAT_FILE.anexo6a,
        status: FILE_STATUS.complete,
      },
      {
        id: '5',
        fileName: SIMAT_FILE.anexo6b,
        status: FILE_STATUS.complete,
      },
      {
        id: '6',
        fileName: SIMAT_FILE.anexo6o,
        status: FILE_STATUS.complete,
      }
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.simat;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(throwError(() => {}));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal error alert if loadFiles without id return an error', () => {
    component.files = [
      {
        id: '1',
        fileName: SIMAT_FILE.anexo5a,
        status: FILE_STATUS.complete,
      },
    ]
    
    spyOn(dataSourcesService, 'loadFiles').and.returnValue(throwError(() => {}));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for saber tests if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: SABER_TESTS_FILE.sb111,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: SABER_TESTS_FILE.sb112,
        status: FILE_STATUS.complete,
      }
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.saberTests;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for internal eficiency if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: INTERNAL_EFFICIENCY_FILE.consolidated,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.internalEfficiency;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for technical averange if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: TECHNICAL_AVERANGE_FILE.averange,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.technicalAverage;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for school environment protector if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: SCHOOL_ENVIRONMENT_PROTECTOR_FILE.actionsReport,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: SCHOOL_ENVIRONMENT_PROTECTOR_FILE.attentionReport,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.schoolEnvironmentProtector;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for projection of quotas if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: PROJECTION_OF_QUOTAS_FILE.quotasProjection,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.projectionOfQuotas;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should change status of all files', () => {
    component.files = [
      {
        id: '1',
        fileName: 'TEST',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'TEST',
        status: FILE_STATUS.complete,
      }
    ]

    component.onRemoveFile('1');
    const everyIsPending = component.files.every(file => file.status === FILE_STATUS.pending);
    expect(!everyIsPending).toBeTrue();
  });

  it('Should show modal error alert if loadFiles have already been loaded before', () => {
    let loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: [],
    }       

    component.files = [
      {
        id: '1',
        fileName: SIMAT_FILE.anexo5a,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: SIMAT_FILE.anexo5b,
        status: FILE_STATUS.complete,
      },
      {
        id: '3',
        fileName: SIMAT_FILE.anexo5o,
        status: FILE_STATUS.complete,
      },
      {
        id: '4',
        fileName: SIMAT_FILE.anexo6a,
        status: FILE_STATUS.complete,
      },
      {
        id: '5',
        fileName: SIMAT_FILE.anexo6b,
        status: FILE_STATUS.complete,
      },
      {
        id: '6',
        fileName: SIMAT_FILE.anexo6o,
        status: FILE_STATUS.complete,
      }
    ]    
    component.dataSourceId = DATA_OPTIONS_TYPES.simat;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));

    loadFilesResponse.failed_anexos.push('5a','5b','5o','6a','6b','6o'); 

    component.onLoadFiles();
    expect(loadFilesResponse.detail).toEqual('ok');
    expect(loadFilesResponse.failed_anexos.length != 0);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for human talent if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: HUMAN_TALENT_FILE.baseDocentes,
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: HUMAN_TALENT_FILE.fubDocDir,
        status: FILE_STATUS.complete,
      },
      {
        id: '3',
        fileName: HUMAN_TALENT_FILE.semBaseDocentes,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.humanTalent;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for pae if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: PAE_FILE.ocemPae,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.pae;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the file name does not has anexo', () => {
    const files = [
      new File([""], "5A 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'SIMAT';
    component.files = [{
      id: '1',
      fileName: 'ANEXO 5A',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the rules does not has the correct name', () => {
    const files = [
      new File([""], "ANEXO 5A 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'SIMAT';
    component.files = [{
      id: '1',
      fileName: 'ANEXO 1',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the rules does not has name', () => {
    const files = [
      new File([""], "ANEXO 5A 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'SIMAT';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the file name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "ANEXO 5A 12023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'SIMAT';
    component.files = [{
      id: '1',
      fileName: 'ANEXO 5A',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should not do anything if ruleUse is undefined', () => {
    const files = [
      new File([""], "ANEXO 5A 01122023.csv"),
    ];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the due file name does not has due', () => {
    const files = [
      new File([""], "5A 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [{
      id: '1',
      fileName: 'DUE',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the due rules does not has the correct name', () => {
    const files = [
      new File([""], "DUE 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [{
      id: '1',
      fileName: 'ANEXO 1',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the due rules does not has name', () => {
    const files = [
      new File([""], "DUE 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the due file name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "DUE 120.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [{
      id: '1',
      fileName: 'DUE',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the due file name does not have an extencion', () => {
    const files = [
      new File([""], "DUE 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [{
      id: '1',
      fileName: 'DUE',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Invalid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "DUE 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [
      {
        id: '1',
        fileName: 'DUE',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'DUE 2',
        status: FILE_STATUS.complete,
      }
    ];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is executed but there is an file already exist', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Invalid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "DUE 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [
      {
        id: '1',
        fileName: 'DUE',
        status: FILE_STATUS.complete,
        file: new File([""], "DUE 20211201.csv"),
      },
      {
        id: '2',
        fileName: 'DUE 2',
        status: FILE_STATUS.complete,
        file: new File([""], "DUE 2 20211201.csv"),
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "DUE 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [
      {
        id: '1',
        fileName: 'DUE',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'DUE 2',
        status: FILE_STATUS.complete,
      }
    ];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "DUE 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'DUE';
    component.files = [
      {
        id: '1',
        fileName: 'DUE',
        status: FILE_STATUS.complete,
      },
      {
        id: '3',
        fileName: 'DUE 2',
        status: FILE_STATUS.complete,
      }
    ];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the file name does not has sb111/sb112', () => {
    const files = [
      new File([""], "1 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [{
      id: '1',
      fileName: 'SB11-1',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the saber tests rules does not has the correct name', () => {
    const files = [
      new File([""], "SB11-1 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [{
      id: '1',
      fileName: 'SB11-3',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the saber tests rules does not has name', () => {
    const files = [
      new File([""], "SB11-1 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the saber tests files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "SB11-1 120.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [{
      id: '1',
      fileName: 'SB11-1',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the saber tests file name does not have an extencion', () => {
    const files = [
      new File([""], "SB11-1 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [{
      id: '1',
      fileName: 'SB11-1',
      status: FILE_STATUS.pending,
    }];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with saber tests files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SB11-1 20211201.csv"),
      new File([""], "SB11-2 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [
      {
        id: '1',
        fileName: 'SB11-1',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'SB11-2',
        status: FILE_STATUS.complete,
      }
    ];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with saber tests files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SB11-1 20211201.csv"),
      new File([""], "SB11-2 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [
      {
        id: '1',
        fileName: 'SB11-1',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'SB11-2',
        status: FILE_STATUS.complete,
      }
    ];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with saber tests files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "SB11-1 20211201.csv"),
      new File([""], "SB11-2 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Pruebas Saber';
    component.files = [
      {
        id: '1',
        fileName: 'SB11-1',
        status: FILE_STATUS.complete,
      },
      {
        id: '3',
        fileName: 'SB11-2',
        status: FILE_STATUS.complete,
      }
    ];

    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the file name does not has Cruzan', () => {
    const files = [
      new File([""], "1 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [{
      id: '1',
      fileName: 'Cruzan',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the future computers rules does not has the correct name', () => {
    const files = [
      new File([""], "Cruzan 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [{
      id: '1',
      fileName: 'Cruzzzan',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the future computers rules does not has name', () => {
    const files = [
      new File([""], "Cruzan 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the future computers files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Cruzan 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [{
      id: '1',
      fileName: 'Cruzan',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the future computers file name does not have an extencion', () => {
    const files = [
      new File([""], "Cruzan 03022023"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [{
      id: '1',
      fileName: 'Cruzan',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with future computers files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Cruzan 03022023.csv"),
      new File([""], "Computadores entregados docentes y PP 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [
      {
        id: '1',
        fileName: 'Cruzan',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'Computadores entregados docentes y PP',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with future computers files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Cruzan 20211201.csv"),
      new File([""], "Computadores entregados docentes y PP 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [
      {
        id: '1',
        fileName: 'Cruzan',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'Computadores entregados docentes y PP',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with future computers files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "Cruzan 20211201.csv"),
      new File([""], "Computadores entregados docentes y PP 20211201.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Computadores Futuro';
    component.files = [
      {
        id: '1',
        fileName: 'Cruzan',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'Computadores entregados docentes y PP',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the file name does not has Matriz Proyectos estratégicos', () => {
    const files = [
      new File([""], "1 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [{
      id: '1',
      fileName: 'Matriz Proyectos estratégicos',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the educative infraestructure rules does not has the correct name', () => {
    const files = [
      new File([""], "Matriz Proyectos estratégicos 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [{
      id: '1',
      fileName: 'Maaaaaatriz Proyectos estratégicos',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the educative infraestructure rules does not has name', () => {
    const files = [
      new File([""], "Matriz Proyectos estratégicos 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the educative infraestructure files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Matriz Proyectos estratégicos 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [{
      id: '1',
      fileName: 'Matriz Proyectos estratégicos',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the educative infraestructure file name does not have an extencion', () => {
    const files = [
      new File([""], "Matriz Proyectos estratégicos 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [{
      id: '1',
      fileName: 'Matriz Proyectos estratégicos',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the educative infraestructure INSTITUCIONES_EDUCATIVAS_CON_DOTACION file name does not have an extencion', () => {
    const files = [
      new File([""], "INSTITUCIONES_EDUCATIVAS_CON_DOTACION estratégicos 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [{
      id: '1',
      fileName: 'INSTITUCIONES_EDUCATIVAS_CON_DOTACION',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the educative infraestructure SEM_INFRAESTRUCTURA_IEs file name does not have an extencion', () => {
    const files = [
      new File([""], "SEM_INFRAESTRUCTURA_IEs estratégicos 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [{
      id: '1',
      fileName: 'SEM_INFRAESTRUCTURA_IEs',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with educative infraestructure files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Matriz Proyectos estratégicos 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [
      {
        id: '1',
        fileName: 'Matriz Proyectos estratégicos',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with educative infraestructure files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Matriz Proyectos estratégicos 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [
      {
        id: '1',
        fileName: 'Matriz Proyectos estratégicos',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with future computers files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "Matriz Proyectos estratégicos 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Infraestructura Educativa';
    component.files = [
      {
        id: '1',
        fileName: 'Matriz Proyectos estratégicos',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });
  
  it('should show modal alert error if the file name does not has Informes para OCEM - TEsco', () => {
    const files = [
      new File([""], "1 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [{
      id: '1',
      fileName: 'Informes para OCEM - TEsco',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the transport rules does not has the correct name', () => {
    const files = [
      new File([""], "Informes para OCEM - TEsco 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [{
      id: '1',
      fileName: 'Infoooooormes para OCEM - TEsco',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the transport rules does not has name', () => {
    const files = [
      new File([""], "Informes para OCEM - TEsco 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the transport files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Informes para OCEM - TEsco 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [{
      id: '1',
      fileName: 'Informes para OCEM - TEsco',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the transport file name does not have an extencion', () => {
    const files = [
      new File([""], "Informes para OCEM - TEsco 03022023"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [{
      id: '1',
      fileName: 'Informes para OCEM - TEsco',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with transport files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Informes para OCEM - TEsco Perfil Metro 03022023.csv"),
      new File([""], "Informes para OCEM - TEsco 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [
      {
        id: '1',
        fileName: 'Informes para OCEM - TEsco Perfil Metro',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'Informes para OCEM - TEsco',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with transport files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Informes para OCEM - TEsco Perfil Metro 03022023.csv"),
      new File([""], "Informes para OCEM - TEsco 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [
      {
        id: '1',
        fileName: 'Informes para OCEM - TEsco Perfil Metro',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'Informes para OCEM - TEsco',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with transport files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "Informes para OCEM - TEsco Perfil Metro 03022023.csv"),
      new File([""], "Informes para OCEM - TEsco 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Transporte';
    component.files = [
      {
        id: '1',
        fileName: 'Informes para OCEM - TEsco Perfil Metro',
        status: FILE_STATUS.complete,
      },
      {
        id: '2',
        fileName: 'Informes para OCEM - TEsco',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the internal eficiency rules does not has the correct name', () => {
    const files = [
      new File([""], "Coonsolidado de Eficiencia Interna 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado de Eficiencia Interna';
    component.files = [{
      id: '1',
      fileName: 'Consolidado de Eficiencia Interna',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the internal eficiency rules does not has name', () => {
    const files = [
      new File([""], "Consolidado de Eficiencia Interna 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado de Eficiencia Interna';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the internal eficiency files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Consolidado de Eficiencia Interna 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado de Eficiencia Interna';
    component.files = [{
      id: '1',
      fileName: 'Consolidado de Eficiencia Interna',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the internal eficiency file name does not have an extencion', () => {
    const files = [
      new File([""], "Consolidado de Eficiencia Interna 03022023"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado de Eficiencia Interna';
    component.files = [{
      id: '1',
      fileName: 'Consolidado de Eficiencia Interna',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with internal eficiency files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Consolidado de Eficiencia Interna 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado de Eficiencia Interna';
    component.files = [
      {
        id: '1',
        fileName: 'Consolidado de Eficiencia Interna',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with internal eficiency files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Consolidado de Eficiencia Interna 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado de Eficiencia Interna';
    component.files = [
      {
        id: '1',
        fileName: 'Consolidado de Eficiencia Interna',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with internal eficiency files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "Consolidado de Eficiencia Interna 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado de Eficiencia Interna';
    component.files = [
      {
        id: '1',
        fileName: 'Consolidado de Eficiencia Interna',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the technical average rules does not has the correct name', () => {
    const files = [
      new File([""], "SEEEM_Media_tecnica 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Media Técnica';
    component.files = [{
      id: '1',
      fileName: 'SEM_Media_tecnica',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the technical average rules does not has name', () => {
    const files = [
      new File([""], "SEM_Media_tecnica 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Media Técnica';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the technical average files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "SEM_Media_tecnica 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Media Técnica';
    component.files = [{
      id: '1',
      fileName: 'SEM_Media_tecnica',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the technical average file name does not have an extencion', () => {
    const files = [
      new File([""], "SEM_Media_tecnica 03022023"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Media Técnica';
    component.files = [{
      id: '1',
      fileName: 'SEM_Media_tecnica',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with technical average files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SEM_Media_tecnica 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Media Técnica';
    component.files = [
      {
        id: '1',
        fileName: 'SEM_Media_tecnica',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with technical average files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SEM_Media_tecnica 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Media Técnica';
    component.files = [
      {
        id: '1',
        fileName: 'SEM_Media_tecnica',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with technical average files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "SEM_Media_tecnica 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Media Técnica';
    component.files = [
      {
        id: '1',
        fileName: 'SEM_Media_tecnica',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the school environment protector rules does not has the correct name', () => {
    const files = [
      new File([""], "Reeporte de atenciones 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [{
      id: '1',
      fileName: 'Reporte de atenciones',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the school environment protector rules does not has name', () => {
    const files = [
      new File([""], "Reporte de atenciones 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the school environment protector files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Reporte de atenciones 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [{
      id: '1',
      fileName: 'Reporte de atenciones',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the school environment protector file name does not have an extencion', () => {
    const files = [
      new File([""], "Reporte acciones 03022023"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [{
      id: '1',
      fileName: 'Reporte acciones',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with school environment protector files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Reporte acciones 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [
      {
        id: '1',
        fileName: 'Reporte acciones',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with school environment protector files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Reporte acciones 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [
      {
        id: '1',
        fileName: 'Reporte acciones',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if current day is different to 4 school environment protector files', () => {
    const files = [
      new File([""], "Reporte acciones 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [
      {
        id: '1',
        fileName: 'Reporte acciones',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with school environment protector files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "Reporte acciones 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Entorno Escolar Protector';
    component.files = [
      {
        id: '1',
        fileName: 'Reporte acciones',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the projection of quotas rules does not has the correct name', () => {
    const files = [
      new File([""], "Prooyección de cupos 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [{
      id: '1',
      fileName: 'Proyeccion de Cupos',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the projection of quotas rules does not has name', () => {
    const files = [
      new File([""], "Proyeccion de Cupos 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the projection of quotas rules files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Proyeccion de Cupos 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [{
      id: '1',
      fileName: 'Proyeccion de Cupos',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for detailed strategies if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: DETAILED_STRATEGIES_FILE.detailedStrategies,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.detailedStrategies;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for dropout rate if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: DROPOUT_RATE_FILE.semConsolidated,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.dropoutRate;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for SIMAT consolidated if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: CONSOLIDATED_SIMAT_FILE.simatConsolidated,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.consolidatedSIMAT;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should call loadFiles method for indicative plan and action plan if all is ok', () => {
    const loadFilesResponse: LoadFilesResponse = {
      detail: 'ok',
      failed_anexos: []
    }
    component.files = [
      {
        id: '1',
        fileName: INDICATIVE_PLAN_AND_ACTION_PLAN_FILE.piPAIG,
        status: FILE_STATUS.complete,
      },
    ]
    component.dataSourceId = DATA_OPTIONS_TYPES.indicativePlanAndActionPlan;

    spyOn(dataSourcesService, 'loadFiles').and.returnValue(of(loadFilesResponse));
    component.onLoadFiles();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the human talent rules does not has the correct name', () => {
    const files = [
      new File([""], "SEEM_BAAAAASE_DOOOOCENTES 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [{
      id: '1',
      fileName: 'SEEEEM_BASE_DOCENTES',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the human talent rules does not has name', () => {
    const files = [
      new File([""], "BASE_DOCENTES 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the human talent files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "FUB_DOC y DIR 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [{
      id: '1',
      fileName: 'FUB_DOC y DIR',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the human talent file name does not have an extencion', () => {
    const files = [
      new File([""], "SEM_BASE_DOCENTES 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [{
      id: '1',
      fileName: 'SEM_BASE_DOCENTES',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the human talent BASE_DOCENTES file name does not have an extencion', () => {
    const files = [
      new File([""], "BASE_DOCENTES estratégicos 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [{
      id: '1',
      fileName: 'BASE_DOCENTES',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the human talent FUB_DOC y DIR file name does not have an extencion', () => {
    const files = [
      new File([""], "FUB_DOC y DIR estratégicos 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [{
      id: '1',
      fileName: 'FUB_DOC y DIR',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with human talent files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SEM_BASE_DOCENTES 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [
      {
        id: '1',
        fileName: 'SEM_BASE_DOCENTES',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with human talent files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SEM_BASE_DOCENTES 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Talento Humano';
    component.files = [
      {
        id: '1',
        fileName: 'SEM_BASE_DOCENTES',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the PAE rules does not has the correct name', () => {
    const files = [
      new File([""], "InfoOOOOOrmes OCEM PAE 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'PAE';
    component.files = [{
      id: '1',
      fileName: 'Infoormes OCEM PAE',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the PAE rules does not has name', () => {
    const files = [
      new File([""], "Informes OCEM PAE 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'PAE';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the PAE files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Informes OCEM PAE 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'PAE';
    component.files = [{
      id: '1',
      fileName: 'Informes OCEM PAE',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the PAE file name does not have an extencion', () => {
    const files = [
      new File([""], "Informes OCEM PAE 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'PAE';
    component.files = [{
      id: '1',
      fileName: 'Informes OCEM PAE',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with PAE files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Informes OCEM PAE 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'PAE';
    component.files = [
      {
        id: '1',
        fileName: 'Informes OCEM PAE',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with PAE files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Informes OCEM PAE 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'PAE';
    component.files = [
      {
        id: '1',
        fileName: 'Informes OCEM PAE',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the dropout rate rules does not has the correct name', () => {
    const files = [
      new File([""], "SEEEEEEEEM_CONSOLIDADO_DESERCION_REPITENCIA 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Deserción Repitencia';
    component.files = [{
      id: '1',
      fileName: 'SEEEM_CONSOLIDADO_DESERCION_REPITENCIA',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the dropout rate rules does not has name', () => {
    const files = [
      new File([""], "SEM_CONSOLIDADO_DESERCION_REPITENCIA 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Deserción Repitencia';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the dropout rate files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "SEM_CONSOLIDADO_DESERCION_REPITENCIA 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Deserción Repitencia';
    component.files = [{
      id: '1',
      fileName: 'SEM_CONSOLIDADO_DESERCION_REPITENCIA',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the dropout rate file name does not have an extencion', () => {
    const files = [
      new File([""], "SEM_CONSOLIDADO_DESERCION_REPITENCIA 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Deserción Repitencia';
    component.files = [{
      id: '1',
      fileName: 'SEM_CONSOLIDADO_DESERCION_REPITENCIA',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with dropout rate files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SEM_CONSOLIDADO_DESERCION_REPITENCIA 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Deserción Repitencia';
    component.files = [
      {
        id: '1',
        fileName: 'SEM_CONSOLIDADO_DESERCION_REPITENCIA',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with dropout rate files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "SEM_CONSOLIDADO_DESERCION_REPITENCIA 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Deserción Repitencia';
    component.files = [
      {
        id: '1',
        fileName: 'SEM_CONSOLIDADO_DESERCION_REPITENCIA',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the consolidated SIMAT rules does not has the correct name', () => {
    const files = [
      new File([""], "Consolidado SIIMAT 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado SIMAT';
    component.files = [{
      id: '1',
      fileName: 'Consolidado SIIIIIIMAT',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the consolidated SIMAT rules does not has name', () => {
    const files = [
      new File([""], "Consolidado SIMAT 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado SIMAT';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the consolidated SIMAT files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Consolidado SIMAT 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado SIMAT';
    component.files = [{
      id: '1',
      fileName: 'Consolidado SIMAT',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the consolidated SIMAT file name does not have an extencion', () => {
    const files = [
      new File([""], "Consolidado SIMAT 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado SIMAT';
    component.files = [{
      id: '1',
      fileName: 'Consolidado SIMAT',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with consolidated SIMAT files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Consolidado SIMAT 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado SIMAT';
    component.files = [
      {
        id: '1',
        fileName: 'Consolidado SIMAT',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with consolidated SIMAT files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Consolidado SIMAT 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Consolidado SIMAT';
    component.files = [
      {
        id: '1',
        fileName: 'Consolidado SIMAT',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the detailed strategies rules does not has the correct name', () => {
    const files = [
      new File([""], "Deeeetallado_estrategias 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Detallado Estrategias';
    component.files = [{
      id: '1',
      fileName: 'Deeeeeeeetallado_estrategias',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the detailed strategies rules does not has name', () => {
    const files = [
      new File([""], "Detallado_estrategias 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Detallado Estrategias';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the detailed strategies files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "Detallado_estrategias 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Detallado Estrategias';
    component.files = [{
      id: '1',
      fileName: 'Detallado_estrategias',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the detailed strategies file name does not have an extencion', () => {
    const files = [
      new File([""], "Detallado_estrategias 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Detallado Estrategias';
    component.files = [{
      id: '1',
      fileName: 'Detallado_estrategias',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with detailed strategies files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Detallado_estrategias 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Detallado Estrategias';
    component.files = [
      {
        id: '1',
        fileName: 'Detallado_estrategias',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with detailed strategies files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Detallado_estrategias 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Detallado Estrategias';
    component.files = [
      {
        id: '1',
        fileName: 'Detallado_estrategias',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the indicative plan and action plan rules does not has the correct name', () => {
    const files = [
      new File([""], "PIII PA IG 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Plan Indicativo y Plan de Acción';
    component.files = [{
      id: '1',
      fileName: 'PIIIII PA IG',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the indicative plan and action plan rules does not has name', () => {
    const files = [
      new File([""], "PI PA IG 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Plan Indicativo y Plan de Acción';
    component.files = [{
      id: '1',
      fileName: '',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the indicative plan and action plan files name does not have a number with 8 characters', () => {
    const files = [
      new File([""], "PI PA IG 0302.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Plan Indicativo y Plan de Acción';
    component.files = [{
      id: '1',
      fileName: 'PI PA IG',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the indicative plan and action plan file name does not have an extencion', () => {
    const files = [
      new File([""], "PI PA IG 01129999"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Plan Indicativo y Plan de Acción';
    component.files = [{
      id: '1',
      fileName: 'PI PA IG',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with indicative plan and action plan files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "PI PA IG 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Plan Indicativo y Plan de Acción';
    component.files = [
      {
        id: '1',
        fileName: 'PI PA IG',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with indicative plan and action plan files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "PI PA IG 03022023.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Plan Indicativo y Plan de Acción';
    component.files = [
      {
        id: '1',
        fileName: 'PI PA IG',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if the projection of quotas file name does not have an extencion', () => {
    const files = [
      new File([""], "Proyeccion de Cupos 03022023"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [{
      id: '1',
      fileName: 'Proyeccion de Cupos',
      status: FILE_STATUS.pending,
    }];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully with projection of quotas files', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Proyeccion de Cupos 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [
      {
        id: '1',
        fileName: 'Proyeccion de Cupos',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with projection of quotas files', () => {
    const response: FileValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: false,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: false,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: false,
        detail: 'Valid'
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Proyeccion de Cupos 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [
      {
        id: '1',
        fileName: 'Proyeccion de Cupos',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with projection of quotas files with FileSheetsValidationResponse', () => {
    const response: FileSheetsValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      sheets: [{
        columns: {
          status: false,
          detail: 'Valid',
          missing_columns: [''],
        },
        content: {
          status: false,
          detail: 'Valid',
          invalid_columns: ['']
        },
        name: 'name',
        rows: {
          empty: {
            status: false,
            detail: 'Valid'
          },
          total: 12
        }
      }],
      date: {
        available: 'Test',
        detail: 'Test'
      }
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Proyeccion de Cupos 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [
      {
        id: '1',
        fileName: 'Proyeccion de Cupos',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is invalid with projection of quotas files with FileSheetsValidationResponse and empty sheets', () => {
    const response: FileSheetsValidationResponse = {
      valid: false,
      characters: {
        status: false,
        detail: 'Valid',
        invalid_line: ''
      },
      format: {
        status: false,
        detail: 'Valid'
      },
      name: {
        status: false,
        detail: 'Valid'
      },
      size: {
        status: false,
        detail: 'Valid'
      },
      sheets: [],
      date: {
        available: 'Test',
        detail: 'Test'
      }
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Proyeccion de Cupos 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [
      {
        id: '1',
        fileName: 'Proyeccion de Cupos',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert success if validateFile is executed successfully if all response is valid with projection of quotas files with FileSheetsValidationResponse', () => {
    const response: FileSheetsValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      sheets: [{
        columns: {
          status: true,
          detail: 'Valid',
          missing_columns: [''],
        },
        content: {
          status: true,
          detail: 'Valid',
          invalid_columns: ['']
        },
        name: 'name',
        rows: {
          empty: {
            status: true,
            detail: 'Valid'
          },
          total: 12
        }
      }]
    }

    spyOn(dataSourcesService, 'validateFile').and.returnValue(of(response));

    const files = [
      new File([""], "Proyeccion de Cupos 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [
      {
        id: '1',
        fileName: 'Proyeccion de Cupos',
        status: FILE_STATUS.complete,
      }
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal alert error if validateFile is not executed successfully with projection of quotas files', () => {
    spyOn(dataSourcesService, 'validateFile').and.returnValue(throwError(() => 'Error'));

    const files = [
      new File([""], "Proyeccion de Cupos 03022021.csv"),
    ];

    component.ruleUse = fileRules[0];
    component.dataSourceId = 'Proyección de Cupos';
    component.files = [
      {
        id: '1',
        fileName: 'Proyeccion de Cupos',
        status: FILE_STATUS.complete,
      },
    ];
    
    component.onGetFiles(files);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should set empty value to info message', () => {
    component.onCloseInfoMessage();
    expect(component.infoMessage.message).toEqual('')
  });
});

describe('DataLoadingComponent with paramsMap SIMAT', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'SIMAT' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap DUE', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'DUE' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Pruebas Saber', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Pruebas Saber' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Computadores Futuro', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Computadores Futuro' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Infraestructura Educativa', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Infraestructura Educativa' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Transporte', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Transporte' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Eficiencia Interna', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Consolidado de Eficiencia Interna' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Media Técnica', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Media Técnica' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Entorno Escolar Protector', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Entorno Escolar Protector' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Proyección de Cupos', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Proyección de Cupos' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Consolidado SIMAT', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Consolidado SIMAT' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Detallado Estrategias', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Detallado Estrategias' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('DataLoadingComponent with paramsMap Plan Indicativo y Plan de Acción', () => {
  let component: DataLoadingComponent;
  let fixture: ComponentFixture<DataLoadingComponent>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSourcesService: DataSourcesService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [
        DataLoadingComponent,
        DatagridFilesComponent,
        FileDetailsComponent,
        CheckValidationFilesPipe,
        CheckValidationSomeFilePipe,
        CheckStatusPipe,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule,
        BrowserAnimationsModule
      ],
      providers: [
        DataSourcesService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ type: 'Plan Indicativo y Plan de Acción' }))
          },
        },
      ],
    });
    fixture = TestBed.createComponent(DataLoadingComponent);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    dataSourcesService = TestBed.inject(DataSourcesService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});