import { ChangeDetectorRef, Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription, catchError, of, switchMap } from 'rxjs';
import { TooltipOptions } from 'primeng/api';
import { v4 as uuidv4 } from 'uuid';
import { ModalAlertService, MobileWidthService, SpinnerService } from '../../../../shared/services';
import { DataSourcesService } from '../../services/data-sources.service';
import { EditLoadingPeriodRequest, FileRule, FileUploaded, FileValidationRequest, GetLoadingPeriodResponse } from '../../interfaces';
import { InfoMessage, ModalAlert } from '../../../../shared/interfaces';
import { LoadingPeriodFrecuency } from '../../interfaces/loading-period-frecuency.interface';
import { fileRules } from '../../constants/file-rules';
import { FILE_STATUS, TIME_TYPE } from '../../enums';
import { MODAL_ALERT_TYPE } from '../../../../shared/enums/modal-alert-type.enum';
import { INFO_MESSAGE_TYPE } from 'src/app/shared/enums/info-message-type.enum';
import { determineExtencionByDataSource, determineRequestByDataSource, determineVerificationFile, validFileDetails, } from '../../utils';
import { slideInOutAnimation } from '../../../../shared/animations/animations';
import { executeCallbackLate } from 'src/utils/execute-callback-late';
import { getFirstAndLast, monthToNumber, numberToMonth, numberToWeekDay, weekDayToNumber } from '../../../../../utils/time-conversions';

@Component({
  templateUrl: './data-loading.component.html',
  styleUrls: ['./data-loading.component.scss'],
  animations: [slideInOutAnimation]
})
export class DataLoadingComponent implements OnInit, OnDestroy {

  private _activatedRoute = inject(ActivatedRoute);
  private _router = inject(Router);
  private _fb = inject(FormBuilder);
  private _modalAlertService = inject(ModalAlertService);
  private _mobileWidthService = inject(MobileWidthService);
  private _dataSourcesService = inject(DataSourcesService);
  private _spinnerService = inject(SpinnerService);
  private _cdr = inject(ChangeDetectorRef);

  private _paramsSubscription: Subscription = new Subscription();
  private _widthSubscription: Subscription = new Subscription();
  private _validFileSubscription: Subscription = new Subscription();
  private _loadFilesSubscription: Subscription = new Subscription();
  private _editLoadingPeriodSubscription: Subscription = new Subscription();

  public idDataLoading: number = 0;
  public eachTimeDataLoading: number = 0;
  public timeDataLoading: TIME_TYPE = TIME_TYPE.undefined;
  public weekDaysDataLoading?: string[];
  public monthDaysDataLoading?: number[];
  public yearMonthsDataLoading?: string[];
  public yearDaysDataLoading?: number[];

  public dataSourceId: string = '';
  public extensionsToAccept: string[] = [];
  public ruleUse?: FileRule;
  public files: FileUploaded[] = [];
  public isLoading: boolean = false;
  public isScreenSmall: boolean = false;
  public hideConfiguration: boolean = false;
  public configurationForm: FormGroup = this._fb.group({
    eachTime: ['', [
      Validators.required,
      Validators.maxLength(2),
      Validators.min(1)
    ]],
    time: [null, [
      Validators.required
    ]],
    weekDays: [[]],
    yearMonths: [[]],
    monthDays: [[]],
    yearDays: [[]],
  });
  public tooltipOptions: TooltipOptions = {
    tooltipPosition: 'top',
    tooltipEvent: 'hover',
    positionTop: -10,
    positionLeft: -50,
    tooltipLabel: 'Aún hay archivos por cargar',
    tooltipStyleClass: 'data-loading-load-button'
  }
  public infoMessage: InfoMessage = {
    message: 'Recuerde que el botón "Cargar" se habilitará al subir todos los archivos',
    type: INFO_MESSAGE_TYPE.warning
  }

  ngOnInit(): void {
    this._paramsSubscription = this.configurateToDataSource().subscribe(response => {
        const loadingPeriodFrecuency: LoadingPeriodFrecuency = {
          id: response.id,
          eachTime: response.eachtime,
          time: response.period,
          weekDays: response.week_days ? response.week_days.map(day => numberToWeekDay(day)) : [],
          monthDays: response.month_days ? getFirstAndLast(response.month_days) : [],
          yearMonths: response.year_months ? response.year_months.map(month => numberToMonth(month)) : [],
          yearDays: response.year_days ? getFirstAndLast(response.year_days) : [],
        }

        this.assignValueToConfigurationForm(loadingPeriodFrecuency);

        this.idDataLoading = loadingPeriodFrecuency.id!;
        this.eachTimeDataLoading = loadingPeriodFrecuency.eachTime;
        this.timeDataLoading = loadingPeriodFrecuency.time;
        this.weekDaysDataLoading = loadingPeriodFrecuency.weekDays;
        this.monthDaysDataLoading = loadingPeriodFrecuency.monthDays;
        this.yearMonthsDataLoading = loadingPeriodFrecuency.yearMonths;
        this.yearDaysDataLoading = loadingPeriodFrecuency.yearDays;
    });

    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
  }

  ngOnDestroy(): void {
    this._paramsSubscription.unsubscribe();
    this._widthSubscription.unsubscribe();
    this._validFileSubscription.unsubscribe();
    this._loadFilesSubscription.unsubscribe();
    this._editLoadingPeriodSubscription.unsubscribe();
    this.resetFiles();
    this.configurationForm.reset();
  }

  private assignValueToConfigurationForm(loadingPeriodFrecuency: LoadingPeriodFrecuency) {
    const { eachTime, time, weekDays, monthDays, yearMonths, yearDays } = loadingPeriodFrecuency;

    this.configurationForm.controls['eachTime'].setValue(eachTime);
    this.configurationForm.controls['time'].setValue(time);
    this.configurationForm.controls['weekDays'].setValue(weekDays);
    this.configurationForm.controls['monthDays'].setValue(monthDays);
    this.configurationForm.controls['yearMonths'].setValue(yearMonths);
    this.configurationForm.controls['yearDays'].setValue(yearDays);
  }

  private configurateToDataSource(): Observable<GetLoadingPeriodResponse> {
    return this._activatedRoute.paramMap.pipe(
      switchMap((params) => {
        this.dataSourceId = params.get('type') ?? '';
        this.extensionsToAccept = determineExtencionByDataSource(this.dataSourceId);

        this.ruleUse = fileRules.find(rule => rule.name === this.dataSourceId);

        if (!this.ruleUse) {
          this.files = [];
          return of();
        }

        this.infoMessage = {
          ...this.infoMessage,
          message: 'Recuerde que el botón "Cargar" se habilitará al subir todos los archivos',
        }

        this.hideConfiguration = false;

        this.files = this.ruleUse.files.map(file => ({
          id: uuidv4(),
          fileName: file,
          status: FILE_STATUS.pending,
          file: undefined
        }));

        return this._dataSourcesService.getLoadingPeriod(this.dataSourceId).pipe(
          catchError((error) => {
            this.showModalAlertError(error);
            return of();
          })
        )
      })
    )
  }

  private resetFiles() {
    this.files = this.files.map(file => {
      return {
        ...file,
        status: FILE_STATUS.pending,
        file: undefined
      }
    });
  }

  private replaceFile(newFile: FileUploaded, file: File) {
    const fileToReplace = this.files.find((file) => file.fileName === newFile.fileName);
    if (fileToReplace?.file) {
      this.showModalAlertError('El archivo ya ha sido verificado.')
      return;
    };

    this.files = this.files.map(currentFile => {
      if (currentFile.fileName === newFile.fileName) {
        return {
          ...currentFile,
          file: file,
          status: FILE_STATUS.isLoading
        }
      }
      return currentFile;
    });

    this._validFileSubscription = this.validFile(file).subscribe({
      next: (response) => {
        const details = validFileDetails(response);

        this.files = this.files.map(currentFile => {
          if (currentFile.fileName === newFile.fileName) {
            return {
              ...currentFile,
              file: file,
              status: response.valid && Object.values(details).every(({ valid }) => valid) ? FILE_STATUS.complete : FILE_STATUS.incomplete,
              details
            }
          }
          return currentFile;
        });
      },
      error: (error) => {
        this.showModalAlertError(error);
        this.files = this.files.map(file => {
          if (file.fileName === newFile.fileName) {
            return {
              ...file,
              file: undefined,
              status: FILE_STATUS.pending,
              details: undefined
            }
          }
          return file;
        })
      }
    })
  }

  private validFile(file: File) {
    const request: FileValidationRequest = {
      file
    };
    return this._dataSourcesService.validateFile(this.dataSourceId, request)
  }

  private showModalAlertError(message: string) {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message,
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private showModalAlertSuccess(title: string, message: string) {
    const successAlert: ModalAlert = {
      title,
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Aceptar',
        primary: true
      },
      message,
      highlightText: 'Haga click para continuar',
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  private reloadForm() {
    this.configurationForm.reset();

    executeCallbackLate(() => {
      const loadingPeriodFrecuency: LoadingPeriodFrecuency = {
        id: this.idDataLoading,
        eachTime: this.eachTimeDataLoading,
        time: this.timeDataLoading,
        weekDays: this.weekDaysDataLoading,
        monthDays: this.monthDaysDataLoading,
        yearMonths: this.yearMonthsDataLoading,
        yearDays: this.yearDaysDataLoading,
      }

      this.assignValueToConfigurationForm(loadingPeriodFrecuency);

      this._cdr.detectChanges();
    });
  }

  public showHideConfiguration() {
    this.hideConfiguration = !this.hideConfiguration;
    this.reloadForm();
  }

  public onGetFiles(files: File[]): void {
    if (!this.ruleUse) return;

    files.forEach(file => {
      const newFile: FileUploaded | string = determineVerificationFile(this.dataSourceId, file, this.files);

      if (typeof newFile === 'string') {
        this.showModalAlertError(newFile);
        return;
      }

      typeof newFile !== 'string' && this.replaceFile(newFile, file);
    });
  }

  public onRemoveFile(id: string): void {
    this.files = this.files.map(file => {
      if (file.id === id) {
        return {
          ...file,
          status: FILE_STATUS.pending,
          file: undefined
        }
      }
      return file;
    });
  }

  public onCancel() {
    const warinigAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this.resetFiles();
          this._router.navigateByUrl('/');
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
      },
      message: 'Si hace clic en “Si, cancelar” perderá toda la información diligenciada hasta el momento.',
      highlightText: `¿Está seguro que desea cancelar la carga de datos ${this.dataSourceId}?`,
    }

    this._modalAlertService.showModalAlert(warinigAlert);
  }

  public onCancelLoadingPeriod(continueAction: () => void) {
    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this.reloadForm();
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
        action: () => {
          continueAction();
        }
      },
      message: 'Si hace clic en “Si, cancelar” perderá toda la información diligenciada hasta el momento.',
      highlightText: `¿Está seguro que desea cancelar la edición de ${ this.dataSourceId }?`,
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }

  public onLoadFiles() {
    if (this.files.some(file => file.status !== FILE_STATUS.complete)) return;

    this.isLoading = true;

    const request = determineRequestByDataSource(this.dataSourceId, this.files);

    if (!request) { this.isLoading = false; return; }

    this._loadFilesSubscription = this._dataSourcesService.loadFiles(this.dataSourceId, request).subscribe({
      next: ({ failed_anexos }) => {
        this.isLoading = false;
        this.showModalAlertSuccess('Cargado con éxito', `La carga de datos ${this.dataSourceId} se ha realizado exitosamente.`);
        this.resetFiles();
        if (failed_anexos.length != 0) this.showModalAlertError(`No es posible realizar la carga ${failed_anexos} los archivos ya se encuentran cargados.`);
        this._validFileSubscription.unsubscribe();
      },
      error: (error) => {
        this.showModalAlertError(error);
        this.resetFiles();
        this.isLoading = false;
        this._validFileSubscription.unsubscribe();
      }
    })
  }

  public onEditLoadingPeriod() {
    this._spinnerService.showSpinner = true;

    const request: EditLoadingPeriodRequest = {
      id: this.idDataLoading,
      eachtime: this.configurationForm.controls['eachTime'].value,
      period: this.configurationForm.controls['time'].value,
      week_days: this.configurationForm.controls['weekDays'].value.map((day: string) => weekDayToNumber(day)),
      month_days: this.configurationForm.controls['monthDays'].value,
      year_months: this.configurationForm.controls['yearMonths'].value.map((month: string) => monthToNumber(month)),
      year_days: this.configurationForm.controls['yearDays'].value,
    }

    this._editLoadingPeriodSubscription = this._dataSourcesService.editLoadingPeriod(request).subscribe({
      next: (response) => {
        this._spinnerService.showSpinner = false;
        this.showModalAlertSuccess('Periodo de carga actualizado', `Se ha actualizado exitosamente el periodo de carga de ${ this.dataSourceId }`);
        
        this.eachTimeDataLoading = response.eachtime;
        this.timeDataLoading = response.period;
        this.weekDaysDataLoading = response.week_days?.map(day => numberToWeekDay(day));
        this.monthDaysDataLoading = response.month_days && getFirstAndLast(response.month_days);
        this.yearMonthsDataLoading = response.year_months?.map(month => numberToMonth(month));
        this.yearDaysDataLoading = response.year_days && getFirstAndLast(response.year_days);
      },
      error: (error) => {
        this._spinnerService.showSpinner = false;
        this.showModalAlertError(error);
      }
    })
  }

  public onCloseInfoMessage() { this.infoMessage = { ...this.infoMessage, message: '' } }
}
