import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DataLoadingComponent } from "./data-loading.component";
import { ActivatedRoute, Router, convertToParamMap } from "@angular/router";
import { DataSourcesService } from "../../services/data-sources.service";
import { DatagridFilesComponent } from "../../components/datagrid-files/datagrid-files.component";
import { CheckValidationFilesPipe } from "../../pipes/check-validation-files.pipe";
import { CheckValidationSomeFilePipe } from "../../pipes/check-validation-some-file.pipe";
import { FileDetailsComponent } from "../../components/file-details/file-details.component";
import { CheckStatusPipe } from "../../pipes/check-status.pipe";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { PrimeNgModule } from "src/app/prime-ng/prime-ng.module";
import { HttpRequestService } from "src/app/shared/services";
import { of, throwError } from "rxjs";
import { EditLoadingPeriodResponse, GetLoadingPeriodResponse } from "../../interfaces";
import { TIME_TYPE } from "../../enums";
import Swal from "sweetalert2";
import { SharedModule } from "src/app/shared/shared.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { numberToWeekDay } from "src/utils/time-conversions";


describe('DataLoadingComponent loading period', () => {
    let component: DataLoadingComponent;
    let fixture: ComponentFixture<DataLoadingComponent>;
    let router: Router;
    let route: ActivatedRoute;
    let dataSourcesService: DataSourcesService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [
                DataLoadingComponent,
                DatagridFilesComponent,
                FileDetailsComponent,
                CheckValidationFilesPipe,
                CheckValidationSomeFilePipe,
                CheckStatusPipe,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                SharedModule,
                PrimeNgModule,
                BrowserAnimationsModule
            ],
            providers: [
                DataSourcesService,
                HttpRequestService,
                {
                    provide: ActivatedRoute,
                    useValue: {
                        paramMap: of(convertToParamMap({ type: 'Proyección de Cupos' }))
                    },
                },
            ],
        });
        fixture = TestBed.createComponent(DataLoadingComponent);
        router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
        dataSourcesService = TestBed.inject(DataSourcesService);

        const getLoadingPeriodResponse: GetLoadingPeriodResponse = {
            eachtime: 1,
            period: TIME_TYPE.week,
            week_days: [0],
            data_source: 'SIMAT',
            id: 1,
            is_paused: false,
            notification_days: [0],
            notification_hour: '10:00:00'
        }

        spyOn(dataSourcesService, 'getLoadingPeriod').and.returnValue(of(getLoadingPeriodResponse));

        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should change hideConfiguration to true', () => {
        component.showHideConfiguration();
        expect(component.hideConfiguration).toBeTrue();
    });

    it('should show loading period modal alert when try to cancel and press confirm button', () => {
        const callBack = () => { };
        component.onCancelLoadingPeriod(callBack);

        expect(Swal.isVisible()).toBeTrue();

        const confirmButton = Swal.getConfirmButton()!;
        confirmButton.click();

        expect(confirmButton).toBeTruthy();
    });

    it('should show loading period modal alert when try to cancel and press cancel button', () => {
        const callBack = () => { };
        component.onCancelLoadingPeriod(callBack);

        expect(Swal.isVisible()).toBeTrue();

        const cancelButton = Swal.getCancelButton()!;
        cancelButton.click();

        expect(cancelButton).toBeTruthy();
    });

    it('should assing a new loading period when onEditLoadingPeriod is executed', () => {
        const request: EditLoadingPeriodResponse = {
            eachtime: 1,
            period: TIME_TYPE.week,
            week_days: [0],
            data_source: 'SIMAT',
            id: 1,
            is_paused: false,
            notification_days: [0],
            notification_hour: '10:00:00'
        }
        spyOn(dataSourcesService, 'editLoadingPeriod').and.returnValue(of(request));

        component.onEditLoadingPeriod();
        expect(component.eachTimeDataLoading).toEqual(request.eachtime);
        expect(component.timeDataLoading).toEqual(request.period);
        expect(component.weekDaysDataLoading).toEqual(request.week_days?.map(day => numberToWeekDay(day)));
    });

    it('should assing a new loading period when onEditLoadingPeriod for month is executed', () => {
        const response: EditLoadingPeriodResponse = {
            eachtime: 1,
            period: TIME_TYPE.month,
            data_source: 'SIMAT',
            id: 1,
            is_paused: false,
            month_days: [1,9],
            notification_days: [0],
            notification_hour: '10:00:00'
        }
        spyOn(dataSourcesService, 'editLoadingPeriod').and.returnValue(of(response));

        component.onEditLoadingPeriod();
        expect(component.eachTimeDataLoading).toEqual(response.eachtime);
        expect(component.timeDataLoading).toEqual(response.period);
        expect(component.monthDaysDataLoading).toEqual(response.month_days);
    });

    it('should assing a new loading period when onEditLoadingPeriod for year is executed', () => {
        const response: EditLoadingPeriodResponse = {
            eachtime: 1,
            period: TIME_TYPE.year,
            data_source: 'SIMAT',
            id: 1,
            is_paused: false,
            year_months: [1, 2, 3],
            year_days: [1, 4],
            notification_days: [0],
            notification_hour: '10:00:00'
        }
        spyOn(dataSourcesService, 'editLoadingPeriod').and.returnValue(of(response));

        component.configurationForm.controls['yearMonths'].setValue(['Enero', 'Febrero', 'Marzo']);

        component.onEditLoadingPeriod();
        expect(component.eachTimeDataLoading).toEqual(response.eachtime);
        expect(component.timeDataLoading).toEqual(response.period);
        expect(component.yearDaysDataLoading).toEqual(response.year_days);
    });

    it('should not assing a new loading period and show a modal error alert when onEditLoadingPeriod is executed with bad request', () => {
        spyOn(dataSourcesService, 'editLoadingPeriod').and.returnValue(throwError(() => { }));

        component.onEditLoadingPeriod();
        expect(Swal.isVisible()).toBeTrue();
    });
});

describe('DataLoadingComponent loading period for month', () => {
    let component: DataLoadingComponent;
    let fixture: ComponentFixture<DataLoadingComponent>;
    let router: Router;
    let route: ActivatedRoute;
    let dataSourcesService: DataSourcesService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [
                DataLoadingComponent,
                DatagridFilesComponent,
                FileDetailsComponent,
                CheckValidationFilesPipe,
                CheckValidationSomeFilePipe,
                CheckStatusPipe,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                SharedModule,
                PrimeNgModule,
                BrowserAnimationsModule
            ],
            providers: [
                DataSourcesService,
                HttpRequestService,
                {
                    provide: ActivatedRoute,
                    useValue: {
                        paramMap: of(convertToParamMap({ type: 'Proyección de Cupos' }))
                    },
                },
            ],
        });
        fixture = TestBed.createComponent(DataLoadingComponent);
        router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
        dataSourcesService = TestBed.inject(DataSourcesService);

        const getLoadingPeriodResponse: GetLoadingPeriodResponse = {
            eachtime: 1,
            period: TIME_TYPE.month,
            data_source: 'SIMAT',
            id: 1,
            is_paused: false,
            month_days: [1,2,3,4,5,6],
            notification_days: [0],
            notification_hour: '10:00:00'
        }

        spyOn(dataSourcesService, 'getLoadingPeriod').and.returnValue(of(getLoadingPeriodResponse));

        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should change hideConfiguration to true', () => {
        component.showHideConfiguration();
        expect(component.hideConfiguration).toBeTrue();
    });
});

describe('DataLoadingComponent loading period for year', () => {
    let component: DataLoadingComponent;
    let fixture: ComponentFixture<DataLoadingComponent>;
    let router: Router;
    let route: ActivatedRoute;
    let dataSourcesService: DataSourcesService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [
                DataLoadingComponent,
                DatagridFilesComponent,
                FileDetailsComponent,
                CheckValidationFilesPipe,
                CheckValidationSomeFilePipe,
                CheckStatusPipe,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                SharedModule,
                PrimeNgModule,
                BrowserAnimationsModule
            ],
            providers: [
                DataSourcesService,
                HttpRequestService,
                {
                    provide: ActivatedRoute,
                    useValue: {
                        paramMap: of(convertToParamMap({ type: 'Proyección de Cupos' }))
                    },
                },
            ],
        });
        fixture = TestBed.createComponent(DataLoadingComponent);
        router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
        dataSourcesService = TestBed.inject(DataSourcesService);

        const getLoadingPeriodResponse: GetLoadingPeriodResponse = {
            eachtime: 1,
            period: TIME_TYPE.month,
            data_source: 'SIMAT',
            id: 1,
            is_paused: false,
            year_months: [1,2,3],
            year_days: [1,2,3,4,5,6,7],
            notification_days: [0],
            notification_hour: '10:00:00'
        }

        spyOn(dataSourcesService, 'getLoadingPeriod').and.returnValue(of(getLoadingPeriodResponse));

        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should change hideConfiguration to true', () => {
        component.showHideConfiguration();
        expect(component.hideConfiguration).toBeTrue();
    });
});

describe('DataLoadingComponent loading period with bad request', () => {
    let component: DataLoadingComponent;
    let fixture: ComponentFixture<DataLoadingComponent>;
    let router: Router;
    let route: ActivatedRoute;
    let dataSourcesService: DataSourcesService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [
                DataLoadingComponent,
                DatagridFilesComponent,
                FileDetailsComponent,
                CheckValidationFilesPipe,
                CheckValidationSomeFilePipe,
                CheckStatusPipe,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                SharedModule,
                PrimeNgModule,
                BrowserAnimationsModule
            ],
            providers: [
                DataSourcesService,
                HttpRequestService,
                {
                    provide: ActivatedRoute,
                    useValue: {
                        paramMap: of(convertToParamMap({ type: 'Proyección de Cupos' }))
                    },
                },
            ],
        });
        fixture = TestBed.createComponent(DataLoadingComponent);
        router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
        dataSourcesService = TestBed.inject(DataSourcesService);

        spyOn(dataSourcesService, 'getLoadingPeriod').and.returnValue(throwError(() => { }));

        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});