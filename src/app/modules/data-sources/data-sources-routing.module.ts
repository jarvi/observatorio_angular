import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataLoadingComponent } from './pages/data-loading/data-loading.component';
import { dataOptionsCheckerGuard } from 'src/app/shared/guards/data-options-checker.guard';
import { DATA_ROUTES } from 'src/app/shared/enums/data-options-types';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: `${DATA_ROUTES.dataLoading}/:type`,
    component: DataLoadingComponent,
    canActivate: [dataOptionsCheckerGuard],
    data: {
      breadcrumb: 'Carga de datos',
      permissions: [
        PERMISSION_TYPE.addSource.name,
      ]
    }
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataSourcesRoutingModule { }
