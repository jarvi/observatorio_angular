import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { DataSourcesRoutingModule } from './data-sources-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { DataLoadingComponent } from './pages/data-loading/data-loading.component';
import { LoadingPeriodComponent } from './components/loading-period/loading-period.component';
import { DatagridFilesComponent } from './components/datagrid-files/datagrid-files.component';
import { FileDetailsComponent } from './components/file-details/file-details.component';
import { CheckStatusPipe } from './pipes/check-status.pipe';
import { CheckValidationFilesPipe } from './pipes/check-validation-files.pipe';
import { CheckValidationSomeFilePipe } from './pipes/check-validation-some-file.pipe';


@NgModule({
  declarations: [
    DataLoadingComponent,
    DatagridFilesComponent,
    FileDetailsComponent,
    LoadingPeriodComponent,
    CheckStatusPipe,
    CheckValidationFilesPipe,
    CheckValidationSomeFilePipe,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DataSourcesRoutingModule,
    
    SharedModule,
    PrimeNgModule
  ],
  exports: [
    CheckStatusPipe,
    CheckValidationFilesPipe,
    CheckValidationSomeFilePipe,
  ]
})
export class DataSourcesModule { }
