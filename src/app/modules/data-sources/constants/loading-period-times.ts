import { ChipButton } from "src/app/shared/interfaces/chip-button.interface";

export const weekDays: ChipButton[] = [
    { id: 1, name: 'Lunes', selected: false, blocked: false },
    { id: 2, name: 'Martes', selected: false, blocked: false },
    { id: 3, name: 'Miércoles', selected: false, blocked: false },
    { id: 4, name: 'Jueves', selected: false, blocked: false },
    { id: 5, name: 'Viernes', selected: false, blocked: false },
    { id: 6, name: 'Sábado', selected: false, blocked: false },
    { id: 7, name: 'Domingo', selected: false, blocked: false },
];
export const yearMonths: ChipButton[] = [
    { id: 1, name: 'Enero', selected: false, blocked: false },
    { id: 2, name: 'Febrero', selected: false, blocked: false },
    { id: 3, name: 'Marzo', selected: false, blocked: false },
    { id: 4, name: 'Abril', selected: false, blocked: false },
    { id: 5, name: 'Mayo', selected: false, blocked: false },
    { id: 6, name: 'Junio', selected: false, blocked: false },
    { id: 7, name: 'Julio', selected: false, blocked: false },
    { id: 8, name: 'Agosto', selected: false, blocked: false },
    { id: 9, name: 'Septiembre', selected: false, blocked: false },
    { id: 10, name: 'Octubre', selected: false, blocked: false },
    { id: 11, name: 'Noviembre', selected: false, blocked: false },
    { id: 12, name: 'Diciembre', selected: false, blocked: false },
];