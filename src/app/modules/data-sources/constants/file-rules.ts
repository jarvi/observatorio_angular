import { DATA_OPTIONS_TYPES } from "src/app/shared/enums/data-options-types";
import { FileRule } from "../interfaces/file-rule.interface";
import { 
    SIMAT_FILE, 
    DUE_FILE, 
    SABER_TESTS_FILE, 
    HUMAN_TALENT_FILE, 
    PAE_FILE,
    FUTURES_COMPUTERS_FILE,
    EDUCATIVE_INFRAESTRUCTURE_FILE,      
    INTERNAL_EFFICIENCY_FILE, 
    TECHNICAL_AVERANGE_FILE, 
    SCHOOL_ENVIRONMENT_PROTECTOR_FILE, 
    PROJECTION_OF_QUOTAS_FILE,    
    TRANSPORT_FILE, 
    DROPOUT_RATE_FILE,
    CONSOLIDATED_SIMAT_FILE,
    DETAILED_STRATEGIES_FILE,
    INDICATIVE_PLAN_AND_ACTION_PLAN_FILE
} from "../enums";

export const fileRules: FileRule[] = [
    { name: DATA_OPTIONS_TYPES.simat, files: [SIMAT_FILE.anexo5a, SIMAT_FILE.anexo5b, SIMAT_FILE.anexo5o, SIMAT_FILE.anexo6a, SIMAT_FILE.anexo6b, SIMAT_FILE.anexo6o] },
    { name: DATA_OPTIONS_TYPES.due, files: [DUE_FILE.due] },
    { name: DATA_OPTIONS_TYPES.saberTests, files: [SABER_TESTS_FILE.sb111, SABER_TESTS_FILE.sb112] },
    { name: DATA_OPTIONS_TYPES.humanTalent, files: [HUMAN_TALENT_FILE.fubDocDir, HUMAN_TALENT_FILE.semBaseDocentes, HUMAN_TALENT_FILE.baseDocentes] },
    { name: DATA_OPTIONS_TYPES.pae, files: [PAE_FILE.ocemPae] },
    { name: DATA_OPTIONS_TYPES.futuresComputers, files: [FUTURES_COMPUTERS_FILE.cruzan, FUTURES_COMPUTERS_FILE.computersDelivered] },
    { name: DATA_OPTIONS_TYPES.educativeInfraestructure, files: [EDUCATIVE_INFRAESTRUCTURE_FILE.matrix, EDUCATIVE_INFRAESTRUCTURE_FILE.sem, EDUCATIVE_INFRAESTRUCTURE_FILE.dotation] },
    { name: DATA_OPTIONS_TYPES.transport, files: [TRANSPORT_FILE.ocemProfile, TRANSPORT_FILE.ocem] },
    { name: DATA_OPTIONS_TYPES.internalEfficiency, files: [INTERNAL_EFFICIENCY_FILE.consolidated] },
    { name: DATA_OPTIONS_TYPES.technicalAverage, files: [TECHNICAL_AVERANGE_FILE.averange] },
    { name: DATA_OPTIONS_TYPES.schoolEnvironmentProtector, files: [SCHOOL_ENVIRONMENT_PROTECTOR_FILE.attentionReport, SCHOOL_ENVIRONMENT_PROTECTOR_FILE.actionsReport] },
    { name: DATA_OPTIONS_TYPES.projectionOfQuotas, files: [PROJECTION_OF_QUOTAS_FILE.quotasProjection] },
    { name: DATA_OPTIONS_TYPES.dropoutRate, files: [DROPOUT_RATE_FILE.semConsolidated] },
    { name: DATA_OPTIONS_TYPES.consolidatedSIMAT, files: [CONSOLIDATED_SIMAT_FILE.simatConsolidated] },
    { name: DATA_OPTIONS_TYPES.detailedStrategies, files: [DETAILED_STRATEGIES_FILE.detailedStrategies] },
    { name: DATA_OPTIONS_TYPES.indicativePlanAndActionPlan, files: [INDICATIVE_PLAN_AND_ACTION_PLAN_FILE.piPAIG] },
]