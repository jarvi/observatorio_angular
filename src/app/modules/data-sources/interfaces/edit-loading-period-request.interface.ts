import { TIME_TYPE } from "../enums/time.enum";

export interface EditLoadingPeriodRequest {
    id: number;
    eachtime: number;
    period: TIME_TYPE;
    week_days?: string[];
    year_months?: string[];
    month_days?: number[];
    year_days?: number[];
}