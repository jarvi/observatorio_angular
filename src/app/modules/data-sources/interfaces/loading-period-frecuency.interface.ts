import { TIME_TYPE } from "../enums";

export interface LoadingPeriodFrecuency {
    id?: number;
    eachTime: number;
    time: TIME_TYPE;
    weekDays?: string[];
    yearMonths?: string[];
    monthDays?: number[];
    yearDays?: number[];
}