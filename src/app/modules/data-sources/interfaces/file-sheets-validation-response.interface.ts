export interface FileSheetsValidationResponse {
    valid: boolean;
    sheets: Sheet[];
    characters: Characters;
    format: Format;
    size: Format;
    name: Format;
    date?: DateError;
}

interface Characters {
    status: boolean;
    detail: string;
    invalid_line: string;
}

interface Format {
    status: boolean;
    detail: string;
}

interface Sheet {
    name: string;
    columns: Columns;
    content: Content;
    rows: Rows;
}

interface Columns {
    status: boolean;
    detail: string;
    missing_columns: string[];
}

interface Content {
    status: boolean;
    detail: string;
    invalid_columns: any[];
}

interface Rows {
    total: number;
    empty: Format;
}

interface DateError {
    available: string;
    detail: string;
}
