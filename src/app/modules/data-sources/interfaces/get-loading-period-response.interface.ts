import { TIME_TYPE } from "../enums/time.enum";

export interface GetLoadingPeriodResponse {
    id: number;
    data_source: string;
    is_paused: boolean;
    eachtime: number;
    notification_hour: string;
    period: TIME_TYPE;
    week_days?: number[];
    month_days?: number[];
    year_days?: number[];
    year_months?: number[];
    notification_days: number[];
}