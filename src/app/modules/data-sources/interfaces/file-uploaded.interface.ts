export interface FileUploaded {
    id: string;
    fileName: string;
    status: string;
    file?: File;
    details?: FileDetails;
}

export interface FileDetails {
    characters: {
        valid: boolean;
        detail: string;
    },
    columns: {
        valid: boolean;
        detail: string;
    },
    content: {
        valid: boolean;
        detail: string;
    },
    empty: {
        valid: boolean;
        detail: string;
    },
    format: {
        valid: boolean;
        detail: string;
    },
    name: {
        valid: boolean;
        detail: string;
    },
    size: {
        valid: boolean;
        detail: string;
    },
    date: {
        valid: boolean;
        detail: string;
    }
}