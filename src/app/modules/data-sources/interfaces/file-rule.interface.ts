export interface FileRule {
    name: string;
    files: string[];
}