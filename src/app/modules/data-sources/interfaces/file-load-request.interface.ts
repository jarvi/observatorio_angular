export interface FilesSimatRequest {
    anexo5a: File;
    anexo5b: File;
    anexo5o: File;
    anexo6a: File;
    anexo6b: File;
    anexo6o: File;
}
export interface FilesDueRequest {
    due: File;
}
export interface FilesSaberTestsRequest {
    sb111: File;
    sb112: File;
}
export interface FilesHumanTalentRequest {
    fubDocDir: File;
    semBaseDocentes: File;
    baseDocentes: File;
}
export interface FilesPAERequest {    
    ocemPae: File;
}
export interface FilesFuturesComputersRequest {
    cruzan: File;
    computersDelivered: File;
}
export interface FilesEducativeInfraestructureRequest {
    matrix: File;
    sem: File;
    dotation: File;
}
export interface FilesInternalEfficiencyRequest {
    consolidated: File;
}
export interface FilesTechnicalAverageRequest {
    averange: File;
}
export interface FilesSchoolEnvironmentProtectorRequest {
    attentionReport: File;
    actionsReport: File;
}
export interface FilesProjectionOfQuotasRequest {
    quotasProjection: File;
}
export interface FilesTransportRequest {
    ocem: File;
    ocemProfile: File;
}
export interface FilesDetailedStrategiesRequest {
    detailed: File;
}
export interface FilesDropoutRateRequest {
    dropoutRate: File;
}
export interface FilesSIMATConsolidatedRequest {
    simatConsolidated: File;
}
export interface FilesIndicativePlanAndActionPlanRequest {
    indicativePlanAndActionPlan: File;
}