export interface FileValidationResponse {
    characters: DetailsFileLoadingCharacters;
    columns: DetailsFileLoadingColumns;
    content: DetailsFileLoadingContent;
    empty: DetailsFileLoading;
    format: DetailsFileLoading;
    name: DetailsFileLoading;
    rows: number;
    size: DetailsFileLoading;
    valid: boolean;
    date?: DateError;
}

interface DetailsFileLoading {
    status: boolean;
    detail: string;
}

interface DetailsFileLoadingCharacters extends DetailsFileLoading {
    invalid_line: string;
}

interface DetailsFileLoadingColumns extends DetailsFileLoading {
    missing_colums: string[];
}

interface DetailsFileLoadingContent extends DetailsFileLoading {
    invalid_columns: string[];
}

interface DateError {
    available: string;
    detail: string;
}
