import { TIME_TYPE } from "../enums/time.enum";

export interface TimeType {
    id: number;
    name: TIME_TYPE;
}