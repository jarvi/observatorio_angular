import { FILE_STATUS } from '../enums/file-status.enum';
import { CheckStatusPipe } from './check-status.pipe';

describe('CheckStatusPipe', () => {
  let pipe: CheckStatusPipe;

  beforeEach(() => {
    pipe = new CheckStatusPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should check the complet status and return the correct text', () => {
    const files = FILE_STATUS.complete;

    const resultText = pipe.transform(files);

    expect(resultText).toEqual('Cargado con éxito');
  });

  it('should check the incomplete status and return the correct text', () => {
    const files = FILE_STATUS.incomplete;

    const resultText = pipe.transform(files);

    expect(resultText).toEqual('Error al cargar el archivo');
  });

  it('should check the pending status and return the correct text', () => {
    const files = FILE_STATUS.pending;

    const resultText = pipe.transform(files);

    expect(resultText).toEqual('Archivo sin cargar');
  });

  it('should check the isLoading status and return the correct text', () => {
    const files = FILE_STATUS.isLoading;

    const resultText = pipe.transform(files);

    expect(resultText).toEqual('Cargando archivo...');
  });
});
