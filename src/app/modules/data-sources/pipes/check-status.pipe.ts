import { Pipe, PipeTransform } from '@angular/core';
import { FILE_STATUS } from '../enums/file-status.enum';

@Pipe({
  name: 'checkStatus'
})
export class CheckStatusPipe implements PipeTransform {

  transform(status: FILE_STATUS): string {
    switch (status) {
      case FILE_STATUS.complete:
        return 'Cargado con éxito';
      case FILE_STATUS.incomplete:
        return 'Error al cargar el archivo';
      case FILE_STATUS.pending:
        return 'Archivo sin cargar';
      case FILE_STATUS.isLoading:
        return 'Cargando archivo...';
    }
  }
}
