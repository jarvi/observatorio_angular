import { FILE_STATUS } from '../enums/file-status.enum';
import { CheckValidationFilesPipe } from './check-validation-files.pipe';

describe('CheckValidationFilesPipe', () => {
  let pipe: CheckValidationFilesPipe;

  beforeEach(() => {
    pipe = new CheckValidationFilesPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should check if all file has the correct status and return true', () => {
    const files = [{
      id: '1',
      fileName: 'TEST 1',
      status: FILE_STATUS.complete,
      file: new File([''], 'test.csv'),
    }];

    const result = pipe.transform(files);

    expect(!result).toBeTrue();
  });
});
