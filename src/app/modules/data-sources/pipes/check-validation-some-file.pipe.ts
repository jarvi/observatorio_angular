import { Pipe, PipeTransform } from '@angular/core';
import { FileUploaded } from '../interfaces/file-uploaded.interface';
import { FILE_STATUS } from '../enums/file-status.enum';

@Pipe({
  name: 'checkValidationSomeFile'
})
export class CheckValidationSomeFilePipe implements PipeTransform {

  transform(files: FileUploaded[]): boolean {
    return !(files.length > 0 && files.every(file => file.status === FILE_STATUS.pending || file.status === FILE_STATUS.isLoading));
  }
}
