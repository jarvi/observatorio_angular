import { FILE_STATUS } from '../enums/file-status.enum';
import { CheckValidationSomeFilePipe } from './check-validation-some-file.pipe';

describe('CheckValidationSomeFilePipe', () => {
  let pipe: CheckValidationSomeFilePipe;

  beforeEach(() => {
    pipe = new CheckValidationSomeFilePipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should check if some files has the correct status and return true', () => {
    const files = [{
      id: '1',
      fileName: 'TEST 1',
      status: FILE_STATUS.complete,
      file: new File([''], 'test.csv'),
    }];

    const result = pipe.transform(files);

    expect(result).toBeTrue();
  });
});
