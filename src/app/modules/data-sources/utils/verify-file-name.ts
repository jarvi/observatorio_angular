import { FileUploaded } from "../interfaces/file-uploaded.interface";

const removeExtencion = (texto: string) => {
    const lastPointPosition = texto.lastIndexOf('.');
    if (lastPointPosition !== -1) {
        return texto.slice(0, lastPointPosition);
    } else {
        return texto;
    }
}

export const verifyFileNameSIMAT = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const anexe = 'ANEXO';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containAnexe = fileNameMin.includes(anexe.toLowerCase());
    if (!containAnexe) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => {
        if(!file.fileName) return `El archivo ${fileName} no es válido`;
        
        const name = file.fileName.toUpperCase().split(`${anexe} `).pop()!;
        return fileNameMin.includes(name.toLowerCase())
    })
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameDUE = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const due = 'DUE';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containDue = fileNameMin.includes(due.toLowerCase());

    if (!containDue) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === due);
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameSaberTest = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const sb11 = 'SB11';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containSb11 = fileNameMin.includes(sb11.toLowerCase());
    if (!containSb11) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => {
        if(!file.fileName) return `El archivo ${fileName} no es válido`;
        
        const name = file.fileName.toUpperCase().split(`${sb11} `).pop()!;
        
        return fileNameMin.includes(name.toLowerCase())
    })
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameHumanTalent = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const fubDocDir = 'FUB_DOC y DIR';
    const semBaseDocentes = 'SEM_BASE_DOCENTES';
    const baseDocentes = 'BASE_DOCENTES';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containRules = fileNameMin.includes(fubDocDir.toLowerCase()) || fileNameMin.includes(semBaseDocentes.toLowerCase()) || fileNameMin.includes(baseDocentes.toLowerCase());
    if (!containRules) return `El archivo ${fileName} no es válido`;
    
    const fileFound: FileUploaded | undefined = files.find(file => {
        let name = undefined;
        
        if (file.fileName.toUpperCase() === fubDocDir.toUpperCase()) { name = fubDocDir }
        if (file.fileName.toUpperCase() === semBaseDocentes.toUpperCase()) { name = semBaseDocentes }
        if (file.fileName.toUpperCase() === baseDocentes.toUpperCase()) { name = baseDocentes }

        return name ? fileNameMin.includes(name.toLowerCase()): false;
    });
    
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNamePAE = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const ocemPae = 'Informes OCEM PAE';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containRules = fileNameMin.includes(ocemPae.toLowerCase());
    if (!containRules) return `El archivo ${fileName} no es válido`;
    
    const fileFound: FileUploaded | undefined = files.find(file => {
        let name = null;
        
        if (file.fileName.toUpperCase() === ocemPae.toUpperCase()) { name = ocemPae }

        return name ? fileNameMin.includes(name.toLowerCase()): false;
    });
    
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameFuturesComputers = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const cruzan = 'Cruzan';
    const computersDelivered = 'Computadores entregados docentes y PP';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containRules = fileNameMin.includes(cruzan.toLowerCase()) || fileNameMin.includes(computersDelivered.toLowerCase());
    if (!containRules) return `El archivo ${fileName} no es válido`;
    
    const fileFound: FileUploaded | undefined = files.find(file => {
        let name = null;

        if (file.fileName.toUpperCase() === cruzan.toUpperCase()) { name = cruzan }
        if (file.fileName.toUpperCase() === computersDelivered.toUpperCase()) { name = computersDelivered }

        return name ? fileNameMin.includes(name.toLowerCase()): false;
    });
    
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameEducativeInfraestructure = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const matrix = 'Matriz Proyectos estratégicos';
    const dotation = 'INSTITUCIONES_EDUCATIVAS_CON_DOTACION';
    const sem = 'SEM_INFRAESTRUCTURA_IEs';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containRules = fileNameMin.includes(matrix.toLowerCase()) || fileNameMin.includes(dotation.toLowerCase()) || fileNameMin.includes(sem.toLowerCase());
    if (!containRules) return `El archivo ${fileName} no es válido`;
    
    const fileFound: FileUploaded | undefined = files.find(file => {
        let name = null;

        if (file.fileName.toUpperCase() === matrix.toUpperCase()) { name = matrix }
        if (file.fileName.toUpperCase() === dotation.toUpperCase()) { name = dotation }
        if (file.fileName.toUpperCase() === sem.toUpperCase()) { name = sem }

        return name ? fileNameMin.includes(name.toLowerCase()): false;
    });

    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameInternalEfficiency = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const consolidated = 'Consolidado de Eficiencia Interna';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containConsolidated = fileNameMin.includes(consolidated.toLowerCase());
    
    if (!containConsolidated) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === consolidated.toUpperCase());
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameTechnicalAverage = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const technicalAverange = 'SEM_Media_tecnica';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containTechnicalAverange = fileNameMin.includes(technicalAverange.toLowerCase());
    
    if (!containTechnicalAverange) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === technicalAverange.toUpperCase());
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameSchoolEnvironmentProtector = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const attentionReport = 'Reporte de atenciones';
    const actionsReport = 'Reporte acciones';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containRules = fileNameMin.includes(attentionReport.toLowerCase()) || fileNameMin.includes(actionsReport.toLowerCase());
    if (!containRules) return `El archivo ${fileName} no es válido`;
    
    const fileFound: FileUploaded | undefined = files.find(file => {
        let name = undefined;
        
        if (file.fileName.toUpperCase() === actionsReport.toUpperCase()) { name = actionsReport }
        if (file.fileName.toUpperCase() === attentionReport.toUpperCase()) { name = attentionReport }

        return name ? fileNameMin.includes(name.toLowerCase()): false;
    });
    
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameProjectionOfQuotas = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const quotasProjection = 'Proyeccion de cupos';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containQuotasProjection = fileNameMin.includes(quotasProjection.toLowerCase());
    
    if (!containQuotasProjection) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === quotasProjection.toUpperCase());
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameTransport = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const ocem = 'Informes para OCEM - TEsco';
    const ocemProfile = 'Informes para OCEM - TEsco Perfil Metro';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containRules = fileNameMin.includes(ocem.toLowerCase()) || fileNameMin.includes(ocemProfile.toLowerCase());
    if (!containRules) return `El archivo ${fileName} no es válido`;
    
    const fileFound: FileUploaded | undefined = files.find(file => {
        let name = undefined;
        
        if (file.fileName.toUpperCase() === ocemProfile.toUpperCase()) { name = ocemProfile }
        if (file.fileName.toUpperCase() === ocem.toUpperCase()) { name = ocem }

        return name ? fileNameMin.includes(name.toLowerCase()): false;
    });
    
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);
    
    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameDropoutRate = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const semConsolidated = 'SEM_CONSOLIDADO_DESERCION_REPITENCIA';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containSEMConsolidated = fileNameMin.includes(semConsolidated.toLowerCase());
    
    if (!containSEMConsolidated) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === semConsolidated.toUpperCase());
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);

    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameConsolidatedSIMAT = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const simatConsolidated = 'Consolidado SIMAT';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containSIMATConsolidated = fileNameMin.includes(simatConsolidated.toLowerCase());
    
    if (!containSIMATConsolidated) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === simatConsolidated.toUpperCase());
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);

    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameDetailedStrategies = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const detailedStrategies = 'Detallado_estrategias';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containDetailedStrategies = fileNameMin.includes(detailedStrategies.toLowerCase());
    
    if (!containDetailedStrategies) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === detailedStrategies.toUpperCase());
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);

    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}

export const verifyFileNameIndicativePlanAndActionPlan = (fileName: string, files: FileUploaded[]): FileUploaded | string => {
    const piPAIG = 'PI PA IG';
    const fileNameClean = removeExtencion(fileName);
    const fileNameMin = fileNameClean.toLowerCase();
    const containPIPAIG = fileNameMin.includes(piPAIG.toLowerCase());
    
    if (!containPIPAIG) return `El archivo ${fileName} no es válido`;

    const fileFound: FileUploaded | undefined = files.find(file => file.fileName.toUpperCase() === piPAIG.toUpperCase());
    if (!fileFound) return `El archivo ${fileName} no es válido`;

    const pattern = /\d{8,}/;
    const stringDate = pattern.exec(fileNameMin);

    if (!stringDate) return `El archivo ${fileName} no es válido`;

    return fileFound;
}