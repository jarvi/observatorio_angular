import { DATA_OPTIONS_TYPES } from "src/app/shared/enums/data-options-types";
import { FileUploaded } from "../interfaces/file-uploaded.interface";
import { verifyFileNameDUE, verifyFileNamePAE, verifyFileNameEducativeInfraestructure, verifyFileNameFuturesComputers, verifyFileNameInternalEfficiency, verifyFileNameProjectionOfQuotas, verifyFileNameSIMAT, verifyFileNameSaberTest, verifyFileNameSchoolEnvironmentProtector, verifyFileNameTechnicalAverage, verifyFileNameTransport, verifyFileNameHumanTalent, verifyFileNameDropoutRate, verifyFileNameConsolidatedSIMAT, verifyFileNameDetailedStrategies, verifyFileNameIndicativePlanAndActionPlan } from "./verify-file-name";

export const determineVerificationFile = (dataSourceId: string, file: File, files: FileUploaded[]) => {
    let newFile: FileUploaded | string = '';

    switch (dataSourceId) {
        case DATA_OPTIONS_TYPES.simat:
            newFile = verifyFileNameSIMAT(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.due:
            newFile = verifyFileNameDUE(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.saberTests:
            newFile = verifyFileNameSaberTest(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.futuresComputers:
            newFile = verifyFileNameFuturesComputers(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.educativeInfraestructure:
            newFile = verifyFileNameEducativeInfraestructure(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.transport:
            newFile = verifyFileNameTransport(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.internalEfficiency:
            newFile = verifyFileNameInternalEfficiency(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.technicalAverage:
            newFile = verifyFileNameTechnicalAverage(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.schoolEnvironmentProtector:
            newFile = verifyFileNameSchoolEnvironmentProtector(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.projectionOfQuotas:
            newFile = verifyFileNameProjectionOfQuotas(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.humanTalent:
            newFile = verifyFileNameHumanTalent(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.pae:
            newFile = verifyFileNamePAE(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.dropoutRate:
            newFile = verifyFileNameDropoutRate(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.consolidatedSIMAT:
            newFile = verifyFileNameConsolidatedSIMAT(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.detailedStrategies:
            newFile = verifyFileNameDetailedStrategies(file.name, files);
            break;
        case DATA_OPTIONS_TYPES.indicativePlanAndActionPlan:
            newFile = verifyFileNameIndicativePlanAndActionPlan(file.name, files);
            break;
    }

    return newFile;
}