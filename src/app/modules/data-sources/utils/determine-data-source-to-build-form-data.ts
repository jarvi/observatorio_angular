import { DATA_OPTIONS_TYPES } from "src/app/shared/enums/data-options-types";
import { FilesDueRequest, FilesEducativeInfraestructureRequest, FilesFuturesComputersRequest, FilesInternalEfficiencyRequest, FilesProjectionOfQuotasRequest, FilesHumanTalentRequest, FilesPAERequest, FilesSaberTestsRequest, FilesSchoolEnvironmentProtectorRequest, FilesSimatRequest, FilesTechnicalAverageRequest, FilesTransportRequest, FilesDetailedStrategiesRequest, FilesDropoutRateRequest, FilesSIMATConsolidatedRequest, FilesIndicativePlanAndActionPlanRequest } from "../interfaces/file-load-request.interface";

export const determineDataSourceToBuildFormData = <T>(id: string, files: T): FormData => {
    const formData = new FormData();

    switch (id) {
        case DATA_OPTIONS_TYPES.simat:
            const simatFiles = files as FilesSimatRequest;
            formData.append('anexo5a', simatFiles.anexo5a);
            formData.append('anexo5b', simatFiles.anexo5b);
            formData.append('anexo5o', simatFiles.anexo5o);
            formData.append('anexo6a', simatFiles.anexo6a);
            formData.append('anexo6b', simatFiles.anexo6b);
            formData.append('anexo6o', simatFiles.anexo6o);
            break;
        case DATA_OPTIONS_TYPES.due:
            const dueFiles = files as FilesDueRequest;
            formData.append('due', dueFiles.due)
            break;
        case DATA_OPTIONS_TYPES.saberTests:
            const saberTestsFiles = files as FilesSaberTestsRequest;
            formData.append('sb111', saberTestsFiles.sb111);
            formData.append('sb112', saberTestsFiles.sb112);
            break;
        case DATA_OPTIONS_TYPES.humanTalent:
            const humanTalentFiles = files as FilesHumanTalentRequest;
            formData.append('fub_doc_dir', humanTalentFiles.fubDocDir);
            formData.append('sem_base_docentes', humanTalentFiles.semBaseDocentes);
            formData.append('base_docentes', humanTalentFiles.baseDocentes);
            break;
        case DATA_OPTIONS_TYPES.pae:
            const paeFiles = files as FilesPAERequest;
            formData.append('file', paeFiles.ocemPae);
            break;
        case DATA_OPTIONS_TYPES.futuresComputers:
            const futuresComputersFiles = files as FilesFuturesComputersRequest;
            formData.append('Cruzan', futuresComputersFiles.cruzan);
            formData.append('Computadores_entregados_docentes_y_PP', futuresComputersFiles.computersDelivered);
            break;
        case DATA_OPTIONS_TYPES.educativeInfraestructure:
            const educativeInfraestructureFiles = files as FilesEducativeInfraestructureRequest;
            formData.append('matriz_proyectos_estrategicos', educativeInfraestructureFiles.matrix);
            formData.append('sem_infraestructura_ies', educativeInfraestructureFiles.sem);
            formData.append('instituciones_educativas_con_dotacion', educativeInfraestructureFiles.dotation);
            break;
        case DATA_OPTIONS_TYPES.internalEfficiency:
            const internalEfficiencyFiles = files as FilesInternalEfficiencyRequest;
            formData.append('file', internalEfficiencyFiles.consolidated);
            break;
        case DATA_OPTIONS_TYPES.technicalAverage:
            const technicalAverageFiles = files as FilesTechnicalAverageRequest;
            formData.append('media_tecnica', technicalAverageFiles.averange);
            break;
        case DATA_OPTIONS_TYPES.schoolEnvironmentProtector:
            const schoolEnvironmentProtectorFiles = files as FilesSchoolEnvironmentProtectorRequest;
            formData.append('file_1', schoolEnvironmentProtectorFiles.actionsReport);
            formData.append('file_2', schoolEnvironmentProtectorFiles.attentionReport);
            break;
        case DATA_OPTIONS_TYPES.projectionOfQuotas:
            const projectionOfQuotasFiles = files as FilesProjectionOfQuotasRequest;
            formData.append('proyeccion_cupos', projectionOfQuotasFiles.quotasProjection);
            break;
        case DATA_OPTIONS_TYPES.transport:
            const transportFiles = files as FilesTransportRequest;
            formData.append('informes_para_ocem_tesco', transportFiles.ocem);
            formData.append('informes_para_ocem_tesco_metro', transportFiles.ocemProfile);
            break;
        case DATA_OPTIONS_TYPES.detailedStrategies:
            const detailedStrategiesFiles = files as FilesDetailedStrategiesRequest;
            formData.append('detallado_estrategias', detailedStrategiesFiles.detailed);
            break;
        case DATA_OPTIONS_TYPES.dropoutRate:
            const dropoutRateFiles = files as FilesDropoutRateRequest;
            formData.append('desercion_repitencia', dropoutRateFiles.dropoutRate);
            break;
        case DATA_OPTIONS_TYPES.consolidatedSIMAT:
            const simatConsolidated = files as FilesSIMATConsolidatedRequest;
            formData.append('consolidado_simat', simatConsolidated.simatConsolidated);
            break;
        case DATA_OPTIONS_TYPES.indicativePlanAndActionPlan:
            const indicativePlanAndActionPlan = files as FilesIndicativePlanAndActionPlanRequest;
            formData.append('file', indicativePlanAndActionPlan.indicativePlanAndActionPlan);
            break;
    }

    return formData;
}

export const determineDataSourceToBuildVerifyFormData = (id: string, file: File): FormData => {
    const formData = new FormData();

    if (id === DATA_OPTIONS_TYPES.due) {
        formData.append('due', file);
    } else {
        formData.append('file', file);
    }

    return formData;
}
