import { DATA_OPTIONS_TYPES } from "src/app/shared/enums/data-options-types";

export const determineNewDataSourceByDataSourceId = (dataSourceId: string): string => {
    let dataSource = dataSourceId;

    switch (dataSourceId) {
        case DATA_OPTIONS_TYPES.saberTests:
            dataSource = 'prueba_saber';
            break;
        case DATA_OPTIONS_TYPES.futuresComputers:
            dataSource = 'computadores_futuro';
            break;
        case DATA_OPTIONS_TYPES.educativeInfraestructure:
            dataSource = 'infraestructura';
            break;
        case DATA_OPTIONS_TYPES.internalEfficiency:
            dataSource = 'eficiencia_interna';
            break;
        case DATA_OPTIONS_TYPES.technicalAverage:
            dataSource = 'media_tecnica';
            break;
        case DATA_OPTIONS_TYPES.schoolEnvironmentProtector:
            dataSource = 'entorno_escolar_protector';
            break;
        case DATA_OPTIONS_TYPES.projectionOfQuotas:
            dataSource = 'proyeccion_cupos';
            break;
        case DATA_OPTIONS_TYPES.humanTalent:
            dataSource = 'talento_humano';
            break;
        case DATA_OPTIONS_TYPES.pae:
            dataSource = 'pae';
            break;
        case DATA_OPTIONS_TYPES.dropoutRate:
            dataSource = 'desercion_repitencia';
            break;
        case DATA_OPTIONS_TYPES.consolidatedSIMAT:
            dataSource = 'consolidado_simat';
            break;
        case DATA_OPTIONS_TYPES.detailedStrategies:
            dataSource = 'detallado_estrategias';
            break;
        case DATA_OPTIONS_TYPES.indicativePlanAndActionPlan:
            dataSource = 'plan_indicativo_accion';
            break;
    }

    return dataSource;
}