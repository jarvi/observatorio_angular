import { DATA_OPTIONS_TYPES } from "src/app/shared/enums/data-options-types";
import { FileUploaded } from "../interfaces/file-uploaded.interface";
import { DUE_FILE, EDUCATIVE_INFRAESTRUCTURE_FILE, FUTURES_COMPUTERS_FILE, INTERNAL_EFFICIENCY_FILE, PROJECTION_OF_QUOTAS_FILE, HUMAN_TALENT_FILE, SABER_TESTS_FILE, SCHOOL_ENVIRONMENT_PROTECTOR_FILE, SIMAT_FILE, TECHNICAL_AVERANGE_FILE, TRANSPORT_FILE, PAE_FILE, DETAILED_STRATEGIES_FILE, DROPOUT_RATE_FILE, CONSOLIDATED_SIMAT_FILE, INDICATIVE_PLAN_AND_ACTION_PLAN_FILE } from "../enums";

export const determineRequestByDataSource = (dataSourceId: string, files: FileUploaded[]) => {
    let request;

    switch (dataSourceId) {
        case DATA_OPTIONS_TYPES.simat:
            request = {
                anexo5a: files.find(file => file.fileName === SIMAT_FILE.anexo5a)!.file!,
                anexo5b: files.find(file => file.fileName === SIMAT_FILE.anexo5b)!.file!,
                anexo5o: files.find(file => file.fileName === SIMAT_FILE.anexo5o)!.file!,
                anexo6a: files.find(file => file.fileName === SIMAT_FILE.anexo6a)!.file!,
                anexo6b: files.find(file => file.fileName === SIMAT_FILE.anexo6b)!.file!,
                anexo6o: files.find(file => file.fileName === SIMAT_FILE.anexo6o)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.due:
            request = {
                due: files.find(file => file.fileName === DUE_FILE.due)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.saberTests:
            request = {
                sb111: files.find(file => file.fileName === SABER_TESTS_FILE.sb111)!.file!,
                sb112: files.find(file => file.fileName === SABER_TESTS_FILE.sb112)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.humanTalent:
            request = {
                fubDocDir: files.find(file => file.fileName === HUMAN_TALENT_FILE.fubDocDir)!.file!,
                semBaseDocentes: files.find(file => file.fileName === HUMAN_TALENT_FILE.semBaseDocentes)!.file!,
                baseDocentes: files.find(file => file.fileName === HUMAN_TALENT_FILE.baseDocentes)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.pae:
            request = {
                ocemPae: files.find(file => file.fileName === PAE_FILE.ocemPae)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.futuresComputers:
            request = {
                cruzan: files.find(file => file.fileName === FUTURES_COMPUTERS_FILE.cruzan)!.file!,
                computersDelivered: files.find(file => file.fileName === FUTURES_COMPUTERS_FILE.computersDelivered)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.educativeInfraestructure:
            request = {
                matrix: files.find(file => file.fileName === EDUCATIVE_INFRAESTRUCTURE_FILE.matrix)!.file!,
                sem: files.find(file => file.fileName === EDUCATIVE_INFRAESTRUCTURE_FILE.sem)!.file!,
                dotation: files.find(file => file.fileName === EDUCATIVE_INFRAESTRUCTURE_FILE.dotation)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.internalEfficiency:
            request = {
                consolidated: files.find(file => file.fileName === INTERNAL_EFFICIENCY_FILE.consolidated)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.technicalAverage:
            request = {
                averange: files.find(file => file.fileName === TECHNICAL_AVERANGE_FILE.averange)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.schoolEnvironmentProtector:
            request = {
                actionsReport: files.find(file => file.fileName === SCHOOL_ENVIRONMENT_PROTECTOR_FILE.actionsReport)!.file!,
                attentionReport: files.find(file => file.fileName === SCHOOL_ENVIRONMENT_PROTECTOR_FILE.attentionReport)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.projectionOfQuotas:
            request = {
                quotasProjection: files.find(file => file.fileName === PROJECTION_OF_QUOTAS_FILE.quotasProjection)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.transport:
            request = {
                ocem: files.find(file => file.fileName === TRANSPORT_FILE.ocem)!.file!,
                ocemProfile: files.find(file => file.fileName === TRANSPORT_FILE.ocemProfile)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.detailedStrategies:
            request = {
                detailed: files.find(file => file.fileName === DETAILED_STRATEGIES_FILE.detailedStrategies)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.dropoutRate:
            request = {
                dropoutRate: files.find(file => file.fileName === DROPOUT_RATE_FILE.semConsolidated)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.consolidatedSIMAT:
            request = {
                simatConsolidated: files.find(file => file.fileName === CONSOLIDATED_SIMAT_FILE.simatConsolidated)!.file!,
            }
            break;
        case DATA_OPTIONS_TYPES.indicativePlanAndActionPlan:
            request = {
                indicativePlanAndActionPlan: files.find(file => file.fileName === INDICATIVE_PLAN_AND_ACTION_PLAN_FILE.piPAIG)!.file!,
            }
            break;
    }
    return request;
}