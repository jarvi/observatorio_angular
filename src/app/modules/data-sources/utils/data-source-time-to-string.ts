import { TIME_TYPE } from "src/app/modules/data-sources/enums";
import { JoinArrayByPipe } from "src/app/shared/pipes/join-array-by.pipe";
import { LoadingPeriodFrecuency } from "../interfaces/loading-period-frecuency.interface";
import { NotificationTime } from "../../configure-notifications/interfaces/data-source-notification.interface";

export const dataSourceTimeToString = (loadingPeriodFrecuency: LoadingPeriodFrecuency, notificationTime?: NotificationTime) => {
    if (!loadingPeriodFrecuency.time || loadingPeriodFrecuency.time === TIME_TYPE.undefined) return TIME_TYPE.undefined;

    const { eachTime, time } = loadingPeriodFrecuency;

    const eachTimeText = eachTime > 1 ? `${eachTime}` : '';
    const timeText = `${pluralizeTime(time, eachTime)}, `;

    return `Cada ${eachTimeText} ${timeText} ${getTimeText(loadingPeriodFrecuency, notificationTime)}`.trim().replace(/\s+/g, ' ');
}

const getTimeText = (loadingPeriodFrecuency: LoadingPeriodFrecuency, notificationTime?: NotificationTime) => {
    const joinArrayByPipe = new JoinArrayByPipe();

    const { weekDays, monthDays, yearDays, yearMonths } = loadingPeriodFrecuency;

    if (notificationTime !== undefined) {
        return getNotificationTimeText(notificationTime, joinArrayByPipe, weekDays, monthDays, yearMonths, yearDays);
    }

    return getFrecuencyText(joinArrayByPipe, weekDays, monthDays, yearMonths, yearDays);
}

const getNotificationTimeText = (notificationTime: NotificationTime, joinArrayByPipe: JoinArrayByPipe, weekDays?: string[], monthDays?: number[], yearMonths?: string[], yearDays?: number[]) => {
    const weekDaysText = weekDays && weekDays.length > 0 ? `${joinArrayByPipe.transform(notificationTime.weekDays!, ' - ')}` : '';
    const monthDaysText = monthDays?.length ? `${rangeDays(notificationTime.monthDays!)}` : '';
    const yearMonthsText = yearMonths?.length ? `${joinArrayByPipe.transform(yearMonths, ' - ')}` : '';
    const yearDaysText = yearDays?.length ? `${rangeDays(notificationTime.monthDays!)}` : '';

    return `${weekDaysText} ${monthDaysText} ${yearMonthsText} ${yearDaysText}`.trim().replace(/\s+/g, ' ');
}

const getFrecuencyText = (joinArrayByPipe: JoinArrayByPipe, weekDays?: string[], monthDays?: number[], yearMonths?: string[], yearDays?: number[]) => {
    const weekDaysText = weekDays ? `${joinArrayByPipe.transform(weekDays, ' - ')}` : '';
    const monthDaysText = monthDays ? `${rangeDays(monthDays)}` : '';
    const yearMonthsText = yearMonths ? `${joinArrayByPipe.transform(yearMonths, ' - ')}` : '';
    const yearDaysText = yearDays ? `${rangeDays(yearDays)}` : '';

    return `${weekDaysText} ${monthDaysText} ${yearMonthsText} ${yearDaysText}`.trim().replace(/\s+/g, ' ');
}

const pluralizeTime = (time: TIME_TYPE, quantity: number): string => {
    let newTime = '';

    switch (time) {
        case TIME_TYPE.week:
            newTime = quantity > 1 ? 'Semanas' : TIME_TYPE.week;
            break;
        case TIME_TYPE.month:
            newTime = quantity > 1 ? 'Meses' : TIME_TYPE.month;
            break;
        case TIME_TYPE.year:
            newTime = quantity > 1 ? 'Años' : TIME_TYPE.year;
            break;
    }

    return newTime.toLowerCase();
}

const rangeDays = (range: number[]): string => {
    if (!range || range.length === 0) return '';

    if (range.length === 1) return `día ${range[0]}`;
    if (range[0] === range[1]) return `día ${range[0]}`;

    return `del día ${range[0]} al día ${range[1]}`;
}