import { FileSheetsValidationResponse } from "../interfaces/file-sheets-validation-response.interface";
import { FileDetails } from "../interfaces/file-uploaded.interface";
import { FileValidationResponse } from "../interfaces/file-validation-response.interface";

const verifyColumnsSheetsStatus = (response: FileValidationResponse | FileSheetsValidationResponse): boolean => {
    if ('columns' in response) {
        return response.columns.status;
    }

    return response.sheets.length > 0 ? response.sheets.every(({ columns }) => columns.status) : false;
}

const verifyColumnsSheetsDetail = (response: FileValidationResponse | FileSheetsValidationResponse): string => {
    if ('columns' in response) {
        return response.columns.status ? 'Columnas válidas' : response.columns.detail;
    }

    if (response.sheets.length > 0 && response.sheets.every(({ columns }) => columns.status)) return 'Columnas válidas';

    const finalText = response.sheets
        .filter(sheet => sheet.columns.detail !== '')
        .map(sheet => `Hoja ${sheet.name}: ${sheet.columns.detail} ${sheet.columns.missing_columns.join(', ')}`)
        .join('\n');

    return finalText;
}

const verifyContentSheetsStatus = (response: FileValidationResponse | FileSheetsValidationResponse): boolean => {
    if ('content' in response) {
        return response.content.status;
    }

    return response.sheets.length > 0 ? response.sheets.every(({ content }) => content.status) : false;
}

const verifyContentSheetsDetail = (response: FileValidationResponse | FileSheetsValidationResponse): string => {
    if ('content' in response) {
        return response.content.status ? 'Contenido válido' : response.content.detail;
    }

    if (response.sheets.length > 0 && response.sheets.every(({ content }) => content.status)) return 'Contenido válido';

    const finalText = response.sheets
        .filter(sheet => sheet.content.detail !== '')
        .map(sheet => `Hoja ${sheet.name}: ${sheet.content.detail}`)
        .join('\n');

    return finalText;
}

const verifyEmptySheetsStatus = (response: FileValidationResponse | FileSheetsValidationResponse): boolean => {
    if ('empty' in response) {
        return response.empty.status;
    }

    return response.sheets.length > 0 ? response.sheets.every(({ rows }) => rows.empty.status) : false;
}

const verifyEmptySheetsDetail = (response: FileValidationResponse | FileSheetsValidationResponse): string => {
    if ('empty' in response) {
        return response.empty.status ? 'Archivo con contenido' : response.empty.detail;
    }

    if (response.sheets.length > 0 && response.sheets.every(({ rows }) => rows.empty.status)) return 'Archivo con contenido';

    const finalText = response.sheets
        .filter(sheet => sheet.rows.empty.detail !== '')
        .map(sheet => `Hoja ${sheet.name}: ${sheet.rows.empty.detail}`)
        .join('\n');

    return finalText;
}

export const validFileDetails = (response: FileValidationResponse | FileSheetsValidationResponse): FileDetails => {
    const fileDetails: FileDetails = {
        characters: {
            valid: response.characters.status,
            detail: response.characters.status ? 'Caracteres válidos' : response.characters.detail
        },
        columns: {
            valid: verifyColumnsSheetsStatus(response),
            detail: verifyColumnsSheetsDetail(response)
        },
        content: {
            valid: verifyContentSheetsStatus(response),
            detail: verifyContentSheetsDetail(response)
        },
        empty: {
            valid: verifyEmptySheetsStatus(response),
            detail: verifyEmptySheetsDetail(response)
        },
        format: {
            valid: response.format.status,
            detail: response.format.status ? 'Formato válido' : response.format.detail
        },
        name: {
            valid: response.name.status,
            detail: response.name.status ? 'Nombre válido' : response.name.detail
        },
        size: {
            valid: response.size.status,
            detail: response.size.status ? 'Tamaño válido' : response.size.detail
        },
        date: {
            valid: response.date === undefined,
            detail: response.date !== undefined ? `${response.date.available} ${response.date.detail}` : 'Fecha válida'
        }
    }
    return fileDetails;
}