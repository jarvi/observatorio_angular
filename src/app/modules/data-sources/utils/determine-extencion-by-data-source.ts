import { DATA_OPTIONS_TYPES } from "src/app/shared/enums/data-options-types";

export const determineExtencionByDataSource = (dataSourceId: string): string[] => {
    let extencion: string[] = [];

    switch (dataSourceId) {
        case DATA_OPTIONS_TYPES.simat:
        case DATA_OPTIONS_TYPES.saberTests:
            extencion = ['.csv'];
            break;
        case DATA_OPTIONS_TYPES.due:
        case DATA_OPTIONS_TYPES.humanTalent:
        case DATA_OPTIONS_TYPES.futuresComputers:
        case DATA_OPTIONS_TYPES.educativeInfraestructure:
        case DATA_OPTIONS_TYPES.internalEfficiency:
        case DATA_OPTIONS_TYPES.technicalAverage:
        case DATA_OPTIONS_TYPES.schoolEnvironmentProtector:
        case DATA_OPTIONS_TYPES.projectionOfQuotas:
        case DATA_OPTIONS_TYPES.transport:
        case DATA_OPTIONS_TYPES.pae:
        case DATA_OPTIONS_TYPES.dropoutRate:
        case DATA_OPTIONS_TYPES.detailedStrategies:
        case DATA_OPTIONS_TYPES.indicativePlanAndActionPlan:
            extencion = ['.xlsx'];
            break;
        case DATA_OPTIONS_TYPES.consolidatedSIMAT:
            extencion = ['.txt'];
            break;
    }

    return extencion;
}