import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { FileValidationRequest } from '../interfaces/file-validation-request.interface';
import { FileValidationResponse } from '../interfaces/file-validation-response.interface';
import { LoadFilesResponse } from '../interfaces/file-load-response.interface';
import { GetLoadingPeriodResponse } from '../interfaces/get-loading-period-response.interface';
import { EditLoadingPeriodRequest } from '../interfaces/edit-loading-period-request.interface';
import { EditLoadingPeriodResponse } from '../interfaces/edit-loading-period-response.interface';
import { determineDataSourceToBuildFormData, determineDataSourceToBuildVerifyFormData, determineNewDataSourceByDataSourceId } from '../utils';
import { environment } from 'src/environments/environment';
import { getFirstAndLast } from 'src/utils/time-conversions';
import { FileSheetsValidationResponse } from '../interfaces/file-sheets-validation-response.interface';

@Injectable({
  providedIn: 'root'
})
export class DataSourcesService {

  private _httpRequestService$ = inject(HttpRequestService);

  private _pathNotifications: string = environment.pathNotifications;

  public validateFile(dataSourceId: string, fileValidationRequest: FileValidationRequest): Observable<FileValidationResponse | FileSheetsValidationResponse> {
    const newDataSourceId = determineNewDataSourceByDataSourceId(dataSourceId).toLowerCase();
    const formData = determineDataSourceToBuildVerifyFormData(dataSourceId, fileValidationRequest.file);

    return this._httpRequestService$.post<FormData, FileValidationResponse>(`/${newDataSourceId}/verify/`, formData, true)
      .pipe(
        map((response: FileValidationResponse) => response),
        catchError(() => {
          return throwError(() => '¡El archivo no pudo ser validado!');
        })
      );
  }

  public loadFiles<T>(dataSourceId: string, fileLoadRequest: T): Observable<LoadFilesResponse> {
    const newDataSourceId = determineNewDataSourceByDataSourceId(dataSourceId).toLowerCase();
    const formData = determineDataSourceToBuildFormData(dataSourceId, fileLoadRequest);
    
    return this._httpRequestService$.post<FormData, LoadFilesResponse>(`/${newDataSourceId}/upload/`, formData, true)
      .pipe(
        map((response: LoadFilesResponse) => response),
        catchError(() => {
          return throwError(() => '¡No fue posible cargar los archivos!');
        })
      );
  }

  public getLoadingPeriod(dataSourceId: string): Observable<GetLoadingPeriodResponse> {
    return this._httpRequestService$.get<{}, GetLoadingPeriodResponse>(`${this._pathNotifications}/period/${dataSourceId}/`)
      .pipe(
        map((response: GetLoadingPeriodResponse) => response),
        catchError(() => {
          return throwError(() => '¡No fue posible obtener el periodo de carga!')
        })
      );
  }

  public editLoadingPeriod(request: EditLoadingPeriodRequest): Observable<EditLoadingPeriodResponse> {
    return this._httpRequestService$.put<EditLoadingPeriodRequest, EditLoadingPeriodResponse>(`${this._pathNotifications}/upload-period/${request.id}/`, request)
      .pipe(
        map((response: EditLoadingPeriodResponse) => {
          return {
            ...response,
            month_days: request.month_days && getFirstAndLast(request.month_days),
            year_days: request.year_days && getFirstAndLast(request.year_days),
          }
        }),
        catchError(() => {
          return throwError(() => '¡No fue posible modificar el periodo de carga!')
        })
      );
  }
}
