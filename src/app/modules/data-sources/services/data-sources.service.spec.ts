import { TestBed } from '@angular/core/testing';

import { DataSourcesService } from './data-sources.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientModule } from '@angular/common/http';
import { FileValidationRequest } from '../interfaces/file-validation-request.interface';
import { of, take, throwError } from 'rxjs';
import { FileValidationResponse } from '../interfaces/file-validation-response.interface';
import { FilesDetailedStrategiesRequest, FilesDropoutRateRequest, FilesEducativeInfraestructureRequest, FilesFuturesComputersRequest, FilesHumanTalentRequest, FilesIndicativePlanAndActionPlanRequest, FilesInternalEfficiencyRequest, FilesPAERequest, FilesProjectionOfQuotasRequest, FilesSIMATConsolidatedRequest, FilesSaberTestsRequest, FilesSchoolEnvironmentProtectorRequest, FilesSimatRequest, FilesTechnicalAverageRequest, FilesTransportRequest } from '../interfaces/file-load-request.interface';
import { EditLoadingPeriodRequest, EditLoadingPeriodResponse, GetLoadingPeriodResponse } from '../interfaces';
import { TIME_TYPE } from '../enums';
import { weekDayToNumber } from 'src/utils/time-conversions';

describe('DataSourcesService', () => {
  let service: DataSourcesService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      providers: [
        HttpRequestService,
      ]
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    service = TestBed.inject(DataSourcesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should exec validateFile and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('SIMAT', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec validateFile for DUE and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('DUE', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec validateFile and return an error', () => {
    spyOn(httpRequestService, 'post').and.returnValue(throwError(() => {}));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }

    service.validateFile('SIMAT', request)
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('¡El archivo no pudo ser validado!');
        }
      );
  });

  it('should exec loadFiles and return the correct response', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesSimatRequest = {
      anexo5a: new File([''], 'test.csv'),
      anexo5b: new File([''], 'test.csv'),
      anexo5o: new File([''], 'test.csv'),
      anexo6a: new File([''], 'test.csv'),
      anexo6b: new File([''], 'test.csv'),
      anexo6o: new File([''], 'test.csv')
    }

    service.loadFiles('SIMAT', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec validateFile for human talent and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('Talento Humano', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec validateFile for PAE and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('PAE', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec validateFile for dropout rate and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('Deserción Repitencia', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec validateFile for consolidated SIMAT and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('Consolidado SIMAT', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec validateFile for detailed strategies and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('Detallado Estrategias', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec validateFile for indicative plan and action plan and return the correct response', () => {
    const response: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));

    const request: FileValidationRequest = {
      file: new File([''], 'test.csv'),
    }
    const responseExpected: FileValidationResponse = {
      valid: true,
      characters: {
        status: true,
        detail: 'Valid',
        invalid_line: ''
      },
      columns: {
        status: true,
        detail: 'Valid',
        missing_colums: []
      },
      content: {
        status: true,
        detail: 'Valid',
        invalid_columns: []
      },
      empty: {
        status: true,
        detail: 'Valid'
      },
      format: {
        status: true,
        detail: 'Valid'
      },
      name: {
        status: true,
        detail: 'Valid'
      },
      size: {
        status: true,
        detail: 'Valid'
      },
      rows: 12,
    };

    service.validateFile('Plan Indicativo y Plan de Acción', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(responseExpected);
      });
  });

  it('should exec loadFiles and return the correct response for DUE', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesSimatRequest = {
      anexo5a: new File([''], 'test.csv'),
      anexo5b: new File([''], 'test.csv'),
      anexo5o: new File([''], 'test.csv'),
      anexo6a: new File([''], 'test.csv'),
      anexo6b: new File([''], 'test.csv'),
      anexo6o: new File([''], 'test.csv')
    }

    service.loadFiles('DUE', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for human talent', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesHumanTalentRequest = {
      baseDocentes: new File([''], 'test.csv'),
      fubDocDir: new File([''], 'test.csv'),
      semBaseDocentes: new File([''], 'test.csv'),
    }

    service.loadFiles('Talento Humano', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for PAE', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesPAERequest = {
      ocemPae: new File([''], 'test.csv'),
    }

    service.loadFiles('PAE', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Pruebas Saber', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesSaberTestsRequest = {
      sb111: new File([''], 'test.csv'),
      sb112: new File([''], 'test.csv'),
    }

    service.loadFiles('Pruebas Saber', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Detallado Estrategias', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesDetailedStrategiesRequest = {
      detailed: new File([''], 'test.csv'),
    }

    service.loadFiles('Detallado Estrategias', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Computadores Futuro', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesFuturesComputersRequest = {
      cruzan: new File([''], 'test.csv'),
      computersDelivered: new File([''], 'test.csv'),
    }

    service.loadFiles('Computadores Futuro', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Infraestructura Educativa', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesEducativeInfraestructureRequest = {
      matrix: new File([''], 'test.csv'),
      dotation: new File([''], 'test.csv'),
      sem: new File([''], 'test.csv'),
    }

    service.loadFiles('Infraestructura Educativa', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Transporte', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesTransportRequest = {
      ocem: new File([''], 'test.csv'),
      ocemProfile: new File([''], 'test.csv'),
    }

    service.loadFiles('Transporte', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Eficiencia Interna', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesInternalEfficiencyRequest = {
      consolidated: new File([''], 'test.csv'),
    }

    service.loadFiles('Consolidado de Eficiencia Interna', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Media Técnica', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesTechnicalAverageRequest = {
      averange: new File([''], 'test.csv'),
    }

    service.loadFiles('Media Técnica', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Entorno Escolar Protector', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesSchoolEnvironmentProtectorRequest = {
      actionsReport: new File([''], 'test.csv'),
      attentionReport: new File([''], 'test.csv'),
    }

    service.loadFiles('Entorno Escolar Protector', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Proyección de Cupos', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesProjectionOfQuotasRequest = {
      quotasProjection: new File([''], 'test.csv'),
    }

    service.loadFiles('Proyección de Cupos', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Deserción Repitencia', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesDropoutRateRequest = {
      dropoutRate: new File([''], 'test.csv'),
    }

    service.loadFiles('Deserción Repitencia', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Consolidado SIMAT', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesSIMATConsolidatedRequest = {
      simatConsolidated: new File([''], 'test.csv'),
    }

    service.loadFiles('Consolidado SIMAT', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return the correct response for Plan Indicativo y Plan de Acción', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(true));
    
    const request: FilesIndicativePlanAndActionPlanRequest = {
      indicativePlanAndActionPlan: new File([''], 'test.csv'),
    }

    service.loadFiles('Plan Indicativo y Plan de Acción', request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toBeTrue();
      });
  });

  it('should exec loadFiles and return an error when has bad request', () => {
    spyOn(httpRequestService, 'post').and.returnValue(throwError(() => {}));
    
    const request: FilesSimatRequest = {
      anexo5a: new File([''], 'test.csv'),
      anexo5b: new File([''], 'test.csv'),
      anexo5o: new File([''], 'test.csv'),
      anexo6a: new File([''], 'test.csv'),
      anexo6b: new File([''], 'test.csv'),
      anexo6o: new File([''], 'test.csv')
    }

    service.loadFiles('DUE', request)
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('¡No fue posible cargar los archivos!');
        }
      );
  });

  it('should exec getLoadingPeriod and return the loading period', () => {
    const reponseExpected: GetLoadingPeriodResponse  = {
      id: 1,
      data_source: 'SIMAT',
      eachtime: 1,
      period: TIME_TYPE.week,
      week_days: ['Lunes', 'Martes'].map(day => weekDayToNumber(day)),
      is_paused: false,
      notification_days: [1, 2],
      notification_hour: '10:00:00'
    }
    spyOn(httpRequestService, 'get').and.returnValue(of(reponseExpected));

    service.getLoadingPeriod('SIMAT')
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(reponseExpected);
      });
  });

  it('should exec getLoadingPeriod and return an error when has bad request', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.getLoadingPeriod('SIMAT')
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('¡No fue posible obtener el periodo de carga!');
        }
      );
  });

  it('should exec editLoadingPeriod and return the loading period', () => {
    const reponseExpected: EditLoadingPeriodResponse  = {
      id: 1,
      data_source: 'SIMAT',
      eachtime: 1,
      period: TIME_TYPE.week,
      week_days: ['Lunes', 'Martes'].map(day => weekDayToNumber(day)),
      month_days: undefined,
      year_days: undefined,
      is_paused: false,
      notification_days: [1, 2],
      notification_hour: '10:00:00'
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(reponseExpected));

    const request: EditLoadingPeriodRequest = {
      id: 1,
      eachtime: 1,
      period: TIME_TYPE.week,
      week_days: ['Lunes', 'Martes']
    }

    service.editLoadingPeriod(request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(reponseExpected);
      });
  });

  it('should exec editLoadingPeriod and return the loading period for month day', () => {
    const reponseExpected: EditLoadingPeriodResponse  = {
      id: 1,
      data_source: 'SIMAT',
      eachtime: 1,
      period: TIME_TYPE.week,
      month_days: [1,4],
      is_paused: false,
      notification_days: [1, 2],
      notification_hour: '10:00:00',
      year_days: undefined
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(reponseExpected));

    const request: EditLoadingPeriodRequest = {
      id: 1,
      eachtime: 1,
      period: TIME_TYPE.week,
      month_days: [1,2,3,4]
    }

    service.editLoadingPeriod(request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(reponseExpected);
      });
  });

  it('should exec editLoadingPeriod and return the loading period for month day', () => {
    const reponseExpected: EditLoadingPeriodResponse  = {
      id: 1,
      data_source: 'SIMAT',
      eachtime: 1,
      period: TIME_TYPE.week,
      year_days: [1,4],
      month_days: undefined,
      is_paused: false,
      notification_days: [1, 2],
      notification_hour: '10:00:00',
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(reponseExpected));

    const request: EditLoadingPeriodRequest = {
      id: 1,
      eachtime: 1,
      period: TIME_TYPE.week,
      year_days: [1,4],
    }

    service.editLoadingPeriod(request)
      .pipe(take(1))
      .subscribe((response) => {
        expect(response).toEqual(reponseExpected);
      });
  });

  it('should exec getLoadingPeriod and return an error when has bad request', () => {
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => {}));

    const request: EditLoadingPeriodRequest = {
      id: 1,
      eachtime: 1,
      period: TIME_TYPE.week,
      week_days: ['Lunes', 'Martes']
    }

    service.editLoadingPeriod(request)
      .pipe(take(1))
      .subscribe(
        () => {},
        (err) => {
          expect(err).toEqual('¡No fue posible modificar el periodo de carga!');
        }
      );
  });
});
