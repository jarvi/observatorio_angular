export enum SIMAT_FILE {
    anexo5a = 'ANEXO 5A',
    anexo5b = 'ANEXO 5B',
    anexo5o = 'ANEXO 5O',
    anexo6a = 'ANEXO 6A',
    anexo6b = 'ANEXO 6B',
    anexo6o = 'ANEXO 6O'
}