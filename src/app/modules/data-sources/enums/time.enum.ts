export enum TIME_TYPE {
    week = 'Semana',
    month = 'Mes',
    year = 'Año',
    undefined = 'Indefinido'
}