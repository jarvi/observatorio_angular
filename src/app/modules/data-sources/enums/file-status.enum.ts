export enum FILE_STATUS {
    pending = 'Pendiente',
    isLoading = 'Procesando',
    incomplete = 'No completado',
    complete = 'Completado',
}