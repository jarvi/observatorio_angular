import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, inject } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TimeType } from '../../interfaces/time-tipe.interface';
import { ChipButton } from 'src/app/shared/interfaces/chip-button.interface';
import { LoadingPeriodFrecuency } from '../../interfaces/loading-period-frecuency.interface';
import { TIME_TYPE } from '../../enums/time.enum';
import { formHasSomeValue } from 'src/utils/verify-some-value-on-form';
import { weekDays, yearMonths } from '../../constants/loading-period-times';
import { dataSourceTimeToString } from '../../utils/data-source-time-to-string';
import { ValidatorService } from 'src/app/shared/services';

@Component({
  selector: 'data-sources-loading-period',
  templateUrl: './loading-period.component.html',
  styleUrls: ['./loading-period.component.scss']
})
export class LoadingPeriodComponent implements OnInit, OnDestroy {

  @Input() public dataSource: string = '';
  @Input() public configurationForm: FormGroup = new FormGroup({});
  @Input() public eachTime!: number;
  @Input() public time!: TIME_TYPE;
  @Input() public weekDays?: string[];
  @Input() public monthDays?: number[];
  @Input() public yearMonths?: string[];
  @Input() public yearDays?: number[];
  @Output() public editLoadingPeriod: EventEmitter<void> = new EventEmitter<void>();
  @Output() public cancelLoadingPeriod: EventEmitter<() => void> = new EventEmitter<() => void>();

  private _validatorService = inject(ValidatorService);

  private _timeSubscription: Subscription = new Subscription();

  public timeObject = { week: TIME_TYPE.week, month: TIME_TYPE.month, year: TIME_TYPE.year, undefined: TIME_TYPE.undefined }
  public timesType: TimeType[] = [
    { id: 1, name: this.timeObject.week },
    { id: 2, name: this.timeObject.month },
    { id: 3, name: this.timeObject.year },
    { id: 4, name: this.timeObject.undefined },
  ]
  public weekDaysList: ChipButton[] = weekDays;
  public yearMonthsList: ChipButton[] = yearMonths;
  public modalVisible: boolean = false;

  ngOnInit(): void {
    const timeControl = this.configurationForm.controls['time'];
    this.determineValidators(timeControl.value);
    
    this._timeSubscription = timeControl.valueChanges.subscribe(timeSelected => {
      this.determineValidators(timeSelected);
    });
  }

  ngOnDestroy(): void {
    this._timeSubscription.unsubscribe();
  }

  private clearWeekValidators() {
    const weekDays = this.configurationForm.controls['weekDays'];
    weekDays.setValidators([]);
    weekDays.updateValueAndValidity();
    weekDays.setValue([])
  }

  private clearMonthValidators() {
    const monthDays = this.configurationForm.controls['monthDays'];
    monthDays.setValidators([]);
    monthDays.updateValueAndValidity();
    monthDays.setValue([])
  }

  private clearYearValidators() {
    const yearMonths = this.configurationForm.controls['yearMonths'];
    const yearDays = this.configurationForm.controls['yearDays'];
    yearMonths.setValidators([]);
    yearDays.setValidators([]);
    yearMonths.updateValueAndValidity();
    yearDays.updateValueAndValidity();
    yearMonths.setValue([])
    yearDays.setValue([])
  }

  private determineValidators(timeType: TIME_TYPE) {
    switch (timeType) {
      case TIME_TYPE.week:
        this.configurationForm.controls['weekDays'].setValidators([Validators.required]);
        this.configurationForm.controls['weekDays']?.updateValueAndValidity();

        this.clearMonthValidators();
        this.clearYearValidators();
        break;
      case TIME_TYPE.month:
        this.configurationForm.controls['monthDays'].setValidators([Validators.required, this._validatorService.hasLength(5)]);
        this.configurationForm.controls['monthDays']?.updateValueAndValidity();

        this.clearWeekValidators();
        this.clearYearValidators();
        break;
      case TIME_TYPE.year:
        this.configurationForm.controls['yearMonths'].setValidators([Validators.required]);
        this.configurationForm.controls['yearMonths']?.updateValueAndValidity();
        this.configurationForm.controls['yearDays'].setValidators([Validators.required, this._validatorService.hasLength(5)]);
        this.configurationForm.controls['yearDays']?.updateValueAndValidity();

        this.clearWeekValidators();
        this.clearMonthValidators();
        break;
        case TIME_TYPE.undefined:
          this.clearWeekValidators();
          this.clearMonthValidators();
          this.clearYearValidators();
          break;
    }
  }
  
  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
  }

  public getDataSourceTimeInString(): string {
    const loadingPeriodFrecuency: LoadingPeriodFrecuency = {
      eachTime: this.eachTime,
      time: this.time,
      weekDays: this.weekDays,
      monthDays: this.monthDays,
      yearMonths: this.yearMonths,
      yearDays: this.yearDays
    }

    return dataSourceTimeToString(loadingPeriodFrecuency)
  }

  public onEditLoadingPeriod() {
    if (this.configurationForm.invalid) return;

    this.onVisibleChange(false);
    this.editLoadingPeriod.emit();
  }

  public onCancelLoadingPeriod() {
    this.onVisibleChange(false);
    this.cancelLoadingPeriod.emit(() => {
      this.onVisibleChange(true);
    });
  }

  public checkValuesForm(): boolean {
    return formHasSomeValue(this.configurationForm, ['weekDays', 'yearMonths', 'monthDays', 'yearDays']);
  }
}
