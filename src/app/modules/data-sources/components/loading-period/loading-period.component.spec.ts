import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingPeriodComponent } from './loading-period.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { TIME_TYPE } from '../../enums';

describe('LoadingPeriodComponent', () => {
  let component: LoadingPeriodComponent;
  let fixture: ComponentFixture<LoadingPeriodComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingPeriodComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        PrimeNgModule
      ]
    });
    fixture = TestBed.createComponent(LoadingPeriodComponent);
    component = fixture.componentInstance;

    component.configurationForm = new FormGroup({
      eachTime: new FormControl('', [Validators.required]),
      time: new FormControl(null),
      weekDays: new FormControl([]),
      yearMonths: new FormControl([]),
      monthDays: new FormControl([]),
      yearDays: new FormControl([]),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit value when onCancelLoadingPeriod method is executed', () => {
    spyOn(component.cancelLoadingPeriod, 'emit');
    component.onCancelLoadingPeriod();
    expect(component.cancelLoadingPeriod.emit).toHaveBeenCalled();
  });
  
  it('should call closeOverlayCalendar and emit cancelLoadingPeriod with a function', () => {
    
    const mockEmitFunction = jasmine.createSpy('mockEmitFunction');
    
    component.cancelLoadingPeriod.emit = mockEmitFunction;
  
    component.onCancelLoadingPeriod();
  
    expect(mockEmitFunction).toHaveBeenCalled();
  
    expect(mockEmitFunction).toHaveBeenCalledWith(jasmine.any(Function));
    
    const simulatedEvent = new PointerEvent('click', { pointerType: 'mouse' });
    const emittedFunction = mockEmitFunction.calls.mostRecent().args[0];
    emittedFunction(simulatedEvent);
  });

  it('should emit value when onEditLoadingPeriod method is executed', () => {
    spyOn(component.editLoadingPeriod, 'emit');

    component.configurationForm.controls['eachTime'].setValue(2);
    component.onEditLoadingPeriod();
    expect(component.editLoadingPeriod.emit).toHaveBeenCalled();
  });

  it('should not emit value when onEditLoadingPeriod method is executed', () => {
    spyOn(component.editLoadingPeriod, 'emit');

    component.onEditLoadingPeriod();
    expect(component.editLoadingPeriod.emit).not.toHaveBeenCalled();
  });

  it('should show the overmay element when toggleOverlayCalendar method is executed', () => {
    component.onVisibleChange(true);
    expect(component.modalVisible).toEqual(true);
  });

  it('should change validators when time is week', () => {
    component.configurationForm.controls['time'].setValue(TIME_TYPE.week);
    const formControl = component.configurationForm.controls['weekDays'];

    expect(formControl.invalid).toBeTrue();
  });

  it('should change validators when time is month', () => {
    component.configurationForm.controls['time'].setValue(TIME_TYPE.month);
    const formControl = component.configurationForm.controls['monthDays'];

    expect(formControl.invalid).toBeTrue();
  });

  it('should change validators when time is year', () => {
    component.configurationForm.controls['time'].setValue(TIME_TYPE.year);
    const formControlMonths = component.configurationForm.controls['yearMonths'];
    const formControlDays = component.configurationForm.controls['yearDays'];

    expect(formControlMonths.invalid).toBeTrue();
    expect(formControlDays.invalid).toBeTrue();
  });

  it('should change validators when time is undefined', () => {
    component.configurationForm.controls['time'].setValue(TIME_TYPE.undefined);
    const formControlMonths = component.configurationForm.controls['yearMonths'];
    const formControlDays = component.configurationForm.controls['yearDays'];

    expect(formControlMonths.valid).toBeTrue();
    expect(formControlDays.valid).toBeTrue();
  });

  it('should change time week values to string', () => {
    component.eachTime = 1;
    component.time = TIME_TYPE.week;
    component.weekDays = ['Lunes', 'Martes'];

    const resultText = component.getDataSourceTimeInString();
    expect(resultText).toEqual('Cada semana, Lunes - Martes');
  });

  it('should change time month values to string, when is only one number', () => {
    component.eachTime = 1;
    component.time = TIME_TYPE.month;
    component.monthDays = [1];

    const resultText = component.getDataSourceTimeInString();
    expect(resultText).toEqual('Cada mes, día 1');
  });

  it('should change time month values to string, when it does no have anything days', () => {
    component.eachTime = 1;
    component.time = TIME_TYPE.month;
    component.monthDays = [];

    const resultText = component.getDataSourceTimeInString();
    expect(resultText).toEqual('Cada mes,');
  });

  it('should change time month values to string', () => {
    component.eachTime = 1;
    component.time = TIME_TYPE.month;
    component.monthDays = [1,2,3,4];

    const resultText = component.getDataSourceTimeInString();
    expect(resultText).toEqual('Cada mes, del día 1 al día 2');
  });

  it('should change time month values to string, when is more than 1 eachTime', () => {
    component.eachTime = 2;
    component.time = TIME_TYPE.month;
    component.monthDays = [1,2,3,4];

    const resultText = component.getDataSourceTimeInString();
    expect(resultText).toEqual('Cada 2 meses, del día 1 al día 2');
  });

  it('should change time year values to string', () => {
    component.eachTime = 1;
    component.time = TIME_TYPE.year;
    component.yearMonths = ['Enero', 'Marzo'];
    component.yearDays = [1,2,3,4];

    const resultText = component.getDataSourceTimeInString();
    expect(resultText).toEqual('Cada año, Enero - Marzo del día 1 al día 2');
  });

  it('should change time year values to string, when is more than 1 eachTime', () => {
    component.eachTime = 2;
    component.time = TIME_TYPE.year;
    component.yearMonths = ['Enero', 'Marzo'];
    component.yearDays = [1,2,3,4];

    const resultText = component.getDataSourceTimeInString();
    expect(resultText).toEqual('Cada 2 años, Enero - Marzo del día 1 al día 2');
  });
});
