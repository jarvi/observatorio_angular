import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FileUploaded } from '../../interfaces/file-uploaded.interface';
import { FILE_STATUS } from '../../enums/file-status.enum';

@Component({
  selector: 'data-sources-datagrid-files',
  templateUrl: './datagrid-files.component.html',
  styleUrls: ['./datagrid-files.component.scss']
})
export class DatagridFilesComponent {

  @Input() files: FileUploaded[] = [];
  @Output() removeFile: EventEmitter<string> = new EventEmitter();
  
  public fileSelectedToDetails: FileUploaded | undefined = undefined;
  public status = {
    complete: FILE_STATUS.complete,
    incomplete: FILE_STATUS.incomplete,
    pending: FILE_STATUS.pending,
    isLoading: FILE_STATUS.isLoading
  }

  public onRemoveFile({ id, status }: FileUploaded) {
    if (status !== FILE_STATUS.complete && status !== FILE_STATUS.incomplete) return;
    this.removeFile.emit(id);
  }

  public onShowDetails(file: FileUploaded | undefined) {
    this.fileSelectedToDetails = file;
  }
}
