import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatagridFilesComponent } from './datagrid-files.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FileDetailsComponent } from '../file-details/file-details.component';
import { FILE_STATUS } from '../../enums/file-status.enum';

describe('DatagridFilesComponent', () => {
  let component: DatagridFilesComponent;
  let fixture: ComponentFixture<DatagridFilesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DatagridFilesComponent,
        FileDetailsComponent
      ],
      imports: [
        SharedModule,
        PrimeNgModule,
      ],
    });
    fixture = TestBed.createComponent(DatagridFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the id file when onRemoveFile is call with complete status', () => {
    spyOn(component.removeFile, 'emit');

    const file = {
      id: '1',
      fileName: 'TEST 1',
      status: FILE_STATUS.complete,
      file: new File([''], 'test.csv'),
    }
    component.files = [file]

    component.onRemoveFile(file);

    expect(component.removeFile.emit).toHaveBeenCalledWith(file.id);
  });

  it('should emit the id file when onRemoveFile is call with incomplete status', () => {
    spyOn(component.removeFile, 'emit');

    const file = {
      id: '1',
      fileName: 'TEST 1',
      status: FILE_STATUS.incomplete,
      file: new File([''], 'test.csv'),
    }
    component.files = [file]

    component.onRemoveFile(file);

    expect(component.removeFile.emit).toHaveBeenCalledWith(file.id);
  });

  it('should not emit the id file when onRemoveFile is call with another status', () => {
    spyOn(component.removeFile, 'emit');

    const file = {
      id: '1',
      fileName: 'TEST 1',
      status: FILE_STATUS.pending,
      file: new File([''], 'test.csv'),
    }
    component.files = [file]

    component.onRemoveFile(file);

    expect(component.removeFile.emit).not.toHaveBeenCalled();
  });
});
