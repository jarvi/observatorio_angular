import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileDetailsComponent } from './file-details.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileUploaded } from '../../interfaces/file-uploaded.interface';

describe('FileDetailsComponent', () => {
  let component: FileDetailsComponent;
  let fixture: ComponentFixture<FileDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FileDetailsComponent],
      imports: [
        SharedModule,
      ],
    });
    fixture = TestBed.createComponent(FileDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnChanges when file changes and change file name', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newFile: FileUploaded = {
      fileName: 'TEST 1',
      file: new File([''], 'test.csv'),
      id: '1',
      status: 'VALIDATED',
      details: {
        characters: {
          valid: true,
          detail: 'Valid'
        },
        columns: {
          valid: true,
          detail: 'Valid'
        },
        content: {
          valid: true,
          detail: 'Valid'
        },
        empty: {
          valid: true,
          detail: 'Valid'
        },
        format: {
          valid: true,
          detail: 'Valid'
        },
        name: {
          valid: true,
          detail: 'Valid'
        },
        size: {
          valid: false,
          detail: 'Invalid'
        },
        date: {
          valid: false,
          detail: 'Invalid'
        }
      },
    };
    component.file = newFile;
    fixture.detectChanges();
    component.ngOnChanges({ file: {
      previousValue: '',
      currentValue: newFile,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.fileName ).toEqual(newFile.fileName);
  });

  it('should call ngOnChanges when file changes without details and change file name', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newFile: FileUploaded = {
      fileName: 'TEST 1',
      file: new File([''], 'test.csv'),
      id: '1',
      status: 'VALIDATED',
    };
    component.file = newFile;
    fixture.detectChanges();
    component.ngOnChanges({ file: {
      previousValue: '',
      currentValue: newFile,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.fileName ).toEqual(newFile.fileName);
  });
});
