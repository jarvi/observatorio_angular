import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { ModalDetails } from 'src/app/shared/interfaces';
import { FileUploaded } from '../../interfaces/file-uploaded.interface';

@Component({
  selector: 'data-sources-file-details',
  templateUrl: './file-details.component.html'
})
export class FileDetailsComponent implements OnChanges {

  @Input() public file: FileUploaded | undefined = undefined;
  @Output() public fileChange: EventEmitter<FileUploaded | undefined> = new EventEmitter<FileUploaded | undefined>();

  public modalVisible: boolean = false;
  public fileName: string = '';
  public fileInformationDetails: ModalDetails[][] = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['file']) {
      this.onVisibleChange(this.file !== undefined);
      
      if (!this.file) return;
      this.buildDetails(this.file);
    }
  }

  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
    this.fileChange.emit(visible ? this.file : undefined);
  }

  private buildDetails(file: FileUploaded) {
    this.fileName = file.fileName;

    const { details } = file;
    if (!details) return;

    this.fileInformationDetails = [
      [
        { label: 'Caracteres del archivo', value: details.characters.detail, valid: details.characters.valid },
        { label: 'Columnas del archivo', value: details.columns.detail, valid: details.columns.valid },
        { label: 'Contenido del archivo', value: details.content.detail, valid: details.content.valid },
        { label: 'Archivo con datos', value: details.empty.detail, valid: details.empty.valid },
      ],
      [
        { label: 'Formato del archivo', value: details.format.detail, valid: details.format.valid },
        { label: 'Nombre del archivo', value: details.name.detail, valid: details.name.valid },
        { label: 'Tamaño del archivo', value: details.size.detail, valid: details.size.valid },
        { label: 'Fecha de carga', value: details.date.detail, valid: details.date.valid },
      ]
    ];
  }
}
