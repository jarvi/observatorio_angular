import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrationRoutingModule } from './administration-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';


@NgModule({
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    SharedModule,
    PrimeNgModule
  ]
})
export class AdministrationModule { }
