import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { RoleRoutingModule } from './role-routing.module';
import { RoleAdministrationComponent } from './pages/role-administration/role-administration.component';
import { RoleCreationComponent } from './pages/role-creation/role-creation.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchRolesFormComponent } from './components/search-roles-form/search-roles-form.component';
import { PermissionSwitchComponent } from './components/permission-switch/permission-switch.component';
import { DatagridRolesComponent } from './components/datagrid-roles/datagrid-roles.component';
import { RoleInformationFormComponent } from './components/role-information-form/role-information-form.component';
import { PermissionsFormComponent } from './components/permissions-form/permissions-form.component';
import { RoleDetailsComponent } from './components/role-details/role-details.component';
import { RoleEditComponent } from './components/role-edit/role-edit.component';


@NgModule({
  declarations: [
    RoleAdministrationComponent,
    RoleCreationComponent,
    SearchRolesFormComponent,
    PermissionSwitchComponent,
    DatagridRolesComponent,
    RoleInformationFormComponent,
    PermissionsFormComponent,
    RoleDetailsComponent,
    RoleEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

    RoleRoutingModule,

    PrimeNgModule,
    SharedModule
  ],
  exports: [
    RoleInformationFormComponent,
    PermissionsFormComponent
  ]
})
export class RoleModule { }
