import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleCreationComponent } from './role-creation.component';
import { RoleService } from 'src/app/shared/services/role.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RoleInformationFormComponent } from '../../components/role-information-form/role-information-form.component';
import { PermissionsFormComponent } from '../../components/permissions-form/permissions-form.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PermissionSwitchComponent } from '../../components/permission-switch/permission-switch.component';
import { Role } from 'src/app/shared/interfaces';
import Swal from 'sweetalert2';
import { of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

describe('RoleCreationComponent', () => {
  let component: RoleCreationComponent;
  let fixture: ComponentFixture<RoleCreationComponent>;
  let roleService: RoleService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        RoleCreationComponent,
        RoleInformationFormComponent,
        PermissionsFormComponent,
        PermissionSwitchComponent,
      ],
      providers: [
        RoleService,
        HttpRequestService,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        PrimeNgModule
      ]
    });
    fixture = TestBed.createComponent(RoleCreationComponent);
    router = TestBed.inject(Router);
    roleService = TestBed.inject(RoleService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should preload value to switches', () => {
    const role: Role = {
      id: 1,
      name: 'test',
      isActive: true,
      permissions: ['view_user']
    }

    component.roleInformationForm.controls['role'].setValue(role);

    expect(component.form.controls['consultUsers'].value).toEqual('view_user');
  });

  it('should preload value to switches and the another change to false', () => {
    const role: Role = {
      id: 1,
      name: 'test',
      isActive: true
    }

    component.roleInformationForm.controls['role'].setValue(role);
    
    expect(component.form.controls['consultUsers'].value).toEqual('');
  });

  it('should check form validation', () => {
    component.checkValuesSwitchForm();
    expect(component.form.invalid).toBeTrue();
  });

  it('should show modal warning alert', (done) => {
    fixture.ngZone!.run(() => {
      spyOn(router, 'navigateByUrl');
      component.onCancelRoleCreation();
  
      expect(Swal.isVisible()).toBeTrue();
      const confirmButton = Swal.getConfirmButton()!;
      confirmButton.click();
  
      expect(confirmButton).toBeTruthy();
      done();
    });
  });

  it('should not do anything if form is invalid', () => {
    component.onCreateRole();
    expect(component.form.invalid).toBeTrue();
  });

  it('should show modal success alert if form is valid and onCreateRole is executed and press the confirm button', () => {
    component.roleInformationForm.controls['roleName'].setValue('test');
    component.form.controls['consultUsers'].setValue(true);

    spyOn(roleService, 'createRole').and.returnValue(of('new role'));
    component.onCreateRole();

    expect(component.form.valid).toBeTrue();
    expect(Swal.isVisible()).toBeTrue();

    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should show modal success alert if form is valid and onCreateRole is executed and press the cancel button', () => {
    component.roleInformationForm.controls['roleName'].setValue('test');
    component.form.controls['consultUsers'].setValue(true);

    spyOn(roleService, 'createRole').and.returnValue(of('new role'));
    component.onCreateRole();

    expect(component.form.valid).toBeTrue();
    expect(Swal.isVisible()).toBeTrue();

    const cancelButton = Swal.getCancelButton()!;
    cancelButton.click();
    expect(cancelButton).toBeTruthy();
  });

  it('should show modal success alert if form is valid and onCreateRole is executed and press the confirm button', () => {
    component.roleInformationForm.controls['roleName'].setValue('test');
    component.form.controls['consultUsers'].setValue(true);

    spyOn(roleService, 'createRole').and.returnValue(throwError(() => 'Error'));
    component.onCreateRole();

    expect(component.form.valid).toBeTrue();
    expect(Swal.isVisible()).toBeTrue();

    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();
    expect(confirmButton).toBeTruthy();
  });
    
  it('should be activated consultUsers when selecting inactivateUsers', () => {   
    component.form.controls['inactivateUsers'].setValue(true);
    expect(component.form.controls['consultUsers'].value).toEqual('view_user');
  });

  it('should be activated consultUsers when selecting editUsers', () => {   
    component.form.controls['editUsers'].setValue(true);
    expect(component.form.controls['consultUsers'].value).toEqual('view_user');
  });

  it('should be activated consultRoles when selecting editUsers', () => {   
    component.form.controls['editUsers'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting createUsers', () => {   
    component.form.controls['createUsers'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting inactivateRoles', () => {   
    component.form.controls['inactivateRoles'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting editRoles', () => {   
    component.form.controls['editRoles'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting createRoles', () => {   
    component.form.controls['createRoles'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting editNotifications', () => {   
    component.form.controls['editNotifications'].setValue(true);
    expect(component.form.controls['viewNotifications'].value).toEqual(PERMISSION_TYPE.viewNotification.name);
  });
});
