import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TooltipOptions } from 'primeng/api';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { RoleService } from 'src/app/shared/services/role.service';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { formHasSomeValue } from 'src/utils/verify-some-value-on-form';
import { ModalAlert, Role, RoleCreationRequest } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { getPermissionsFromForm, preloadPermissions } from 'src/utils/permissions';
import { switchedFormStructure } from '../../../../../../../utils/creation-edition-role-validators';
import { roleSwitchesObservable } from '../../utils/role-switches-observable';

@Component({
  templateUrl: './role-creation.component.html',
  styleUrls: ['./role-creation.component.scss']
})
export class RoleCreationComponent implements OnInit, OnDestroy {

  private _fb = inject(FormBuilder);
  private _router = inject(Router);
  private _roleService = inject(RoleService);
  private _validatorService = inject(ValidatorService);
  private _modalAlertService = inject(ModalAlertService);
  private _mobileWidthService = inject(MobileWidthService);

  private _roleFieldSubscription: Subscription = new Subscription();
  private _roleCreationSubscription: Subscription = new Subscription();
  private _widthSubscription: Subscription = new Subscription();
  private _roleSwitchesCreationObservableSubscription: Subscription = new Subscription();

  public isScreenSmall: boolean = false;
  public isLoading: boolean = false;
  public roleInformationForm: FormGroup = this._fb.group({
    roleName: ['', [
      Validators.required, 
      Validators.maxLength(30),
      this._validatorService.whiteSpacesStartValidator()
    ]],
    preload: [false],
    role: [null],
  });
  public form: FormGroup = this._fb.group({
    roleInformationForm: this.roleInformationForm,
    ...switchedFormStructure()
  });
  public tooltipOptions: TooltipOptions = {
    tooltipPosition: 'top',
    tooltipEvent: 'hover',
    positionTop: -10,
    positionLeft: -50,
    tooltipLabel: 'Complete todos los campos',
    tooltipStyleClass: 'role-creation-create-button'
  }

  ngOnInit(): void {
    this._roleFieldSubscription = this.roleInformationForm.controls['role']?.valueChanges.subscribe((value: Role) => {
      preloadPermissions(this.form, value?.permissions ?? []);
    });

    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });

    this._roleSwitchesCreationObservableSubscription = roleSwitchesObservable(this.form).subscribe();
  }

  ngOnDestroy(): void {
    this._roleFieldSubscription.unsubscribe();
    this._roleCreationSubscription.unsubscribe();
    this._widthSubscription.unsubscribe();
    this._roleSwitchesCreationObservableSubscription.unsubscribe();
  }

  private resetForm(): void {
    this.form.reset();
  }

  public checkValuesSwitchForm(): boolean {
    return formHasSomeValue(this.form, ['roleInformationForm']);
  }

  public onCreateRole(): void {
    if (this.form.invalid) {
      this._validatorService.isValidField(this.form);
      return;
    }

    const newRol: RoleCreationRequest = {
      name: this.roleInformationForm.controls['roleName'].value,
      permissions: getPermissionsFromForm(this.form)
    }
    
    this.isLoading = true;

    this._roleCreationSubscription = this._roleService.createRole(newRol).subscribe({
      next: (userName) => {
        this.isLoading = false;
        this.showSuccessAlertRoleCreation(userName);
      },
      error: (error) => {
        this.isLoading = false;
        error && this.showErrorAlertRoleCreation(error);
      }
    });
  }

  public onCancelRoleCreation(): void {
    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this._router.navigateByUrl('/administration/roles/role-administration');
          this.resetForm();
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false
      },
      message: 'Si hace clic en “Si, cancelar” perderá toda la información diligenciada hasta el momento.',
      highlightText: '¿Está seguro que desea cancelar la creación del rol?'
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }

  public showSuccessAlertRoleCreation(userName: string): void {
    const successAlert: ModalAlert = {
      title: 'Rol creado con éxito',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Continuar',
        primary: true,
        action: () => {
          this.resetForm();
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.resetForm();
        }
      },
      message: `El rol ${userName} ha sido creado en el sistema exitosamente`,
      highlightText: 'Haga click para continuar'
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  public showErrorAlertRoleCreation(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Continuar',
        primary: true,
      },
      message
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }
}
