import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleAdministrationComponent } from './role-administration.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RoleService } from 'src/app/shared/services/role.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchRolesFormComponent } from '../../components/search-roles-form/search-roles-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PermissionSwitchComponent } from '../../components/permission-switch/permission-switch.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, throwError } from 'rxjs';
import { RolesSearchResponse } from 'src/app/shared/interfaces';
import { PermissionsFormComponent } from '../../components/permissions-form/permissions-form.component';
import { RADIOBUTTON_VALUE } from '../../enums/radiobutton-value.enum';

describe('RoleAdministrationComponent', () => {
  let component: RoleAdministrationComponent;
  let fixture: ComponentFixture<RoleAdministrationComponent>;
  let roleService: RoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        RoleAdministrationComponent,
        SearchRolesFormComponent,
        PermissionSwitchComponent,
        PermissionsFormComponent,
      ],
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        SharedModule,
        PrimeNgModule,
      ],
      providers: [
        RoleService,
        HttpRequestService,
      ],
    });
    fixture = TestBed.createComponent(RoleAdministrationComponent);
    roleService = TestBed.inject(RoleService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change page value to 1', () => {
    component.onPageChange(1);
    const pageValue = component.searchRolesForm.get('page')?.value;
    expect(pageValue).toEqual(1);
  });

  it('should change page value to 1 and perPage value to 10', () => {
    component.changePerPage('10');
    const perPageValue = component.searchRolesForm.controls['perPage']?.value;
    const pageValue = component.searchRolesForm.controls['page']?.value;
    expect(perPageValue).toEqual('10');
    expect(pageValue).toEqual(1);
  });

  it('should get any role when searchRoles is executed', () => {
    const response: RolesSearchResponse = {
      count: 0,
      next: null,
      previous: null,
      results: [],
    }

    spyOn(roleService, 'searchRoles').and.returnValue(of(response));

    component.searchRolesForm.controls['role'].setValue(1);
    component.searchRolesForm.controls['state'].setValue(1);
    component.searchRolesForm.controls['consultUsers'].setValue(true);
    component.searchRolesForm.controls['createUsers'].setValue(true);

    component.searchRoles();
    expect(component.roles.length).toEqual(0);
  });

  it('should get one role when onSearchRoles is executed', () => {
    const response: RolesSearchResponse = {
      count: 1,
      next: null,
      previous: null,
      results: [{
        id: 1,
        is_active: true,
        name: 'test',
        permissions: []
      }],
    }

    spyOn(roleService, 'searchRoles').and.returnValue(of(response));

    component.searchRolesForm.controls['role'].setValue(1);
    component.searchRolesForm.controls['state'].setValue(RADIOBUTTON_VALUE.activate);
    component.searchRolesForm.controls['consultUsers'].setValue(true);
    component.searchRolesForm.controls['createUsers'].setValue(true);

    component.onSearchRoles();
    expect(component.roles.length).toEqual(1);
    expect(component.searchRolesForm.controls['page'].value).toEqual(1);
    expect(component.searchRolesForm.controls['perPage'].value).toEqual(10);
  });

  it('should get one role when searchRoles is executed', () => {
    const response: RolesSearchResponse = {
      count: 1,
      next: null,
      previous: null,
      results: [{
        id: 1,
        is_active: true,
        name: 'test',
        permissions: []
      }],
    }

    spyOn(roleService, 'searchRoles').and.returnValue(of(response));

    component.searchRolesForm.controls['role'].setValue(1);
    component.searchRolesForm.controls['state'].setValue(RADIOBUTTON_VALUE.activate);
    component.searchRolesForm.controls['consultUsers'].setValue(true);
    component.searchRolesForm.controls['createUsers'].setValue(true);

    component.searchRoles();
    expect(component.roles.length).toEqual(1);
  });

  it('should get any role when searchRoles is executed with bad request', () => {
    spyOn(roleService, 'searchRoles').and.returnValue(throwError(() => { }));

    component.searchRolesForm.controls['role'].setValue(null);
    component.searchRolesForm.controls['state'].setValue(RADIOBUTTON_VALUE.inactivate);
    component.searchRolesForm.controls['consultUsers'].setValue(true);
    component.searchRolesForm.controls['createUsers'].setValue(true);

    component.searchRoles();
    expect(component.roles.length).toEqual(0);
  });

  it('should clear all form and roles', () => {
    spyOn(component.searchRolesForm, 'reset');
    component.clearSearchRolesForm();

    expect(component.searchRolesForm.reset).toHaveBeenCalled();
    expect(component.roles.length).toEqual(0);
  });
});
