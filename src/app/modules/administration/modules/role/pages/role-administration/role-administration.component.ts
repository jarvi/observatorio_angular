import { Component, ElementRef, ViewChild, inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TooltipOptions } from 'primeng/api';
import { RoleService } from 'src/app/shared/services/role.service';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { formHasSomeValue } from 'src/utils/verify-some-value-on-form';
import { slideInOutAnimation } from 'src/app/shared/animations/animations';
import { perPageOptions } from 'src/app/shared/constants/perpage-options';
import { Role, RolesSearchRequest } from 'src/app/shared/interfaces';
import { getPermissionsFromSwitches } from '../../../../../../../utils/permissions';
import { switchedFormStructure } from 'src/utils/creation-edition-role-validators';
import { RADIOBUTTON_VALUE } from '../../enums/radiobutton-value.enum';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  templateUrl: './role-administration.component.html',
  animations: [slideInOutAnimation],
  styleUrls: ['./role-administration.component.scss']
})
export class RoleAdministrationComponent {

  @ViewChild('roleTable') roleTable!: ElementRef;

  private _fb = inject(FormBuilder);
  private _validatorService = inject(ValidatorService);
  private _roleService = inject(RoleService);

  private rolesSearchSubscription: Subscription = new Subscription();

  public showForm: boolean = true;
  public formIsDirty: boolean = false;
  public isLoading: boolean = false;
  public roles: Role[] = [];
  public totalRoles: number = 0;
  public perpageResultsOptions: string[] = perPageOptions.map((option) => option.toString());
  public rolePermissions = {
    create: PERMISSION_TYPE.addRol.name,
    consult: PERMISSION_TYPE.viewRol.name,
  }
  public tooltipOptions: TooltipOptions = {
    tooltipPosition: 'top',
    tooltipEvent: 'hover',
    positionTop: -10,
    positionLeft: -50,
    tooltipLabel: 'El botón se habilita al diligenciar el campo para la búsqueda',
    tooltipStyleClass: 'role-administration-search-button'
  }

  private scrollToTable() {
    this.roleTable?.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  public searchRolesForm: FormGroup = this._fb.group({
    role: [null],
    state: [null],
    ...switchedFormStructure(),
    perPage: [10],
    page: [1],
  });

  public checkValuesForm(): boolean {
    return formHasSomeValue(this.searchRolesForm, ['perPage', 'page']);
  }

  public clearSearchRolesForm(): void {
    this.searchRolesForm.reset();
    this.searchRolesForm.controls['perPage'].setValue(10);
    this.searchRolesForm.controls['page'].setValue(1);
    this.roles = [];
    this.formIsDirty = false;
  }

  public getRoles(): void {
    this.rolesSearchSubscription.unsubscribe();
    this.isLoading = true;
    this.formIsDirty = true;

    let radioButtonIsActive;
    const state = this.searchRolesForm.value.state;

    if (state === RADIOBUTTON_VALUE.activate) {
      radioButtonIsActive = true;
    } else if (state === RADIOBUTTON_VALUE.inactivate) {
      radioButtonIsActive = false;
    } else {
      radioButtonIsActive = null;
    }

    const rolesSearchRequest: RolesSearchRequest = {
      id: this.searchRolesForm.value.role || '',
      active: radioButtonIsActive ?? '',
      permissions: getPermissionsFromSwitches(this.searchRolesForm),
      page: this.searchRolesForm.value.page,
      page_size: this.searchRolesForm.value.perPage,
    }

    this.rolesSearchSubscription = this._roleService.searchRoles(rolesSearchRequest).subscribe({
      next: (response) => {
        executeCallbackLate(() => this.scrollToTable());

        this.isLoading = false;
        if (response.results.length === 0) {
          this.roles = [];
          return;
        }

        this.totalRoles = response.count;
        this.roles = response.results.map(({ id, name, is_active }) => {
          return {
            id,
            name,
            isActive: is_active,
          }
        });
      },
      error: (error) => {
        this.isLoading = false;
        this.roles = [];
      }
    });
  }

  public onSearchRoles() {
    this.searchRolesForm.controls['perPage'].setValue(10);
    this.searchRolesForm.controls['page'].setValue(1);
    this.searchRoles();
  }

  public searchRoles(): void {
    if (this.searchRolesForm.invalid || !this.checkValuesForm()) {
      this._validatorService.isValidField(this.searchRolesForm);
      return;
    }

    this.getRoles();
  }

  public changePerPage(perPage: string): void {
    this.searchRolesForm.controls['perPage'].setValue(perPage);
    this.searchRolesForm.controls['page'].setValue(1);
    this.searchRoles();
  }

  public onPageChange(page: number): void {
    this.searchRolesForm.controls['page'].setValue(page);
    this.searchRoles();
  }
}
