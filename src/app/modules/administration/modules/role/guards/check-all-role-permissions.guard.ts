import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { map } from 'rxjs';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';
import { PermissionType } from 'src/app/shared/interfaces/permission-type.interface';

export const checkAllRolePermissionsGuard: CanActivateFn = (route, state) => {

  const router = inject(Router);
  const userPermissionsService = inject(UserPermissionsService);

  const verifyRolePermissions = (permissions: PermissionType[]): boolean => {
    const userPermissionsList = permissions.map((permission) => permission.codename);
    const rolePermissions = [
      PERMISSION_TYPE.changeRol.name,
      PERMISSION_TYPE.deleteRol.name,
      PERMISSION_TYPE.viewRol.name,
    ]

    const permissionsMatch: boolean = rolePermissions.some((rolePermission: string) => userPermissionsList.includes(rolePermission));

    if (!permissionsMatch) {
      router.navigateByUrl('/administration/roles/role-creation')
    }

    return permissionsMatch;
  }

  return userPermissionsService.getPermissions().pipe(
    map((permissions) => {
      return verifyRolePermissions(permissions);
    })
  );
};
