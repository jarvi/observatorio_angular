import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, CanActivateFn, Router } from '@angular/router';

import { checkAllRolePermissionsGuard } from './check-all-role-permissions.guard';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';
import { of, take } from 'rxjs';

describe('checkAllRolePermissionsGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => checkAllRolePermissionsGuard(...guardParameters));

  let router: Router;
  let userPermissionsService: UserPermissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        UserPermissionsService,
        HttpRequestService,
      ]
    });
    router = TestBed.inject(Router);
    userPermissionsService = TestBed.inject(UserPermissionsService);
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });

  it('should access to route if the user has some role permission', () => {
    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.changeRol.name,
    }]));

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const executeGuardResult: any = executeGuard(route, state);

    executeGuardResult.pipe(take(1))
    .subscribe((result: boolean) => {
      expect(result).toBeTrue();
    });
  });

  it('should deny to route if the user does not have any role permission', () => {
    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.addReport.name,
    }]));
    spyOn(router, 'navigateByUrl');

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const executeGuardResult: any = executeGuard(route, state);

    executeGuardResult.pipe(take(1))
    .subscribe((result: boolean) => {
      expect(result).toBeFalse();
    });
  });
});
