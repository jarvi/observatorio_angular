import { Component } from '@angular/core';
import { Permission } from '../../interfaces/permission.interface';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

@Component({
  selector: 'role-permissions-form',
  templateUrl: './permissions-form.component.html',
  styleUrls: ['./permissions-form.component.scss']
})
export class PermissionsFormComponent {

  public userPermissions: Permission[] = [
    { id: 'consult-users', formControlName: PERMISSION_TYPE.viewUser.formName, name: 'Consultar usuarios', value: PERMISSION_TYPE.viewUser.name },
    { id: 'create-users', formControlName: PERMISSION_TYPE.addUser.formName, name: 'Crear usuarios', value: PERMISSION_TYPE.addUser.name },
    { id: 'edit-users', formControlName: PERMISSION_TYPE.changeUser.formName, name: 'Editar usuarios', value: PERMISSION_TYPE.changeUser.name },
    { id: 'inactivate-users', formControlName: PERMISSION_TYPE.deleteUser.formName, name: 'Inactivar usuarios', value: PERMISSION_TYPE.deleteUser.name },
  ];
  public rolePermissions: Permission[] = [
    { id: 'consult-roles', formControlName: PERMISSION_TYPE.viewRol.formName, name: 'Consultar roles', value: PERMISSION_TYPE.viewRol.name },
    { id: 'create-roles', formControlName: PERMISSION_TYPE.addRol.formName, name: 'Crear roles', value: PERMISSION_TYPE.addRol.name },
    { id: 'edit-roles', formControlName: PERMISSION_TYPE.changeRol.formName, name: 'Editar roles', value: PERMISSION_TYPE.changeRol.name },
    { id: 'inactivate-roles', formControlName: PERMISSION_TYPE.deleteRol.formName, name: 'Inactivar roles', value: PERMISSION_TYPE.deleteRol.name },
  ];
  public dataSourcesPermissions: Permission[] = [
    { id: 'manage-data-sources', formControlName: PERMISSION_TYPE.addSource.formName, name: 'Gestionar fuentes de datos', value: PERMISSION_TYPE.addSource.name },
  ];
  public biBoardsPermissions: Permission[] = [
    { id: 'view-boards', formControlName: PERMISSION_TYPE.viewBoard.formName, name: 'Ver tableros', value: PERMISSION_TYPE.viewBoard.name },
  ];
  public notificationsPermissions: Permission[] = [
    { id: 'consult-notifications', formControlName: PERMISSION_TYPE.viewNotification.formName, name: 'Consultar notificaciones', value: PERMISSION_TYPE.viewNotification.name },
    { id: 'edit-notifications', formControlName: PERMISSION_TYPE.changeNotification.formName, name: 'Editar notificaciones', value: PERMISSION_TYPE.changeNotification.name },
  ];
  public reportsPermissions: Permission[] = [
    { id: 'generate-Reports', formControlName: PERMISSION_TYPE.addReport.formName, name: 'Generar informes', value: PERMISSION_TYPE.addReport.name },
    { id: 'view-Reports', formControlName: PERMISSION_TYPE.viewReport.formName, name: 'Ver informes', value: PERMISSION_TYPE.viewReport.name },
  ];
}
