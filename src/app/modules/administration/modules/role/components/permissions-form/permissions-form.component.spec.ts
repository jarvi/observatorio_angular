import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionsFormComponent } from './permissions-form.component';
import { PermissionSwitchComponent } from '../permission-switch/permission-switch.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { ControlContainer, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Component, inject } from '@angular/core';
import { RoleModule } from '../../role.module';

@Component({
  standalone: true,
  template: `
    <form [formGroup]="form">
      <role-permissions-form />
    </form>
  `,
  imports: [
    RoleModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
class TestComponent {
  private _fb = inject(FormBuilder);

  form: FormGroup = this._fb.group({
    consultUsers: [false],
    createUsers: [false],
    editUsers: [false],
    inactivateUsers: [false],
    consultRoles: [false],
    createRoles: [false],
    editRoles: [false],
    inactivateRoles: [false],
    manageDataSources: [false],
    viewBoards: [false],
    viewNotifications: [false],
    editNotifications: [false],
    generateReports: [false],
    viewReports: [false],
  });
}

describe('PermissionsFormComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        PrimeNgModule
      ],
      providers: [
        { provide: ControlContainer }
      ]
    });
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
