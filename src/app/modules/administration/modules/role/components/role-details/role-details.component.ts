import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { RoleService } from 'src/app/shared/services/role.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { Role, ModalDetails } from 'src/app/shared/interfaces';

@Component({
  selector: 'role-role-details',
  templateUrl: './role-details.component.html'
})
export class RoleDetailsComponent implements OnChanges, OnDestroy {

  @Input() public roleId: number = 0;
  @Output() public roleIdChange: EventEmitter<number> = new EventEmitter<number>();
  
  private _roleService = inject(RoleService);
  public spinnerService = inject(SpinnerService);

  private _subscription: Subscription = new Subscription();

  public modalVisible: boolean = false;
  public roleName: string = '';
  public roleInformationDetails: ModalDetails[][] = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['roleId']) {
      this.onVisibleChange(this.roleId > 0);
      
      if (!this.roleId) return;
      this.getRole(this.roleId);
    }
  }
  
  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  private getRole(id: number) {
    this._subscription.unsubscribe();

    this.spinnerService.showSpinner = true;
    this._subscription = this._roleService.getRole(id).subscribe({
      next: (role) => {
        this.roleName = role.name;
        this.spinnerService.showSpinner = false;

        this.buildDetails(role);
      },
      error: () => {
        this.spinnerService.showSpinner = false;
        this.onVisibleChange(false);
      }
    });
  }

  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
    this.roleIdChange.emit(visible ? this.roleId : 0);
  }

  private buildDetails(role: Role) {
    this.roleInformationDetails = [
      [
        { label: 'Concepto', value: role.name },
        { label: 'Estado', value: role.isActive ? 'Activo' : 'Inactivo' },
      ],
      [
        { label: 'Privilegios', iterableValue: role.permissions }
      ]
    ];
  }
}
