import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleDetailsComponent } from './role-details.component';
import { RoleService } from 'src/app/shared/services/role.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { Role } from 'src/app/shared/interfaces';
import { of, throwError } from 'rxjs';

describe('RoleDetailsComponent', () => {
  let component: RoleDetailsComponent;
  let fixture: ComponentFixture<RoleDetailsComponent>;
  let roleService: RoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RoleDetailsComponent],
      imports: [
        HttpClientTestingModule,
        SharedModule
      ],
      providers: [
        RoleService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(RoleDetailsComponent);
    roleService = TestBed.inject(RoleService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit roleIdChange value when onVisibleChange is executed', () => {
    spyOn(component.roleIdChange, 'emit');

    component.onVisibleChange(true);
    expect(component.roleIdChange.emit).toHaveBeenCalledWith(component.roleId);
  });

  it('should emit roleIdChange value when onVisibleChange is executed with false', () => {
    spyOn(component.roleIdChange, 'emit');

    component.onVisibleChange(false);
    expect(component.roleIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should call ngOnChanges when roleId changes and change role name', () => {
    const role: Role = {
      id: 1,
      isActive: true,
      name: 'Prueba',
      permissions: []
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(of(role))

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({ roleId: {
      previousValue: '',
      currentValue: newRoleId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.roleName).toEqual(role.name);
  });

  it('should call ngOnChanges when roleId changes and change role name when is inactivate', () => {
    const role: Role = {
      id: 1,
      isActive: false,
      name: 'Prueba',
      permissions: []
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(of(role))

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({ roleId: {
      previousValue: '',
      currentValue: newRoleId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.roleName).toEqual(role.name);
  });

  it('should call ngOnChanges when roleId changes with bad request and call the onVisibleChange method', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(throwError(() => {}));
    spyOn(component, 'onVisibleChange');

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({ roleId: {
      previousValue: '',
      currentValue: newRoleId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.onVisibleChange).toHaveBeenCalledWith(false);
  });

  it('should call ngOnChanges when roleId changes and call the onVisibleChange method', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(component, 'onVisibleChange');

    const newRoleId = 0;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({ roleId: {
      previousValue: '',
      currentValue: newRoleId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.onVisibleChange).toHaveBeenCalledWith(false);
  });

  it('should call ngOnChanges when roleId changes and does not do anything if the role returned is same', () => {
    const role: Role = {
      id: 1,
      isActive: true,
      name: 'Prueba',
      permissions: []
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(of(role))

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({ roleId: {
      previousValue: '',
      currentValue: newRoleId,
      firstChange: true,
      isFirstChange: () => true
    }});
    component.ngOnChanges({ roleId: {
      previousValue: '',
      currentValue: newRoleId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.roleName).toEqual(role.name);
  });
});
