import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionSwitchComponent } from './permission-switch.component';

describe('PermissionSwitchComponent', () => {
  let component: PermissionSwitchComponent;
  let fixture: ComponentFixture<PermissionSwitchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PermissionSwitchComponent]
    });
    fixture = TestBed.createComponent(PermissionSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
