import { Component, Input, SkipSelf } from '@angular/core';
import { Permission } from '../../interfaces/permission.interface';
import { ControlContainer } from '@angular/forms';

@Component({
  selector: 'role-permission-switch',
  templateUrl: './permission-switch.component.html',
  styleUrls: ['./permission-switch.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: (container: ControlContainer) => container,
      deps: [[new SkipSelf(), ControlContainer]]
    }
  ]
})
export class PermissionSwitchComponent {

  @Input() public permissions: Permission[] = [];
  @Input() public label: string = '';
}
