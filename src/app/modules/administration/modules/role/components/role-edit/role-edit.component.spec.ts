import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleEditComponent } from './role-edit.component';
import { RoleService } from 'src/app/shared/services/role.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { RoleInformationFormComponent } from '../role-information-form/role-information-form.component';
import { PermissionSwitchComponent } from '../permission-switch/permission-switch.component';
import { PermissionsFormComponent } from '../permissions-form/permissions-form.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import Swal from 'sweetalert2';
import { Role } from 'src/app/shared/interfaces';
import { of, throwError } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

describe('RoleEditComponent', () => {
  let component: RoleEditComponent;
  let fixture: ComponentFixture<RoleEditComponent>;
  let roleService: RoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        RoleEditComponent,
        RoleInformationFormComponent,
        PermissionsFormComponent,
        PermissionSwitchComponent,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        RoleService,
        HttpRequestService,
      ],
    });
    fixture = TestBed.createComponent(RoleEditComponent);
    roleService = TestBed.inject(RoleService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    Swal.close();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnChanges when roleId changes and change role name', () => {
    const role: Role = {
      id: 1,
      isActive: true,
      name: 'Administrador',
      permissions: undefined,
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(of(role))

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({
      roleId: {
        previousValue: '',
        currentValue: newRoleId,
        firstChange: true,
        isFirstChange: () => true
      }
    });

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.roleName).toEqual(role.name);
  });

  it('should call ngOnChanges when roleId changes and change role name', () => {
    const role: Role = {
      id: 1,
      isActive: true,
      name: 'Administrador',
      permissions: [PERMISSION_TYPE.addReport.labeledValue, 'Another_value'],
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(of(role))

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({
      roleId: {
        previousValue: '',
        currentValue: newRoleId,
        firstChange: true,
        isFirstChange: () => true
      }
    });

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.roleName).toEqual(role.name);
  });

  it('should call ngOnChanges when roleId changes with bad request and call the onVisibleChange method', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(throwError(() => { }));
    spyOn(component, 'onVisibleChange');

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({
      roleId: {
        previousValue: '',
        currentValue: newRoleId,
        firstChange: true,
        isFirstChange: () => true
      }
    });

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.onVisibleChange).toHaveBeenCalledWith(false);
  });

  it('should call ngOnChanges when roleId changes and does not do anything if the role returned is same', () => {
    const role: Role = {
      id: 1,
      isActive: true,
      name: 'Administrador',
      permissions: [],
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(roleService, 'getRole').and.returnValue(of(role))

    const newRoleId = 1;
    component.roleId = newRoleId;
    fixture.detectChanges();
    component.ngOnChanges({
      roleId: {
        previousValue: '',
        currentValue: newRoleId,
        firstChange: true,
        isFirstChange: () => true
      }
    });
    component.ngOnChanges({
      roleId: {
        previousValue: '',
        currentValue: newRoleId,
        firstChange: true,
        isFirstChange: () => true
      }
    });

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.roleName).toEqual(role.name);
  });

  it('should show warning modal alert when onCancel method is executed and emit roleIdChange value when is confirmed', () => {
    spyOn(component.roleIdChange, 'emit');

    component.onCancel();
    expect(Swal.isVisible()).toBeTrue();
    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();

    expect(component.roleIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should show warning modal alert when onCancel method is executed and emit roleIdChange value when is canceled', () => {
    spyOn(component.roleIdChange, 'emit');

    component.onCancel();
    expect(Swal.isVisible()).toBeTrue();
    const confirmButton = Swal.getCancelButton()!;
    confirmButton.click();

    expect(component.roleIdChange.emit).toHaveBeenCalledWith(component.roleId);
  });

  it('should exec on edit role method when all is ok', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();

    const roleName = compiled.querySelector('#roleName') as HTMLInputElement;
    const switchRole = compiled.querySelector('#consult-users') as HTMLInputElement;

    roleName.value = 'Lector';
    switchRole.value = 'true';

    roleName.dispatchEvent(new Event('input'));
    switchRole.dispatchEvent(new Event('input'));

    component.onEditRole();

    expect(component.form.valid).toBeTruthy();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should exec on edit role method when all is ok and show a modal warning alert and press the confirm button', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();

    const roleName = compiled.querySelector('#roleName') as HTMLInputElement;
    const switchRole = compiled.querySelector('#consult-users') as HTMLInputElement;

    roleName.value = 'Lector';
    switchRole.value = 'true';

    roleName.dispatchEvent(new Event('input'));
    switchRole.dispatchEvent(new Event('input'));

    component.onEditRole();

    const cancelButton = Swal.getConfirmButton()!;
    cancelButton.click();

    expect(component.form.valid).toBeTruthy();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should exec edit role method when all is ok and emit with confirm button', () => {
    const roleResponse: Role = {
      id: 1,
      isActive: true,
      name: 'Administrador',
      permissions: undefined,
    }

    spyOn(roleService, 'editRole').and.returnValue(of(roleResponse));
    spyOn(component.roleIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();

    const roleName = compiled.querySelector('#roleName') as HTMLInputElement;
    const switchRole = compiled.querySelector('#consult-users') as HTMLInputElement;

    roleName.value = 'Lector';
    switchRole.value = 'true';

    roleName.dispatchEvent(new Event('input'));
    switchRole.dispatchEvent(new Event('input'));

    expect(component.form.valid).toBeTruthy();

    component.editRole();

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();

    expect(roleService.editRole).toHaveBeenCalled();
    expect(component.roleIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should exec edit role method when all is ok and emit with cancel button', () => {
    const roleResponse: Role = {
      id: 1,
      isActive: true,
      name: 'Administrador',
      permissions: undefined,
    }

    spyOn(roleService, 'editRole').and.returnValue(of(roleResponse));
    spyOn(component.roleIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();

    const roleName = compiled.querySelector('#roleName') as HTMLInputElement;
    const switchRole = compiled.querySelector('#consult-users') as HTMLInputElement;

    roleName.value = 'Lector';
    switchRole.value = 'true';

    roleName.dispatchEvent(new Event('input'));
    switchRole.dispatchEvent(new Event('input'));

    expect(component.form.valid).toBeTruthy();

    component.editRole();

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getCancelButton()!;
    confirmButton.click();

    expect(roleService.editRole).toHaveBeenCalled();
    expect(component.roleIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should exec edit role method with bad request and show a modal error alert and press the confirm button', () => {

    spyOn(roleService, 'editRole').and.returnValue(throwError(() => 'Error'));
    spyOn(component.roleIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();

    const roleName = compiled.querySelector('#roleName') as HTMLInputElement;
    const switchRole = compiled.querySelector('#consult-users') as HTMLInputElement;

    roleName.value = 'Lector';
    switchRole.value = 'true';

    roleName.dispatchEvent(new Event('input'));
    switchRole.dispatchEvent(new Event('input'));

    expect(component.form.valid).toBeTruthy();

    component.editRole();

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();

    expect(roleService.editRole).toHaveBeenCalled();
    expect(component.roleIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should exec edit role method with bad request and show a modal error alert and press the cancel button', () => {

    spyOn(roleService, 'editRole').and.returnValue(throwError(() => 'Error'));
    spyOn(component.roleIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();

    const roleName = compiled.querySelector('#roleName') as HTMLInputElement;
    const switchRole = compiled.querySelector('#consult-users') as HTMLInputElement;

    roleName.value = 'Lector';
    switchRole.value = 'true';

    roleName.dispatchEvent(new Event('input'));
    switchRole.dispatchEvent(new Event('input'));

    expect(component.form.valid).toBeTruthy();

    component.editRole();

    expect(Swal.isVisible()).toBeTruthy();

    const cancelButton = Swal.getCancelButton()!;
    cancelButton.click();

    expect(roleService.editRole).toHaveBeenCalled();
    expect(component.roleIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should not exec on edit role method when form is invalid', () => {
    component.onEditRole();
    expect(component.form.valid).toBeFalsy();
  });

  it('should not be valid the switches validations', () => {
    const switchesAreValid = component.checkValuesSwitchForm();
    expect(switchesAreValid).toBeFalsy();
  });

  it('should be activated consultUsers when selecting inactivateUsers', () => {
    component.form.controls['inactivateUsers'].setValue(true);
    expect(component.form.controls['consultUsers'].value).toEqual('view_user');
  });

  it('should be activated consultUsers when selecting editUsers', () => {
    component.form.controls['editUsers'].setValue(true);
    expect(component.form.controls['consultUsers'].value).toEqual('view_user');
  });

  it('should be activated consultRoles when selecting editUsers', () => {
    component.form.controls['editUsers'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting createUsers', () => {
    component.form.controls['createUsers'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting inactivateRoles', () => {
    component.form.controls['inactivateRoles'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting editRoles', () => {
    component.form.controls['editRoles'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });

  it('should be activated consultRoles when selecting createRoles', () => {
    component.form.controls['createRoles'].setValue(true);
    expect(component.form.controls['consultRoles'].value).toEqual(PERMISSION_TYPE.viewRol.name);
  });
});
