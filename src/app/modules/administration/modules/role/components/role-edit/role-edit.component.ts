import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { RoleService } from 'src/app/shared/services/role.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { ModalAlert, Role, RoleEditionRequest } from 'src/app/shared/interfaces';
import { getPermissionsFromForm, getPermissionsFromLabels, preloadPermissions } from 'src/utils/permissions';
import { formHasSomeValue } from 'src/utils/verify-some-value-on-form';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { switchedFormStructure } from 'src/utils/creation-edition-role-validators';
import { roleSwitchesObservable } from '../../utils/role-switches-observable';

@Component({
  selector: 'role-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss']
})
export class RoleEditComponent implements OnChanges, OnDestroy {

  @Input() public roleId: number = 0;
  @Output() public roleIdChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() public roleEdited: EventEmitter<Role> = new EventEmitter<Role>();

  private _fb = inject(FormBuilder);
  private _validatorService = inject(ValidatorService);
  private _roleService = inject(RoleService);
  private _modalAlertService = inject(ModalAlertService);
  public spinnerService = inject(SpinnerService);

  private _roleSwitchesEditionObservableSubscription: Subscription = new Subscription();
  
  private _roleEditionSubscription: Subscription = new Subscription();
  private _getRolesubscription: Subscription = new Subscription();
  private _roleToEdit: Role = {
    id: 0,
    name: '',
    isActive: false,
    permissions: []
  };

  public modalVisible: boolean = false;
  public roleName: string = '';

  public roleInformationForm: FormGroup = this._fb.group({
    roleName: ['', [
      Validators.required, 
      Validators.maxLength(30),
      this._validatorService.whiteSpacesStartValidator()
    ]],
  });

  public form: FormGroup = this._fb.group({
    roleInformationForm: this.roleInformationForm,
    ...switchedFormStructure()
  });

  ngOnInit() {
    this._roleSwitchesEditionObservableSubscription = roleSwitchesObservable(this.form).subscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['roleId']) {
      const isRoleIdPresent = this.roleId > 0;
      this.onVisibleChange(isRoleIdPresent);
      
      if (!this.roleId) return;
      this.getRole(this.roleId);
    }
  }

  ngOnDestroy(): void {
    this._getRolesubscription.unsubscribe();
    this._roleEditionSubscription.unsubscribe();
    this._roleSwitchesEditionObservableSubscription.unsubscribe();
  }
  
  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
    this.roleIdChange.emit(visible ? this.roleId : 0);
  }

  private returnRoleEdited(roleEdited: Role) {
    this.roleEdited.emit(roleEdited);
  }

  private getRole(id: number): void {
    this._getRolesubscription.unsubscribe();

    this.spinnerService.showSpinner = true;

    this._getRolesubscription = this._roleService.getRole(id).subscribe({
      next: (role) => {
        this._roleToEdit = {
          id: role.id,
          isActive: role.isActive,
          name: role.name,
          permissions: getPermissionsFromLabels(role.permissions ?? [])
        };
        this.roleName = role.name;
        this.spinnerService.showSpinner = false;
        
        this.fillForm();
      },
      error: () => {
        this.spinnerService.showSpinner = false;
        this.onVisibleChange(false);
      }
    });
  }

  private fillForm(): void {
    this.form.patchValue({
      roleInformationForm: {
        roleName: this._roleToEdit.name,
      },
    });
    
    this._roleToEdit.permissions && preloadPermissions(this.form, this._roleToEdit.permissions);
  }

  public checkValuesSwitchForm(): boolean {
    return formHasSomeValue(this.form, ['roleInformationForm']);
  }

  public editRole(): void {
    const roleToEdit: RoleEditionRequest = {
      id: this._roleToEdit.id,
      name: this.form.value.roleInformationForm.roleName,
      permissions: getPermissionsFromForm(this.form)
    }
    
    this.spinnerService.showSpinner = true;

    this._roleEditionSubscription = this._roleService.editRole(roleToEdit).subscribe({
      next: (roleEdited) => {
        this.spinnerService.showSpinner = false;
        this.showSuccessAlert(roleEdited.name);
        this.returnRoleEdited(roleEdited);
      },
      error: (error) => {
        this.spinnerService.showSpinner = false;
        error && this.showErrorAlert(error);
      }
    });
  }

  private showErrorAlert(error: string): void {
    this.onVisibleChange(false);

    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
        action: () => {
          this.form.reset();
          this.onVisibleChange(false);
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.form.reset();
          this.onVisibleChange(false);
        }
      },
      highlightText: error
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private showWarningAlert(roleName: string): void {
    this.onVisibleChange(false);

    const warningAlert: ModalAlert = {
      title: 'Actualizar rol',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, actualizar',
        primary: true,
        action: () => {
          this.editRole();
        }
      },
      cancelButton: {
        label: 'Cancelar',
        primary: false,
        action: () => {
          this.form.reset();
          this.onVisibleChange(false);
        }
      },
      highlightText: `¿Está seguro que desea actualizar la información del rol ${roleName}?`
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }

  private showSuccessAlert(roleName: string): void {
    const successAlert: ModalAlert = {
      title: 'Rol actualizado',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
        action: () => {
          this.form.reset();
          this.onVisibleChange(false);
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.form.reset();
          this.onVisibleChange(false);
        }
      },
      message: `El rol ${ roleName } se ha actualizado exitosamente.`,
      highlightText: 'Haga click para continuar'
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  public onEditRole(): void {
    if (this.form.invalid) {
      this._validatorService.isValidField(this.roleInformationForm);
      return;
    }

    this.showWarningAlert(this.roleName);
  }

  public onCancel(): void {
    this.onVisibleChange(false);

    const warningAlert: ModalAlert = {
      title: 'Cancelar edición',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this.form.reset();
          this.onVisibleChange(false);
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
        action: () => {
          this.onVisibleChange(true);
        }
      },
      highlightText: `¿Está seguro que desea cancelar la edición del rol ${ this.roleName }?`
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }
}
