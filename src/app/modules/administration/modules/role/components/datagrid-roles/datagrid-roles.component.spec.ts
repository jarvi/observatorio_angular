import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatagridRolesComponent } from './datagrid-roles.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RoleService } from 'src/app/shared/services/role.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import Swal from 'sweetalert2';
import { RoleDetailsComponent } from '../role-details/role-details.component';
import { of } from 'rxjs';
import { RoleEditComponent } from '../role-edit/role-edit.component';
import { RoleInformationFormComponent } from '../role-information-form/role-information-form.component';
import { PermissionsFormComponent } from '../permissions-form/permissions-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PermissionSwitchComponent } from '../permission-switch/permission-switch.component';
import { Role } from 'src/app/shared/interfaces';

describe('DatagridRolesComponent', () => {
  let component: DatagridRolesComponent;
  let fixture: ComponentFixture<DatagridRolesComponent>;
  let roleService: RoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        DatagridRolesComponent,
        RoleDetailsComponent,
        RoleEditComponent,
        RoleInformationFormComponent,
        PermissionsFormComponent,
        PermissionSwitchComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule
      ],
      providers: [
        RoleService,
        HttpRequestService,
      ]
    });
    fixture = TestBed.createComponent(DatagridRolesComponent);
    roleService = TestBed.inject(RoleService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit pageChange value when onPageChange is executed', () => {
    spyOn(component.pageChange, 'emit');

    component.onPageChange(1);
    expect(component.pageChange.emit).toHaveBeenCalledWith(1);
  });

  it('should change roleToGet value', () => {
    component.onShowRoleDetails(1);
    expect(component.roleToGet).toEqual(1);
  });

  it('should show modal error alert when admin role is try to inactive', () => {
    spyOn(roleService, 'activateRole').and.returnValue(of(true));

    component.roles = [{
      id: 1,
      isActive: true,
      name: 'Administrador',
      permissions: []
    }];

    component.onSwitchClick(true, 1, 'Administrador');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal error alert when admin role is try to edit', () => {
    component.onShowEditRole(1, 'Administrador');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal warning alert to inactivate role and press confirm button to inactivate role', () => {
    spyOn(roleService, 'activateRole').and.returnValue(of(true));

    component.roles = [{
      id: 1,
      isActive: true,
      name: 'Prueba',
      permissions: []
    }];

    component.onSwitchClick(true, 1, 'Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal success alert when activateRole method is executed', () => {

    component.roles = [{
      id: 1,
      isActive: true,
      name: 'Prueba',
      permissions: []
    }];

    spyOn(roleService, 'activateRole').and.returnValue(of(true));

    component.activateRole(true, 1, 'Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal success alert when activateRole method is executed with false', () => {

    component.roles = [{
      id: 1,
      isActive: true,
      name: 'Prueba',
      permissions: []
    }];

    spyOn(roleService, 'activateRole').and.returnValue(of(true));

    component.activateRole(false, 1, 'Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal error alert when activateRole method is executed', () => {

    component.roles = [{
      id: 1,
      isActive: true,
      name: 'Prueba',
      permissions: []
    }];
    
    spyOn(roleService, 'activateRole').and.returnValue(of(false));

    component.activateRole(true, 1, 'Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal warning alert to activate role and press confirm button to activate role', () => {
    spyOn(roleService, 'activateRole').and.returnValue(of(true));

    component.roles =  [{
      id: 1,
      isActive: true,
      name: 'Prueba',
      permissions: []
    }];

    component.onSwitchClick(false, 1, 'Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should change roleToEdit value', () => {
    component.onShowEditRole(1);
    expect(component.roleToEdit).toEqual(1);
  });

  it('should edit an specific role', () => {
    component.roles = [
      {
        id: 1,
        isActive: true,
        name: 'Prueba',
        permissions: []
      },
      {
        id: 2,
        isActive: true,
        name: 'Prueba 2',
        permissions: []
      }
    ]

    const roleEdited: Role = {
      id: 1,
      isActive: true,
      name: 'Prueba 11',
      permissions: [] 
    }
    component.changeRoleEdited(roleEdited);
    
    const rolesExpected = component.roles.find(role => role.name === 'Prueba 11');
    expect(rolesExpected).toEqual(roleEdited);
  });
});
