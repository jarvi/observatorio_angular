import { Component, EventEmitter, Input, OnDestroy, Output, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { RoleService } from 'src/app/shared/services/role.service';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { ModalAlert, Role, RoleActivateRequest } from 'src/app/shared/interfaces';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

@Component({
  selector: 'role-datagrid-roles',
  templateUrl: './datagrid-roles.component.html',
  styleUrls: ['./datagrid-roles.component.scss']
})
export class DatagridRolesComponent implements OnDestroy {

  @Input() public roles: Role[] = [];
  @Input() public totalRoles: number = 0;
  @Input() public perPage: number = 0;
  @Input() public currentPage: number = 0;
  @Output() public pageChange: EventEmitter<number> = new EventEmitter<number>();

  private _modalAlertService = inject(ModalAlertService);
  private _roleService = inject(RoleService);
  public spinnerService = inject(SpinnerService);

  private _activateRoleSubscription: Subscription = new Subscription();

  public roleToGet: number = 0;
  public roleToEdit: number = 0;
  public rolePermissions = {
    inactive: PERMISSION_TYPE.deleteRol.name,
    edit: PERMISSION_TYPE.changeRol.name,
    details: PERMISSION_TYPE.viewRol.name,
  };

  ngOnDestroy(): void {
    this._activateRoleSubscription.unsubscribe();
  }

  public activateRole(activate: boolean, roleId: number, roleName: string): void {
    this.spinnerService.showSpinner = true;

    const request: RoleActivateRequest = {
      id: roleId,
      is_active: activate
    }

    this._activateRoleSubscription = this._roleService.activateRole(request).subscribe((response) => {
      if (response) {
        this.spinnerService.showSpinner = false;
  
        this.roles = this.roles.map((role) => {
          if (role.id === roleId) role.isActive = activate;
          return role;
        });
  
        this.showActivateInactivateRoleSuccessAlert(!activate, roleName);
      } else {
        this.spinnerService.showSpinner = false;
        this.showErrorModalToActivateInactivateRole(roleName);
      }
    });
  }

  private showErrorModalToActivateInactivateRole(roleName: string): void {

    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message: `El rol ${roleName} tiene usuarios activos asociados y no puede ser inactivado.`,
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private showErrorModalAdminRole(roleName: string, isEdit: boolean): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message: isEdit ? `No se debe editar los privilegios del rol ${roleName}.` : `No se debe inactivar rol ${roleName}.`,
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private showActivateInactivateRoleAlert(isActivate: boolean, roleId: number, roleName: string): void {
    const optionString = isActivate ? 'Inactivar' : 'Activar';

    if (roleName.toLowerCase() === 'administrador') {
      this.showErrorModalAdminRole(roleName, false);
      return;
    }

    const warinigAlert: ModalAlert = {
      title: `${optionString} rol`,
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: `Si, ${optionString.toLowerCase()}`,
        primary: true,
        action: () => {
          this.activateRole(!isActivate, roleId, roleName);
        }
      },
      cancelButton: {
        label: 'Cancelar',
        primary: false,
      },
      highlightText: `¿Está seguro que desea ${optionString.toLowerCase()} el rol ${roleName}?`,
    }

    this._modalAlertService.showModalAlert(warinigAlert);
  }

  private showActivateInactivateRoleSuccessAlert(isActivate: boolean, roleName: string): void {
    const optionString = isActivate ? 'inactivado' : 'activado';

    const successAlert: ModalAlert = {
      title: `Usuario ${optionString}`,
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Aceptar',
        primary: true
      },
      message: `El rol ${roleName} se ha ${optionString} exitosamente.`,
      highlightText: 'Haga click para continuar',
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  public onPageChange(page: number): void {
    this.pageChange.emit(page);
  }

  public onShowRoleDetails(id: number): void {
    this.roleToGet = id;
  }

  public onShowEditRole(id: number, roleName?: string): void {
    if (roleName && roleName.toLowerCase() === 'administrador') {
      this.showErrorModalAdminRole(roleName, true);
      return;
    }

    this.roleToEdit = id;
  }

  public onSwitchClick(isActivate: boolean, roleId: number, roleName: string): void {
    this.showActivateInactivateRoleAlert(isActivate, roleId, roleName);
  }
  
  public changeRoleEdited(roleEdited: Role) {
    this.roles = this.roles.map(role =>
      role.id === roleEdited.id ? roleEdited : role
    );
  }
}
