import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { RoleService } from 'src/app/shared/services/role.service';
import { InputRadiobutton, Role } from 'src/app/shared/interfaces';
import { RADIOBUTTON_VALUE } from '../../enums/radiobutton-value.enum';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'role-search-roles-form',
  templateUrl: './search-roles-form.component.html',
  styleUrls: ['./search-roles-form.component.scss'],
})
export class SearchRolesFormComponent implements OnInit, OnDestroy {

  @Input() public searchRolesForm: FormGroup = new FormGroup({});

  private _cdr = inject(ChangeDetectorRef);
  private _roleService = inject(RoleService);

  private _rolesSubscription: Subscription = new Subscription();

  public roles: Role[] = [];
  public radioButtonOptions: InputRadiobutton<string>[] = [
    { id: 'active', name: 'Activo', value: RADIOBUTTON_VALUE.activate },
    { id: 'inactive', name: 'Inactivo', value: RADIOBUTTON_VALUE.inactivate },
  ];

  ngOnInit(): void {
    executeCallbackLate(() => {
      this._rolesSubscription = this._roleService.getRoles().subscribe((role) => {
        this.roles = role;
        this._cdr.detectChanges();
      });
    });
  }

  ngOnDestroy(): void {
    this._rolesSubscription.unsubscribe();
  }
}
