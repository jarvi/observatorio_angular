import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { SearchRolesFormComponent } from './search-roles-form.component';
import { RoleService } from 'src/app/shared/services/role.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PermissionSwitchComponent } from '../permission-switch/permission-switch.component';
import { PermissionsFormComponent } from '../permissions-form/permissions-form.component';
import { of } from 'rxjs';

describe('SearchRolesFormComponent', () => {
  let component: SearchRolesFormComponent;
  let fixture: ComponentFixture<SearchRolesFormComponent>;
  let roleService: RoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchRolesFormComponent,
        PermissionSwitchComponent,
        PermissionsFormComponent,
      ],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        RoleService,
        HttpRequestService
      ],
    });
    fixture = TestBed.createComponent(SearchRolesFormComponent);
    roleService = TestBed.inject(RoleService);

    spyOn(roleService, 'getRoles').and.returnValue(of([{
      id: 1,
      name: 'Administrador',
      isActive: true,
    }]));

    component = fixture.componentInstance;
    component.searchRolesForm = new FormGroup({
      role: new FormControl(''),
      state: new FormControl(''),
      consultUsers: new FormControl(''),
      createUsers: new FormControl(''),
      editUsers: new FormControl(''),
      inactivateUsers: new FormControl(''),
      consultRoles: new FormControl(''),
      createRoles: new FormControl(''),
      editRoles: new FormControl(''),
      inactivateRoles: new FormControl(''),
      manageDataSources: new FormControl(''),
      viewBoards: new FormControl(''),
      viewNotifications: new FormControl(''),
      editNotifications: new FormControl(''),
      generateReports: new FormControl(''),
      viewReports: new FormControl(''),
      perPage: new FormControl(''),
      page: new FormControl(''),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should verify the quantity of roles', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(component.roles.length).toBeGreaterThan(0);
  }));
});
