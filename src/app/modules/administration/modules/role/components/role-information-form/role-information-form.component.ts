import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, inject } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { RoleService } from 'src/app/shared/services/role.service';
import { Role } from 'src/app/shared/interfaces';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'role-role-information-form',
  templateUrl: './role-information-form.component.html',
  styleUrls: ['./role-information-form.component.scss']
})
export class RoleInformationFormComponent implements OnInit, OnDestroy {

  @Input() public roleInformationForm: FormGroup = new FormGroup({});
  @Input() public isEdit: boolean = false;

  private _cdr = inject(ChangeDetectorRef);
  private _roleService = inject(RoleService);
  private _mobileWidthService = inject(MobileWidthService);

  private _widthSubscription: Subscription = new Subscription();
  private _preloadFieldSubscription: Subscription = new Subscription();
  private _rolesSubscription: Subscription = new Subscription();

  public roles: Role[] = [];
  public isScreenSmall: boolean = false;
  public showRoleDropdown: boolean = false;

  ngOnInit(): void {
    executeCallbackLate(() => {
      this._rolesSubscription = this._roleService.getRoles().subscribe((role) => {
        this.roles = role;
        this._cdr.detectChanges();
      });
    });

    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });

    this._preloadFieldSubscription = this.roleInformationForm.controls['preload']?.valueChanges.subscribe((value: boolean) => {
      this.showRoleDropdown = value;
      this.roleInformationForm.controls['role']?.setValidators(!value ? [] : [Validators.required]);
      this.roleInformationForm.controls['role']?.updateValueAndValidity();

      if (!value) {
        this.roleInformationForm.controls['role']?.setValue(null);
      } 
    });
  }

  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
    this._rolesSubscription.unsubscribe();
    !this.isEdit && this._preloadFieldSubscription.unsubscribe();
  }
}
