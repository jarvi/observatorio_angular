import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { RoleInformationFormComponent } from './role-information-form.component';
import { RoleService } from 'src/app/shared/services/role.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';

describe('RoleInformationFormComponent', () => {
  let component: RoleInformationFormComponent;
  let fixture: ComponentFixture<RoleInformationFormComponent>;
  let roleService: RoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RoleInformationFormComponent],
      providers: [
        RoleService,
        HttpRequestService
      ],
      imports: [
        HttpClientTestingModule,
        PrimeNgModule,
        FormsModule,
        ReactiveFormsModule
      ]
    });
    fixture = TestBed.createComponent(RoleInformationFormComponent);
    component = fixture.componentInstance;
    roleService = TestBed.inject(RoleService);

    spyOn(roleService, 'getRoles').and.returnValue(of([{
      id: 1,
      name: 'test',
      isActive: true,
    }]));

    component.roleInformationForm = new FormGroup({
      roleName: new FormControl(''),
      preload: new FormControl(true),
      role: new FormControl(''),
    });
    fixture.detectChanges();
  });

  it('should verify the quantity of roles', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(component.roles.length).toBeGreaterThan(0);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset role when preload is false', () => {
    component.roleInformationForm.controls['preload'].setValue(false);
    expect(component.roleInformationForm.controls['role'].value).toBeNull();
  });

  it('should change role when preload is true', () => {
    component.roleInformationForm.controls['preload'].setValue(true);
    expect(component.roleInformationForm.controls['role'].value).toEqual('');
  });
});
