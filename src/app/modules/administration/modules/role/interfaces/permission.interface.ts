export interface Permission {
    id: string;
    formControlName: string;
    name: string;
    value: string
} 