import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleAdministrationComponent } from './pages/role-administration/role-administration.component';
import { RoleCreationComponent } from './pages/role-creation/role-creation.component';
import { checkAllRolePermissionsGuard } from './guards/check-all-role-permissions.guard';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

const routes: Routes = [
  {
    path: 'role-administration',
    component: RoleAdministrationComponent,
    data: {
      breadcrumb: ''
    },
    canActivate: [checkAllRolePermissionsGuard]
  },
  {
    path: 'role-creation',
    component: RoleCreationComponent,
    data: {
      breadcrumb: 'Crear rol',
      permissions: [
        PERMISSION_TYPE.addRol.name,
      ]
    }
  },
  {
    path: '**',
    redirectTo: 'role-administration'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule { }
