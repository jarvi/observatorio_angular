import { FormGroup } from "@angular/forms";
import { merge, tap } from "rxjs";
import { PERMISSION_TYPE } from "src/app/shared/enums/permission-type.enum";

export const roleSwitchesObservable = (form: FormGroup) => {
    const inactivateUsersRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.deleteUser.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewUser.formName].setValue(PERMISSION_TYPE.viewUser.name);
            })
        );

    const editUsersRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.changeUser.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewUser.formName].setValue(PERMISSION_TYPE.viewUser.name);
            })
        );

    const consultUsersRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.viewUser.formName].valueChanges
        .pipe(
            tap(value => {
                if (!value) {
                    form.controls[PERMISSION_TYPE.changeUser.formName].setValue('');
                    form.controls[PERMISSION_TYPE.deleteUser.formName].setValue('');
                }
            })
        );

    const createUsersRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.addUser.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewRol.formName].setValue(PERMISSION_TYPE.viewRol.name);
            })
        );

    const inactivateRolesRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.deleteRol.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewRol.formName].setValue(PERMISSION_TYPE.viewRol.name);
            })
        );

    const editRolesRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.changeRol.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewRol.formName].setValue(PERMISSION_TYPE.viewRol.name);
            })
        );

    const createRolesRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.addRol.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewRol.formName].setValue(PERMISSION_TYPE.viewRol.name);
            })
        );

    const editUsersUsersPrivilegeObservable = form.controls[PERMISSION_TYPE.changeUser.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewRol.formName].setValue(PERMISSION_TYPE.viewRol.name);
            })
        );

    const consultRolesRolesPrivilegeObservable = form.controls[PERMISSION_TYPE.viewRol.formName].valueChanges
        .pipe(
            tap(value => {
                if (!value) {
                    form.controls[PERMISSION_TYPE.addUser.formName].setValue('');
                    form.controls[PERMISSION_TYPE.changeUser.formName].setValue('');
                    form.controls[PERMISSION_TYPE.deleteRol.formName].setValue('');
                    form.controls[PERMISSION_TYPE.changeRol.formName].setValue('');
                    form.controls[PERMISSION_TYPE.addRol.formName].setValue('');
                }
            })
        );

    const editNotificationsPrivilegeObservable = form.controls[PERMISSION_TYPE.changeNotification.formName].valueChanges
        .pipe(
            tap(value => {
                if (value) form.controls[PERMISSION_TYPE.viewNotification.formName].setValue(PERMISSION_TYPE.viewNotification.name);
            })
        );

    const consultNotificationsPrivilegeObservable = form.controls[PERMISSION_TYPE.viewNotification.formName].valueChanges
        .pipe(
            tap(value => {
                if (!value) form.controls[PERMISSION_TYPE.changeNotification.formName].setValue('');
            })
        );

    return merge(
        inactivateUsersRolesPrivilegeObservable,
        editUsersRolesPrivilegeObservable,
        consultUsersRolesPrivilegeObservable,
        createUsersRolesPrivilegeObservable,
        inactivateRolesRolesPrivilegeObservable,
        editRolesRolesPrivilegeObservable,
        createRolesRolesPrivilegeObservable,
        editUsersUsersPrivilegeObservable,
        consultRolesRolesPrivilegeObservable,
        editNotificationsPrivilegeObservable,
        consultNotificationsPrivilegeObservable
    )
}
