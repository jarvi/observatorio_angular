import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { map } from 'rxjs';
import { UserPermissionsService } from 'src/app/shared/services/user-permissions.service';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';
import { PermissionType } from 'src/app/shared/interfaces/permission-type.interface';

export const checkAllUserPermissionsGuard: CanActivateFn = (route, state) => {

  const router = inject(Router);
  const userPermissionsService = inject(UserPermissionsService);

  const verifyUserPermissions = (permissions: PermissionType[]): boolean => {
    const userPermissionsList = permissions.map((permission) => permission.codename);
    const userPermissions = [
      PERMISSION_TYPE.changeUser.name,
      PERMISSION_TYPE.deleteUser.name,
      PERMISSION_TYPE.viewUser.name,
    ]

    const permissionsMatch: boolean = userPermissions.some((userPermission: string) => userPermissionsList.includes(userPermission));

    if (!permissionsMatch) {
      router.navigateByUrl('/administration/systems-users/user-creation')
    }

    return permissionsMatch;
  }

  return userPermissionsService.getPermissions().pipe(
    map((permissions) => {
      return verifyUserPermissions(permissions);
    })
  );
};
