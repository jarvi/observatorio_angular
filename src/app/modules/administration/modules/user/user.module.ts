import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { UserRoutingModule } from './user-routing.module';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { UserCreationComponent } from './pages/user-creation/user-creation.component';
import { PersonalInformationFormComponent } from './components/personal-information-form/personal-information-form.component';
import { PasswordFormComponent } from './components/password-form/password-form.component';
import { UserAdministrationComponent } from './pages/user-administration/user-administration.component';
import { SearchUsersFormComponent } from './components/search-users-form/search-users-form.component';
import { DatagridUsersComponent } from './components/datagrid-users/datagrid-users.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';


@NgModule({
  declarations: [
    UserCreationComponent,
    PersonalInformationFormComponent,
    PasswordFormComponent,
    UserAdministrationComponent,
    SearchUsersFormComponent,
    DatagridUsersComponent,
    UserDetailsComponent,
    UserEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

    UserRoutingModule,

    PrimeNgModule,
    SharedModule
  ]
})
export class UserModule { }
