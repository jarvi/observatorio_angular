import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAdministrationComponent } from './pages/user-administration/user-administration.component';
import { UserCreationComponent } from './pages/user-creation/user-creation.component';
import { checkAllUserPermissionsGuard } from './guards/check-all-user-permissions.guard';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

const routes: Routes = [
  {
    path: 'user-administration',
    component: UserAdministrationComponent,
    data: {
      breadcrumb: ''
    },
    canActivate: [checkAllUserPermissionsGuard]
  },
  {
    path: 'user-creation',
    component: UserCreationComponent,
    data: {
      breadcrumb: 'Crear usuario',
      permissions: [
        PERMISSION_TYPE.addUser.name,
      ]
    }
  },
  {
    path: '**',
    redirectTo: 'user-administration'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
