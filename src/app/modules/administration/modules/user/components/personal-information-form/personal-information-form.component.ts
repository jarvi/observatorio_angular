import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IdentificationTypeService } from '../../../../../../shared/services/identification-type.service';
import { RoleService } from '../../../../../../shared/services/role.service';
import { genderOptions } from 'src/app/shared/constants/gender-options';
import { IdentificationType, Gender, Role} from 'src/app/shared/interfaces';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'user-personal-information-form',
  templateUrl: './personal-information-form.component.html',
  styleUrls: ['./personal-information-form.component.scss']
})
export class PersonalInformationFormComponent implements OnInit, OnDestroy {

  @Input() public personalInformationForm: FormGroup = new FormGroup({});
  @Input() public isEdit: boolean = false;

  private _cdr = inject(ChangeDetectorRef);
  private _identificationTypeService = inject(IdentificationTypeService);
  private _roleService = inject(RoleService);

  private _identificationTypesSubscription: Subscription = new Subscription();
  private _rolesSubscription: Subscription = new Subscription();
  private _identificationTypeFieldSubscription: Subscription = new Subscription();

  public isPassport: boolean = false;
  public genders: Gender[] = genderOptions;
  public identificationTypes: IdentificationType[] = [];
  public roles: Role[] = [];

  ngOnInit(): void {
    executeCallbackLate(() => {
      this._identificationTypesSubscription = this._identificationTypeService.getIdentificationTypes().subscribe((identificationTypes) => {
        this.identificationTypes = identificationTypes;
        this._cdr.detectChanges();
      });

      this._rolesSubscription = this._roleService.getRoles().subscribe((roles) => {
        this.roles = roles;
        this._cdr.detectChanges();
      });
    });

    this._identificationTypeFieldSubscription = this.personalInformationForm.controls['identificationType'].valueChanges.subscribe((identificationType) => {
      this.onIdentificationTypeChange(identificationType);
    });
  }

  ngOnDestroy(): void {
    this._identificationTypesSubscription.unsubscribe();
    this._rolesSubscription.unsubscribe();
    this._identificationTypeFieldSubscription.unsubscribe();
  }

  private onIdentificationTypeChange(identificationType: IdentificationType): void {
    if (!identificationType?.name) return;

    const identificationNumber = this.personalInformationForm.controls['identificationNumber'];
    
    if (identificationNumber.value) {
      this.personalInformationForm.controls['identificationNumber'].setValue('');
    }
    
    if (identificationType.name.toLocaleLowerCase() !== 'pasaporte') {
      this.isPassport = false;
      return;
    };

    this.isPassport = true;
  }
}
