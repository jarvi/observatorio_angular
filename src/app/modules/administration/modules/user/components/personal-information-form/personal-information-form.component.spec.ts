import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { PersonalInformationFormComponent } from './personal-information-form.component';
import { IdentificationTypeService } from 'src/app/shared/services/identification-type.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownChangeEvent } from 'primeng/dropdown';
import { SharedModule } from 'src/app/shared/shared.module';
import { of } from 'rxjs';
import { RoleService } from 'src/app/shared/services/role.service';

describe('PersonalInformationFormComponent', () => {
  let component: PersonalInformationFormComponent;
  let fixture: ComponentFixture<PersonalInformationFormComponent>;
  let identificationTypeService: IdentificationTypeService;
  let roleService: RoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PersonalInformationFormComponent],
      imports: [
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PrimeNgModule,
      ],
      providers: [
        IdentificationTypeService,
        HttpRequestService,
      ]
    });
    fixture = TestBed.createComponent(PersonalInformationFormComponent);
    identificationTypeService = TestBed.inject(IdentificationTypeService);
    roleService = TestBed.inject(RoleService);

    spyOn(identificationTypeService, 'getIdentificationTypes').and.returnValue(of([
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
    ]));

    spyOn(roleService, 'getRoles').and.returnValue(of([{
      id: 1,
      name: 'Administrador',
      isActive: true,
    }]));

    component = fixture.componentInstance;
    component.personalInformationForm = new FormGroup({
      identificationType: new FormControl(''),
      identificationNumber: new FormControl(''),
      name: new FormControl(''),
      surnames: new FormControl(''),
      email: new FormControl(''),
      gender: new FormControl(''),
      phone: new FormControl(''),
      contactNumber: new FormControl(''),
      address: new FormControl(''),
      role: new FormControl(''),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should verify the quantity of document types and role', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(component.identificationTypes.length).toBeGreaterThan(0);
    expect(component.roles.length).toBeGreaterThan(0);
  }));

  it('should select passport option', () => {
    component.identificationTypes = [
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
    ];

    const pDropdown = fixture.debugElement.query(By.css('#identificationType')).nativeElement;
    const container = pDropdown.querySelector('.p-dropdown');
    container.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));

    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    const identificationType = component.personalInformationForm.controls['identificationType'].value;
    
    expect(identificationType.name).toEqual('Pasaporte');
    expect(component.isPassport).toBeTrue();
  });

  it('should select passport option and clear identification number', () => {
    component.identificationTypes = [
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
    ];
    component.personalInformationForm.controls['identificationNumber'].setValue('123456');

    const pDropdown = fixture.debugElement.query(By.css('#identificationType')).nativeElement;
    const container = pDropdown.querySelector('.p-dropdown');
    container.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));

    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    const identificationType = component.personalInformationForm.controls['identificationType'].value;
    
    expect(identificationType.name).toEqual('Pasaporte');
    expect(component.isPassport).toBeTrue();
  });

  it('should select foreigner identity option', () => {
    component.identificationTypes = [
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
      { id: 3, name: 'Cédula de extranjería', alias: 'C.E.' }
    ];

    const pDropdown = fixture.debugElement.query(By.css('#identificationType')).nativeElement;
    const container = pDropdown.querySelector('.p-dropdown');
    container.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));

    items.children[2].children[0].nativeElement.click();
    fixture.detectChanges();

    const identificationType = component.personalInformationForm.controls['identificationType'].value;
    
    expect(identificationType.name).toEqual('Cédula de extranjería');
    expect(component.isPassport).toBeFalse();
  });
});
