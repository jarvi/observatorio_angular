import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { generateRandomPassword } from 'src/utils/password-generator';
import { PasswordConditions } from '../../interfaces/password-conditions.interface';

@Component({
  selector: 'user-password-form',
  templateUrl: './password-form.component.html',
  styleUrls: ['./password-form.component.scss']
})
export class PasswordFormComponent implements OnInit, OnDestroy {

  @Input() public passwordForm: FormGroup = new FormGroup({});

  private _passwordSubscription: Subscription = new Subscription();
  private _passwordConfirmationSubscription: Subscription = new Subscription();

  public hidePassword: boolean = false;
  public hidePasswordConfirmation: boolean = false;
  public passwordConditions: PasswordConditions[] = [
    {
      id: 'minLength',
      description: 'Debe ingresar al menos 7 caracteres',
      isValid: false
    },
    {
      id: 'upperCaseLowerCase',
      description: 'Combine mayúsculas y minúsculas',
      isValid: false
    },
    {
      id: 'specialCharacter',
      description: 'Incluya al menos 1 carácter especial',
      isValid: false
    },
  ]
  public passwordConfirmationConditions: PasswordConditions[] = [
    {
      id: 'matching',
      description: 'Las contraseñas coinciden',
      isValid: false
    },
  ]

  ngOnInit(): void {
    this._passwordSubscription = this.passwordForm.controls['password'].valueChanges.subscribe((value) => {
      this.onPasswordChange(value);
      this.onPasswordConfirmationChange();
    });

    this._passwordConfirmationSubscription = this.passwordForm.controls['passwordConfirmation'].valueChanges.subscribe((value) => {
      this.onPasswordConfirmationChange();
    });
  }

  ngOnDestroy(): void {
    this._passwordSubscription.unsubscribe();
    this._passwordConfirmationSubscription.unsubscribe();
  }

  public togglePasswordVisibility(isPassword: boolean): void {
    if (isPassword) this.hidePassword = !this.hidePassword;
    else this.hidePasswordConfirmation = !this.hidePasswordConfirmation;
  }

  public enabledShowPasswordButton(isPassword: boolean): boolean {
    const controlName = isPassword ? 'password' : 'passwordConfirmation';
    const control = this.passwordForm.controls[controlName];
    const enabled = !!control?.value;
  
    if (!enabled) {
      if (isPassword) {
        this.hidePassword = false;
      } else {
        this.hidePasswordConfirmation = false;
      }
    }
  
    return !enabled;
  }

  public onPasswordChange(password: string): void {
    this.passwordConditions = this.passwordConditions.map((condition) => {
      switch (condition.id) {
        case 'minLength':
          condition.isValid = password?.trim().length >= 7 || false;
          break;
        case 'upperCaseLowerCase':
          const upperCase = password?.match(/[A-Z]/g) ?? false;
          const lowerCase = password?.match(/[a-z]/g) ?? false;
          condition.isValid = !!upperCase && !!lowerCase;
          break;
        case 'specialCharacter':
          const specialCharacter = password?.match(/[^a-zA-Z0-9\s]/g) ?? false;
          condition.isValid = !!specialCharacter;
          break;
      }
      return condition;
    });
  }

  public onPasswordConfirmationChange(): void {
    const passwordValue = this.passwordForm.controls['password'].value;
    const passwordConfirmationValue = this.passwordForm.controls['passwordConfirmation'].value;
    const passwordMatch = this.passwordForm.controls['passwordMatch'];
    const isValid = passwordConfirmationValue && passwordValue && passwordConfirmationValue === passwordValue;
    passwordMatch.setValue(isValid);

    this.passwordConfirmationConditions = this.passwordConfirmationConditions.map((condition) => {
      if (condition.id === 'matching') {
        condition.isValid = isValid;
      }
      return condition;
    })
  }

  public generatePassword(): void {
    const randomPassword = generateRandomPassword();
    const passwordControl = this.passwordForm.controls['password'];
    const passwordConfirmationControl = this.passwordForm.controls['passwordConfirmation'];

    passwordControl.setValue(randomPassword);
    passwordConfirmationControl.setValue(randomPassword);
    passwordControl.markAsDirty();
    passwordConfirmationControl.markAsDirty();
  }
}
