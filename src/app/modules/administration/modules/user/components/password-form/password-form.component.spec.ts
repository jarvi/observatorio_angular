import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

import { PasswordFormComponent } from './password-form.component';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

describe('PasswordFormComponent', () => {
  let component: PasswordFormComponent;
  let fixture: ComponentFixture<PasswordFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PasswordFormComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        PrimeNgModule,
        SharedModule
      ]
    });
    fixture = TestBed.createComponent(PasswordFormComponent);
    component = fixture.componentInstance;
    component.passwordForm = new FormGroup({
      password: new FormControl(''),
      passwordConfirmation: new FormControl(''),
      passwordMatch: new FormControl(false),
      sendEmail: new FormControl(false),
    });
    fixture.detectChanges();
  });

  it('should set all password conditions to valid', () => {
    component.passwordForm.controls['password'].setValue('123456As@');
    expect(component.passwordConditions.every(condition => condition.isValid)).toBeTrue();
  });

  it('should set all password conditions to valid exept minLength', () => {
    component.passwordForm.controls['password'].setValue('12As@');
    expect(component.passwordConditions.every(condition => condition.isValid)).toBeFalse();
  });

  it('should set all password conditions to valid exept upperCase', () => {
    component.passwordForm.controls['password'].setValue('12s13123123s@');
    expect(component.passwordConditions.every(condition => condition.isValid)).toBeFalse();
  });

  it('should set all password conditions to valid exept lowerCase', () => {
    component.passwordForm.controls['password'].setValue('12AAAAAAA@');
    expect(component.passwordConditions.every(condition => condition.isValid)).toBeFalse();
  });

  it('should set all password conditions to valid exept specialCharacter', () => {
    component.passwordForm.controls['password'].setValue('12AAAAAAA');
    expect(component.passwordConditions.every(condition => condition.isValid)).toBeFalse();
  });

  it('should set password confirmation condition to valid', () => {
    component.passwordForm.controls['password'].setValue('123456As@');
    component.passwordForm.controls['passwordConfirmation'].setValue('123456As@');

    expect(component.passwordConfirmationConditions .every(condition => condition.isValid)).toBeTruthy();
  });

  it('should generate a random password and set it to password an passwordConfirmation fields', () => {
    component.generatePassword();
    const passwordValue = component.passwordForm.controls['password'].value;
    const passwordConfirmationValue = component.passwordForm.controls['passwordConfirmation'].value;

    expect(passwordValue).toEqual(passwordConfirmationValue);
  });

  it('should change hidePassword value to true', () => {
    component.togglePasswordVisibility(true);
    expect(component.hidePassword).toBeTrue();
  });

  it('should change hidePasswordConfirmation value to true', () => {
    component.togglePasswordVisibility(false);
    expect(component.hidePasswordConfirmation).toBeTrue();
  });

});
