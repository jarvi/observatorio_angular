import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailsComponent } from './user-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from 'src/app/shared/services/user.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { of, throwError } from 'rxjs';
import { User } from 'src/app/shared/interfaces';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserDetailsComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        UserService,
        HttpRequestService,
      ],
    });
    fixture = TestBed.createComponent(UserDetailsComponent);
    userService = TestBed.inject(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit userIdChange value when onVisibleChange is executed', () => {
    spyOn(component.userIdChange, 'emit');

    component.onVisibleChange(true);
    expect(component.userIdChange.emit).toHaveBeenCalledWith(component.userId);
  });

  it('should emit userIdChange value when onVisibleChange is executed with false', () => {
    spyOn(component.userIdChange, 'emit');

    component.onVisibleChange(false);
    expect(component.userIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should call ngOnChanges when userId changes and change user name', () => {
    const user: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: [{
        id: 1,
        isActive: true,
        name: 'test',
        permissions: [PERMISSION_TYPE.addReport.name],
      }]
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(userService, 'getUser').and.returnValue(of(user))

    const newUserId = 1;
    component.userId = newUserId;
    fixture.detectChanges();
    component.ngOnChanges({ userId: {
      previousValue: '',
      currentValue: newUserId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.userName).toEqual(user.completeName);
  });

  it('should call ngOnChanges when userId changes with bad request and call the onVisibleChange method', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(userService, 'getUser').and.returnValue(throwError(() => {}));
    spyOn(component, 'onVisibleChange');

    const newUserId = 1;
    component.userId = newUserId;
    fixture.detectChanges();
    component.ngOnChanges({ userId: {
      previousValue: '',
      currentValue: newUserId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.onVisibleChange).toHaveBeenCalledWith(false);
  });

  it('should call ngOnChanges when userId changes and call the onVisibleChange method', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(component, 'onVisibleChange');

    const newUserId = 0;
    component.userId = newUserId;
    fixture.detectChanges();
    component.ngOnChanges({ userId: {
      previousValue: '',
      currentValue: newUserId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.onVisibleChange).toHaveBeenCalledWith(false);
  });

  it('should call ngOnChanges when userId changes and does not do anything if the user returned is same', () => {
    const user: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: []
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(userService, 'getUser').and.returnValue(of(user))

    const newUserId = 1;
    component.userId = newUserId;
    fixture.detectChanges();
    component.ngOnChanges({ userId: {
      previousValue: '',
      currentValue: newUserId,
      firstChange: true,
      isFirstChange: () => true
    }});
    component.ngOnChanges({ userId: {
      previousValue: '',
      currentValue: newUserId,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.userName).toEqual(user.completeName);
  });
});
