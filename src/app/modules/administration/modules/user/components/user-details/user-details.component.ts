import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from '../../../../../../shared/services/user.service';
import { SpinnerService } from '../../../../../../shared/services/spinner.service';
import { ModalDetails, User } from 'src/app/shared/interfaces';

@Component({
  selector: 'user-user-details',
  templateUrl: './user-details.component.html'
})
export class UserDetailsComponent implements OnChanges, OnDestroy {

  @Input() public userId: number = 0;
  @Output() public userIdChange: EventEmitter<number> = new EventEmitter<number>();
  
  private _userService = inject(UserService);
  public spinnerService = inject(SpinnerService);

  private _subscription: Subscription = new Subscription();

  public modalVisible: boolean = false;
  public userName: string = '';
  public userInformationDetails: ModalDetails[][] = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['userId']) {
      this.onVisibleChange(this.userId > 0);
      
      if (!this.userId) return;
      this.getUser(this.userId);
    }
  }
  
  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  private getUser(id: number) {
    this._subscription.unsubscribe();

    this.spinnerService.showSpinner = true;
    this._subscription = this._userService.getUser(id).subscribe({
      next: (user) => {
        this.userName = user.completeName;
        this.spinnerService.showSpinner = false;

        this.buildDetails(user);
      },
      error: () => {
        this.spinnerService.showSpinner = false;
        this.onVisibleChange(false);
      }
    });
  }

  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
    this.userIdChange.emit(visible ? this.userId : 0);
  }

  private buildDetails(user: User) {
    this.userInformationDetails = [
      [
        { label: 'Documento de identidad', value: user.documentNumber },
        { label: 'Nombres y apellidos', value: user.completeName },
        { label: 'Número de celular', value: user.cellPhone },
        { label: 'Correo electrónico', value: user.email },
      ],
      [
        { label: 'Número de contacto', value: user.contactNumber },
        { label: 'Dirección', value: user.address },
        { label: 'Roles', iterableValue: user.roles.map(role => role.name) }
      ]
    ];
  }
}
