import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { UserService } from 'src/app/shared/services/user.service';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { personalInformationFormStructure } from 'src/utils/creation-edition-user-validators';
import { ModalAlert, User, UserEditionRequest } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'user-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnChanges, OnDestroy {

  @Input() public userId: number = 0;
  @Output() public userIdChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() public userEdited: EventEmitter<User> = new EventEmitter<User>();

  private _fb = inject(FormBuilder);
  private _validatorService = inject(ValidatorService);
  private _userService = inject(UserService);
  private _modalAlertService = inject(ModalAlertService);

  public spinnerService = inject(SpinnerService);
  
  private _userEditionSubscription: Subscription = new Subscription();
  private _getUsersubscription: Subscription = new Subscription();
  private _userToEdit: User = {
    id: 0,
    address: '',
    cellPhone: '',
    completeName: '',
    contactNumber: '',
    documentNumber: '',
    documentType: {
      id: 0,
      name: '',
      alias: ''
    },
    email: '',
    firstName: '',
    gender: '',
    isActive: false,
    lastName: '',
    roles: []
  };

  public modalVisible: boolean = false;
  public userName: string = '';
  public personalInformationForm: FormGroup = this._fb.group(personalInformationFormStructure(true));
  public sendEmailForm: FormGroup = this._fb.group({
    sendEmail: [true]
  });
  public form: FormGroup = this._fb.group({
    personalInformation: this.personalInformationForm,
    sendEmail: this.sendEmailForm
  });

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['userId']) {
      const isUserIdPresent = this.userId > 0;
      this.onVisibleChange(isUserIdPresent);
      
      if (!this.userId) return;
      this.getUser(this.userId);
    }
  }

  ngOnDestroy(): void {
    this._getUsersubscription.unsubscribe();
    this._userEditionSubscription.unsubscribe();
  }

  private getUser(id: number): void {
    this._getUsersubscription.unsubscribe();

    this.spinnerService.showSpinner = true;

    this._getUsersubscription = this._userService.getUser(id).subscribe({
      next: (user) => {
        this._userToEdit = user;
        this.userName = user.completeName;
        this.spinnerService.showSpinner = false;
        
        this.fillForm();
      },
      error: () => {
        this.spinnerService.showSpinner = false;
        this.onVisibleChange(false);
      }
    });
  }

  private fillForm(): void {
    this.form.patchValue({
      personalInformation: {
        address: this._userToEdit.address,
        phone: this._userToEdit.cellPhone,
        contactNumber: this._userToEdit.contactNumber,
        email: this._userToEdit.email,
        name: this._userToEdit.firstName,
        surnames: this._userToEdit.lastName,
        gender: this._userToEdit.gender,
        role: this._userToEdit.roles?.map(role => role.id)
      },
      sendEmail: {
        sendEmail: true
      }
    });

    executeCallbackLate(() => {
      this.form.patchValue({
        personalInformation: {
          identificationType: this._userToEdit.documentType,
        }
      });
    });

    setTimeout(() => {
      this.form.patchValue({
        personalInformation: {
          identificationNumber: this._userToEdit.documentNumber,
        }
      });
    }, 100);
  }

  private returnUserEdited(userEdited: User) {
    this.userEdited.emit(userEdited);
  }

  private showErrorAlert(error: string): void {
    this.onVisibleChange(false);

    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
        action: () => {
          this.resetForm();
          this.onVisibleChange(false);
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.resetForm();
          this.onVisibleChange(false);
        }
      },
      highlightText: error
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  private showWarningAlert(userName: string): void {
    this.onVisibleChange(false);

    const warningAlert: ModalAlert = {
      title: 'Actualizar usuario',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, actualizar',
        primary: true,
        action: () => {
          this.editUser();
        }
      },
      cancelButton: {
        label: 'Cancelar',
        primary: false,
        action: () => {
          this.resetForm();
          this.onVisibleChange(false);
        }
      },
      highlightText: `¿Está seguro que desea actualizar la información del usuario ${userName}?`
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }

  private showSuccessAlert(userName: string): void {
    const successAlert: ModalAlert = {
      title: 'Usuario actualizado',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
        action: () => {
          this.resetForm();
          this.onVisibleChange(false);
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.resetForm();
          this.onVisibleChange(false);
        }
      },
      message: `El usuario ${ userName } se ha actualizado exitosamente.`,
      highlightText: 'Haga click para continuar'
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  public editUser(): void {
    const userToEdit: UserEditionRequest = {
      address: this.form.value.personalInformation.address,
      cell_phone: this.form.value.personalInformation.phone,
      contact_number: this.form.value.personalInformation.contactNumber,
      email: this.form.value.personalInformation.email,
      first_name: this.form.value.personalInformation.name,
      gender: this.form.value.personalInformation.gender,
      last_name: this.form.value.personalInformation.surnames,
      groups: this.form.value.personalInformation.role,
      id: this._userToEdit.id,
      send_email: this.form.value.sendEmail.sendEmail
    }
    
    this.spinnerService.showSpinner = true;

    this._userEditionSubscription = this._userService.editUser(userToEdit).subscribe({
      next: (userEdited) => {
        this.spinnerService.showSpinner = false;
        this.showSuccessAlert(userEdited.completeName);
        this.returnUserEdited(userEdited);
      },
      error: (error) => {
        this.spinnerService.showSpinner = false;
        error && this.showErrorAlert(error);
      }
    });
  }

  public onEditUser(): void {
    if (this.form.invalid) {
      this._validatorService.isValidField(this.personalInformationForm);
      this._validatorService.isValidField(this.sendEmailForm);
      return;
    }

    this.showWarningAlert(this.userName);
  }

  public onCancel(): void {
    this.onVisibleChange(false);

    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this.resetForm();
          this.onVisibleChange(false);
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
        action: () => {
          this.onVisibleChange(true);
        }
      },
      message: 'Si hace clic en “Si, cancelar” perderá toda la información diligenciada hasta el momento.',
      highlightText: '¿Está seguro que desea cancelar la actualización del usuario?'
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }
  
  public onVisibleChange(visible: boolean): void {
    this.modalVisible = visible;
    this.userIdChange.emit(visible ? this.userId : 0);
  }

  public resetForm(): void {
    this.form.reset();
    this.sendEmailForm.controls['sendEmail'].setValue(true);
  }
}
