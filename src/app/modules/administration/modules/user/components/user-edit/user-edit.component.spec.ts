import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { UserEditComponent } from './user-edit.component';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { UserService } from 'src/app/shared/services/user.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { PersonalInformationFormComponent } from '../personal-information-form/personal-information-form.component';
import { PasswordFormComponent } from '../password-form/password-form.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { User } from 'src/app/shared/interfaces';
import { of, throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { By } from '@angular/platform-browser';

describe('UserEditComponent', () => {
  let component: UserEditComponent;
  let fixture: ComponentFixture<UserEditComponent>;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserEditComponent,
        PersonalInformationFormComponent,
        PasswordFormComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        UserService,
        HttpRequestService,
      ]
    });
    fixture = TestBed.createComponent(UserEditComponent);
    userService = TestBed.inject(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    Swal.close();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnChanges when userId changes and change user name', () => {
    const user: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: [{
        id: 1,
        isActive: true,
        name: 'test',
        permissions: ['addReport']
      }]
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(userService, 'getUser').and.returnValue(of(user))

    const newUserId = 1;
    component.userId = newUserId;
    fixture.detectChanges();
    component.ngOnChanges({
      userId: {
        previousValue: '',
        currentValue: newUserId,
        firstChange: true,
        isFirstChange: () => true
      }
    });

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.userName).toEqual(user.completeName);
  });

  it('should call ngOnChanges when userId changes with bad request and call the onVisibleChange method', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(userService, 'getUser').and.returnValue(throwError(() => { }));
    spyOn(component, 'onVisibleChange');

    const newUserId = 1;
    component.userId = newUserId;
    fixture.detectChanges();
    component.ngOnChanges({
      userId: {
        previousValue: '',
        currentValue: newUserId,
        firstChange: true,
        isFirstChange: () => true
      }
    });

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.onVisibleChange).toHaveBeenCalledWith(false);
  });

  it('should call ngOnChanges when userId changes and does not do anything if the user returned is same', () => {
    const user: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: [{
        id: 1,
        isActive: true,
        name: 'test',
        permissions: ['addReport']
      }]
    }

    spyOn(component, 'ngOnChanges').and.callThrough();
    spyOn(userService, 'getUser').and.returnValue(of(user))

    const newUserId = 1;
    component.userId = newUserId;
    fixture.detectChanges();
    component.ngOnChanges({
      userId: {
        previousValue: '',
        currentValue: newUserId,
        firstChange: true,
        isFirstChange: () => true
      }
    });
    component.ngOnChanges({
      userId: {
        previousValue: '',
        currentValue: newUserId,
        firstChange: true,
        isFirstChange: () => true
      }
    });

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.userName).toEqual(user.completeName);
  });

  it('should reset the form when resetForm is executed', () => {
    spyOn(component.form, 'reset');

    component.resetForm();
    const sendEmail = component.sendEmailForm.get('sendEmail')?.value;
    expect(sendEmail).toBeTrue();
    expect(component.form.reset).toHaveBeenCalled();
  });

  it('should show warning modal alert when onCancel method is executed and emit userIdChange value when is confirmed', () => {
    spyOn(component.userIdChange, 'emit');

    component.onCancel();
    expect(Swal.isVisible()).toBeTrue();
    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();

    expect(component.userIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should show warning modal alert when onCancel method is executed and emit userIdChange value when is canceled', () => {
    spyOn(component.userIdChange, 'emit');

    component.onCancel();
    expect(Swal.isVisible()).toBeTrue();
    const confirmButton = Swal.getCancelButton()!;
    confirmButton.click();

    expect(component.userIdChange.emit).toHaveBeenCalledWith(component.userId);
  });

  it('should exec on edit user method when all is ok', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));

    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    component.onEditUser();

    expect(component.form.valid).toBeTruthy();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should exec on edit user method when all is ok and show a modal warning alert and press the confirm button', () => {
    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));

    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    component.onEditUser();

    const cancelButton = Swal.getConfirmButton()!;
    cancelButton.click();

    expect(component.form.valid).toBeTruthy();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should exec edit user method when all is ok and emit with confirm button', () => {
    const userResponse: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: [{
        id: 1,
        isActive: true,
        name: 'test',
        permissions: ['addReport']
      }]
    }

    spyOn(userService, 'editUser').and.returnValue(of(userResponse));
    spyOn(component.userIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));

    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    expect(component.form.valid).toBeTruthy();

    component.editUser();

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();

    expect(userService.editUser).toHaveBeenCalled();
    expect(component.userIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should exec edit user method when all is ok and emit with cancel button', () => {
    const userResponse: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: [{
        id: 1,
        isActive: true,
        name: 'test',
        permissions: ['addReport']
      }]
    }

    spyOn(userService, 'editUser').and.returnValue(of(userResponse));
    spyOn(component.userIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));

    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    expect(component.form.valid).toBeTruthy();

    component.editUser();

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getCancelButton()!;
    confirmButton.click();

    expect(userService.editUser).toHaveBeenCalled();
    expect(component.userIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should exec edit user method with bad request and show a modal error alert and press the confirm button', () => {

    spyOn(userService, 'editUser').and.returnValue(throwError(() => 'Error'));
    spyOn(component.userIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));

    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    expect(component.form.valid).toBeTruthy();

    component.editUser();

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    confirmButton.click();

    expect(userService.editUser).toHaveBeenCalled();
    expect(component.userIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should exec edit user method with bad request and show a modal error alert and press the cancel button', () => {

    spyOn(userService, 'editUser').and.returnValue(throwError(() => 'Error'));
    spyOn(component.userIdChange, 'emit');

    const compiled = fixture.nativeElement as HTMLElement;
    component.onVisibleChange(true);
    fixture.detectChanges();
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));

    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    expect(component.form.valid).toBeTruthy();

    component.editUser();

    expect(Swal.isVisible()).toBeTruthy();

    const cancelButton = Swal.getCancelButton()!;
    cancelButton.click();

    expect(userService.editUser).toHaveBeenCalled();
    expect(component.userIdChange.emit).toHaveBeenCalledWith(0);
  });

  it('should not exec on edit user method when form is invalid', () => {
    component.onEditUser();
    expect(component.form.valid).toBeFalsy();
  });
});
