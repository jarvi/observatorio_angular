import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

import { DatagridUsersComponent } from './datagrid-users.component';
import { UserDetailsComponent } from '../user-details/user-details.component';
import { UserService } from 'src/app/shared/services/user.service';

import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import Swal from 'sweetalert2';
import { of } from 'rxjs';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { PersonalInformationFormComponent } from '../personal-information-form/personal-information-form.component';
import { PasswordFormComponent } from '../password-form/password-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { User } from 'src/app/shared/interfaces';

describe('DatagridUsersComponent', () => {
  let component: DatagridUsersComponent;
  let fixture: ComponentFixture<DatagridUsersComponent>;
  let userService: UserService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    Swal.close();
    TestBed.configureTestingModule({
      declarations: [
        DatagridUsersComponent,
        UserDetailsComponent,
        UserEditComponent,
        PersonalInformationFormComponent,
        PasswordFormComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        UserService,
        HttpRequestService,
      ],
    });
    fixture = TestBed.createComponent(DatagridUsersComponent);
    httpRequestService = TestBed.inject(HttpRequestService);
    userService = TestBed.inject(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit pageChange value when onPageChange is executed', () => {
    spyOn(component.pageChange, 'emit');

    component.onPageChange(1);
    expect(component.pageChange.emit).toHaveBeenCalledWith(1);
  });

  it('should change userToGet value', () => {
    component.onShowUserDetails(1);
    expect(component.userToGet).toEqual(1);
  });

  it('should show modal warning alert to inactivate user and press confirm button to inactivate user', () => {
    spyOn(userService, 'activateUser').and.returnValue(of(true));

    component.users = [{
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: []
    }];

    component.onSwitchClick(true, 1, 'Usuario Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal warning alert to activate user and press confirm button to activate user', () => {
    spyOn(userService, 'activateUser').and.returnValue(of(true));

    component.users = [{
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: false,
      lastName: 'PruebaApellido',
      roles: []
    }];

    component.onSwitchClick(false, 1, 'Usuario Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal success alert when activateUser method is executed', () => {
    spyOn(userService, 'activateUser').and.returnValue(of(true));

    component.users = [{
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: false,
      lastName: 'PruebaApellido',
      roles: []
    }];

    component.activateUser(false, 1, 'Usuario Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal success alert when activateUser method is executed with activate', () => {
    spyOn(userService, 'activateUser').and.returnValue(of(true));

    component.users = [{
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: false,
      lastName: 'PruebaApellido',
      roles: []
    }];

    component.activateUser(true, 1, 'Usuario Prueba');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton()!;
    expect(confirmButton).toBeTruthy();
    confirmButton.click();

    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should edit allow user when onShowEditUser method identifies that idUser is different than logged in user', () => {  
    const id = 1;
    component.onShowEditUser(id); 
    expect(component.userToEdit).toEqual(id);
  });

  it('should edit allow user when onSwitchClick method identifies that idUser is different than logged in user', () => {
    userService.userInformation.id = 2;     
    const infoUsuario = {
      isActivate: false, 
      userId: 2, 
      userName: 'PEPITO PEREZ',
    }

    component.onSwitchClick(infoUsuario.isActivate, infoUsuario.userId, infoUsuario.userName);      
    expect(userService.userInformation.id === infoUsuario.userId);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should edit an specific user', () => {
    component.users = [
      {
        address: 'Calle falsa 123',
        cellPhone: '1234567890',
        completeName: 'Prueba PruebaApellido',
        contactNumber: '1234567890',
        documentNumber: '1234567890',
        documentType: {
          id: 1,
          name: 'Cédula de ciudadanía',
          alias: 'CC'
        },
        email: 'prueba@mail.com',
        firstName: 'Prueba',
        gender: 'M',
        id: 1,
        isActive: true,
        lastName: 'PruebaApellido',
        roles: [{
          id: 1,
          isActive: true,
          name: 'Prueba',
        }]
      },
      {
        address: 'Calle falsa 123',
        cellPhone: '1234567890',
        completeName: 'Prueba AA',
        contactNumber: '1234567890',
        documentNumber: '22222',
        documentType: {
          id: 1,
          name: 'Cédula de ciudadanía',
          alias: 'CC'
        },
        email: 'prueba@mail.com',
        firstName: 'Prueba',
        gender: 'M',
        id: 2,
        isActive: true,
        lastName: 'AA',
        roles: [{
          id: 1,
          isActive: true,
          name: 'Prueba',
        }]
      }
    ]

    const userEdited: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Pruebaaaa PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: [{
        id: 1,
        isActive: true,
        name: 'Prueba',
      }]
    }
    component.changeUserEdited(userEdited);
    
    const usersExpected = component.users.find(user => user.completeName === 'Pruebaaaa PruebaApellido');
    expect(usersExpected).toEqual(userEdited);
  });
});
