import { Component, EventEmitter, Input, Output, inject, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { UserService } from 'src/app/shared/services/user.service';
import { SpinnerService } from '../../../../../../shared/services/spinner.service';
import { ModalAlert, User, UserActivateRequest } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

@Component({
  selector: 'user-datagrid-users',
  templateUrl: './datagrid-users.component.html',
  styleUrls: ['./datagrid-users.component.scss']
})
export class DatagridUsersComponent implements OnDestroy {

  @Input() public users: User[] = [];
  @Input() public totalUsers: number = 0;
  @Input() public perPage: number = 0;
  @Input() public currentPage: number = 0;
  @Output() public pageChange: EventEmitter<number> = new EventEmitter<number>();

  private _modalAlertService = inject(ModalAlertService);
  private _userService = inject(UserService);
  public spinnerService = inject(SpinnerService);

  private _activateUserSubscription: Subscription = new Subscription();

  public userToGet: number = 0;
  public userToEdit: number = 0;
  public userPermissions = {
    inactive: PERMISSION_TYPE.deleteUser.name,
    edit: PERMISSION_TYPE.changeUser.name,
    details: PERMISSION_TYPE.viewUser.name,
  };

  ngOnDestroy(): void {
    this._activateUserSubscription.unsubscribe();
  }

  public activateUser(activate: boolean, userId: number, userName: string): void {
    this.spinnerService.showSpinner = true;

    const request: UserActivateRequest = {
      id: userId,
      is_active: activate
    }

    this._activateUserSubscription = this._userService.activateUser(request).subscribe((response) => {
      if (response) {
        this.spinnerService.showSpinner = false;
  
        this.users = this.users.map((user) => {
          if (user.id === userId) user.isActive = activate;
          return user;
        });
  
        this.showActivateInactivateUserSuccessAlert(!activate, userName);
      } else {
        this.spinnerService.showSpinner = false;
      }
    });
  }

  private showActivateInactivateUserAlert(isActivate: boolean, userId: number, userName: string): void {
    const optionString = isActivate ? 'Inactivar' : 'Activar';

    const warinigAlert: ModalAlert = {
      title: `${optionString} usuario`,
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: `Si, ${optionString.toLowerCase()}`,
        primary: true,
        action: () => {
          this.activateUser(!isActivate, userId, userName);
        }
      },
      cancelButton: {
        label: 'Cancelar',
        primary: false,
      },
      highlightText: `¿Está seguro que desea ${optionString.toLowerCase()} el usuario ${userName}?`,
    }

    this._modalAlertService.showModalAlert(warinigAlert);
  }

  private showActivateInactivateUserSuccessAlert(isActivate: boolean, userName: string): void {
    const optionString = isActivate ? 'inactivado' : 'activado';

    const successAlert: ModalAlert = {
      title: `Usuario ${optionString}`,
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Aceptar',
        primary: true
      },
      message: `El usuario ${userName} se ha ${optionString} exitosamente. ${ !isActivate ? '\nRecuerde asignar el rol.' : ''}`,
      highlightText: 'Haga click para continuar',
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  public onPageChange(page: number): void {
    this.pageChange.emit(page);
  }

  public onShowUserDetails(id: number): void {
    this.userToGet = id;
  }

  public onShowEditUser(id: number): void {     
    if(id !== this._userService.userInformation.id){
      this.userToEdit = id; 
      return;   
    }    
    this.showModalAlertError(`No es posible editar el usuario.`);  
  }

  public onSwitchClick(isActivate: boolean, userId: number, userName: string): void {
    if(userId !== this._userService.userInformation.id){
      this.showActivateInactivateUserAlert(isActivate, userId, userName);
      return;
    }
      this.showModalAlertError(`No es posible inactivar el usuario.`);    
  }

  public showModalAlertError(message: string) {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message,
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }
  
  public changeUserEdited(userEdited: User) {
    this.users = this.users.map(user =>
      user.id === userEdited.id ? userEdited : user
    );
  }
}
