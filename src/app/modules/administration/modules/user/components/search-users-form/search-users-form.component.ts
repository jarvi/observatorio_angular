import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IdentificationTypeService } from 'src/app/shared/services/identification-type.service';
import { IdentificationType } from 'src/app/shared/interfaces';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'user-search-users-form',
  templateUrl: './search-users-form.component.html',
  styleUrls: ['./search-users-form.component.scss']
})
export class SearchUsersFormComponent implements OnInit, OnDestroy {

  @Input() public searchUsersForm: FormGroup = new FormGroup({});

  private _cdr = inject(ChangeDetectorRef);
  private _identificationTypeService = inject(IdentificationTypeService);

  private _identificationTypesSubscription: Subscription = new Subscription();
  private _identificationTypeField: Subscription = new Subscription();

  public isPassport: boolean = false;
  public identificationTypes: IdentificationType[] = [];

  ngOnInit(): void {
    executeCallbackLate(() => {
      this._identificationTypesSubscription = this._identificationTypeService.getIdentificationTypes().subscribe((identificationTypes) => {
        this.identificationTypes = identificationTypes;
        this._cdr.detectChanges();
      });
    });

    this._identificationTypeField = this.searchUsersForm.controls['identificationType'].valueChanges.subscribe((identificationType) => {
      this.onIdentificationTypeChange(identificationType);
    });
  }

  ngOnDestroy(): void {
    this._identificationTypesSubscription.unsubscribe();
    this._identificationTypeField.unsubscribe();
  }

  private onIdentificationTypeChange(identificationType: IdentificationType): void {
    if (!identificationType?.name) return;

    const identificationNumber = this.searchUsersForm.controls['identificationNumber'];
    
    if (identificationNumber.value) {
      this.searchUsersForm.controls['identificationNumber'].setValue('');
    }
    
    if (identificationType.name.toLocaleLowerCase() !== 'pasaporte') {
      this.isPassport = false;
      return;
    };

    this.isPassport = true;
  }
}
