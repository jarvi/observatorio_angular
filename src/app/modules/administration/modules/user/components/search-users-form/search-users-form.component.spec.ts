import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { SearchUsersFormComponent } from './search-users-form.component';
import { IdentificationTypeService } from 'src/app/shared/services/identification-type.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('SearchUsersFormComponent', () => {
  let component: SearchUsersFormComponent;
  let fixture: ComponentFixture<SearchUsersFormComponent>;
  let httpRequestService: HttpRequestService;
  let identificationTypeService: IdentificationTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SearchUsersFormComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule
      ],
      providers: [
        IdentificationTypeService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(SearchUsersFormComponent);
    httpRequestService = TestBed.inject(HttpRequestService);
    identificationTypeService = TestBed.inject(IdentificationTypeService);

    spyOn(identificationTypeService, 'getIdentificationTypes').and.returnValue(of([
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
    ]));

    component = fixture.componentInstance;
    component.searchUsersForm = new FormGroup({
      identificationType: new FormControl(''),
      identificationNumber: new FormControl(''),
      name: new FormControl(''),
      surnames: new FormControl(''),
      email: new FormControl(''),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should verify the quantity of document types', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(component.identificationTypes.length).toBeGreaterThan(0);
  }));

  it('should select foreigner identity option', () => {
    component.identificationTypes = [
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
      { id: 3, name: 'Cédula de extranjería', alias: 'C.E.' }
    ];

    const pDropdown = fixture.debugElement.query(By.css('#identificationTypeSearchUser')).nativeElement;
    const container = pDropdown.querySelector('.p-dropdown');
    container.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));

    items.children[2].children[0].nativeElement.click();
    fixture.detectChanges();

    const identificationType = component.searchUsersForm.controls['identificationType'].value;
    
    expect(identificationType.name).toEqual('Cédula de extranjería');
    expect(component.isPassport).toBeFalse();
  });

  it('should select passport option', () => {
    component.identificationTypes = [
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
    ];

    const pDropdown = fixture.debugElement.query(By.css('#identificationTypeSearchUser')).nativeElement;
    const container = pDropdown.querySelector('.p-dropdown');
    container.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));

    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    const identificationType = component.searchUsersForm.controls['identificationType'].value;
    
    expect(identificationType.name).toEqual('Pasaporte');
    expect(component.isPassport).toBeTrue();
  });

  it('should select passport option and clear identification number', () => {
    component.identificationTypes = [
      { id: 1, name: 'Cédula de ciudadanía', alias: 'C.C.' },
      { id: 2, name: 'Pasaporte', alias: 'PAS' },
    ];
    component.searchUsersForm.controls['identificationNumber'].setValue('123456');

    const pDropdown = fixture.debugElement.query(By.css('#identificationTypeSearchUser')).nativeElement;
    const container = pDropdown.querySelector('.p-dropdown');
    container.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));

    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    const identificationType = component.searchUsersForm.controls['identificationType'].value;
    
    expect(identificationType.name).toEqual('Pasaporte');
    expect(component.isPassport).toBeTrue();
  });
});
