export interface PasswordConditions {
    id: string;
    description: string;
    isValid: boolean;
}