import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TooltipOptions } from 'primeng/api';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { UserService } from 'src/app/shared/services/user.service';
import { passwordFormStructure, personalInformationFormStructure } from 'src/utils/creation-edition-user-validators';
import { MobileWidthService } from 'src/app/shared/services/mobile-width.service';
import { ModalAlert, UserCreationRequest } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { Router } from '@angular/router';

@Component({
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.scss']
})
export class UserCreationComponent implements OnInit, OnDestroy {

  private _fb = inject(FormBuilder);
  private _validatorService = inject(ValidatorService);
  private _modalAlertService = inject(ModalAlertService);
  private _userService = inject(UserService);
  private _mobileWidthService = inject(MobileWidthService);
  private router = inject(Router);

  private _userCreationSubscription: Subscription = new Subscription();
  private _widthSubscription: Subscription = new Subscription();

  public isScreenSmall: boolean = false;
  public isLoading: boolean = false;
  public personalInformationForm: FormGroup = this._fb.group(personalInformationFormStructure(false));
  public passwordForm: FormGroup = this._fb.group(passwordFormStructure);
  public form: FormGroup = this._fb.group({
    personalInformation: this.personalInformationForm,
    password: this.passwordForm
  });
  public tooltipOptions: TooltipOptions = {
    tooltipPosition: 'top',
    tooltipEvent: 'hover',
    positionTop: -10,
    positionLeft: -50,
    tooltipLabel: 'Complete todos los campos',
    tooltipStyleClass: 'user-creation-create-button'
  }
  
  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
  }

  ngOnDestroy(): void {
    this._userCreationSubscription.unsubscribe();
    this._widthSubscription.unsubscribe();
  }

  private resetForm(): void {
    this.form.reset();
    this.passwordForm.controls['sendEmail'].setValue(true);
  }

  public onCreateUser(): void {
    if (this.form.invalid) {
      this._validatorService.isValidField(this.personalInformationForm);
      this._validatorService.isValidField(this.passwordForm);
      return;
    }

    const newUser: UserCreationRequest = {
      address: this.form.value.personalInformation.address,
      cell_phone: this.form.value.personalInformation.phone,
      contact_number: this.form.value.personalInformation.contactNumber,
      document_number: this.form.value.personalInformation.identificationNumber,
      document_type: this.form.value.personalInformation.identificationType.id,
      email: this.form.value.personalInformation.email,
      first_name: this.form.value.personalInformation.name,
      gender: this.form.value.personalInformation.gender,
      last_name: this.form.value.personalInformation.surnames,
      password: this.form.value.password.password,
      send_email:this.form.value.password.sendEmail,
      groups: this.form.value.personalInformation.role,
    }
    
    this.isLoading = true;

    this._userCreationSubscription = this._userService.createUser(newUser).subscribe({
      next: (userName) => {        
        this.isLoading = false;
        this.showSuccessAlertUserCreation(userName);       
      },
      error: (error) => {
        this.isLoading = false;
        error && this.showErrorAlertUserCreation(error);
      }
    });     
  }

  public onCancelUserCreation(): void {
    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
        this.resetForm()
        this.router.navigateByUrl('/')
        }
      },
      cancelButton: {
        primary: false,
        label: 'Continuar',
      },
      message: 'Si hace clic en “Si, cancelar” perderá toda la información diligenciada hasta el momento.'
    }

    this._modalAlertService.showModalAlert(warningAlert);
  }

  public showSuccessAlertUserCreation(userName: string): void {
    const successAlert: ModalAlert = {
      title: 'Usuario creado con éxito',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Continuar',
        primary: true,
        action: () => {
          this.resetForm();
        }
      },
      cancelButton: {
        label: '',
        primary: false,
        action: () => {
          this.resetForm();
        }
      },
      message: `El usuario ${userName} ha sido creado en el sistema exitosamente`,
      highlightText: 'Haga click para continuar'
    }

    this._modalAlertService.showModalAlert(successAlert);
  }

  public showErrorAlertUserCreation(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Continuar',
        primary: true,
      },
      message
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }
}
