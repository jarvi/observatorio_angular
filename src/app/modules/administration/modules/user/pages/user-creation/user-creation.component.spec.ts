import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCreationComponent } from './user-creation.component';
import { UserService } from 'src/app/shared/services/user.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PersonalInformationFormComponent } from '../../components/personal-information-form/personal-information-form.component';
import { PasswordFormComponent } from '../../components/password-form/password-form.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import Swal from 'sweetalert2';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of, throwError } from 'rxjs';
import { SharedModule } from 'src/app/shared/shared.module';

describe('UserCreationComponent', () => {
  let component: UserCreationComponent;
  let fixture: ComponentFixture<UserCreationComponent>;
  let validatorService: ValidatorService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserCreationComponent,
        PersonalInformationFormComponent,
        PasswordFormComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        PrimeNgModule,
        SharedModule,
      ],
      providers: [
        UserService,
        HttpRequestService,
      ]
    });
    fixture = TestBed.createComponent(UserCreationComponent);
    validatorService = TestBed.inject(ValidatorService);
    userService = TestBed.inject(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    Swal.close();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should appear the cancel modal and press the confirm button', (done) => {
    fixture.ngZone!.run(() => {
      component.onCancelUserCreation();
      expect(Swal.isVisible()).toBeTruthy();
  
      const confirmButton = Swal.getConfirmButton();
      confirmButton?.click();
      expect(confirmButton).toBeTruthy();
      done();
    });
  });

  it('should appear the success modal and press the confirm button', () => {
    component.showSuccessAlertUserCreation('User');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton();
    confirmButton?.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should appear the success modal and press the close button', () => {
    component.showSuccessAlertUserCreation('User');
    expect(Swal.isVisible()).toBeTruthy();

    const cancelButton = Swal.getCancelButton();
    cancelButton?.click();
    expect(cancelButton).toBeTruthy();
  });

  it('should appear the error modal and press the confirm button', () => {
    component.showErrorAlertUserCreation('Error');
    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton();
    confirmButton?.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should exec isValidField method if form is not valid', () => {
    spyOn(validatorService, 'isValidField').and.callThrough();
    
    component.onCreateUser();
    expect(validatorService.isValidField).toHaveBeenCalled();
  });

  it('should exec create user method when all is ok', () => {

    const compiled = fixture.nativeElement as HTMLElement;
    
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    
    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));
    
    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    const password = compiled.querySelector('#password') as HTMLInputElement;
    const passwordConfirmation = compiled.querySelector('#password-confirmation') as HTMLInputElement;

    password.value = '1234567@Aa';
    passwordConfirmation.value = '1234567@Aa';

    password.dispatchEvent(new Event('input'));
    passwordConfirmation.dispatchEvent(new Event('input'));

    spyOn(userService, 'createUser').and.returnValue(of('New user'))

    component.onCreateUser();

    expect(component.form.valid).toBeTruthy();
    expect(userService.createUser).toHaveBeenCalled();
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should exec create user method with error', () => {

    const compiled = fixture.nativeElement as HTMLElement;
    
    component.personalInformationForm.controls['identificationType'].setValue({
      id: 1,
      name: 'Cédula de ciudadanía'
    });
    component.personalInformationForm.controls['role'].setValue([1]);

    const identificationNumber = compiled.querySelector('#identificationNumber') as HTMLInputElement;
    const name = compiled.querySelector('#name') as HTMLInputElement;
    const surnames = compiled.querySelector('#surnames') as HTMLInputElement;
    const email = compiled.querySelector('#email') as HTMLInputElement;
    const phone = compiled.querySelector('#phone') as HTMLInputElement;
    const contactNumber = compiled.querySelector('#contactNumber') as HTMLInputElement;
    const address = compiled.querySelector('#address') as HTMLInputElement;

    
    identificationNumber.value = '123456789';
    name.value = 'Name';
    surnames.value = 'Surnames';
    email.value = 'test@medellin.gov.co';
    phone.value = '123456789';
    contactNumber.value = '123456789';
    address.value = 'Address';

    identificationNumber.dispatchEvent(new Event('input'));
    name.dispatchEvent(new Event('input'));
    surnames.dispatchEvent(new Event('input'));
    email.dispatchEvent(new Event('input'));
    phone.dispatchEvent(new Event('input'));
    contactNumber.dispatchEvent(new Event('input'));
    address.dispatchEvent(new Event('input'));
    
    const pDropdown = fixture.debugElement.query(By.css('#gender')).nativeElement;
    const containerGender = pDropdown.querySelector('.p-dropdown');
    containerGender.click();
    fixture.detectChanges();

    const items = fixture.debugElement.query(By.css('.p-dropdown-items'));
    items.children[1].children[0].nativeElement.click();
    fixture.detectChanges();

    const password = compiled.querySelector('#password') as HTMLInputElement;
    const passwordConfirmation = compiled.querySelector('#password-confirmation') as HTMLInputElement;

    password.value = '1234567@Aa';
    passwordConfirmation.value = '1234567@Aa';

    password.dispatchEvent(new Event('input'));
    passwordConfirmation.dispatchEvent(new Event('input'));

    spyOn(userService, 'createUser').and.returnValue(throwError(() => 'error'))

    component.onCreateUser();

    expect(component.form.valid).toBeTruthy();
    expect(userService.createUser).toHaveBeenCalled();
    expect(Swal.isVisible()).toBeTruthy();
  });
});
