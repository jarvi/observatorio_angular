import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SharedModule } from 'src/app/shared/shared.module';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

import { UserAdministrationComponent } from './user-administration.component';
import { UserService } from 'src/app/shared/services/user.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { SearchUsersFormComponent } from '../../components/search-users-form/search-users-form.component';
import { of, throwError } from 'rxjs';
import { UsersSearchResponse } from 'src/app/shared/interfaces/users-search-response.interface';

describe('UserAdministrationComponent', () => {
  let component: UserAdministrationComponent;
  let fixture: ComponentFixture<UserAdministrationComponent>;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchUsersFormComponent,
        UserAdministrationComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        PrimeNgModule
      ],
      providers: [
        HttpRequestService,
        UserService,
      ]
    });
    fixture = TestBed.createComponent(UserAdministrationComponent);
    userService = TestBed.inject(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change page value when onPageChange has been executed', () => {
    component.onPageChange(2);
    expect(component.searchUsersForm.controls['page'].value).toEqual(2);
  });

  it('should change page and perPage value when changePerPage has been executed', () => {
    component.changePerPage('10');

    expect(component.searchUsersForm.controls['perPage'].value).toEqual('10');
    expect(component.searchUsersForm.controls['page'].value).toEqual(1);
  });

  it('should reset form when clearSearchUsersForm has been executed and return default value', () => {
    component.clearSearchUsersForm();
    
    expect(component.searchUsersForm.controls['perPage'].value).toEqual(10);
    expect(component.searchUsersForm.controls['page'].value).toEqual(1);
  });

  it('should get the users successfully and fill the user variable', () => {
    component.searchUsersForm.controls['perPage'].setValue(10);
    component.searchUsersForm.controls['page'].setValue(1);
    component.searchUsersForm.controls['identificationType'].setValue(1);
    component.searchUsersForm.controls['identificationNumber'].setValue('123456789');

    const response: UsersSearchResponse = {
      count: 1,
      next: '',
      previous: '',
      results: [{
        address: 'Calle falsa 123',
        cell_phone: '1234567890',
        contact_number: '1234567890',
        document_number: '1234567890',
        document_type: {
          id: 1,
          alias: 'CC',
          name: 'Cédula de ciudadanía'
        },
        email: 'prueba@mail.com',
        first_name: 'Prueba',
        gender: 'M',
        id: 1,
        is_active: true,
        last_name: 'PruebaApellido',
        groups: [{
          id: 1,
          is_active: true,
          name: 'Administrador',
        }]
      }]
    }

    spyOn(userService, 'searchUsers').and.returnValue(of(response));

    component.searchUsers();
    
    expect(component.users.length).toEqual(1);
  });

  it('should get the users successfully and fill the user variable when onSearchUsers is executed', () => {
    component.searchUsersForm.controls['perPage'].setValue(10);
    component.searchUsersForm.controls['page'].setValue(1);
    component.searchUsersForm.controls['identificationType'].setValue(1);
    component.searchUsersForm.controls['identificationNumber'].setValue('123456789');

    const response: UsersSearchResponse = {
      count: 1,
      next: '',
      previous: '',
      results: [{
        address: 'Calle falsa 123',
        cell_phone: '1234567890',
        contact_number: '1234567890',
        document_number: '1234567890',
        document_type: {
          id: 1,
          alias: 'CC',
          name: 'Cédula de ciudadanía'
        },
        email: 'prueba@mail.com',
        first_name: 'Prueba',
        gender: 'M',
        id: 1,
        is_active: true,
        last_name: 'PruebaApellido',
        groups: [{
          id: 1,
          is_active: true,
          name: 'Administrador',
        }]
      }]
    }

    spyOn(userService, 'searchUsers').and.returnValue(of(response));

    component.onSearchUsers();
    
    expect(component.users.length).toEqual(1);
    expect(component.searchUsersForm.controls['perPage'].value).toEqual(10);
    expect(component.searchUsersForm.controls['page'].value).toEqual(1);
  });

  it('should get the users successfully and fill the user variable when address and contact_number is null', () => {
    component.searchUsersForm.controls['perPage'].setValue(10);
    component.searchUsersForm.controls['page'].setValue(1);
    component.searchUsersForm.controls['identificationType'].setValue(1);
    component.searchUsersForm.controls['identificationNumber'].setValue('123456789');

    const response: UsersSearchResponse = {
      count: 1,
      next: '',
      previous: '',
      results: [{
        address: null,
        cell_phone: '1234567890',
        contact_number: null,
        document_number: '1234567890',
        document_type: {
          id: 1,
          alias: 'CC',
          name: 'Cédula de ciudadanía'
        },
        email: 'prueba@mail.com',
        first_name: 'Prueba',
        gender: 'M',
        id: 1,
        is_active: true,
        last_name: 'PruebaApellido',
        groups: []
      }]
    }

    spyOn(userService, 'searchUsers').and.returnValue(of(response));

    component.searchUsers();
    
    expect(component.users.length).toEqual(1);
  });

  it('should get the users successfully and does not fill the user variable', () => {
    component.searchUsersForm.controls['perPage'].setValue(10);
    component.searchUsersForm.controls['page'].setValue(1);
    component.searchUsersForm.controls['identificationType'].setValue(1);
    component.searchUsersForm.controls['identificationNumber'].setValue('123456789');

    const response: UsersSearchResponse = {
      count: 0,
      next: '',
      previous: '',
      results: []
    }

    spyOn(userService, 'searchUsers').and.returnValue(of(response));

    component.searchUsers();
    
    expect(component.users.length).toEqual(0);
  });

  it('should get the users successfully and does not fill the user variable with user name param', () => {
    component.searchUsersForm.controls['perPage'].setValue(10);
    component.searchUsersForm.controls['page'].setValue(1);
    component.searchUsersForm.controls['identificationType'].setValue(1);
    component.searchUsersForm.controls['name'].setValue('Prueba');

    const response: UsersSearchResponse = {
      count: 0,
      next: '',
      previous: '',
      results: []
    }

    spyOn(userService, 'searchUsers').and.returnValue(of(response));

    component.searchUsers();
    
    expect(component.users.length).toEqual(0);
  });

  it('should does not get the users successfully and doest fill the user variable', () => {
    component.searchUsersForm.controls['perPage'].setValue(10);
    component.searchUsersForm.controls['page'].setValue(1);
    component.searchUsersForm.controls['identificationType'].setValue(1);
    component.searchUsersForm.controls['identificationNumber'].setValue('123456789');

    spyOn(userService, 'searchUsers').and.returnValue(throwError(() => {}));

    component.searchUsers();
    
    expect(component.users.length).toEqual(0);
  });
});
