import { Component, ElementRef, OnDestroy, ViewChild, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TooltipOptions } from 'primeng/api';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { UserService } from '../../../../../../shared/services/user.service';
import { formHasSomeValue } from 'src/utils/verify-some-value-on-form';
import { perPageOptions } from 'src/app/shared/constants/perpage-options';
import { slideInOutAnimation } from 'src/app/shared/animations/animations';
import { User, UsersSearchRequest } from 'src/app/shared/interfaces';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  templateUrl: './user-administration.component.html',
  animations: [slideInOutAnimation],
  styleUrls: ['./user-administration.component.scss']
})
export class UserAdministrationComponent implements OnDestroy {

  @ViewChild('userTable') userTable!: ElementRef;

  private _fb = inject(FormBuilder);
  private _validatorService = inject(ValidatorService);
  private _userService = inject(UserService);

  private usersSearchSubscription: Subscription = new Subscription();

  public perpageResultsOptions: string[] = perPageOptions.map((option) => option.toString());
  public showForm: boolean = true;
  public isLoading: boolean = false;
  public formIsDirty: boolean = false;
  public users: User[] = [];
  public totalUsers: number = 0;
  public userPermissions = {
    create: PERMISSION_TYPE.addUser.name,
    consult: PERMISSION_TYPE.viewUser.name,
  }
  public searchUsersForm: FormGroup = this._fb.group({
    identificationType: [null],
    identificationNumber: [null, [
      this._validatorService.minMaxLengthValidator(0, 30),
      this._validatorService.whiteSpacesStartValidator()
    ]],
    name: ['', [
      Validators.maxLength(30),
      this._validatorService.whiteSpacesStartValidator()
    ]],
    surnames: ['', [
      Validators.maxLength(30),
      this._validatorService.whiteSpacesStartValidator()
    ]],
    email: ['', [
      Validators.maxLength(40),
      this._validatorService.whiteSpacesStartValidator()
    ]],
    perPage: [10],
    page: [1],
  });
  public tooltipOptions: TooltipOptions = {
    tooltipPosition: 'top',
    tooltipEvent: 'hover',
    positionTop: -10,
    positionLeft: -50,
    tooltipLabel: 'El botón se habilita al diligenciar el campo para la búsqueda',
    tooltipStyleClass: 'user-administration-search-button'
  }
  
  ngOnDestroy(): void {
    this.usersSearchSubscription.unsubscribe();
  }

  private scrollToTable() {
    this.userTable?.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  public checkValuesForm(): boolean {
    return formHasSomeValue(this.searchUsersForm, ['perPage', 'page']);
  }

  public getUsers(): void {
    this.usersSearchSubscription.unsubscribe();
    this.isLoading = true;
    this.formIsDirty = true;

    const usersSearchRequest: UsersSearchRequest = {
      document_number: (this.searchUsersForm.value.identificationNumber || '').trim(),
      document_type: this.searchUsersForm.value.identificationType?.id || '',
      email: (this.searchUsersForm.value.email || '').trim(),
      first_name: (this.searchUsersForm.value.name || '').trim(),
      last_name: (this.searchUsersForm.value.surnames || '').trim(),
      page: this.searchUsersForm.value.page,
      page_size: this.searchUsersForm.value.perPage,
    }

    this.usersSearchSubscription = this._userService.searchUsers(usersSearchRequest).subscribe({
      next: (response) => {
        executeCallbackLate(() => this.scrollToTable());

        this.isLoading = false;
        if (response.results.length === 0) {
          this.users = [];
          return;
        }
        
        this.totalUsers = response.count;
        this.users = response.results.map((user) => {
          return {
            address: user.address ?? '',
            cellPhone: user.cell_phone,
            completeName: `${user.first_name} ${user.last_name}`,
            contactNumber: user.contact_number ?? '',
            documentNumber: user.document_number,
            documentType: user.document_type,
            email: user.email,
            firstName: user.first_name,
            gender: user.gender,
            id: user.id,
            isActive: user.is_active,
            lastName: user.last_name,
            roles: user.groups.map((group) => {
              return {
                id: group.id,
                name: group.name,
                isActive: group.is_active,
              }
            })
          }
        });
      },
      error: (error) => {
        this.isLoading = false;
        this.users = [];
      }
    });
  }

  public onSearchUsers() {
    this.searchUsersForm.controls['perPage'].setValue(10);
    this.searchUsersForm.controls['page'].setValue(1);
    this.searchUsers();
  }

  public searchUsers(): void {
    if (this.searchUsersForm.invalid || !this.checkValuesForm()) {
      this._validatorService.isValidField(this.searchUsersForm);
      return;
    }
    
    this.getUsers();
  }

  public clearSearchUsersForm(): void {
    this.searchUsersForm.reset();
    this.searchUsersForm.controls['perPage'].setValue(10);
    this.searchUsersForm.controls['page'].setValue(1);
    this.users = [];
    this.formIsDirty = false;
  }

  public changePerPage(perPage: string): void {
    this.searchUsersForm.controls['perPage'].setValue(perPage);
    this.searchUsersForm.controls['page'].setValue(1);
    this.searchUsers();
  }

  public onPageChange(page: number): void {
    this.searchUsersForm.controls['page'].setValue(page);
    this.searchUsers();
  }
}
