import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PERMISSION_TYPE } from 'src/app/shared/enums/permission-type.enum';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'systems-users',
    loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule),
    data: {
      breadcrumb: 'Usuarios del sistema',
      permissions: [
        PERMISSION_TYPE.addUser.name,
        PERMISSION_TYPE.changeUser.name,
        PERMISSION_TYPE.deleteUser.name,
        PERMISSION_TYPE.viewUser.name,
      ]
    }
  },
  {
    path: 'roles',
    loadChildren: () => import('./modules/role/role.module').then(m => m.RoleModule),
    data: {
      breadcrumb: 'Roles',
      permissions: [
        PERMISSION_TYPE.addRol.name,
        PERMISSION_TYPE.changeRol.name,
        PERMISSION_TYPE.deleteRol.name,
        PERMISSION_TYPE.viewRol.name,
      ]
    }
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
