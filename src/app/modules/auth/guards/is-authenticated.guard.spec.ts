import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, CanActivateFn, Router } from '@angular/router';

import { isAuthenticatedGuard } from './is-authenticated.guard';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthModule } from '../auth.module';

describe('isAuthenticatedGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => isAuthenticatedGuard(...guardParameters));

  let router: Router;
  const authorizationTokens = {
    accessToken: '12345',
    refreshToken: '12345'
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AuthModule
      ]
    });
    router = TestBed.inject(Router);
  });

  afterEach(() => {
    sessionStorage.clear();
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });

  it('should deny access when authStatus is true', () => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));

    sessionStorage.setItem('accessToken', authorizationTokens.accessToken);
    sessionStorage.setItem('refreshToken', authorizationTokens.refreshToken);

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const accessDeny = executeGuard(route, state);

    expect(accessDeny).toBeTrue();
  });

  it('should allow access when authStatus is false', () => {
    spyOn(router, 'navigateByUrl');

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const accessDeny = executeGuard(route, state);
    
    expect(accessDeny).toBeFalse();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/auth');
  });
});
