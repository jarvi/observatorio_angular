import { TestBed } from '@angular/core/testing';

import { BroadcastSessionService } from './broadcast-session.service';
import { UserService } from 'src/app/shared/services/user.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BroadcastSessionService', () => {
  let service: BroadcastSessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService,
        UserService
      ]
    });
    service = TestBed.inject(BroadcastSessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
