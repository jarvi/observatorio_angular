import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginRequest } from '../interfaces/login-request.interface';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { Observable, of, throwError } from 'rxjs';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { IRecoveryPassword } from '../interfaces/reset-password.interface';

describe('AuthService', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;
  let router: Router;
  let spyHttpRequestService: jasmine.Spy;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    localStorage.setItem('accessToken', '123123123');
    localStorage.setItem('refreshToken', '123123123');

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    router = TestBed.inject(Router);

    spyHttpRequestService = spyOn(httpRequestService, 'post')
    spyHttpRequestService.and.returnValue(of({
      access: '123123123'
    }));

    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should exec the login method and response true', () => {
    const loginRequest: LoginRequest = {
      email: 'prueba@medellin.gov.co',
      password: '12345678',
      keepLoggedIn: false
    };

    spyHttpRequestService.and.returnValue(of({
      access: '123123123',
      refresh: '123123123'
    }));

    service.login(loginRequest).subscribe((response) => {
      expect(response).toBeTruthy();
    });
  });

  it('should exec the login method with bad request when user is inactive, and show modal error alert', () => {
    const loginRequest: LoginRequest = {
      email: 'prueba@medellin.gov.co',
      password: '12345678',
      keepLoggedIn: false
    };

    spyHttpRequestService.and.returnValue(throwError(() => {
      return {
        code: 'inactive_user'
      }
    }));

    service.login(loginRequest).subscribe(
      () => { },
      (err) => {
        expect(Swal.isVisible()).toBeTruthy();
      }
    );
  });

  it('should exec the login method and response true when keepLoggedIn is true', () => {
    const loginRequest: LoginRequest = {
      email: 'prueba@medellin.gov.co',
      password: '12345678',
      keepLoggedIn: true
    };

    spyHttpRequestService.and.returnValue(of({
      access: '123123123',
      refresh: '123123123'
    }));

    service.login(loginRequest).subscribe((response) => {
      expect(response).toBeTruthy();
    });
  });

  it('should exec a bad http get request and return the error message when is status code is greater than 0', () => {
    const error = {
      detail: 'Error de prueba',
    };

    const loginRequest: LoginRequest = {
      email: 'prueba@medellin.gov.co',
      password: '12345678',
      keepLoggedIn: false
    };

    spyHttpRequestService.and.returnValue(throwError(() => error));

    service.login(loginRequest).subscribe(
      () => { },
      (err) => {
        expect(err).toEqual(error.detail);
      }
    );
  });

  it('should exec a bad http get request and return the error message without detail when is status code is greater than 0', () => {
    const error = {
      noDetail: 'Error de prueba',
    };

    const loginRequest: LoginRequest = {
      email: 'prueba@medellin.gov.co',
      password: '12345678',
      keepLoggedIn: false
    };

    spyHttpRequestService.and.returnValue(throwError(() => error));

    service.login(loginRequest).subscribe(
      () => { },
      (err) => {
        expect(err).toEqual('');
      }
    );
  });

  it('should remove all session in local and session storages', () => {
    spyOn(router, 'navigateByUrl');

    service.logout();

    expect(localStorage.getItem('accessToken')).toBeFalsy();
    expect(localStorage.getItem('refreshToken')).toBeFalsy();
    expect(sessionStorage.getItem('accessToken')).toBeFalsy();
    expect(sessionStorage.getItem('refreshToken')).toBeFalsy();
  });

  it('should return true when user is authenticated', () => {
    localStorage.setItem('accessToken', '123123123');
    localStorage.setItem('refreshToken', '123123123');

    const isAuthenticated = service.isAuthenticated();

    expect(isAuthenticated).toBeTrue();
  });

  it('should return false when user is not authenticated', () => {
    spyOn(router, 'navigateByUrl');
    service.logout();
    const isAuthenticated = service.isAuthenticated();

    expect(isAuthenticated).toBeFalse();
  });

  it('should return true the recovery password service ', () => {
    const request: IRecoveryPassword = {
      email: 'prueba@medellin.gov.co',
      document_number: '123456',
      domain: 'domain'
    };

    spyHttpRequestService.and.returnValue(of(true));

    service.recoveryPasswordService(request).subscribe((response) => {
      expect(response).toBeTruthy();
    });
  });

  it('should return false if the recovery password service return a bad request', () => {
    const request: IRecoveryPassword = {
      email: 'prueba@medellin.gov.co',
      document_number: '123456',
      domain: 'domain'
    };
    spyHttpRequestService.and.returnValue(throwError(() => false));

    service.recoveryPasswordService(request).subscribe(
      () => { },
      (err) => {
        expect(err).toEqual('');
      }
    );
  });

  it('should return true the change password service ', () => {
    const request: IRecoveryPassword = {
      email: 'prueba@medellin.gov.co',
      document_number: '123456',
      domain: 'domain'
    };

    spyHttpRequestService.and.returnValue(of(true));

    service.changePasswordService(request).subscribe((response) => {
      expect(response).toBeTruthy();
    });
  });

  it('should return false if the change password service return a bad request', () => {
    const request: IRecoveryPassword = {
      email: 'prueba@medellin.gov.co',
      document_number: '123456',
      domain: 'domain'
    };
    spyHttpRequestService.and.returnValue(throwError(() => false));

    service.changePasswordService(request).subscribe(
      () => { },
      (err) => {
        expect(err).toEqual('');
      }
    );
  });
});

describe('AuthService sessionStorage', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    sessionStorage.setItem('accessToken', '123123123');
    sessionStorage.setItem('refreshToken', '123123123');

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);

    spyOn(httpRequestService, 'post').and.returnValue(of({
      access: '123123123'
    }));

    service = TestBed.inject(AuthService);
  });

  it('should exec by default the post method when constructor is builded', () => {
    expect(httpRequestService.post).toHaveBeenCalled();
  });
});

describe('AuthService token invalid', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    router = TestBed.inject(Router);
    spyOn(router, 'navigateByUrl');

    httpRequestService = TestBed.inject(HttpRequestService);

    service = TestBed.inject(AuthService);
  });

  it('should return false when token is invalid', () => {
    localStorage.setItem('accessToken', '123123123');
    localStorage.setItem('refreshToken', '123123123');
  
    const isAuthenticated = service.isAuthenticated();
  
    expect(isAuthenticated).toBeFalse();
  });
});

describe('AuthService sessionStorage', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    sessionStorage.setItem('accessToken', '123123123');
    sessionStorage.setItem('refreshToken', '123123123');

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);

    spyOn(httpRequestService, 'post').and.returnValue(of({
      access: '123123123'
    }));

    service = TestBed.inject(AuthService);
  });

  it('should exec by default the post method when constructor is builded', () => {
    expect(httpRequestService.post).toHaveBeenCalled();
  });
});

describe('AuthService sessionStorage with error in authentication', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;
  let router: Router;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    const error = {
      detail: 'Error de prueba',
    };

    sessionStorage.setItem('accessToken', '123123123');
    sessionStorage.setItem('refreshToken', '123123123');

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    router = TestBed.inject(Router);
    spyOn(router, 'navigateByUrl');

    httpRequestService = TestBed.inject(HttpRequestService);
    spyOn(httpRequestService, 'post').and.returnValue(throwError(() => error));

    service = TestBed.inject(AuthService);
  });

  it('should exec by default the post method when constructor is builded', () => {
    expect(httpRequestService.post).toHaveBeenCalled();
  });
});

describe('AuthService without access', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;
  let router: Router;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    router = TestBed.inject(Router);

    spyOn(router, 'navigateByUrl');

    service = TestBed.inject(AuthService);
  });

  it('should exec by default the post method when constructor is builded', () => {
    const accessToken = localStorage.getItem('accessToken');
    const refreshToken = localStorage.getItem('refreshToken');
    expect(accessToken && refreshToken).toBeFalsy();
  });
});

describe('AuthService when verify is false', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;
  let router: Router;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    sessionStorage.setItem('accessToken', '123123123');
    sessionStorage.setItem('refreshToken', '123123123');

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    router = TestBed.inject(Router);
    spyOn(router, 'navigateByUrl');

    httpRequestService = TestBed.inject(HttpRequestService);
    spyOn(httpRequestService, 'post').and.callFake((path: string, body?: Partial<any>): Observable<any> => {
      if (path === '/user/token/verify/') {
        expect(body).toEqual({ token: '123123123' });
        return of({});
      } else {
        expect(body).toEqual({ refresh: '123123123' });
        return throwError(() => ({ detail: 'Error de prueba' }));
      }
    })

    service = TestBed.inject(AuthService);
  });

  it('should exec by default the post method when constructor is builded', () => {
    expect(httpRequestService.post).toHaveBeenCalled();
  });
});

describe('AuthService when the route is /login', () => {
  let service: AuthService;
  let httpRequestService: HttpRequestService;
  let router: Router;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(() => {
    sessionStorage.setItem('accessToken', '123123123');
    sessionStorage.setItem('refreshToken', '123123123');

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: Router,
          useValue: {
            url: '/auth',
            navigateByUrl: () => { }
          }
        }
      ]
    });
    router = TestBed.inject(Router);
    spyOn(router, 'navigateByUrl');

    httpRequestService = TestBed.inject(HttpRequestService);
    spyOn(httpRequestService, 'post').and.returnValue(of({
      access: '123123123'
    }));

    service = TestBed.inject(AuthService);
  });

  it('should does not exec navigateByUrl if the current route is /login', () => {
    expect(router.navigateByUrl).not.toHaveBeenCalled();
  });
});