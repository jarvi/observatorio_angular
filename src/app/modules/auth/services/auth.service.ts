import { Injectable, OnDestroy, inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription, catchError, map, mergeMap, of, throwError } from 'rxjs';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';
import { BroadcastSessionService } from './broadcast-session.service';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { UserService } from 'src/app/shared/services/user.service';
import { CleanerStatesService } from 'src/app/shared/services/cleaner-states.service';
import { verifyTokenExpiration } from 'src/utils/verify-token-expiration';
import { LoginRequest } from '../interfaces/login-request.interface';
import { LoginResponse } from '../interfaces/login-response.interface';
import { RefreshTokenRequest } from '../interfaces/refresh-token-request.interface';
import { RefreshTokenResponse } from '../interfaces/refresh-token-response.interface';
import { VerifyTokenRequest } from '../interfaces/verify-token-request.interface';
import { IRecoveryPassword } from '../interfaces/reset-password.interface';
import { ModalAlert } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {

  private _router = inject(Router);
  private _httpRequestService = inject(HttpRequestService);
  private _broadcastSessionService = inject(BroadcastSessionService);
  private _userService = inject(UserService);
  private _modalAlertService = inject(ModalAlertService);
  private _cleanerStatesService = inject(CleanerStatesService);

  private _subscription: Subscription = new Subscription();
  private _pathUser: string = environment.pathUser;

  constructor() {
    this._broadcastSessionService.broadcastSessionStorage(() => {
      sessionStorage.clear();
      localStorage.clear();
      
      this.removeLocalStorageSession();
      this.removeSessionStorageSession();
      
      if (this._router.url === '/auth') return;

      this._router.navigateByUrl('/auth');
    }, () => {
      this._router.navigateByUrl('/');
    })

    this._subscription = this.checkAuthStatus().subscribe();
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  private useKeepLoggedIn(): boolean {
    const accessToken = localStorage.getItem('accessToken');
    const refreshToken = localStorage.getItem('refreshToken');
    
    return !!accessToken && !!refreshToken;
  }

  private removeLocalStorageSession(): void {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
  }

  private removeSessionStorageSession(): void {
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('refreshToken');
  }

  private setAuthentication(accessToken: string, refreshToken: string, keepLoggedIn: boolean): boolean {
    if (keepLoggedIn) {
      localStorage.setItem('accessToken', accessToken);
      localStorage.setItem('refreshToken', refreshToken);

    } else {
      this.removeLocalStorageSession();
      
      sessionStorage.setItem('accessToken', accessToken);
      sessionStorage.setItem('refreshToken', refreshToken);
    }
    return true;
  }

  private checkAuthStatus(): Observable<boolean> {
    if (!this.isAuthenticated()) {
      this.localLogout();
      return of(false);
    }

    const useKeepLoggedIn = this.useKeepLoggedIn();
    const refresh: string = useKeepLoggedIn ? localStorage.getItem('refreshToken')! : sessionStorage.getItem('refreshToken')!;

    return this.verifyToken().pipe(
      mergeMap((isTokenValid) => {
        if (!isTokenValid) {
          this.logout();
          return of(false);
        }

        return this._httpRequestService.post<RefreshTokenRequest, RefreshTokenResponse>(`${this._pathUser}/token/refresh/`, { refresh })
        .pipe(
          map(({ access }) => this.setAuthentication(access, refresh, useKeepLoggedIn)),
          catchError(() => {
            this.logout();
            return of(false);
          })
        );
      })
    );
  }

  private getAccessToken(): string {
    let accessToken = sessionStorage.getItem('accessToken');
    if (this.useKeepLoggedIn()) {
      accessToken = localStorage.getItem('accessToken');
    }
    return accessToken!;
  }

  private verifyToken(): Observable<boolean> {
    return this._httpRequestService.post<VerifyTokenRequest, {}>(`${this._pathUser}/token/verify/`, { token: this.getAccessToken() })
      .pipe(
        map(() => true),
        catchError(() => {
          return of(false)
        })
      );
  }

  public isAuthenticated(): boolean {
    let accessToken = sessionStorage.getItem('accessToken');
    let refreshToken = sessionStorage.getItem('refreshToken');
    
    if (this.useKeepLoggedIn()) {
      accessToken = localStorage.getItem('accessToken');
      refreshToken = localStorage.getItem('refreshToken');
    }
    
    if (!accessToken || !refreshToken) { return false; }

    const accessTokenIsValid = verifyTokenExpiration(accessToken);
    const refreshTokenIsValid = verifyTokenExpiration(refreshToken);

    return accessTokenIsValid && refreshTokenIsValid;
  }

  public login(request: LoginRequest): Observable<boolean> {
    return this._httpRequestService.post<LoginRequest, LoginResponse>(`${this._pathUser}/login/`, request)
      .pipe(
        map(({ access, refresh }) => {
          const login = this.setAuthentication(access, refresh, request.keepLoggedIn);
          if (login && request.keepLoggedIn) this._broadcastSessionService.postMessage('reloadSession');
          return login;
        }),
        catchError(error => {
          if (error?.code === 'inactive_user') {
            this.showModalErrorAlertOnLogin();
            return throwError(() => '');
          }
          return throwError(() => error?.detail || '');
        })
      );
  }

  public recoveryPasswordService(request: IRecoveryPassword) {
    return this._httpRequestService.post(`${this._pathUser}/valid-doc-email/`, request).pipe(
      map(resp => {
        return resp
      }), catchError(error => {
        return throwError(() => error?.message || '');
      })
    )
  }

  public changePasswordService(request: IRecoveryPassword) {
    return this._httpRequestService.post(`${this._pathUser}/recovery-password/`, request).pipe(
      map(resp => {
        return resp
      }), catchError(error => {
        return throwError(() => error?.message || '');
      })
    )
  }

  private showModalErrorAlertOnLogin(): void {
    const modalAlertError: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true
      },
      message: '¡No es posible iniciar sesión!',
      highlightText: 'Por favor comuníquese con el administrador del sistema.'
    }

    this._modalAlertService.showModalAlert(modalAlertError);
  }

  public logout(): void {
    this.removeLocalStorageSession();
    this.removeSessionStorageSession();
    this._router.navigateByUrl('/auth');
    this._broadcastSessionService.postMessage(`clearSession_${this._userService.userInformation.id || ''}`);
    
    this._cleanerStatesService.clearStates();
  }

  private localLogout(): void {
    this.removeLocalStorageSession();
    this.removeSessionStorageSession();
  }
}
