import { Injectable, NgZone, inject } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class BroadcastSessionService {
  
  private _ngZone = inject(NgZone)
  private _userService = inject(UserService);

  private _broadcastChannel: BroadcastChannel;

  constructor() {
    this._broadcastChannel = new BroadcastChannel('sessionStorage_channel');
  }

  public broadcastSessionStorage(callbackClear: () => void, callbackReload: () => void): void {
    this._broadcastChannel.onmessage = (event) => {
      this._ngZone.run(() => {
        if (event.data === `clearSession_${this._userService.userInformation.id || ''}`) {
          callbackClear();
        }
        if(event.data === 'reloadSession') {
          callbackReload();
        }
      });
    };
  }

  public postMessage(message: string): void {
    this._broadcastChannel.postMessage(message);
  }
}
