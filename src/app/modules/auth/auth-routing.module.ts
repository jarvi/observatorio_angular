import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';
import { isNotAuthenticatedGuard } from './guards';
import { RecoveryPasswordComponent } from './pages/recovery-password/recovery-password.component';

const routes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
    canActivateChild: [isNotAuthenticatedGuard],
    children: [
      {
        path: '',
        component: LoginPageComponent,
      },
      {
        path: 'recovery-password',
        component: RecoveryPasswordComponent,
      },
      {
        path: 'recovery-password/:token',
        component: RecoveryPasswordComponent,
      },
      {
        path: '**',
        redirectTo: ''
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
