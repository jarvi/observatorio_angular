export interface LoginRequest {
    email: string;
    password: string;
    keepLoggedIn: boolean;
}