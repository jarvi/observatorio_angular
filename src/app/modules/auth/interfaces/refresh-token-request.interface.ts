export interface RefreshTokenRequest {
    refresh: string;
}