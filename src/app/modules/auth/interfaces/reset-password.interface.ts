export interface IRecoveryPassword {
    document_number: string;
    email: string;
    domain: string;
}

export interface IResponseValidConfirm {
    message: string;
}