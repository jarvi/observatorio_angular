import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthService } from '../../services/auth.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';

import { LoginPageComponent } from './login-page.component';
import { AuthModule } from '../../auth.module';
import { Router } from '@angular/router';
import { INFO_MESSAGE_TYPE } from 'src/app/shared/enums/info-message-type.enum';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { of, throwError } from 'rxjs';
import { LoginRequest } from '../../interfaces/login-request.interface';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let authService: AuthService;
  let router: Router;
  let validatorService: ValidatorService;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(async() => {
    await TestBed.configureTestingModule({
      declarations: [LoginPageComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        AuthModule
      ],
      providers: [
        AuthService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(LoginPageComponent);
    authService = TestBed.inject(AuthService);
    validatorService = TestBed.inject(ValidatorService);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should return to default value of infoMessage when onCloseInfoMessage is executed', () => {
    component.onCloseInfoMessage();
    expect(component.infoMessage.message).toBe('');
    expect(component.infoMessage.type).toBe(INFO_MESSAGE_TYPE.error);
  });

  it('should change the hidePassword variable value when togglePasswordVisibility is executed', () => {
    const hidePassword = component.hidePassword;
    component.togglePasswordVisibility();
    expect(component.hidePassword).toBe(!hidePassword);
  });

  it('should exec the login method successfully when form is valid', () => {

    const compiled = fixture.nativeElement as HTMLElement;
    const emailInput = compiled.querySelector('#email') as HTMLInputElement;
    const passwordInput = compiled.querySelector('#password') as HTMLInputElement;
    const form = compiled.querySelector('.form') as HTMLFormElement;
    
    emailInput.value = 'prueba@medellin.gov.co';
    emailInput.dispatchEvent(new Event('input'));
    passwordInput.value = '12345678';
    passwordInput.dispatchEvent(new Event('input'));
    form.dispatchEvent(new Event('submit'));
    
    expect(component.infoMessage.message).toEqual('');
  });

  it('should exec the login method unsuccessfully when form is invalid', () => {

    spyOn(validatorService, 'isValidField');

    const compiled = fixture.nativeElement as HTMLElement;
    const emailInput = compiled.querySelector('#email') as HTMLInputElement;
    const passwordInput = compiled.querySelector('#password') as HTMLInputElement;
    const form = compiled.querySelector('.form') as HTMLFormElement;
    
    emailInput.value = 'prueba@medelling.gov.co';
    emailInput.dispatchEvent(new Event('input'));
    passwordInput.value = '12345678';
    passwordInput.dispatchEvent(new Event('input'));
    form.dispatchEvent(new Event('submit'));
    
    expect(validatorService.isValidField).toHaveBeenCalled();
  });

  it('should navigate to dashboard', () => {
    spyOn(authService, 'login').and.returnValue(of(true));
    spyOn(router, 'navigateByUrl');

    const request: LoginRequest = {
      email: 'prueba@medellin.gov.co',
      password: '12345678',
      keepLoggedIn: false
    }
    component.login(request);
    expect(router.navigateByUrl).toHaveBeenCalledWith('/');
  });

  it('should exec a bad http get request and return the error message when is status code is greater than 0', () => {
    const error = { 
      status: 500, message: 'Internal Server Error'
    };

    const request: LoginRequest = {
      email: 'prueba@medellin.gov.co',
      password: '12345678',
      keepLoggedIn: false
    };

    spyOn(authService, 'login').and.returnValue(throwError(() => error.message));

    component.login(request);
    expect(component.infoMessage.message).toEqual(error.message);
  });
});
