import { Component, OnDestroy, inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { LoginRequest } from '../../interfaces/login-request.interface';
import { InfoMessage } from 'src/app/shared/interfaces';
import { INFO_MESSAGE_TYPE } from 'src/app/shared/enums/info-message-type.enum';

@Component({
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnDestroy {
  
  private _router = inject(Router);
  private _fb = inject(FormBuilder);
  private _authService = inject(AuthService);
  private _validatorService = inject(ValidatorService);

  private _subscription: Subscription = new Subscription();

  public isLoading: boolean = false;
  public hidePassword: boolean = false;
  public infoMessage: InfoMessage = {
    message: '',
    type: INFO_MESSAGE_TYPE.error
  }

  public loginForm: FormGroup = this._fb.group({
    email: ['', [
      Validators.required,
      Validators.maxLength(40), 
      this._validatorService.emailValidator(),
      this._validatorService.medellinEmailValidator(),
      this._validatorService.whiteSpacesStartValidator()
    ]],
    keepLoggedIn: [false],
    password: ['', [
      Validators.required,
      Validators.minLength(7),
      this._validatorService.whiteSpacesValidator()
    ]]
  });

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  public togglePasswordVisibility(): void { this.hidePassword = !this.hidePassword; }

  public onCloseInfoMessage(): void {
    this.infoMessage = {
      message: '',
      type: INFO_MESSAGE_TYPE.error
    }
  }

  public enabledShowPasswordButton(): boolean {
    const enabled = !!this.loginForm.controls['password']?.value;
    if (!enabled) this.hidePassword = false;
    return !enabled;
  }

  public login(request: LoginRequest): void {
    this.isLoading = true;
    this._subscription = this._authService.login(request).subscribe({
      next: () => {
        this.isLoading = false;
        this._router.navigateByUrl('/');
      },
      error: (error) => {
        this.isLoading = false;
        this.infoMessage = {
          message: error,
          type: INFO_MESSAGE_TYPE.error
        }
      }
    });
  }

  public onLogin(): void {
    if (!this.loginForm.valid) {
      this._validatorService.isValidField(this.loginForm);
      return;
    }

    this.onCloseInfoMessage();

    const request: LoginRequest = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
      keepLoggedIn: this.loginForm.value.keepLoggedIn
    }

    this.login(request);
  }
}
