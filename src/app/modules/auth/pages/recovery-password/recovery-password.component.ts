import { Component, EventEmitter, OnDestroy, OnInit, Output, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { InfoMessage } from 'src/app/shared/interfaces/info-message.interface';
import { INFO_MESSAGE_TYPE } from 'src/app/shared/enums/info-message-type.enum';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { ModalAlert } from 'src/app/shared/interfaces';
import { MODAL_ALERT_TYPE } from 'src/app/shared/enums/modal-alert-type.enum';
import { ModalAlertService } from 'src/app/shared/services/modal-alert.service';
import { PasswordConditions } from 'src/app/modules/administration/modules/user/interfaces/password-conditions.interface';
import { onPasswordChangeValidateConditions } from 'src/utils/password-change-validate-conditions';

@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.scss']
})
export class RecoveryPasswordComponent implements OnInit, OnDestroy {

  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private fb = inject(FormBuilder);
  private authService = inject(AuthService);
  private validatorService = inject(ValidatorService);
  private modalAlertService = inject(ModalAlertService);

  private _subscription: Subscription = new Subscription();
  public visible: boolean = false;
  public hidePasswordConfirmation: boolean = false;

  @Output() public visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public changePassword: EventEmitter<void> = new EventEmitter();

  private _passwordSubscription: Subscription = new Subscription();
  private _passwordConfirmationSubscription: Subscription = new Subscription();

  public isLoading: boolean = false;
  public hidePassword: boolean = false;
  public onDestroToken$: any;
  public token_url: string | undefined = '';
  public passwordConditions: PasswordConditions[] = [
    {
      id: 'minLength',
      description: 'Debe ingresar al menos 7 caracteres',
      isValid: false
    },
    {
      id: 'upperCaseLowerCase',
      description: 'Combine mayúsculas y minúsculas',
      isValid: false
    },
    {
      id: 'specialCharacter',
      description: 'Incluya al menos 1 carácter especial',
      isValid: false
    },
  ]
  public passwordConfirmationConditions: PasswordConditions[] = [
    {
      id: 'matching',
      description: 'Las contraseñas coinciden',
      isValid: false
    },
  ]
  public infoMessage: InfoMessage = {
    message: '',
    type: INFO_MESSAGE_TYPE.error
  }
  public resetPasswordForm: FormGroup = this.fb.group({
    email: ['', [
      Validators.required,
      Validators.maxLength(40),
      this.validatorService.emailValidator(),
      this.validatorService.medellinEmailValidator(),
      this.validatorService.whiteSpacesStartValidator()
    ]],
    document_number: ['', [
      Validators.required,
      Validators.maxLength(20),
      this.validatorService.whiteSpacesValidator()
    ]],
    domain: ['', [Validators.required]]
  });
  public changePasswordFormUser: FormGroup = this.fb.group({
    token: [''],
    password: ['', [Validators.required,
    Validators.minLength(7),
    this.validatorService.whiteSpacesValidator()]],
    confirm_password: ['', [Validators.required,
    Validators.minLength(7),
    this.validatorService.whiteSpacesValidator()]],
    passwordMatch: [false, [Validators.requiredTrue]]
  });

  public onVisibleChange(visible: boolean): void {
    this.visibleChange.emit(visible);
  }

  ngOnInit(): void {
    const fullUrl = window.location.href + '/';
    this.resetPasswordForm.get('domain')?.setValue(fullUrl);
    this.onDestroToken$ = this.route.params.subscribe(({
      next: (resp) => {
        this.token_url = resp['token']
        if (this.token_url) this.visible = true;
        this.changePasswordFormUser.get('token')?.setValue(this.token_url)
      },
      error: (error) => {
        this.token_url = undefined
      }
    }))
    this._passwordSubscription = this.changePasswordFormUser.controls['password'].valueChanges.subscribe((value) => {
      this.onPasswordChange(value);
      this.onPasswordConfirmationChange();
    });

    this._passwordConfirmationSubscription = this.changePasswordFormUser.controls['confirm_password'].valueChanges.subscribe((value) => {
      this.onPasswordConfirmationChange();
    });
  }

  ngOnDestroy(): void {
    this.onDestroToken$.unsubscribe();
    this._subscription.unsubscribe();
    this._passwordSubscription.unsubscribe();
    this._passwordConfirmationSubscription.unsubscribe();
  }

  private showSuccessAlert(message: string) {
    const successAlert: ModalAlert = {
      title: 'Exitoso',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        label: 'Continuar',
        primary: true,
      },
      message
    }
    this.modalAlertService.showModalAlert(successAlert);
  }

  private showErrorAlert(message: string) {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Continuar',
        primary: true,
      },
      message
    }

    this.modalAlertService.showModalAlert(errorAlert);
  }

  public onCloseInfoMessage(): void {
    this.infoMessage = {
      message: '',
      type: INFO_MESSAGE_TYPE.error
    }
  }
  public enabledShowPasswordButton(formControlName: string): boolean {
    const control = this.changePasswordFormUser.controls[formControlName];
    const enabled = !!control?.value;

    if (!enabled) {
      switch (formControlName) {
        case 'password':
          this.hidePassword = false;
          break;
        case 'confirm_password':
          this.hidePasswordConfirmation = false;
          break;
      }
    }

    return !enabled;
  }

  public togglePasswordVisibility(formControlName: string): void {
    switch (formControlName) {
      case 'password':
        this.hidePassword = !this.hidePassword;
        break;
      case 'confirm_password':
        this.hidePasswordConfirmation = !this.hidePasswordConfirmation;
        break;
    }
  }

  public passwordRecovery(request: any): void {
    this.isLoading = true;
    this._subscription = this.authService.recoveryPasswordService(request).subscribe({
      next: () => {
        this.isLoading = false;
        this.showSuccessAlert('Hemos enviado un enlace satisfactoriamente');
        this.router.navigateByUrl('/');
      },
      error: (error) => {
        this.isLoading = false;
        this.showErrorAlert(error);
      }
    });
  }
  public onPasswordChange(password: string): void {
    onPasswordChangeValidateConditions(password, this.passwordConditions);
  }
  public onPasswordConfirmationChange(): void {
    const passwordValue = this.changePasswordFormUser.controls['password'].value;
    const passwordConfirmationValue = this.changePasswordFormUser.controls['confirm_password'].value;
    const passwordMatch = this.changePasswordFormUser.controls['passwordMatch'];
    const isValid = passwordConfirmationValue && passwordValue && passwordConfirmationValue === passwordValue;
    passwordMatch.setValue(isValid);

    this.passwordConfirmationConditions = this.passwordConfirmationConditions.map((condition) => {
      if (condition.id === 'matching') {
        condition.isValid = isValid;
      }
      return condition;
    })
  }

  public onRecoveryPassword(): void {
    if (!this.resetPasswordForm.valid) {
      this.validatorService.isValidField(this.resetPasswordForm);
      return;
    }

    this.onCloseInfoMessage();

    this.passwordRecovery(this.resetPasswordForm.value);
  }
  public onChangePassword(): void {

    if (!this.changePasswordFormUser.valid) {
      this.validatorService.isValidField(this.changePasswordFormUser);
      return;
    }
    this.onCloseInfoMessage();
    this.changePasswordRecovery(this.changePasswordFormUser.value);
  }

  public changePasswordRecovery(request: any): void {
    this.isLoading = true;
    this.visible = false;
    this._subscription = this.authService.changePasswordService(request).subscribe({
      next: () => {
        this.isLoading = false;
        this.showSuccessAlert('Su contraseña ha sido reestablecida correctamente.');
        this.router.navigateByUrl('/');
      },
      error: (error) => {
        this.isLoading = false;
        this.showErrorAlert(error);
      }
    });
  }

  public onCancelChangePassword(): void {
    this.visible = false;
    const warningAlert: ModalAlert = {
      title: 'Cancelar',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        label: 'Si, cancelar',
        primary: true,
        action: () => {
          this.resetPasswordForm.reset();
          this.changePasswordFormUser.reset();
          this.router.navigate(['auth'])
        }
      },
      cancelButton: {
        label: 'Continuar',
        primary: false,
        action: () => {
          this.visible = true;
        }
      },
      highlightText: '¿Está seguro que desea cancelar el reestablecimiento de contraseña?'
    }

    this.modalAlertService.showModalAlert(warningAlert);
  }
}
