import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthService } from '../../services/auth.service';
import { HttpRequestService } from 'src/app/shared/services/http-request.service';

import { RecoveryPasswordComponent } from './recovery-password.component';
import { AuthModule } from '../../auth.module';
import { ActivatedRoute, Router } from '@angular/router';
import { INFO_MESSAGE_TYPE } from 'src/app/shared/enums/info-message-type.enum';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { of, throwError } from 'rxjs';
import { LoginRequest } from '../../interfaces/login-request.interface';
import { RouterTestingModule } from '@angular/router/testing';
import { IRecoveryPassword } from '../../interfaces/reset-password.interface';
import Swal from 'sweetalert2';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('RecoveryPasswordComponent', () => {
  let component: RecoveryPasswordComponent;
  let fixture: ComponentFixture<RecoveryPasswordComponent>;

  let authService: AuthService;
  let router: Router;
  let validatorService: ValidatorService;

  beforeAll(() => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecoveryPasswordComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        AuthModule
      ],
      providers: [
        AuthService,
        HttpRequestService
      ]
    });
    fixture = TestBed.createComponent(RecoveryPasswordComponent);
    authService = TestBed.inject(AuthService);
    validatorService = TestBed.inject(ValidatorService);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should exec the recovery password contraseña method successfully when form is valid', () => {

    const compiled = fixture.nativeElement as HTMLElement;
    const documentUser = compiled.querySelector('#document_number') as HTMLInputElement;
    const emailInput = compiled.querySelector('#email') as HTMLInputElement;
    const form = compiled.querySelector('.form') as HTMLFormElement;
    emailInput.value = 'prueba@medellin.gov.co';
    emailInput.dispatchEvent(new Event('input'));
    documentUser.value = '12345678';
    documentUser.dispatchEvent(new Event('input'));
    form.dispatchEvent(new Event('submit'));
    expect(component.infoMessage.message).toEqual('');
  });

  it('should exec the recovery password method unsuccessfully when form is invalid', () => {

    spyOn(validatorService, 'isValidField');

    const compiled = fixture.nativeElement as HTMLElement;
    const emailInput = compiled.querySelector('#email') as HTMLInputElement;
    const documentUser = compiled.querySelector('#document_number') as HTMLInputElement;
    const form = compiled.querySelector('.form') as HTMLFormElement;

    emailInput.value = 'prueba@gmail.gov.co';
    emailInput.dispatchEvent(new Event('input'));
    documentUser.value = '12345678';
    documentUser.dispatchEvent(new Event('input'));
    form.dispatchEvent(new Event('submit'));

    expect(validatorService.isValidField).toHaveBeenCalled();
  });

  it('should navigate to dashboard', () => {
    spyOn(authService, 'recoveryPasswordService').and.returnValue(of(true));
    spyOn(router, 'navigateByUrl');

    const request: IRecoveryPassword = {
      document_number: '12345678',
      email: 'prueba@medellin.gov.co',
      domain: "http://localhost:8000"
    }
    component.passwordRecovery(request);
    expect(router.navigateByUrl).toHaveBeenCalledWith('/');
  });

  it('should does not exec the recoveryPasswordService method if response is a bad request', () => {
    spyOn(authService, 'recoveryPasswordService').and.returnValue(throwError(() => false));

    const request: IRecoveryPassword = {
      document_number: '12345678',
      email: 'prueba@medellin.gov.co',
      domain: "http://localhost:8000"
    }
    component.passwordRecovery(request);
    expect(Swal.isVisible()).toBeTrue();
  });

  it('should emit visibleChange value true', () => {
    spyOn(component.visibleChange, 'emit');
    component.onVisibleChange(true);

    expect(component.visibleChange.emit).toHaveBeenCalledWith(true);
  });

  it('should exec cancelButton method when modal change password is present', () => {
    component.onCancelChangePassword();
    const cancelButton = Swal.getCancelButton()!;
    cancelButton.click();
    
    expect(component.visible).toBeFalse();
  });

  it('should exec confirmButton method when modal change password is present', (done) => {
    fixture.ngZone!.run(() => {
      spyOn(router, 'navigate');
      spyOn(component.changePasswordFormUser, 'reset');
    
      component.onCancelChangePassword();
      const confirmButton = Swal.getConfirmButton()!;
      confirmButton.click();
      
      expect(confirmButton).toBeTruthy();
      done();
    });
  });

  it('should exec changePasswordRecovery method and show a success modal alert', (done) => {
    fixture.ngZone!.run(() => {
      spyOn(router, 'navigateByUrl');
      spyOn(authService, 'changePasswordService').and.returnValue(of(true));
    
      component.changePasswordRecovery(true);

      expect(Swal.isVisible()).toBeTrue();

      done();
    });
  });

  it('should exec changePasswordRecovery method and show a error modal alert', (done) => {
    fixture.ngZone!.run(() => {
      spyOn(router, 'navigateByUrl');
      spyOn(authService, 'changePasswordService').and.returnValue(throwError(() => false));
    
      component.changePasswordRecovery(true);

      expect(Swal.isVisible()).toBeTrue();

      done();
    });
  });

  it('should change changePasswordFormUser to valid', () => {
    component.changePasswordFormUser.controls['password'].setValue('12345Aa@');
    component.changePasswordFormUser.controls['confirm_password'].setValue('12345Aa@');
    component.changePasswordFormUser.controls['passwordMatch'].setValue(true);

    component.onChangePassword();
    expect(component.changePasswordFormUser.valid).toBeTrue();
  });

  it('should change changePasswordFormUser to valid', () => {
    component.onChangePassword();
    expect(component.changePasswordFormUser.invalid).toBeTrue();
  });

  it('should change password visibility when togglePasswordVisibility method id executed', () => {
    component.togglePasswordVisibility('password');
    expect(component.hidePassword).toBeTrue();
  });

  it('should change confirm password visibility when togglePasswordVisibility method id executed', () => {
    component.togglePasswordVisibility('confirm_password');
    expect(component.hidePassword).toBeFalse();
  });
});

describe('RecoveryPasswordComponent params', () => {
  let component: RecoveryPasswordComponent;
  let fixture: ComponentFixture<RecoveryPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecoveryPasswordComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        AuthModule,
        BrowserAnimationsModule
      ],
      providers: [
        AuthService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              token: 'token'
            })
          },
        },
      ]
    });
    fixture = TestBed.createComponent(RecoveryPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('RecoveryPasswordComponent params error', () => {
  let component: RecoveryPasswordComponent;
  let fixture: ComponentFixture<RecoveryPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecoveryPasswordComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        AuthModule,
        BrowserAnimationsModule
      ],
      providers: [
        AuthService,
        HttpRequestService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: throwError(() => '')
          },
        },
      ]
    });
    fixture = TestBed.createComponent(RecoveryPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
