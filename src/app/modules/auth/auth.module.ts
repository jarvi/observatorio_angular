import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';
import { RecoveryPasswordComponent } from './pages/recovery-password/recovery-password.component';


@NgModule({
  declarations: [
    LoginPageComponent,
    LoginLayoutComponent,
    RecoveryPasswordComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

    AuthRoutingModule,

    PrimeNgModule,
    SharedModule,
  ]
})
export class AuthModule { }
