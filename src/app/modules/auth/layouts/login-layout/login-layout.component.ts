import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'auth-login-layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.scss']
})
export class LoginLayoutComponent {
  public appVersion: string = environment.appVersion;
  public poweredBy: string = environment.poweredBy;

}
