import { NgModule } from '@angular/core';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { SidebarModule } from 'primeng/sidebar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { TableModule } from 'primeng/table';
import { InputSwitchModule } from 'primeng/inputswitch';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { ScrollerModule } from 'primeng/scroller';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
  declarations: [],
  exports: [
    ButtonModule,
    InputTextModule,
    CheckboxModule,
    ConfirmDialogModule,
    SidebarModule,
    PanelMenuModule,
    DropdownModule,
    DialogModule,
    OverlayPanelModule,
    ConfirmPopupModule,
    TableModule,
    InputSwitchModule,
    PaginatorModule,
    RadioButtonModule,
    MultiSelectModule,
    CalendarModule,
    CardModule,
    ScrollerModule,
    SelectButtonModule,
    ProgressSpinnerModule,
  ],
})
export class PrimeNgModule { }
