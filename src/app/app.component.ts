import { AfterViewInit, Component, OnInit, inject } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, AfterViewInit {
  
  private _primengConfig = inject(PrimeNGConfig);
  private _translateService = inject(TranslateService)

  ngOnInit() {
    this._primengConfig.ripple = true;
    this._translateService.setDefaultLang('es');
  }

  ngAfterViewInit() {
    this._translateService.use('es');
    this._translateService
      .get('primeng')
      .subscribe(res => this._primengConfig.setTranslation(res));
  }
}
