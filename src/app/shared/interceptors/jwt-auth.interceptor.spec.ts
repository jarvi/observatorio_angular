import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { HttpRequestService } from '../services/http-request.service';

import { JwtAuthInterceptor } from './jwt-auth.interceptor';
import { HttpEvent, HttpHandler, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';

describe('JwtAuthInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      AuthService,
      JwtAuthInterceptor,
      HttpRequestService
    ]
  }));

  afterEach(() => {
    localStorage.clear();
  });

  it('should be created', () => {
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));

    const interceptor: JwtAuthInterceptor = TestBed.inject(JwtAuthInterceptor);
    expect(interceptor).toBeTruthy();
  });

  it('should intercept the request and add the correctly header', () => {
    
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));

    localStorage.setItem('accessToken', '12345');
    localStorage.setItem('refreshToken', '12345');
    
    const interceptor: JwtAuthInterceptor = TestBed.inject(JwtAuthInterceptor);
    const request = new HttpRequest('GET', 'https://example.com');
    const next: HttpHandler = {
      handle: (req: HttpRequest<unknown>): Observable<HttpEvent<unknown>> => {
        return of(new HttpResponse({ status: 200, body: { message: 'OK' } }));
      }
    };

    interceptor.intercept(request, next).subscribe(
      (e: any) => {
        expect(e.status).toEqual(200);
      }
    );
  });

  it('should intercept the request and not add the header', () => {
    
    spyOn(window, 'atob').and.returnValue(JSON.stringify({
      exp: new Date().getTime() / 1000 + 1000
    }));
    
    const interceptor: JwtAuthInterceptor = TestBed.inject(JwtAuthInterceptor);
    const request = new HttpRequest('GET', 'https://example.com');
    const next: HttpHandler = {
      handle: (req: HttpRequest<unknown>): Observable<HttpEvent<unknown>> => {
        return of(new HttpResponse({ status: 200, body: { message: 'OK' } }));
      }
    };

    interceptor.intercept(request, next).subscribe(
      (e: any) => {
        expect(e.status).toEqual(200);
      }
    );
  });
});
