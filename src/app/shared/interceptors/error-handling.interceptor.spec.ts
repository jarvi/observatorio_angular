import { TestBed } from '@angular/core/testing';

import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';

import { ErrorHandlingInterceptor } from './error-handling.interceptor';

describe('ErrorHandlingInterceptor', () => {
  
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ErrorHandlingInterceptor
    ]
  }));

  afterEach(() => {
    Swal.close();
  })

  it('should be created', () => {
    const interceptor: ErrorHandlingInterceptor = TestBed.inject(ErrorHandlingInterceptor);
    expect(interceptor).toBeTruthy();
  });

  it('should intercept the request when is a 404 error and return it', () => {
    const interceptor: ErrorHandlingInterceptor = TestBed.inject(ErrorHandlingInterceptor);
    const request = new HttpRequest('GET', 'https://example.com');
    const error = { status: 404, message: 'Not Found' };
    const next: HttpHandler = {
      handle: (req: HttpRequest<unknown>): Observable<HttpEvent<unknown>> => {
        return throwError(() => error)
      }
    };

    interceptor.intercept(request, next).subscribe(
      () => {},
      (errorResponse) => {
        expect(errorResponse.status).toEqual(error.status);
        expect(errorResponse.message).toEqual(error.message);
      }
    );
  });

  it('should intercept the request when is a 404 error, return it and display the modal alert', () => {
    const interceptor: ErrorHandlingInterceptor = TestBed.inject(ErrorHandlingInterceptor);
    const request = new HttpRequest('GET', 'https://example.com');
    const error = { status: 400, message: 'Bad Request' };
    const next: HttpHandler = {
      handle: (req: HttpRequest<unknown>): Observable<HttpEvent<unknown>> => {
        return throwError(() => error)
      }
    };

    interceptor.intercept(request, next).subscribe(
      () => {},
      (errorResponse) => {
        expect(errorResponse.status).toEqual(error.status);
        expect(errorResponse.message).toEqual(error.message);
        expect(Swal.isVisible()).toBeTruthy();
      }
    );
  });

  it('should intercept the request when is a 404 error, return it and display the modal alert', () => {
    const interceptor: ErrorHandlingInterceptor = TestBed.inject(ErrorHandlingInterceptor);
    const request = new HttpRequest('GET', 'https://example.com');
    const error = { status: 400, message: 'Bad Request' };
    const next: HttpHandler = {
      handle: (req: HttpRequest<unknown>): Observable<HttpEvent<unknown>> => {
        return throwError(() => error)
      }
    };

    interceptor.intercept(request, next).subscribe(
      () => {},
      (errorResponse) => {
        expect(errorResponse.status).toEqual(error.status);
        expect(errorResponse.message).toEqual(error.message);
        expect(Swal.isVisible()).toBeTruthy();

        const confirmButton = Swal.getConfirmButton();

        confirmButton!.click();
      }
    );
  });
});
