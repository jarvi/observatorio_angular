import { Injectable, inject } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { ModalAlertService } from '../services/modal-alert.service';
import { ModalAlert } from '../interfaces/modal-alert.interface';
import { MODAL_ALERT_TYPE } from '../enums/modal-alert-type.enum';

@Injectable()
export class ErrorHandlingInterceptor implements HttpInterceptor {

  private _modalAlertService = inject(ModalAlertService);

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((error) => {
        
        if (error.status > 0 && error.status <= 401 && !error.error?.code) return throwError(() => error);

        const errorAlert: ModalAlert = {
          title: 'Error',
          type: MODAL_ALERT_TYPE.error,
          confirmButton: {
            label: 'Aceptar',
            primary: true
          },
          message: 'Se ha presentado un error inesperado.',
          highlightText: 'Por favor intente nuevamente.'
        }

        this._modalAlertService.showModalAlert(errorAlert);
        throw error;
      })
    );
  }
}
