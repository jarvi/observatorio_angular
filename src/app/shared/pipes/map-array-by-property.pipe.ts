import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapArrayByProperty'
})
export class MapArrayByPropertyPipe implements PipeTransform {

  transform(value: any[], property: string): string[] {
    return value.map((item) => item[property]);
  }

}
