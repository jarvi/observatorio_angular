import { DateToTimeStringPipe } from './date-to-time-string.pipe';

describe('DateToTimeStringPipe', () => {
  let pipe: DateToTimeStringPipe;

  beforeEach(() => {
    pipe = new DateToTimeStringPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should change date value to time string', () => {
    const value = new Date('2023-09-25T13:30:00');

    const valueChanged = pipe.transform(value);

    expect(valueChanged).toEqual('1:30 PM');
  });
});
