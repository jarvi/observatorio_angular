import { JoinArrayByPipe } from './join-array-by.pipe';

describe('JoinArrayByPipe', () => {
  let pipe: JoinArrayByPipe;

  beforeEach(() => {
    pipe = new JoinArrayByPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform array to join with , and convert to string', () => {
    const array = ['Administrador', 'Usuario'];

    const transformedArray = pipe.transform(array, ',');

    const expectedString = array.join(',');

    expect(transformedArray).toEqual(expectedString);
  });
});