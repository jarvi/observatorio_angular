import { BooleanToStringPipe } from './boolean-to-string.pipe';

describe('BooleanToStringPipe', () => {
  let pipe: BooleanToStringPipe;

  beforeEach(() => {
    pipe = new BooleanToStringPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should change true value to Yes string', () => {
    const value = true;

    const valueChanged = pipe.transform(value, ['Yes', 'No']);

    expect(valueChanged).toEqual('Yes');
  });

  it('should change true value to No string', () => {
    const value = false;

    const valueChanged = pipe.transform(value, ['Yes', 'No']);

    expect(valueChanged).toEqual('No');
  });
});
