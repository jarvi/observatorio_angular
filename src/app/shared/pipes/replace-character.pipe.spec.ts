import { ReplaceCharacterPipe } from './replace-character.pipe';

describe('ReplaceCharacterPipe', () => {
  let pipe: ReplaceCharacterPipe;

  beforeEach(() => {
    pipe = new ReplaceCharacterPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should remove a specific character for another', () => {
    const value = '.CSV';
    const transformedValue = pipe.transform(value, '.', '');

    expect(transformedValue).toEqual('CSV');
  });
});
