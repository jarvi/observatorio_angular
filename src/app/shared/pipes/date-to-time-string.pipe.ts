import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateToTimeString'
})
export class DateToTimeStringPipe implements PipeTransform {
  transform(date: Date): string {
    return date.toLocaleString('en-US', { hour: 'numeric', minute: '2-digit', hour12: true });
  }
}
