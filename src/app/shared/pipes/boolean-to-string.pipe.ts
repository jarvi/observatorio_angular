import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'booleanToString'
})
export class BooleanToStringPipe implements PipeTransform {
  transform(value: boolean, labels: string[]): string {
    return value ? labels[0] : labels[1];
  }
}
