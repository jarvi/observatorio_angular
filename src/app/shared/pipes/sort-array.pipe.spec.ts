import { SortArrayPipe } from './sort-array.pipe';

describe('SortArrayPipe', () => {
  let pipe: SortArrayPipe;

  beforeEach(() => {
    pipe = new SortArrayPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should sort array by specified field', () => {
    const array = [
      { name: 'John', age: 25 },
      { name: 'Alice', age: 30 },
      { name: 'Bob', age: 20 }
    ];

    const sortedArray = pipe.transform(array, 'name');

    expect(sortedArray).toEqual([
      { name: 'Alice', age: 30 },
      { name: 'Bob', age: 20 },
      { name: 'John', age: 25 }
    ]);
  });

  it('should return original array if field is not specified', () => {
    const array = [
      { name: 'John', age: 25 },
      { name: 'Alice', age: 30 },
      { name: 'Bob', age: 20 }
    ];

    const sortedArray = pipe.transform(array, '');

    expect(sortedArray).toEqual(array);
  });

  it('should return 0 if values are equal', () => {
    const array = [
      { name: 'John', age: 25 },
      { name: 'Alice', age: 30 },
      { name: 'Bob', age: 20 },
      { name: 'Dave', age: 20 }
    ];
  
    const sortedArray = pipe.transform(array, 'age');
  
    expect(sortedArray).toEqual([
      { name: 'Bob', age: 20 },
      { name: 'Dave', age: 20 },
      { name: 'John', age: 25 },
      { name: 'Alice', age: 30 }
    ]);
  });
});
