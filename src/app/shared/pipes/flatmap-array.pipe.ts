import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flatmapArray'
})
export class FlatmapArrayPipe implements PipeTransform {

  transform<T>(value: T[][], useFlatMap: boolean): T[][] {
    if (useFlatMap) {
      const newArray = value.flatMap((array: T[]) => array);
      return [newArray];
    }
    
    return value;
  }

}
