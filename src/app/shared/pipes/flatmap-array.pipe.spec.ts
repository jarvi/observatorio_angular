import { FlatmapArrayPipe } from './flatmap-array.pipe';

describe('FlatmapArrayPipe', () => {
  let pipe: FlatmapArrayPipe;

  beforeEach(() => {
    pipe = new FlatmapArrayPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform array when only has one position', () => {
    const array = [[1, 2, 3]];

    const transformedArray = pipe.transform(array, true);

    const expectedArray = [[1, 2, 3]];

    expect(transformedArray).toEqual(expectedArray);
  });

  it('should transform array when only has two positions', () => {
    const array = [[1, 2, 3], [4, 5, 6]];

    const transformedArray = pipe.transform(array, true);

    const expectedArray = [[1, 2, 3, 4, 5, 6]];

    expect(transformedArray).toEqual(expectedArray);
  });

  it('should not transform array when set false param', () => {
    const array = [[1, 2, 3], [4, 5, 6]];

    const transformedArray = pipe.transform(array, false);

    expect(transformedArray).toEqual(array);
  });
});
