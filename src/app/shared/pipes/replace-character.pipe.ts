import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceCharacter'
})
export class ReplaceCharacterPipe implements PipeTransform {
  transform(value: string, replace: string, replaceFor: string): string {
    return value.replace(replace, replaceFor);
  }
}
