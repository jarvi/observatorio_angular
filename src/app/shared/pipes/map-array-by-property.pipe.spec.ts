import { MapArrayByPropertyPipe } from './map-array-by-property.pipe';

describe('MapArrayByPropertyPipe', () => {
  let pipe: MapArrayByPropertyPipe;

  beforeEach(() => {
    pipe = new MapArrayByPropertyPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform array to new array', () => {
    const array = [{ id: 1, name: 'Administrador' }, { id: 2, name: 'Usuario' }];

    const transformedArray = pipe.transform(array, 'name');

    const expectedArray = array.map((item) => item.name);

    expect(transformedArray).toEqual(expectedArray);
  });
});