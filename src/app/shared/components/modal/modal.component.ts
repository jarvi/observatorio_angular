import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'shared-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  
  @Input() visible: boolean = false;
  @Input() showFooter: boolean = true;
  @Input() closable: boolean = true;
  @Input() customClass: string = '';
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  public onVisibleChange(visible: boolean): void {
    this.visibleChange.emit(visible);
  }
}
