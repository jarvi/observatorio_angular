import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';

import { ModalComponent } from './modal.component';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModalComponent],
      imports: [
        PrimeNgModule,
      ],
    });
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit true when onVisibleChange is called with true param', () => {
    spyOn(component.visibleChange, 'emit');
    component.onVisibleChange(true);
    expect(component.visibleChange.emit).toHaveBeenCalledWith(true);
  });
});
