import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { ChipButton } from '../../interfaces/chip-button.interface';
import { executeCallbackLate } from 'src/utils/execute-callback-late';

@Component({
  selector: 'shared-chip-buttons',
  templateUrl: './chip-buttons.component.html',
  styleUrls: ['./chip-buttons.component.scss']
})
export class ChipButtonsComponent implements OnInit, OnChanges {
  
  @Input() public chips: ChipButton[] = [];
  @Input() public chipsFormControl: AbstractControl = new FormControl();
  @Input() public unlockedChips?: string[];
  @Input() public useOnlyOne: boolean = false;

  ngOnInit(): void {
    executeCallbackLate(() => {
      this.chipsFormControl.value?.forEach((chipName: string) => {
        this.selectChip(chipName);
      })
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['unlockedChips']) {
      if (this.unlockedChips && this.unlockedChips.length > 0) {
        this.blockChips(this.unlockedChips);
      }
    }
  }

  private blockChips(chips: string[]) {
    this.chips = this.chips.map(chip => {
      if (!chips.includes(chip.name)) {
        return {
          ...chip,
          blocked: true
        }
      }

      return { ...chip, blocked: false };
    });
  }

  private setValueToForm() {
    const chipsSelected = this.chips.filter(chip => chip.selected).map(chip => chip.name);
    this.chipsFormControl.setValue(chipsSelected)
  }

  public selectChip(chipName: string) {
    this.chips = this.chips.map(chip => {
      if (this.useOnlyOne) {
        if (chip.name === chipName) {
          return { ...chip, selected: !chip.selected }
        }

        return {
          ...chip,
          selected: false
        }
      }

      if (chip.name === chipName) {
        return { ...chip, selected: !chip.selected }
      }
  
      return chip;
    });

    this.setValueToForm();
  }
}
