import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChipButtonsComponent } from './chip-buttons.component';
import { FormControl } from '@angular/forms';

describe('ChipButtonsComponent', () => {
  let component: ChipButtonsComponent;
  let fixture: ComponentFixture<ChipButtonsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChipButtonsComponent]
    });
    fixture = TestBed.createComponent(ChipButtonsComponent);
    component = fixture.componentInstance;
    
    component.chips = [
      { id: 1, name: 'Valor 1', selected: false, blocked: false },
      { id: 2, name: 'Valor 2', selected: false, blocked: false },
      { id: 3, name: 'Valor 3', selected: false, blocked: false }
    ]

    component.chipsFormControl = new FormControl();
    component.chipsFormControl.setValue(['Valor 1', 'Valor 2'])

    fixture.detectChanges();
  });

  it('should select Valor 1 chip', () => {
    component.selectChip('Valor 1');
    expect(component.chips[0].selected).toBeTrue();
  });

  it('should block specific chips in the chip array', () => {
    component.chips = [
      { id: 1, name: 'Valor 1', selected: false, blocked: false },
      { id: 2, name: 'Valor 2', selected: false, blocked: false },
      { id: 3, name: 'Valor 3', selected: false, blocked: false }
    ]

    spyOn(component, 'ngOnChanges').and.callThrough();

    const newUnlockedChips: string[] = ['Valor 1', 'Valor 2'];
    component.unlockedChips = newUnlockedChips;
    fixture.detectChanges();
    component.ngOnChanges({ unlockedChips: {
      previousValue: [],
      currentValue: newUnlockedChips,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    const unlockedChipsFiltered = component.chips.filter(chip => !chip.blocked);
    expect(unlockedChipsFiltered.length).toEqual(2);
  });

  it('should select Valor 1 chip when useOnlyOne is true', () => {
    component.useOnlyOne = true;
    component.selectChip('Valor 1');
    expect(component.chips[0].selected).toBeTrue();
  });
});
