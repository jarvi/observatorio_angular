import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Paginator, PaginatorState } from 'primeng/paginator';

@Component({
  selector: 'shared-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnChanges {

  @Input() public rows: number = 0;
  @Input() public totalRecords: number = 0;
  @Input() public currentPage: number = 0;
  @Output() public pageChange: EventEmitter<number> = new EventEmitter<number>();
  @ViewChild('paginator') paginator!: Paginator;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['currentPage']) {
      this.paginator?.changePage(this.currentPage - 1);
    }
    if (changes['rows']) {
      this.paginator?.changePage(0);
    }
  }
  
  public onPageChange(event: PaginatorState): void {
    if (event.page === undefined) return;
    if (event.page + 1 === this.currentPage) return;
    this.pageChange.emit(event.page + 1);
  }
}
