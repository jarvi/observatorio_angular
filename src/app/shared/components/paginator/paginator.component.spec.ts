import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginatorComponent } from './paginator.component';
import { PrimeNgModule } from '../../../prime-ng/prime-ng.module';
import { PaginatorState } from 'primeng/paginator';

describe('PaginatorComponent', () => {
  let component: PaginatorComponent;
  let fixture: ComponentFixture<PaginatorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PaginatorComponent],
      imports: [
        PrimeNgModule,
      ],
    });
    fixture = TestBed.createComponent(PaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit pageChange value when onPageChange is executed', () => {
    spyOn(component.pageChange, 'emit');

    const paginatorState: PaginatorState = {
      page: 1,
    }

    component.onPageChange(paginatorState);
    expect(component.pageChange.emit).toHaveBeenCalledWith(2);
  });

  it('should not emit pageChange value when onPageChange is executed with same page', () => {
    spyOn(component.pageChange, 'emit');

    const paginatorState: PaginatorState = {
      page: 0,
    }
    component.currentPage = 1;

    component.onPageChange(paginatorState);
    expect(component.pageChange.emit).not.toHaveBeenCalled();
  });

  it('should not emit pageChange value when onPageChange is executed with noone paginator state', () => {
    spyOn(component.pageChange, 'emit');

    const paginatorState: PaginatorState = {}

    component.onPageChange(paginatorState);
    expect(component.pageChange.emit).not.toHaveBeenCalled();
  });

  it('should call ngOnChanges when rows changes', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newRows = 2;
    component.rows = newRows;
    fixture.detectChanges();
    component.ngOnChanges({ rows: {
      previousValue: '',
      currentValue: newRows,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.rows).toEqual(2);
  });
});
