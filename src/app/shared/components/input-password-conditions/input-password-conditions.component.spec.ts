import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputPasswordConditionsComponent } from './input-password-conditions.component';

describe('InputPasswordConditionsComponent', () => {
  let component: InputPasswordConditionsComponent;
  let fixture: ComponentFixture<InputPasswordConditionsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InputPasswordConditionsComponent]
    });
    fixture = TestBed.createComponent(InputPasswordConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
