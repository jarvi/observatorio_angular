import { Component, Input } from '@angular/core';
import { PasswordConditions } from 'src/app/modules/administration/modules/user/interfaces/password-conditions.interface';

@Component({
  selector: 'shared-input-password-conditions',
  templateUrl: './input-password-conditions.component.html',
  styleUrls: ['./input-password-conditions.component.scss']
})
export class InputPasswordConditionsComponent {

  @Input() conditions: PasswordConditions[] = [];
}
