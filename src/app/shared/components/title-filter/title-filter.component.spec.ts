import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleFilterComponent } from './title-filter.component';
import { SharedModule } from '../../shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpRequestService } from '../../services/http-request.service';
import { UserPermissionsService } from '../../services/user-permissions.service';

describe('TitleFilterComponent', () => {
  let component: TitleFilterComponent;
  let fixture: ComponentFixture<TitleFilterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TitleFilterComponent],
      providers: [
        UserPermissionsService,
        HttpRequestService
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        SharedModule
      ],
    });
    fixture = TestBed.createComponent(TitleFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit hideFilters value when onHideFilters is executed', () => {
    spyOn(component.hideFilters, 'emit');
    
    component.onHideFilters();
    expect(component.hideFilters.emit).toHaveBeenCalled();
  });
});
