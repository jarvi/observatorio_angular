import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TooltipOptions } from 'primeng/api';

@Component({
  selector: 'shared-title-filter',
  templateUrl: './title-filter.component.html',
  styleUrls: ['./title-filter.component.scss']
})
export class TitleFilterComponent {

  @Input() public title: string = '';
  @Input() public showForm: boolean = true;
  @Input() public showFormPermission: string = '';
  @Input() public buttonRedirectLabel: string = '';
  @Input() public buttonRedirectRoute: string = '';
  @Input() public buttonRedirectPermission: string = '';
  @Input() public showFilters: boolean = true;
  @Output() public hideFilters: EventEmitter<void> = new EventEmitter<void>();

  public tooltipOptions: TooltipOptions = {
    tooltipPosition: 'bottom',
    tooltipEvent: 'hover',
    positionTop: 5,
    positionLeft: -30,
    tooltipLabel: 'Clic aquí para ver/ocultar filtros',
    tooltipStyleClass: 'shared-title-filter-filter-button'
  }

  public onHideFilters(): void {
    this.showFilters && this.hideFilters.emit();
  }
}
