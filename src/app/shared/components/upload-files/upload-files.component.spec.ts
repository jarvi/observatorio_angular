import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadFilesComponent } from './upload-files.component';
import Swal from 'sweetalert2';
import { SharedModule } from '../../shared.module';

describe('UploadFilesComponent', () => {
  let component: UploadFilesComponent;
  let fixture: ComponentFixture<UploadFilesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UploadFilesComponent],
      imports: [
        SharedModule
      ]
    });
    fixture = TestBed.createComponent(UploadFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  
  afterEach(() => {
    Swal.close();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should only call importFiles if try to upload none file', () => {
    component.multiple = false;

    const event: any = {
      target: {
        files: []
      }
    }
    component.importFiles(event);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal error alert if try to upload more of 1 file if multiple variable is true', () => {
    component.multiple = false;

    const event: any = {
      target: {
        files: [
          new File([''], 'test.csv'),
          new File([''], 'test.csv'),
        ]
      }
    }
    component.importFiles(event);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should only call droppedFiles method if try to upload nothing file', () => {
    component.multiple = false;
    spyOn(component, 'droppedFiles');

    const event: any = {
      target: {
        files: []
      }
    }
    component.importFiles(event);
    expect(component.droppedFiles).toHaveBeenCalled();
  });

  it('should show modal error alert if try to upload a file with empty extencion', () => {
    component.multiple = false;
    component.accept = ['.csv'];

    const event: any = {
      target: {
        files: [
          new File([''], ''),
        ]
      }
    }
    component.importFiles(event);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal error alert if try to upload a file with the incorrect extencion', () => {
    component.multiple = false;
    component.accept = ['.csv'];

    const event: any = {
      target: {
        files: [
          new File([''], 'test.xlsx'),
        ]
      }
    }
    component.importFiles(event);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should show modal error alert if try to upload a file with size greater than 1', () => {
    component.multiple = false;
    component.accept = ['.csv'];
    component.maxMBFileSize = 1;

    const newFile = new File([''], 'test.csv');
    Object.defineProperty(newFile, 'size', { value: 2 * 1024 * 1024 })

    const event: any = {
      target: {
        files: [newFile]
      }
    }
    component.importFiles(event);
    expect(Swal.isVisible()).toBeTruthy();
  });

  it('should only call importFiles if try to upload a file with size 0', () => {
    component.multiple = false;
    component.accept = ['.csv'];

    const newFile = new File([''], 'test.csv');
    Object.defineProperty(newFile, 'size', { value: 2 * 1024 * 1024 })

    const event: any = {
      target: {
        files: [newFile]
      }
    }
    component.importFiles(event);
    expect(component.maxMBFileSize).toEqual(0);
  });

  it('should call click event when triggerFileInput method is executed', () => {
    spyOn(component.fileInput.nativeElement, 'click');
    component.triggerFileInput();
    expect(component.fileInput.nativeElement.click).toHaveBeenCalled();
  });
});
