import { Component, ElementRef, EventEmitter, Input, Output, ViewChild, inject } from '@angular/core';
import { ModalAlertService } from '../../services/modal-alert.service';
import { ModalAlert } from '../../interfaces';
import { MODAL_ALERT_TYPE } from '../../enums/modal-alert-type.enum';

@Component({
  selector: 'shared-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss']
})
export class UploadFilesComponent {

  @Input() public accept: string[] = [];
  @Input() public multiple: boolean = false;
  @Input() public maxMBFileSize: number = 0;
  @Output() public files: EventEmitter<File[]> = new EventEmitter<File[]>();
  @ViewChild('fileInput', { static: true }) public fileInput!: ElementRef;

  private _modalAlertService = inject(ModalAlertService);

  private checkFilesExtension(files: File[]): boolean {
    return files.some((file) => {
      const extension = file.name.split('.').pop()!;
      const extensionsAllowed = this.accept
        .map((extension) => extension.toLowerCase().replace('.', ''));
      
      return !extensionsAllowed.includes(extension.toLowerCase());
    });
  }

  private checkFilesSize(files: File[]): boolean {
    if (!this.maxMBFileSize) return false;
    const bytes = this.maxMBFileSize * 1024 * 1024;

    return files.some((file) => file.size > bytes);
  }

  private showErrorModal(message: string): void {
    const errorAlert: ModalAlert = {
      title: 'Error',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        label: 'Aceptar',
        primary: true,
      },
      highlightText: 'Haga click para continuar',
      message,
    }

    this._modalAlertService.showModalAlert(errorAlert);
  }

  public triggerFileInput() {
    this.fileInput.nativeElement.click();
  }
  
  public droppedFiles(files: File[]): void {
    if(!files.length) return;
    
    if (!this.multiple && files.length > 1) {
      this.showErrorModal('No es posible cargar más de un archivo a la vez');
      return;
    }
    if (this.checkFilesExtension(files)) {
      this.showErrorModal('No es posible cargar archivos con extensión no permitida');
      return;
    }
    if (this.checkFilesSize(files)) {
      this.showErrorModal('No es posible cargar archivos con tamaño mayor al permitido');
      return;
    }

    this.files.emit(files);
    this.fileInput.nativeElement.value = '';
  }

  public importFiles(event: Event): void {
    const inputElement = event.target as HTMLInputElement;

    if (inputElement.files) {
      const file: File[] = Array.from(inputElement.files);
      this.droppedFiles(file);
    }
  }
}
