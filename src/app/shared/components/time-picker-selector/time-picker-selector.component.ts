import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'shared-time-picker-selector',
  templateUrl: './time-picker-selector.component.html',
  styleUrls: ['./time-picker-selector.component.scss']
})
export class TimePickerSelectorComponent {

  @Input() public value?: number | string;
  @Output() public up: EventEmitter<void> = new EventEmitter<void>();
  @Output() public down: EventEmitter<void> = new EventEmitter<void>();
  
  public onUp() { this.up.emit() }
  public onDown() { this.down.emit() }
}
