import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimePickerSelectorComponent } from './time-picker-selector.component';

describe('TimePickerSelectorComponent', () => {
  let component: TimePickerSelectorComponent;
  let fixture: ComponentFixture<TimePickerSelectorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TimePickerSelectorComponent]
    });
    fixture = TestBed.createComponent(TimePickerSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit up value when onUp method is executed', () => {
    spyOn(component.up, 'emit');

    component.onUp();
    expect(component.up.emit).toHaveBeenCalledWith();
  });

  it('should emit down value when onDown method is executed', () => {
    spyOn(component.down, 'emit');

    component.onDown();
    expect(component.down.emit).toHaveBeenCalledWith();
  });
});
