import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputRadiobuttonComponent } from './input-radiobutton.component';

describe('InputRadiobuttonComponent', () => {
  let component: InputRadiobuttonComponent<boolean>;
  let fixture: ComponentFixture<InputRadiobuttonComponent<boolean>>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InputRadiobuttonComponent]
    });
    fixture = TestBed.createComponent(InputRadiobuttonComponent<boolean>);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
