import { Component, Input, SkipSelf } from '@angular/core';
import { InputRadiobutton } from '../../interfaces/input-radiobutton.interface';
import { ControlContainer } from '@angular/forms';

@Component({
  selector: 'shared-input-radiobutton',
  templateUrl: './input-radiobutton.component.html',
  styleUrls: ['./input-radiobutton.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: (container: ControlContainer) => container,
      deps: [[new SkipSelf(), ControlContainer]]
    }
  ]
})
export class InputRadiobuttonComponent<T> {
  
  @Input() inputsRadiobutton: InputRadiobutton<T>[] = [];
  @Input() formControlNameInput: string = '';
}
