import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { DayPicker } from '../../interfaces/day-picker.interface';
import { DatePickerRange } from '../../interfaces/day-picker-range.interface';

@Component({
  selector: 'shared-day-picker',
  templateUrl: './day-picker.component.html',
  styleUrls: ['./day-picker.component.scss']
})
export class DayPickerComponent implements OnInit, OnChanges {

  @Input() public dayPickerFormControl: AbstractControl = new FormControl();
  @Input() public unlockedDays?: number[];
  @Input() public isSeparated?: boolean;

  public range: DatePickerRange = {
    from: null,
    to: null
  }
  public days: DayPicker[] = Array.from({ length: 31 }, (_, i) => {
    return {
      name: i + 1,
      selected: false,
      focused: false,
      blocked: false
    }
  });

  ngOnInit(): void { this.getInitialDays(); }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['unlockedDays']) {
      this.unlockedDays && this.unlockedDays.length > 1 &&
        this.blockDays(this.unlockedDays);
    }
  }

  private getInitialDays() {
    let initialDays = [];

    if (this.dayPickerFormControl.value?.length === 1) {
      const uniqueValue = this.dayPickerFormControl.value[0];
      initialDays = [uniqueValue, uniqueValue];
    } else {
      initialDays = this.dayPickerFormControl.value;
    }
    
    initialDays?.forEach((day: number) => this.selectDay(day))
  }

  private blockDays(range: number[]) {
    this.days = this.days.map(day => {
      if (day.name < range[0] || day.name > range[1]) {
        return {
          ...day,
          blocked: true
        }
      }

      return {...day, blocked: false};
    })
    
  }
  
  private selectDays() {
    if (!this.range.from || !this.range.to) {
      this.days = this.days.map(day => { return { ...day, selected: false } });
      return;
    };

    this.days = this.days.map((day, index) => ({
      ...day,
      selected: day.name >= this.range.from! &&  day.name <= this.range.to!,
      focused: false
    }));
  }

  private defineRange(daySelected: number) {
    if (this.range.from && this.range.to) {
      this.range.from = daySelected;
      this.range.to = null;
      return;
    }
    if (this.range.from) {
      this.range.to = daySelected;
      return;
    }

    this.range.from = daySelected;
  }

  private focusDay(daySelected: number) {
    this.days = this.days.map(day => {
      return {
        ...day,
        focused: day.name === daySelected
      }
    })
  }

  private setValueToForm() {
    const daysSelected = this.days.filter(day => day.selected).map(day => day.name);
    this.dayPickerFormControl.setValue(daysSelected)
  }

  public selectDay(daySelected: number) {
    if (!this.isSeparated) {
      this.focusDay(daySelected);
      this.defineRange(daySelected);
      this.selectDays();
    } else {
      this.days = this.days.map(day => {
        if (day.name === daySelected) {
          return { 
            ...day, 
            selected: !day.selected
          }
        }
        return day;
      });
    }

    this.setValueToForm();
  }
}
