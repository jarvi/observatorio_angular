import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayPickerComponent } from './day-picker.component';
import { FormControl } from '@angular/forms';

describe('DayPickerComponent', () => {
  let component: DayPickerComponent;
  let fixture: ComponentFixture<DayPickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DayPickerComponent]
    });
    fixture = TestBed.createComponent(DayPickerComponent);
    component = fixture.componentInstance;
    component.dayPickerFormControl = new FormControl();
    component.dayPickerFormControl.setValue([1,5])

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change from range value to 3 and range to to null', () => {
    component.selectDay(3);
    expect(component.range.from).toEqual(3);
    expect(component.range.to).toEqual(null);
  });

  it('should change from range value to 3 and range to to 4', () => {
    component.selectDay(3);
    component.selectDay(4);
    expect(component.range.from).toEqual(3);
    expect(component.range.to).toEqual(4);
  });

  it('should block specific days in the day array', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newUnlockedDays: number[] = [1, 3];
    component.unlockedDays = newUnlockedDays;
    fixture.detectChanges();
    component.ngOnChanges({ unlockedDays: {
      previousValue: [],
      currentValue: newUnlockedDays,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    const unlockedDaysFiltered = component.days.filter(day => !day.blocked);
    expect(unlockedDaysFiltered.length).toEqual(3);
  });

  it('should change from range value to 3 and range to to null', () => {
    const firstNumber = 6;
    const secondNumber = 27;
    
    component.isSeparated = true;

    component.selectDay(firstNumber);
    component.selectDay(secondNumber);
    
    const firstArrayDay = component.days.find(day => day.name === firstNumber);
    const secondArrayDay = component.days.find(day => day.name === secondNumber);

    expect(firstArrayDay?.selected).toBeTrue();
    expect(secondArrayDay?.selected).toBeTrue();
  });
});

describe('DayPickerComponent with only one position in dayPickerFormControl array', () => {
  let component: DayPickerComponent;
  let fixture: ComponentFixture<DayPickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DayPickerComponent]
    });
    fixture = TestBed.createComponent(DayPickerComponent);
    component = fixture.componentInstance;
    component.dayPickerFormControl = new FormControl();
    component.dayPickerFormControl.setValue([5])

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});