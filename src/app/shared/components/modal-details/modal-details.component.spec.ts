import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetailsComponent } from './modal-details.component';
import { SharedModule } from '../../shared.module';

describe('ModalDetailsComponent', () => {
  let component: ModalDetailsComponent;
  let fixture: ComponentFixture<ModalDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModalDetailsComponent],
      imports: [
        SharedModule
      ],
    });
    fixture = TestBed.createComponent(ModalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    spyOn(component.visibleChange, 'emit');
    component.onVisibleChange(true);
    
    expect(component.visibleChange.emit).toHaveBeenCalledWith(true);
  });
});
