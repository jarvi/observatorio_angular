import { Component, EventEmitter, Input, Output, inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { MobileWidthService } from '../../services/mobile-width.service';
import { SpinnerService } from '../../services/spinner.service';
import { ModalDetails } from '../../interfaces';

@Component({
  selector: 'shared-modal-details',
  templateUrl: './modal-details.component.html',
  styleUrls: ['./modal-details.component.scss']
})
export class ModalDetailsComponent {

  @Input() public informationDetails: ModalDetails[][] = [];
  @Input() public modalTitle: string = '';
  @Input() public modalVisible: boolean = false;
  @Input() public useValidationIcons: boolean = false;
  @Output() public visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  private _mobileWidthService = inject(MobileWidthService);
  public spinnerService = inject(SpinnerService);

  private _widthSubscription: Subscription = new Subscription();

  public isScreenSmall: boolean = false;

  ngOnInit(): void {
    this._widthSubscription = this._mobileWidthService.getIsScreenSmall().subscribe(isScreenSmall => {
      this.isScreenSmall = isScreenSmall;
    });
  }
  
  ngOnDestroy(): void {
    this._widthSubscription.unsubscribe();
  }

  public onVisibleChange(visible: boolean): void {
    this.visibleChange.emit(visible);
  }
}
