import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'shared-version-app',
  templateUrl: './version-app.component.html',
  styleUrls: ['./version-app.component.scss']
})
export class VersionAppComponent {

  public appVersion: string = environment.appVersion;
  public poweredBy: string = environment.poweredBy;

}
