import { Component, EventEmitter, Input, Output } from '@angular/core';
import { INFO_MESSAGE_TYPE } from '../../enums/info-message-type.enum';

@Component({
  selector: 'shared-info-message',
  templateUrl: './info-message.component.html',
  styleUrls: ['./info-message.component.scss']
})
export class InfoMessageComponent {

  @Input() public message: string = '';
  @Input() public type: INFO_MESSAGE_TYPE = INFO_MESSAGE_TYPE.error;
  @Output() onCloseMessage: EventEmitter<void> = new EventEmitter<void>();

  public getMessageType(): string {
    return `container ${this.type}`
  }

  public closeMessage(): void {
    this.onCloseMessage.emit();
  }
}
