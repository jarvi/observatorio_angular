import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoMessageComponent } from './info-message.component';

describe('InfoMessageComponent', () => {
  let component: InfoMessageComponent;
  let fixture: ComponentFixture<InfoMessageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InfoMessageComponent]
    });
    fixture = TestBed.createComponent(InfoMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    spyOn(component.onCloseMessage, 'emit');
    const compiled = fixture.nativeElement as HTMLElement;
    const closeButton = compiled.querySelector('.close-button') as HTMLButtonElement;
    closeButton!.click();
    expect(component.onCloseMessage.emit).toHaveBeenCalled();
  });
});
