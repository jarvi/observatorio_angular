import { Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild, inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { OverlayPanel } from 'primeng/overlaypanel';
import * as moment from 'moment';
import { ValidatorService } from '../../services';
import { addLeadingZero } from 'src/utils/add-leading-zero';

@Component({
  selector: 'shared-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss']
})
export class TimePickerComponent implements OnInit, OnChanges, OnDestroy {

  @Input() public timeFormControl: AbstractControl = new FormControl();
  @Input() public id: string = '';
  @Input() public useHourHalf: boolean = false;
  @ViewChild('overlayTarget') overlayTarget!: ElementRef;
  @ViewChild('overlayTime') overlay!: OverlayPanel;

  private _fb = inject(FormBuilder);
  private _validatorService = inject(ValidatorService);

  private _inputFormTimeSubscription: Subscription = new Subscription();

  public timeType = ['AM', 'PM'];
  public hourHalfType = ['00', '30'];
  public inputFormTime: FormGroup = this._fb.group({
    hour: [null, [
      Validators.required,
      this._validatorService.minMaxLengthValidator(0, 2),
      this._validatorService.numericValidator(),
      this._validatorService.numberIsGreaterThan(12)
    ]],
    minute: [null, [
      Validators.required,
      this._validatorService.minMaxLengthValidator(0, 2),
      this._validatorService.numericValidator(),
      this._validatorService.numberIsGreaterThan(59)
    ]],
    ampm: ['AM', [Validators.required]],
  });

  ngOnInit(): void {
    this.inputFormDateValidations();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['timeFormControl']) { this.getDate(); }
  }

  ngOnDestroy(): void {
    this._inputFormTimeSubscription.unsubscribe();
  }

  private inputFormDateValidations() {
    this._inputFormTimeSubscription = this.inputFormTime.valueChanges.subscribe((formValue) => {
      const { hour, minute, ampm } = formValue;

      if (!hour || !minute || !ampm) {
        this.timeFormControl.setValue(null);
        return;
      };

      const newDate = moment(`${hour}:${minute} ${ampm}`, 'hh:mm A');

      if (!newDate?.isValid()) {
        this.timeFormControl.setValue(null);
        return;
      };

      this.timeFormControl.setValue(newDate.toDate());
    });
  }

  private getDate() {
    if (!this.timeFormControl.value) return;
    const date = new Date(this.timeFormControl.value).getTime();
    if (isNaN(date)) return;

    const newDate = moment(this.timeFormControl.value);

    const hour = newDate.format('hh');
    const minute = newDate.format('mm');
    const ampm = newDate.format('A');

    this.inputFormTime.patchValue({
      hour: addLeadingZero(hour.toString()),
      minute: addLeadingZero(minute.toString()),
      ampm: ampm
    }, { emitEvent: false });
  }

  public toggleOverlayCalendar(event: Event) {
    this.overlay.show(event, this.overlayTarget.nativeElement);
  }

  public closeOverlayCalendar() { this.overlay.hide(); }

  public addTimeValue(quantityToAdd: number, formControlSelector: AbstractControl, limit: number) {
    let currentValue = formControlSelector.value;
    
    if(!currentValue) { currentValue = '00' }

    const numberValue = parseInt(currentValue);
    if (numberValue >= limit) {
      formControlSelector.setValue(addLeadingZero(limit.toString()));
      return
    };

    const newValue = numberValue + quantityToAdd;
    
    formControlSelector.setValue(addLeadingZero(newValue.toString()));
  }

  public substractTimeValue(quantityToSubstract: number, formControlSelector: AbstractControl, minLimit: number, maxLimit: number) {
    let currentValue = formControlSelector.value;
    
    if(!currentValue) { currentValue = '00' }

    const numberValue = parseInt(currentValue);
    if (numberValue <= minLimit) {
      formControlSelector.setValue(addLeadingZero(minLimit.toString()));
      return
    };
    if (numberValue > maxLimit) {
      formControlSelector.setValue(addLeadingZero(maxLimit.toString()));
      return
    };

    const newValue = numberValue - quantityToSubstract;
    
    formControlSelector.setValue(addLeadingZero(newValue.toString()));
  }

  public changeTypeTime(value: string, formControlSelector: AbstractControl) {
    formControlSelector.setValue(value);
  }
}
