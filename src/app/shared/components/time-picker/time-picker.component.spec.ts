import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimePickerComponent } from './time-picker.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { AbstractControl, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared.module';

describe('TimePickerComponent', () => {
  let component: TimePickerComponent;
  let fixture: ComponentFixture<TimePickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TimePickerComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        PrimeNgModule,
        SharedModule
      ]
    });
    fixture = TestBed.createComponent(TimePickerComponent);
    component = fixture.componentInstance;
    component.inputFormTime = new FormGroup({
      hour: new FormControl(),
      minute: new FormControl(),
      ampm: new FormControl(),
    });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set value to timeFormControl if all inputFormTime formControls have value', () => {
    component.inputFormTime.controls['hour'].setValue(3);
    component.inputFormTime.controls['minute'].setValue(12);
    component.inputFormTime.controls['ampm'].setValue('PM');

    const timeFormControlValue = component.timeFormControl.value;

    expect(timeFormControlValue).toBeTruthy();
  });

  it('should set null value to timeFormControl if all inputFormTime formControls have an incorrect value', () => {
    component.inputFormTime.controls['hour'].setValue('rrrr');
    component.inputFormTime.controls['minute'].setValue('rrrr');
    component.inputFormTime.controls['ampm'].setValue('rrrr');

    const timeFormControlValue = component.timeFormControl.value;

    expect(timeFormControlValue).toEqual(null);
  });

  it('should change inputFormTime value if timeFormControl has an initial value', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newUnlockedChips: AbstractControl = new FormControl();
    newUnlockedChips.setValue(new Date());
    
    component.timeFormControl = newUnlockedChips;
    fixture.detectChanges();
    component.ngOnChanges({ timeFormControl: {
      previousValue: [],
      currentValue: newUnlockedChips,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.inputFormTime.value).not.toEqual(null);
  });

  it('should not change inputFormTime value if timeFormControl has an initial incorrect value', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newUnlockedChips: AbstractControl = new FormControl();
    newUnlockedChips.setValue('rrrr');
    
    component.timeFormControl = newUnlockedChips;
    fixture.detectChanges();
    component.ngOnChanges({ timeFormControl: {
      previousValue: [],
      currentValue: newUnlockedChips,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.inputFormTime.controls['hour'].value).toEqual(null);
  });

  it('should call show overlay method if toggleOverlayCalendar is executed', () => {
    spyOn(component.overlay, 'show');

    const simulatedEvent = new PointerEvent('click', { pointerType: 'mouse' });
    component.toggleOverlayCalendar(simulatedEvent);

    expect(component.overlay.show).toHaveBeenCalled();
  });

  it('should call hide overlay method if closeOverlayCalendar is executed', () => {
    spyOn(component.overlay, 'hide');

    component.closeOverlayCalendar();

    expect(component.overlay.hide).toHaveBeenCalled();
  });

  it('should add one day to formControl', () => {
    const formControl = new FormControl();
    formControl.setValue(2);

    component.addTimeValue(1, formControl, 5);

    expect(formControl.value).toEqual('03');
  });

  it('should not add one day to formControl if this is equal or highest to limit', () => {
    const formControl = new FormControl();
    formControl.setValue(2);

    component.addTimeValue(1, formControl, 2);

    expect(formControl.value).toEqual('02');
  });

  it('should not add one day to formControl if this is null', () => {
    const formControl = new FormControl();
    formControl.setValue(null);

    component.addTimeValue(1, formControl, 2);

    expect(formControl.value).toEqual('01');
  });

  it('should substract one day to formControl', () => {
    const formControl = new FormControl();
    formControl.setValue(2);

    component.substractTimeValue(1, formControl, 0, 5);

    expect(formControl.value).toEqual('01');
  });

  it('should not substract one day to formControl if this is equal or highest to min limit', () => {
    const formControl = new FormControl();
    formControl.setValue(0);

    component.substractTimeValue(1, formControl, 0, 2);

    expect(formControl.value).toEqual('00');
  });

  it('should not substract one day to formControl if this is equal or highest to max limit', () => {
    const formControl = new FormControl();
    formControl.setValue(7);

    component.substractTimeValue(1, formControl, 0, 2);

    expect(formControl.value).toEqual('02');
  });

  it('should change formControl value with changeTypeTime method', () => {
    const formControl = new FormControl();
    component.changeTypeTime('1', formControl);

    expect(formControl.value).toEqual('1');
  });

  it('should not do anything if timeFormControl does not have any value', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newTimeFormControl: AbstractControl = new FormControl();
    newTimeFormControl.setValue(null);
    
    component.timeFormControl = newTimeFormControl;
    fixture.detectChanges();
    component.ngOnChanges({ timeFormControl: {
      previousValue: [],
      currentValue: newTimeFormControl,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
    expect(component.inputFormTime.controls['hour'].value).toEqual(null);
  });
});
