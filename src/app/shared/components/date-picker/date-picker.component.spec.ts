import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePickerComponent } from './date-picker.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import { AbstractControl, FormControl, ReactiveFormsModule } from '@angular/forms';

describe('DatePickerComponent', () => {
  let component: DatePickerComponent;
  let fixture: ComponentFixture<DatePickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatePickerComponent],
      imports: [
        PrimeNgModule,
        ReactiveFormsModule
      ]
    });
    fixture = TestBed.createComponent(DatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnChanges when dateFormControl changes and does not do anything', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newFormControl = new FormControl();
    component.dateFormControl = newFormControl;
    fixture.detectChanges();
    component.ngOnChanges({ dateFormControl: {
      previousValue: '',
      currentValue: newFormControl,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
  });

  it('should call ngOnChanges when dateFormControl changes and assign the date', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newFormControl = new FormControl();
    newFormControl.setValue(new Date());
    component.dateFormControl = newFormControl;
    fixture.detectChanges();
    component.ngOnChanges({ dateFormControl: {
      previousValue: '',
      currentValue: newFormControl,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
  });

  it('should call ngOnChanges when dateFormControl changes and does not do anything if date is not a Date', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    const newFormControl = new FormControl();
    newFormControl.setValue('test');
    component.dateFormControl = newFormControl;
    fixture.detectChanges();
    component.ngOnChanges({ dateFormControl: {
      previousValue: '',
      currentValue: newFormControl,
      firstChange: true,
      isFirstChange: () => true
    }});

    expect(component.ngOnChanges).toHaveBeenCalled();
  });

  it('should change dateFormControl value when all is correct', () => {
    component.inputFormDate.patchValue({
      day: 1,
      month: 1,
      year: 2023
    })
    
    const dateFormControl = component.dateFormControl.value;

    expect(dateFormControl).toEqual(new Date('01-01-2023'));
  });

  it('should change dateFormControl value to null when day is incorrect', () => {
    component.inputFormDate.patchValue({
      day: null,
      month: 1,
      year: 2023
    })
    
    const dateFormControl = component.dateFormControl.value;

    expect(dateFormControl).toEqual(null);
  });

  it('should change dateFormControl value to null when all is incorrect', () => {
    component.inputFormDate.patchValue({
      day: 99,
      month: 99,
      year: 9999
    })
    
    const dateFormControl = component.dateFormControl.value;

    expect(dateFormControl).toEqual(null);
  });

  it('should change formDate date value to some valid value', () => {
    const date = new Date();
    component.formDate.patchValue({
      date: date
    })
    
    const dateFormControl = component.dateFormControl.value;

    expect(dateFormControl).toEqual(date);
  });

  it('should change formDate date value to some valid value', () => {
    const date = null;
    component.formDate.patchValue({
      date: date
    })
    
    const dateFormControl = component.dateFormControl.value;

    expect(dateFormControl).toEqual(date);
  });
});
