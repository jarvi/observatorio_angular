import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { ValidatorService } from '../../services/validator.service';
import { addLeadingZero } from 'src/utils/add-leading-zero';

@Component({
  selector: 'shared-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
})
export class DatePickerComponent implements OnInit, OnChanges, OnDestroy {

  @Input() public dateFormControl: AbstractControl = new FormControl();
  @Input() public id: string = '';

  private _fb = inject(FormBuilder);
  private _validatorService = inject(ValidatorService);

  private _inputFormDateSubscription: Subscription = new Subscription();
  private _formDateSubscription: Subscription = new Subscription();

  public inputFormDate: FormGroup = this._fb.group({
    day: [null, [
      Validators.required,
      this._validatorService.minMaxLengthValidator(0, 2),
      this._validatorService.numericValidator()
    ]],
    month: [null, [
      Validators.required,
      this._validatorService.minMaxLengthValidator(0, 2),
      this._validatorService.numericValidator()
    ]],
    year: [null, [
      Validators.required,
      this._validatorService.minMaxLengthValidator(0, 4),
      this._validatorService.numericValidator()
    ]],
  });
  public formDate: FormGroup = this._fb.group({
    date: [null, [Validators.required]]
  });

  ngOnInit(): void {
    this.inputFormDateValidations();
    this.formDateValidations();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['dateFormControl']) { this.getDate(); }
  }

  ngOnDestroy(): void {
    this._formDateSubscription.unsubscribe();
    this._inputFormDateSubscription.unsubscribe();
  }

  private getDate() {
    if (!this.dateFormControl.value) return;
    const date = new Date(this.dateFormControl.value).getTime();
    if (isNaN(date)) return;

    const newDate = moment(this.dateFormControl.value);
    this.formDate.patchValue({ date: newDate.toDate() }, { emitEvent: false });

    const day = newDate.date()
    const month = newDate.month() + 1;
    const year = newDate.year();

    this.inputFormDate.patchValue({
      day: addLeadingZero(day.toString()),
      month: addLeadingZero(month.toString()),
      year: addLeadingZero(year.toString())
    }, { emitEvent: false });
  }

  private inputFormDateValidations() {
    this._inputFormDateSubscription = this.inputFormDate.valueChanges.subscribe((formValue) => {
      const { day, month, year } = formValue;

      if (!day || !month || !year) {
        this.formDate.patchValue({ date: null }, { emitEvent: false });
        this.dateFormControl.setValue(null);
        return;
      };

      const newDate = moment({ year, month: month - 1, day });

      if (!newDate?.isValid()) {
        this.formDate.patchValue({ date: null }, { emitEvent: false });
        this.dateFormControl.setValue(null);
        return;
      };

      this.formDate.patchValue({ date: newDate.toDate() }, { emitEvent: false });
      this.dateFormControl.setValue(newDate.toDate());
    });
  }

  private formDateValidations() {
    this._formDateSubscription = this.formDate.controls['date'].valueChanges.subscribe((dateValue) => {
      this.dateFormControl.setValue(dateValue);

      const newDate = moment(dateValue);

      if (!newDate.isValid()) return;

      const day = newDate.date()
      const month = newDate.month() + 1;
      const year = newDate.year();

      this.inputFormDate.patchValue({
        day: addLeadingZero(day.toString()),
        month: addLeadingZero(month.toString()),
        year: addLeadingZero(year.toString())
      }, { emitEvent: false });
    })
  }
}
