export enum MODAL_ALERT_TYPE {
    success = 'success',
    info = 'info',
    warning = 'warning',
    error = 'error',
}