export enum INPUT_TYPE_ERROR {
    required = 'Campo obligatorio',
    maxlength = 'Longitud máxima de caracteres',
    minlength = 'Longitud mínima de caracteres',
    validEmail = 'Correo no válido',
    medellinValidEmail = 'El correo debe ser de la alcaldía de Medellín',
    whiteSpaces = 'No se permiten espacios en blanco'
}