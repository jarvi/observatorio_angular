export const PERMISSION_TYPE = {
    viewUser: { name: 'view_user', formName: 'consultUsers', labeledValue: 'Consultar usuarios' },
    addUser: { name: 'add_user', formName: 'createUsers', labeledValue: 'Crear usuarios' },
    changeUser: { name: 'change_user', formName: 'editUsers', labeledValue: 'Editar usuarios' },
    deleteUser: { name: 'delete_user', formName: 'inactivateUsers', labeledValue: 'Inactivar usuarios' },
    viewRol: { name: 'view_rol', formName: 'consultRoles', labeledValue: 'Consultar roles' },
    addRol: { name: 'add_rol', formName: 'createRoles', labeledValue: 'Crear roles' },
    changeRol: { name: 'change_rol', formName: 'editRoles', labeledValue: 'Editar roles' },
    deleteRol: { name: 'delete_rol', formName: 'inactivateRoles', labeledValue: 'Inactivar roles' },
    addSource: { name: 'add_source', formName: 'manageDataSources', labeledValue: 'Gestionar fuentes de datos' },
    viewBoard: { name: 'view_board', formName: 'viewBoards', labeledValue: 'Ver tableros' },
    viewNotification: { name: 'view_notificationset', formName: 'viewNotifications', labeledValue: 'Consultar notificaciones' },
    changeNotification: { name: 'change_notificationset', formName: 'editNotifications', labeledValue: 'Editar notificaciones' },
    addReport: { name: 'add_report', formName: 'generateReports', labeledValue: 'Generar informes' },
    viewReport: { name: 'view_report', formName: 'viewReports', labeledValue: 'Ver informes' },
}