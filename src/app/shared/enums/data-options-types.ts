export enum DATA_OPTIONS_TYPES {
    simat = 'SIMAT',
    due = 'DUE',
    saberTests = 'Pruebas Saber',
    humanTalent = 'Talento Humano',
    pae = 'PAE',
    futuresComputers = 'Computadores Futuro',
    educativeInfraestructure = 'Infraestructura Educativa',
    internalEfficiency = 'Consolidado de Eficiencia Interna',
    technicalAverage= 'Media Técnica',
    schoolEnvironmentProtector = 'Entorno Escolar Protector',
    projectionOfQuotas = 'Proyección de Cupos',
    transport = 'Transporte',
    dropoutRate = 'Deserción Repitencia',
    consolidatedSIMAT = 'Consolidado SIMAT',
    detailedStrategies = 'Detallado Estrategias',
    indicativePlanAndActionPlan = 'Plan Indicativo y Plan de Acción'
}

export enum DATA_ROUTES {
    dataLoading = 'data-loading',
    board = 'board'
}