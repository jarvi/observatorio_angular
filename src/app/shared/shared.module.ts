import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PrimeNgModule } from '../prime-ng/prime-ng.module';
import { InfoMessageComponent } from './components/info-message/info-message.component';
import { VersionAppComponent } from './components/version-app/version-app.component';
import { ModalComponent } from './components/modal/modal.component';
import { InputPasswordConditionsComponent } from './components/input-password-conditions/input-password-conditions.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { NoResultsComponent } from './components/no-results/no-results.component';
import { DatagridLayoutComponent } from './layouts/datagrid-layout/datagrid-layout.component';
import { TitleFilterComponent } from './components/title-filter/title-filter.component';
import { InputRadiobuttonComponent } from './components/input-radiobutton/input-radiobutton.component';
import { ModalDetailsComponent } from './components/modal-details/modal-details.component';
import { UploadFilesComponent } from './components/upload-files/upload-files.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { ChipButtonsComponent } from './components/chip-buttons/chip-buttons.component';
import { DayPickerComponent } from './components/day-picker/day-picker.component';
import { SortArrayPipe } from './pipes/sort-array.pipe';
import { BooleanToStringPipe } from './pipes/boolean-to-string.pipe';
import { FlatmapArrayPipe } from './pipes/flatmap-array.pipe';
import { JoinArrayByPipe } from './pipes/join-array-by.pipe';
import { MapArrayByPropertyPipe } from './pipes/map-array-by-property.pipe';
import { DateToTimeStringPipe } from './pipes/date-to-time-string.pipe';
import { ReplaceCharacterPipe } from './pipes/replace-character.pipe';
import { ValidInputErrorsDirective } from './directives/valid-input-errors.directive';
import { ValidInputWithoutMessageDirective } from './directives/valid-input-without-message.directive';
import { CheckPermissionToRenderDirective } from './directives/check-permission-to-render.directive';
import { DropZoneDirective } from './directives/drop-zone.directive';
import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { TimePickerSelectorComponent } from './components/time-picker-selector/time-picker-selector.component';

@NgModule({
  declarations: [
    InfoMessageComponent,
    VersionAppComponent,
    ModalComponent,
    InputPasswordConditionsComponent,
    PaginatorComponent,
    NoResultsComponent,
    DatagridLayoutComponent,
    TitleFilterComponent,
    InputRadiobuttonComponent,
    ModalDetailsComponent,
    UploadFilesComponent,
    DatePickerComponent,
    ChipButtonsComponent,
    DayPickerComponent,
    TimePickerComponent,
    TimePickerSelectorComponent,
    ValidInputErrorsDirective,
    ValidInputWithoutMessageDirective,
    CheckPermissionToRenderDirective,
    DropZoneDirective,
    SortArrayPipe,
    BooleanToStringPipe,
    FlatmapArrayPipe,
    JoinArrayByPipe,
    MapArrayByPropertyPipe,
    DateToTimeStringPipe,
    ReplaceCharacterPipe
  ],
  imports: [
    ReactiveFormsModule,

    CommonModule,
    PrimeNgModule,
  ],
  exports: [
    InfoMessageComponent,
    VersionAppComponent,
    ModalComponent,
    InputPasswordConditionsComponent,
    PaginatorComponent,
    NoResultsComponent,
    DatagridLayoutComponent,
    TitleFilterComponent,
    InputRadiobuttonComponent,
    ModalDetailsComponent,
    UploadFilesComponent,
    DatePickerComponent,
    ChipButtonsComponent,
    DayPickerComponent,
    TimePickerComponent,
    TimePickerSelectorComponent,
    ValidInputErrorsDirective,
    ValidInputWithoutMessageDirective,
    CheckPermissionToRenderDirective,
    DropZoneDirective,
    SortArrayPipe,
    BooleanToStringPipe,
    FlatmapArrayPipe,
    JoinArrayByPipe,
    MapArrayByPropertyPipe,
    DateToTimeStringPipe,
    ReplaceCharacterPipe
  ]
})
export class SharedModule { }
