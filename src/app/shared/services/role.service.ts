import { Injectable, inject } from '@angular/core';
import { Observable, catchError, map, of, throwError } from 'rxjs';
import { HttpRequestService } from './http-request.service';
import { environment } from 'src/environments/environment';
import { Role } from '../interfaces/role.interface';
import { RolesSearchRequest } from '../interfaces/roles-search-request.interface';
import { RolesSearchResponse } from '../interfaces/roles-search-response.interface';
import { GetRoleResponse, RoleCreationRequest, RoleCreationResponse, RoleEditionRequest } from '../interfaces';
import { RoleActivateRequest } from '../interfaces/role-activate-request.interface';
import { RoleEditionResponse } from '../interfaces/role-edition-response.interface';
import { RoleType } from '../interfaces/role-type.interface';
import { getLabelsFromPermissions } from 'src/utils/permissions';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private _httpRequestService$ = inject(HttpRequestService);
  
  private _pathRole: string = environment.pathRole;

  public roles: RoleType[] = [];

  public getRoles(): Observable<RoleType[]> {
    if (this.roles.length > 0) {
      return of(this.roles);
    }

    return this._httpRequestService$.get<RolesSearchRequest, RolesSearchResponse>(`${this._pathRole}/`, { active: '' }).pipe(
      map((roles: RolesSearchResponse) => {
        const newRoles: RoleType[] = roles.results.map((role) => {
          return {
            id: role.id,
            name: role.name,
            isActive: role.is_active,
            permissions: role.permissions.map((permission) => permission.codename)
          }
        });
        this.roles = newRoles;
        return newRoles;
      })
    );
  }

  public searchRoles(rolesSearchRequest: RolesSearchRequest): Observable<RolesSearchResponse> {
    return this._httpRequestService$.get<RolesSearchRequest, RolesSearchResponse>(`${this._pathRole}/`, rolesSearchRequest)
      .pipe(
        map((response) => response),
        catchError((error) => {
          return throwError(() => error?.detail || '');
        })
      )
  }

  public createRole(roleInformation: RoleCreationRequest): Observable<string> {
    return this._httpRequestService$.post<RoleCreationRequest, RoleCreationResponse>(`${this._pathRole}/create/`, roleInformation)
      .pipe(
        map(() => roleInformation.name),
        catchError((error) => {
          return throwError(() => error?.message || '');
        })
      )
  }

  public getRole(id: number): Observable<Role> {
    return this._httpRequestService$.get<{}, GetRoleResponse>(`${this._pathRole}/${id}/permissions/`, {})
      .pipe(
        map((response) => {
          const permissions = response.permissions.map((permission) => permission.codename);

          return {
            id: response.id,
            name: response.name,
            isActive: response.is_active,
            permissions: getLabelsFromPermissions(permissions)
          }
        }),
        catchError((error) => {
          return throwError(() => error?.detail || '');
        })
      )
  }

  public activateRole({ id, is_active }: RoleActivateRequest): Observable<boolean> {
    return this._httpRequestService$.put<RoleActivateRequest, {}>(`${this._pathRole}/${id}/`, { is_active })
      .pipe(
        map(() => true),
        catchError((error) => {
          return of(false);
        })
      );
  }

  public editRole(roleInformation: RoleEditionRequest): Observable<Role> {
    return this._httpRequestService$.put<RoleEditionRequest, RoleEditionResponse>(`${this._pathRole}/update/${roleInformation.id}/`, roleInformation)
      .pipe(
        map(({ result }) => {
          return {
            id: result.id,
            isActive: result.is_active,
            name: result.name,
            permissions: result?.permissions?.map(permission => permission.codename) ?? []
          }
        }),
        catchError((error) => {
          return throwError(() => error?.detail || '');
        })
      )
  }
}
