import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { RoleService } from './role.service';
import { HttpRequestService } from './http-request.service';
import { of, take, throwError } from 'rxjs';
import { GetRoleResponse, Role, RoleActivateRequest, RoleCreationRequest, RoleCreationResponse, RoleEditionRequest, RolesSearchRequest, RolesSearchResponse } from '../interfaces';
import { PERMISSION_TYPE } from '../enums/permission-type.enum';
import { RoleEditionResponse } from '../interfaces/role-edition-response.interface';


describe('RolService', () => {
  let service: RoleService;
  let httpRequestService: HttpRequestService;
  let spyHttpRequestService: jasmine.Spy;

  const rolesResponse: RolesSearchResponse = {
    count: 1,
    next: null,
    previous: null,
    results: [{
      id: 1,
      is_active: true,
      name: 'test',
      permissions: [{
        id: 1,
        codename: PERMISSION_TYPE.addReport.name,
      }]
    }]
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);

    spyHttpRequestService = spyOn(httpRequestService, 'get')
    spyHttpRequestService.and.returnValue(of(rolesResponse));

    service = TestBed.inject(RoleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get roles', () => {
    service.getRoles()
      .pipe(take(1))
      .subscribe((rolesResponse) => {
        expect(rolesResponse).toEqual(rolesResponse);
    });
    service.getRoles()
      .pipe(take(1))
      .subscribe((rolesResponse) => {
        expect(rolesResponse).toEqual(rolesResponse);
    });
  });

  it('should get roles and call http request', () => {
    service.getRoles();
    expect(spyHttpRequestService).toHaveBeenCalled();
  });

  it('should create new rol', () => {
    const response: RoleCreationResponse = {
      id: 1,
      name: 'Administrador',
      permissions: ['edit'],
      is_active: true,
    }

    spyOn(httpRequestService, 'post').and.returnValue(of(response));
    
    const request: RoleCreationRequest = {
      name: 'Administrador',
      permissions: ['edit']
    }

    service.createRole(request)
    .pipe(take(1))
    .subscribe((roleName) => {
      expect(roleName).toEqual(request.name);
    });
  });

  it('should not create new row', () => {
    spyOn(httpRequestService, 'post').and.returnValue(throwError(() => {}));
    
    const request: RoleCreationRequest = {
      name: 'Administrador',
      permissions: ['edit']
    }

    service.createRole(request)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('');
      }
    );
  });
});

describe('RolService with another get', () => {
  let service: RoleService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    service = TestBed.inject(RoleService);
  });

  it('should get a specific role when method is executed', () => {
    const role: Role = {
      id: 3,
      name: 'Lector',
      isActive: true,
      permissions: [PERMISSION_TYPE.addReport.labeledValue, ''],
    }
    const response: GetRoleResponse = {
      id: 3,
      name: 'Lector',
      is_active: true,
      permissions: [
        { id: 1, codename: PERMISSION_TYPE.addReport.name },
        { id: 2, codename: 'Another' },
      ]
    }
    spyOn(httpRequestService, 'get').and.returnValue(of(response));

    service.getRole(1)
    .subscribe((response) => {
      expect(response).toEqual(role);
    });
  });

  it('should not get aby role when method is executed with bad request', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.getRole(1)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => { expect(err).toEqual(''); }
    );
  });

  it('should activate a specific role when method is executed', () => {
    const roleActivateRequest: RoleActivateRequest = {
      id: 1,
      is_active: true
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(true));
    
    service.activateRole(roleActivateRequest)
    .subscribe((response) => {
      expect(response).toBeTrue();
    });
  });

  it('should not activate any role when method is executed with bad request', () => {
    const roleActivateRequest: RoleActivateRequest = {
      id: 1,
      is_active: true
    }
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => {}));

    service.activateRole(roleActivateRequest)
    .pipe(take(1))
    .subscribe(
      (res) => {
        expect(res).toEqual(false);
      },
      () => {}
    );
  });

  it('should edit a specific role when method is executed', () => {
    const roleEditionRequest: RoleEditionRequest = {
      id: 1,
      name: 'Administrador',
      permissions: ['edit'],
    }
    
    const roleResponse: RoleEditionResponse = {
      message: '',
      result: {
        id: 1,
        name: 'Administrador',
        is_active: true,
        permissions: [{
          id: 1,
          codename: 'edit'
        }]
      }
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(roleResponse));
    
    service.editRole(roleEditionRequest)
    .subscribe((response) => {
      expect(response.name).toEqual('Administrador');
    });
  });

  it('should edit a specific role when method is executed without permissions', () => {
    const roleEditionRequest: RoleEditionRequest = {
      id: 1,
      name: 'Administrador',
      permissions: ['edit'],
    }
    
    const roleResponse: RoleEditionResponse = {
      message: '',
      result: {
        id: 1,
        name: 'Administrador',
        is_active: true
      }
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(roleResponse));
    
    service.editRole(roleEditionRequest)
    .subscribe((response) => {
      expect(response.name).toEqual('Administrador');
    });
  });

  it('should search roles by filters and call http request', () => {

    const rolesSearchResponse: RolesSearchResponse = {
      count: 1,
      next: null,
      previous: null,
      results: [{
        id: 1,
        is_active: true,
        name: 'test',
        permissions: [{
          id: 1,
          codename: PERMISSION_TYPE.addReport.name,
        }]
      }]
    }

    spyOn(httpRequestService, 'get').and.returnValue(of(rolesSearchResponse));

    const rolesSearchRequest: RolesSearchRequest = {
      id: 1,
      active: true,
      permissions: PERMISSION_TYPE.addReport.name,
      page_size: 10,
      page: 1,
    }
    
    service.searchRoles(rolesSearchRequest)
    .subscribe((response) => {
      expect(response).toEqual(rolesSearchResponse);
    });
  });

  it('should not search roles when method is executed with bad request', () => {

    const rolesSearchRequest: RolesSearchRequest = {
      id: 1,
      active: true,
      permissions: PERMISSION_TYPE.addReport.name,
      page_size: 10,
      page: 1,
    }

    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.searchRoles(rolesSearchRequest)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {expect(err).toEqual('');}
    );
  });
});
