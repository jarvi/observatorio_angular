import { Injectable } from '@angular/core';
import { Observable, fromEvent, map, startWith } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MobileWidthService {
  private _isScreenSmall$: Observable<boolean>;

  constructor() {
    this._isScreenSmall$ = fromEvent(window, 'resize').pipe(
      startWith(null),
      map(() => window.innerWidth <= 900)
    );
  }

  getIsScreenSmall(): Observable<boolean> {
    return this._isScreenSmall$;
  }
}
