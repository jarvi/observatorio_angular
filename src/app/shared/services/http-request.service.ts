import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {
  
  private _http = inject(HttpClient);
  private _baseUrl: string = environment.urlBase;

  public get<REQ, RES>(path: string, params?: Partial<REQ>, isFile?: boolean): Observable<RES> {
    return this._http.get<RES>(`${this._baseUrl}${path}`, isFile ? { params: { ...params }, responseType: 'blob' as 'json' } : { params: { ...params }})
      .pipe(
        catchError(({ error, status }) => {
          if (status > 0) return throwError(() => error);
          return throwError(() => {});
        }),
      );
  }

  public post<REQ, RES>(path: string, body?: Partial<REQ>, isFormData?: boolean): Observable<RES> {
    return this._http.post<RES>(`${this._baseUrl}${path}`, isFormData ? body : { ...body })
      .pipe(
        catchError((error) => {
          if (error.status > 0) return throwError(() => error.error);
          if (error.message || error.detail) return throwError(() => error);
          return throwError(() => {});
        }),
      );
  }

  public put<REQ, RES>(path: string, body?: Partial<REQ>): Observable<RES> {
    return this._http.put<RES>(`${this._baseUrl}${path}`, { ...body })
      .pipe(
        catchError((error) => {
          if (error.status > 0) return throwError(() => error.error);
          if (error.message || error.detail) return throwError(() => error);
          return throwError(() => {});
        }),
      );
  }

  public delete<RES>(path: string): Observable<RES> {
    return this._http.delete<RES>(`${this._baseUrl}${path}`)
      .pipe(
        catchError((error) => {
          if (error.status > 0) return throwError(() => error.error);
          if (error.message || error.detail) return throwError(() => error);
          return throwError(() => {});
        }),
      );
  }
}
