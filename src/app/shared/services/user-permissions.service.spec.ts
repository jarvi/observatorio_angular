import { TestBed } from '@angular/core/testing';

import { UserPermissionsService } from './user-permissions.service';
import { HttpRequestService } from './http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GetPermissionsResponse } from '../interfaces/get-permissions-response.interface';
import { of, take, throwError } from 'rxjs';
import { PermissionType } from '../interfaces/permission-type.interface';

describe('UserPermissionsService', () => {
  let service: UserPermissionsService;
  let httpRequestService: HttpRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService
      ]
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    service = TestBed.inject(UserPermissionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should assign the permissions created bynow', () => {
    const response: GetPermissionsResponse = {
      id: 1,
      codename: 'permission',
    }

    let permissionsLocal: PermissionType[] = []

    spyOn(httpRequestService, 'get').and.returnValue(of([response]));

    service.getPermissions()
    .pipe(take(1))
    .subscribe((permissions) => { permissionsLocal = permissions });

    service.permissions$
    .pipe(take(1))
    .subscribe((permissions) => {
      expect(permissions).toEqual(permissionsLocal);
    })

    service.getPermissions()
    .pipe(take(1))
    .subscribe((permissions) => { permissionsLocal = permissions });

    service.permissions$
    .pipe(take(1))
    .subscribe((permissions) => {
      expect(permissions).toEqual(permissionsLocal);
    })
  });

  it('should not assign the permissions when there is an error', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => ''));

    service.getPermissions()
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('');
      }
    );
  });
});
