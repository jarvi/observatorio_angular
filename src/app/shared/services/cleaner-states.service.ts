import { Injectable, inject } from '@angular/core';
import { RoleService } from './role.service';
import { UserPermissionsService } from './user-permissions.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class CleanerStatesService {
  
  public _roleService = inject(RoleService);
  public _userPermissionsService = inject(UserPermissionsService);
  public _userService = inject(UserService);
  
  public clearStates(): void {
    this._roleService.roles = [];
    this._userPermissionsService.resetPermissions();
    this._userService.userInformation = {
      id: 0,
      email: '',
      firstName: '',
      lastName: '',
      completeName: '',
      isActive: false,
      documentType: {
        id: 0,
        name: '',
        alias: ''
      },
      documentNumber: '',
      address: '',
      cellPhone: '',
      contactNumber: '',
      gender: '',
      roles: []
    };
  }
}
