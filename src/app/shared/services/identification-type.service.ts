import { Injectable, inject } from '@angular/core';
import { Observable, of, map } from 'rxjs';
import { HttpRequestService } from './http-request.service';
import { IdentificationTypeResponse } from '../interfaces/identification-type-response.interface';
import { IdentificationType } from '../interfaces/identification-type.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IdentificationTypeService {

  private _httpRequestService = inject(HttpRequestService);

  private _pathUser: string = environment.pathUser;

  public identificationTypes: IdentificationType[] = [];

  getIdentificationTypes(): Observable<IdentificationType[]> {
    if (this.identificationTypes.length > 0) {
      return of(this.identificationTypes);
    }

    return this._httpRequestService.get<{}, IdentificationTypeResponse[]>(`${this._pathUser}/doc-types/`).pipe(
      map((identificationTypes: IdentificationType[]) => {
        this.identificationTypes = identificationTypes;
        return identificationTypes;
      }),
    );
  }
}
