import { TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ValidatorService } from './validator.service';

describe('ValidatorService', () => {
  let service: ValidatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should validate email properly', () => {
    const form = new FormGroup({
      email: new FormControl('', [service.emailValidator()])
    });

    form.controls.email.setValue('test@example.com');
    expect(form.controls.email.valid).toBe(true);

    form.controls.email.setValue('invalid-email');
    expect(form.controls.email.valid).toBe(false);
    expect(form.controls.email.errors).toEqual({ validEmail: false });
  });

  it('should validate Medellin email properly', () => {
    const form = new FormGroup({
      email: new FormControl('', [service.medellinEmailValidator()])
    });

    form.controls.email.setValue('test@medellin.gov.co');
    expect(form.controls.email.valid).toBe(true);

    form.controls.email.setValue('test@example.com');
    expect(form.controls.email.valid).toBe(false);
    expect(form.controls.email.errors).toEqual({ medellinValidEmail: false });
  });

  it('should mark invalid fields as touched', () => {
    const form = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });

    form.controls.email.setValue('');
    form.controls.password.setValue('123456');
    expect(form.controls.email.touched).toBe(false);
    expect(form.controls.password.touched).toBe(false);

    service.isValidField(form);
    expect(form.controls.email.touched).toBe(true);
    expect(form.controls.password.touched).toBe(false);
  });

  it('should validate white spaces in input password', () => {
    const form = new FormGroup({
      password: new FormControl('', service.whiteSpacesValidator()),
    });

    form.controls.password.setValue('123456');
    expect(form.controls.password.valid).toBe(true);

    form.controls.password.setValue('123 456');
    expect(form.controls.password.valid).toBe(false);
    expect(form.controls.password.errors).toEqual({ whiteSpaces: true });
  });

  it('should validate min and max length input', () => {
    const form = new FormGroup({
      input: new FormControl('', service.minMaxLengthValidator(0, 3)),
    });

    form.controls.input.setValue('123456');
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ length: true });

    form.controls.input.setValue('123');
    expect(form.controls.input.valid).toBe(true);
  });

  it('should validate upper case and lower case value input', () => {
    const form = new FormGroup({
      input: new FormControl('', service.upperCaseLowerCaseValidator()),
    });

    form.controls.input.setValue('abc');
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ upperCaseLowerCase: true });

    form.controls.input.setValue('abcABC');
    expect(form.controls.input.valid).toBe(true);
  });

  it('should validate special character on value input', () => {
    const form = new FormGroup({
      input: new FormControl('', service.specialCharacterValidator()),
    });

    form.controls.input.setValue('abc');
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ specialCharacter: true });

    form.controls.input.setValue('abc@');
    expect(form.controls.input.valid).toBe(true);
  });

  it('should validate that all characters are numbers', () => {
    const form = new FormGroup({
      input: new FormControl('', service.numericValidator()),
    });

    form.controls.input.setValue('abc');
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ numeric: true });

    form.controls.input.setValue('123');
    expect(form.controls.input.valid).toBe(true);
  });

  it('should validate if number is greater than specific number', () => {
    const form = new FormGroup({
      input: new FormControl(0, service.numberIsGreaterThan(12)),
    });

    form.controls.input.setValue(14);
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ numberIsGreaterThan: true });

    form.controls.input.setValue(11);
    expect(form.controls.input.valid).toBe(true);
  });

  it('should validate if the value input start with a space', () => {
    const form = new FormGroup({
      input: new FormControl('', service.whiteSpacesStartValidator()),
    });

    form.controls.input.setValue('  A');
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ whiteSpacesStart: true });

    form.controls.input.setValue('A');
    expect(form.controls.input.valid).toBe(true);
  });

  it('should validate if the value length input has a specific length', () => {
    const form = new FormGroup({
      input: new FormControl([1], service.hasLength(3)),
    });

    form.controls.input.setValue([1,2]);
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ lengthIsGreaterThan: true });

    form.controls.input.setValue([1,2,3]);
    expect(form.controls.input.valid).toBe(true);
  });

  it('should validate if date is greater than especific time', () => {
    const form = new FormGroup({
      input: new FormControl(new Date(), service.dateIsLessThan(7, 55)),
    });

    const badDate = new Date(new Date().setHours(7, 30, 0, 0));
    const niceDate = new Date(new Date().setHours(7, 55, 0, 0));

    form.controls.input.setValue(badDate);
    expect(form.controls.input.valid).toBe(false);
    expect(form.controls.input.errors).toEqual({ timeInvalid: true });

    form.controls.input.setValue(niceDate);
    expect(form.controls.input.valid).toBe(true);
  });
});
