import { TestBed } from '@angular/core/testing';

import { ModalAlertService } from './modal-alert.service';
import { ModalAlert } from '../interfaces/modal-alert.interface';
import { MODAL_ALERT_TYPE } from '../enums/modal-alert-type.enum';
import Swal from 'sweetalert2';

describe('ModalAlertService', () => {
  let service: ModalAlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalAlertService);
  });

  afterEach(() => {
    Swal.close();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should display an error modal alert', () => {
    const modalAlertError: ModalAlert = {
      title: 'Error',
      message: 'Error message',
      type: MODAL_ALERT_TYPE.error,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: true
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: false
      },
      highlightText: 'highlight text'
    }

    service.showModalAlert(modalAlertError);

    expect(Swal.isVisible()).toBeTruthy();

    const htmlContainer = Swal.getHtmlContainer();
    expect(htmlContainer).toHaveClass('error-type');

    const confirmButton = Swal.getConfirmButton();
    confirmButton?.click();
    expect(confirmButton).toBeTruthy();
  });

  it('should display a warning modal alert', () => {
    const modalAlertWarning: ModalAlert = {
      title: 'Warning',
      message: 'Warning message',
      type: MODAL_ALERT_TYPE.warning,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: true
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: false
      },
      highlightText: 'highlight text'
    }

    service.showModalAlert(modalAlertWarning);

    expect(Swal.isVisible()).toBeTruthy();

    const htmlContainer = Swal.getHtmlContainer();
    expect(htmlContainer).toHaveClass('warning-type');

    const cancelButton = Swal.getCancelButton();
    cancelButton?.click();
    expect(cancelButton).toBeTruthy();
  });

  it('should display an info modal alert', () => {
    const modalAlertInfo: ModalAlert = {
      title: 'Info',
      message: 'Info message',
      type: MODAL_ALERT_TYPE.info,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: true
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: false
      },
      highlightText: 'highlight text'
    }

    service.showModalAlert(modalAlertInfo);

    expect(Swal.isVisible()).toBeTruthy();

    const htmlContainer = Swal.getHtmlContainer();
    expect(htmlContainer).toHaveClass('info-type');
  });

  it('should display a success modal alert', () => {
    const modalAlertSuccess: ModalAlert = {
      title: 'Success',
      message: 'Success message',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: true
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: false
      },
      highlightText: 'highlight text'
    }

    service.showModalAlert(modalAlertSuccess);

    expect(Swal.isVisible()).toBeTruthy();

    const htmlContainer = Swal.getHtmlContainer();
    expect(htmlContainer).toHaveClass('success-type');
  });

  it('should display a highlight text in the modal alert', () => {
    const modalAlertSuccess: ModalAlert = {
      title: 'Success',
      message: 'Success message',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: true
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: false
      },
      highlightText: 'highlight text'
    }

    service.showModalAlert(modalAlertSuccess);

    expect(Swal.isVisible()).toBeTruthy();

    const htmlContainer = Swal.getHtmlContainer();
    const highlightText = htmlContainer?.querySelector('.modal-highlighted-text');

    expect(highlightText?.textContent).toEqual(modalAlertSuccess.highlightText);
  });

  it('should not display a highlight text in the modal alert', () => {
    const modalAlertSuccess: ModalAlert = {
      title: 'Success',
      message: 'Success message',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: true
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: false
      },
    }

    service.showModalAlert(modalAlertSuccess);

    expect(Swal.isVisible()).toBeTruthy();

    const htmlContainer = Swal.getHtmlContainer();
    const highlightText = htmlContainer?.querySelector('.modal-highlighted-text');

    expect(highlightText).toBeNull();
  });

  it('should not display the modal-alert-button-primary class in confirm button', () => {
    const modalAlertSuccess: ModalAlert = {
      title: 'Success',
      message: 'Success message',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: false
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: false
      },
    }

    service.showModalAlert(modalAlertSuccess);

    expect(Swal.isVisible()).toBeTruthy();

    const confirmButton = Swal.getConfirmButton();

    expect(confirmButton).not.toHaveClass('modal-alert-button-primary');
  });

  it('should display the modal-alert-button-primary class in cancel button', () => {
    const modalAlertSuccess: ModalAlert = {
      title: 'Success',
      message: 'Success message',
      type: MODAL_ALERT_TYPE.success,
      confirmButton: {
        action: () => { },
        label: 'Ok',
        primary: false
      },
      cancelButton: {
        action: () => { },
        label: 'Cancel',
        primary: true
      },
    }

    service.showModalAlert(modalAlertSuccess);

    expect(Swal.isVisible()).toBeTruthy();

    const cancelButton = Swal.getCancelButton();

    expect(cancelButton).toHaveClass('modal-alert-button-primary');
  });
});
