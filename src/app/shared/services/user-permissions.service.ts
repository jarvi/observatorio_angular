import { Injectable, inject } from '@angular/core';
import { BehaviorSubject, Observable, catchError, map, of, tap, throwError } from 'rxjs';
import { HttpRequestService } from './http-request.service';
import { SpinnerService } from './spinner.service';
import { environment } from 'src/environments/environment';
import { GetPermissionsResponse } from '../interfaces/get-permissions-response.interface';
import { PermissionType } from '../interfaces/permission-type.interface';

@Injectable({
  providedIn: 'root'
})
export class UserPermissionsService {

  private _httpRequestService$ = inject(HttpRequestService);
  private _spinnerService = inject(SpinnerService);

  private _pathRole: string = environment.pathRole;
  private permissionsSubject: BehaviorSubject<PermissionType[]> = new BehaviorSubject<PermissionType[]>([]);

  get permissions$() {
    return this.permissionsSubject.asObservable();
  }

  public getPermissions(): Observable<PermissionType[]> {
    if (this.permissionsSubject.value.length > 0) {
      return of(this.permissionsSubject.value);
    }

    this._spinnerService.showSpinner = true;

    return this._httpRequestService$.get<{}, GetPermissionsResponse[]>(`${this._pathRole}/user_permissions/`).pipe(
      tap((identificationTypes: PermissionType[]) => {
        this.permissionsSubject.next(identificationTypes);
        this._spinnerService.showSpinner = false;
      }),
      map((permissions) => permissions ),
      catchError((error) => {
        this._spinnerService.showSpinner = false;
        return throwError(() => error);
      })
    );
  }

  public resetPermissions() {
    this.permissionsSubject.next([]);
  }
}
