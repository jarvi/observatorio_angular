import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { ModalAlert } from '../interfaces/modal-alert.interface';
import { MODAL_ALERT_TYPE } from '../enums/modal-alert-type.enum';

@Injectable({
  providedIn: 'root'
})
export class ModalAlertService {

  public showModalAlert(modalAlert: ModalAlert): void {
    Swal.fire({
      customClass: {
        popup: 'modal-alert-popup',
        closeButton: 'modal-alert-close-button',
        htmlContainer: `modal-alert-html-container ${this.getClassByType(modalAlert.type)}`,
        actions: 'modal-alert-actions',
        confirmButton: `modal-alert-button ${modalAlert.confirmButton.primary ? 'modal-alert-button-primary' : ''}`,
        cancelButton: `modal-alert-button ${modalAlert.cancelButton?.primary ? 'modal-alert-button-primary' : ''}`,
      },
      showCloseButton: true,
      closeButtonHtml: '<i class="pi pi-times"></i>',
      html: `
        <div class="modal-alert-icon">
          <img src="${this.getIconByType(modalAlert.type)}" alt="${modalAlert.type}">
        </div>
        <h3 class="modal-title">${modalAlert.title}</h3>
        ${modalAlert.message ? `<p class="modal-text">${modalAlert.message}</p>` : ''}
        ${modalAlert.highlightText ? `<p class="modal-highlighted-text">${modalAlert.highlightText}</p>` : ''}
      `,
      confirmButtonText: modalAlert.confirmButton.label,
      showCancelButton: !!modalAlert.cancelButton?.label,
      cancelButtonText: modalAlert.cancelButton?.label,
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        modalAlert.confirmButton.action?.();
      }
      if (result.isDismissed) {
        modalAlert.cancelButton?.action?.();
      }
    });
  }

  private getIconByType(type: MODAL_ALERT_TYPE): string {
    switch (type) {
      case MODAL_ALERT_TYPE.error:
        return '../../../assets/images/modalAlertIcons/errorIcon.svg';
      case MODAL_ALERT_TYPE.warning:
        return '../../../assets/images/modalAlertIcons/warningIcon.svg';
      case MODAL_ALERT_TYPE.info:
        return '../../../assets/images/modalAlertIcons/infoIcon.svg';
      case MODAL_ALERT_TYPE.success:
        return '../../../assets/images/modalAlertIcons/successIcon.svg';
    }
  }

  private getClassByType(type: MODAL_ALERT_TYPE): string {
    switch (type) {
      case MODAL_ALERT_TYPE.error:
        return 'error-type';
      case MODAL_ALERT_TYPE.warning:
        return 'warning-type';
      case MODAL_ALERT_TYPE.info:
        return 'info-type';
      case MODAL_ALERT_TYPE.success:
        return 'success-type';
    }
  }
}
