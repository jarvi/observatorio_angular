import { Injectable } from '@angular/core';
import { FormGroup, ValidatorFn, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  private _emailPattern: string = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+[.]{1}[a-zA-Z]{2,4}$";
  private _medellinEmailPattern: string = "^[a-zA-Z0-9._%+-]+@[Mm][Ee][Dd][Ee][Ll][Ll][Ii][Nn]+[.]{1}[Gg][Oo][Vv]+[.]{1}[Cc][Oo]$";

  public emailValidator(): ValidatorFn {
    return (control) => {
      return Validators.pattern(this._emailPattern)(control) ? { validEmail: false } : null;
    }
  }

  public medellinEmailValidator(): ValidatorFn {
    return (control) => {
      return Validators.pattern(this._medellinEmailPattern)(control) ? { medellinValidEmail: false } : null;
    }
  }

  public isValidField(form: FormGroup) {
    const formControls = form.controls;

    return Object.keys(formControls).reduce((_, key) => {
      const control = formControls[key];

      if (control.invalid) {
        control.markAsTouched();
        control.updateValueAndValidity();
      }

      return null;
    }, null);
  }

  public whiteSpacesValidator(): ValidatorFn {
    return (control) => {
      const value = control.value as string;
      if (value && value.indexOf(' ') !== -1) {
        return { whiteSpaces: true };
      }
      return null;
    };
  }

  public minMaxLengthValidator(min: number, max: number): ValidatorFn {
    return (control): { [key: string]: boolean } | null => {
      if (control.value && (control.value.toString().length < min || control.value.toString().length > max)) {
        return { 'length': true };
      }
      return null;
    };
  }

  public upperCaseLowerCaseValidator(): ValidatorFn {
    return (control) => {
      const value = control.value as string;
      if (value && (!value.match(/[A-Z]/g) || !value.match(/[a-z]/g))) {
        return { upperCaseLowerCase: true };
      }
      return null;
    };
  }

  public specialCharacterValidator(): ValidatorFn {
    return (control) => {
      const value = control.value as string;
      if (value && !value.match(/[^a-zA-Z0-9\s]/g)) {
        return { specialCharacter: true };
      }
      return null;
    };
  }

  public numericValidator(): ValidatorFn {
    return (control) => {
      const value = control.value?.toString();

      if (value && !value.match(/^\d+$/)) {
        return { numeric: true };
      }
      return null;
    };
  }

  public numberIsGreaterThan(maxNumber: number): ValidatorFn {
    return (control) => {
      const value = control.value;

      if (value && value > maxNumber) {
        return { numberIsGreaterThan: true };
      }
      return null;
    };
  }

  public whiteSpacesStartValidator(): ValidatorFn {
    return (control) => {
      let value = control.value as string;
      if (typeof control.value !== 'string') {
        value = value + '';
      }
      
      if (value && value.trimStart() !== value) {
        return { whiteSpacesStart: true };
      }
      return null;
    };
  }

  public hasLength(maxLength: number): ValidatorFn {
    return (control) => {
      const value = control.value;

      if (value && value.length < maxLength) {
        return { lengthIsGreaterThan: true };
      }
      return null;
    };
  }

  public dateIsLessThan(hour: number, minute: number): ValidatorFn {
    return (control) => {
      const inputTime = control.value;
      const comparisonTime = new Date(inputTime);
  
      const newDate = new Date();
      newDate.setHours(hour, minute, 0, 0);
  
      if (comparisonTime < newDate) {
        return { timeInvalid: true };
      }
    
      return null;
    };
  }
}
