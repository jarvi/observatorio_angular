import { TestBed } from '@angular/core/testing';

import { HttpRequestService } from './http-request.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of, throwError } from 'rxjs';

describe('HttpRequestService', () => {
  let service: HttpRequestService;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpClient = TestBed.inject(HttpClient);
    service = TestBed.inject(HttpRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should exec a http get request and return the response', () => {
    const response = {
      data: {
        id: 1,
      }
    }

    spyOn(httpClient, 'get').and.returnValue(of(response));

    service.get('/users').subscribe((res) => {
      expect(res).toEqual(response);
    });
  });

  it('should exec a http get request and return the file response', () => {
    const response: Blob = new Blob();

    spyOn(httpClient, 'get').and.returnValue(of(response));

    service.get('/users', {}, true).subscribe((res) => {
      expect(res).toEqual(response);
    });
  });

  it('should exec a bad http get request and return the error message when is status code is greater than 0', () => {
    const error = { 
      status: 500, error: 'Internal Server Error'
    };

    spyOn(httpClient, 'get').and.returnValue(throwError(() => error));

    service.get('/users').subscribe(
      () => {},
      (err) => {
        expect(err).toEqual(error.error);
      }
    );
  });

  it('should exec a bad http get request and return the error message when is status code is equal to 0', () => {
    const error = { 
      status: 0, message: 'Unknown Error'
    };

    spyOn(httpClient, 'get').and.returnValue(throwError(() => error));

    service.get('/users').subscribe(
      () => {},
      (err) => { expect(err).toEqual(undefined); }
    );
  });

  it('should exec a http post request and return the response', () => {
    const response = {
      data: {
        id: 1,
      }
    }

    spyOn(httpClient, 'post').and.returnValue(of(response));

    service.post('/users').subscribe((res) => {
      expect(res).toEqual(response);
    });
  });

  it('should exec a bad http post request and return the error message when is status code is greater than 0', () => {
    const error = { 
      status: 500, error: 'Internal Server Error'
    };

    spyOn(httpClient, 'post').and.returnValue(throwError(() => error));

    service.post('/users').subscribe(
      () => {},
      (err) => {
        expect(err).toEqual(error.error);
      }
    );
  });

  it('should exec a bad http post request and return the error message when is status code is equal to 0', () => {
    const error = { 
      status: 0, error: 'Unknown Error'
    };

    spyOn(httpClient, 'post').and.returnValue(throwError(() => error));

    service.post('/users').subscribe(
      () => {},
      (err) => { expect(err).toEqual(undefined); }
    );
  });

  it('should exec a bad', () => {
    const error = { 
      status: 0, message: 'Unknown Error'
    };

    spyOn(httpClient, 'post').and.returnValue(throwError(() => error));

    service.post('/users').subscribe(
      () => {},
      (err) => {
        expect(err).toEqual(error);}
    );
  });

  it('should exec a http put request and return the response', () => {
    const response = {
      data: {
        id: 1,
      }
    }

    spyOn(httpClient, 'put').and.returnValue(of(response));

    service.put('/users').subscribe((res) => {
      expect(res).toEqual(response);
    });
  });

  it('should exec a bad http put request and return the error message when is status code is greater than 0', () => {
    const error = { 
      status: 500, error: 'Internal Server Error'
    };

    spyOn(httpClient, 'put').and.returnValue(throwError(() => error));

    service.put('/users').subscribe(
      () => {},
      (err) => {
        expect(err).toEqual(error.error);
      }
    );
  });

  it('should exec a bad http put request and return the error message when is status code is equal to 0', () => {
    const error = { 
      status: 0, error: 'Unknown Error'
    };

    spyOn(httpClient, 'put').and.returnValue(throwError(() => error));

    service.put('/users').subscribe(
      () => {},
      (err) => { expect(err).toEqual(undefined); }
    );
  });

  it('should exec a http delete request and return the response', () => {
    const response = {
      data: {
        id: 1,
      }
    }

    spyOn(httpClient, 'delete').and.returnValue(of(response));

    service.delete('/users').subscribe((res) => {
      expect(res).toEqual(response);
    });
  });

  it('should exec a bad http delete request and return the error message when is status code is greater than 0', () => {
    const error = { 
      status: 500, error: 'Internal Server Error'
    };

    spyOn(httpClient, 'delete').and.returnValue(throwError(() => error));

    service.delete('/users').subscribe(
      () => {},
      (err) => {
        expect(err).toEqual(error.error);
      }
    );
  });

  it('should exec a bad http delete request and return the error message when is status code is equal to 0', () => {
    const error = { 
      status: 0, message: 'Unknown Error'
    };

    spyOn(httpClient, 'delete').and.returnValue(throwError(() => error));

    service.delete('/users').subscribe(
      () => {},
      (err) => { expect(err).toEqual(error); }
    );
  });

  it('should exec a bad http delete request and return the error detail when is status code is equal to 0', () => {
    const error = { 
      status: 0, detail: 'Unknown Error'
    };

    spyOn(httpClient, 'delete').and.returnValue(throwError(() => error));

    service.delete('/users').subscribe(
      () => {},
      (err) => { expect(err).toEqual(error); }
    );
  });

  it('should exec a bad http delete request and return the error when is status code is equal to 0', () => {
    const error = { 
      status: 0, error: 'Unknown Error'
    };

    spyOn(httpClient, 'delete').and.returnValue(throwError(() => error));

    service.delete('/users').subscribe(
      () => {},
      (err) => { expect(err).toEqual(undefined); }
    );
  });
});
