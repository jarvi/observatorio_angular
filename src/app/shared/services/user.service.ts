import { Injectable, OnDestroy, inject } from '@angular/core';
import { Observable, Subscription, catchError, map, of, throwError } from 'rxjs';
import { HttpRequestService } from './http-request.service';
import { UserResponse, User, UserCreationRequest } from '../interfaces';
import { UserCreationResponse } from '../interfaces/user-creation-response.interface';
import { ChangePasswordRequest } from '../interfaces/change-password-request.interface';
import { UsersSearchRequest } from '../interfaces/users-search-request.interface';
import { UsersSearchResponse } from '../interfaces/users-search-response.interface';
import { GetUserResponse } from '../interfaces/get-user-response.interface';
import { UserActivateRequest } from '../interfaces/user-activate-request.interface';
import { UserEditionRequest } from '../interfaces/user-edition-request.interface';
import { UserEditionResponse } from '../interfaces/user-edition-response.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnDestroy {
  
  public httpRequestService$ = inject(HttpRequestService);

  private _userInformationSubscription: Subscription = new Subscription();
  private _pathUser: string = environment.pathUser;

  public userInformation: User = {
    id: 0,
    email: '',
    firstName: '',
    lastName: '',
    completeName: '',
    isActive: false,
    documentType: {
      id: 0,
      name: '',
      alias: ''
    },
    documentNumber: '',
    address: '',
    cellPhone: '',
    contactNumber: '',
    gender: '',
    roles: []
  }

  ngOnDestroy(): void {
    this._userInformationSubscription.unsubscribe();
  }

  public getUserInformation(): Observable<User> {
    if (this.userInformation.id !== 0) {
      return of(this.userInformation);
    }
    
    return this.httpRequestService$.get<{}, UserResponse>(`${this._pathUser}/info/`)
      .pipe(
        map((response: UserResponse) => {
          this.userInformation = {
            id: response.id,
            email: response.email,
            firstName: response.first_name,
            lastName: response.last_name,
            completeName: `${response.first_name} ${response.last_name}`,
            isActive: response.is_active,
            documentType: response.document_type,
            documentNumber: response.document_number,
            address: response.address,
            cellPhone: response.cell_phone,
            contactNumber: response.contact_number,
            gender: response.gender,
            roles: response.groups.map((group) => {
              return {
                id: group.id,
                name: group.name,
                isActive: group.is_active
              }
            })
          };
          return this.userInformation;
        }),
        catchError((error) => {
          return throwError(() => error?.detail || '');
        })
      );
  }

  public createUser(userInformation: UserCreationRequest): Observable<string> {
    return this.httpRequestService$.post<UserCreationRequest, UserCreationResponse>(`${this._pathUser}/register/`, userInformation)
      .pipe(
        map((response: UserCreationResponse) => `${response.first_name} ${response.last_name}`),
        catchError(() => {
          return throwError(() => '¡El usuario ya se encuentra registrado en el sistema!');
        })
      )
  }

  public editUser(userInformation: UserEditionRequest): Observable<User> {
    return this.httpRequestService$.put<UserEditionRequest, UserEditionResponse>(`${this._pathUser}/update/${userInformation.id}/`, userInformation)
      .pipe(
        map((response: UserEditionResponse) => {
          const user: User = {
            id: response.id,
            email: response.email,
            firstName: response.first_name,
            lastName: response.last_name,
            completeName: `${response.first_name} ${response.last_name}`,
            isActive: response.is_active,
            documentType: {
              id: response.document_type.id,
              name: response.document_type.name,
              alias: response.document_type.alias
            },
            documentNumber: response.document_number,
            address: response.address,
            cellPhone: response.cell_phone,
            contactNumber: response.contact_number,
            gender: response.gender,
            roles: response.groups.map(group => {
              return {
                id: group.id,
                name: group.name,
                isActive: group.is_active
              }
            })
          }
          return user;
        }),
        catchError((error) => {
          return throwError(() => error?.detail || '');
        })
      )
  }

  public changePassword(changePasswordRequest: ChangePasswordRequest): Observable<boolean> {
    return this.httpRequestService$.put<ChangePasswordRequest, {}>(`${this._pathUser}/change-password/`, changePasswordRequest)
      .pipe(
        map(() => true),
        catchError((error) => {
          return throwError(() => error?.error || '');
        })
      )
  }

  public searchUsers(usersSearchRequest: UsersSearchRequest): Observable<UsersSearchResponse> {
    return this.httpRequestService$.get<UsersSearchRequest, UsersSearchResponse>(`${this._pathUser}/list/`, usersSearchRequest)
      .pipe(
        map((response) => response),
        catchError((error) => {
          return throwError(() => error?.detail || '');
        })
      )
  }

  public getUser(id: number): Observable<User> {
    return this.httpRequestService$.get<{}, GetUserResponse>(`${this._pathUser}/detail/${id}/`, {})
      .pipe(
        map((response) => {
          return {
            address: response.address || '',
            cellPhone: response.cell_phone,
            completeName: `${response.first_name} ${response.last_name}`,
            contactNumber: response.contact_number || '',
            documentNumber: response.document_number,
            documentType: response.document_type,
            email: response.email,
            firstName: response.first_name,
            gender: response.gender,
            id: response.id,
            isActive: response.is_active,
            lastName: response.last_name,
            roles: response.groups.map((group) => {
              return {
                id: group.id,
                name: group.name,
                isActive: group.is_active
              }
            })
          }
        }),
        catchError((error) => {
          return throwError(() => error?.detail || '');
        })
      )
  }

  public activateUser({ id, is_active }: UserActivateRequest): Observable<boolean> {
    return this.httpRequestService$.put<UserActivateRequest, {}>(`${this._pathUser}/${id}/activate/`, { is_active })
      .pipe(
        map(() => true),
        catchError((error) => {
          return of(false);
        })
      );
  } 
}
