import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { IdentificationTypeService } from './identification-type.service';
import { HttpRequestService } from './http-request.service';
import { of, take } from 'rxjs';
import { IdentificationTypeResponse } from '../interfaces/identification-type-response.interface';

describe('IdentificationTypeService', () => {
  let service: IdentificationTypeService;
  let httpRequestService: HttpRequestService;
  let spyHttpRequestService: jasmine.Spy;

  const identificationTypeResponse: IdentificationTypeResponse[] = [{
    id: 1,
    name: 'Cédula de ciudadanía',
    alias: 'C.C.'
  }]

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);

    spyHttpRequestService = spyOn(httpRequestService, 'get')
    spyHttpRequestService.and.returnValue(of(identificationTypeResponse));

    service = TestBed.inject(IdentificationTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the identification types', () => {
    service.getIdentificationTypes()
      .pipe(take(1))
      .subscribe((identificationTypesResponse) => {
        expect(identificationTypesResponse).toEqual(identificationTypeResponse);
    });
    service.getIdentificationTypes()
      .pipe(take(1))
      .subscribe((identificationTypesResponse) => {
        expect(identificationTypesResponse).toEqual(identificationTypeResponse);
    });
  });

  it('should get the identification types and call http request', () => {
    service.getIdentificationTypes();
    expect(spyHttpRequestService).toHaveBeenCalled();
  });
});
