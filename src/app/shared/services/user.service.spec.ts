import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { UserService } from './user.service';
import { HttpRequestService } from './http-request.service';
import { of, take, throwError } from 'rxjs';
import { User, UserResponse } from '../interfaces';
import { UserCreationResponse } from '../interfaces/user-creation-response.interface';
import { UserCreationRequest } from '../interfaces/user-creation-request.interface';
import { ChangePasswordRequest } from '../interfaces/change-password-request.interface';
import { UsersSearchRequest } from '../interfaces/users-search-request.interface';
import { UsersSearchResponse } from '../interfaces/users-search-response.interface';
import { GetUserResponse } from '../interfaces/get-user-response.interface';
import { UserActivateRequest } from '../interfaces/user-activate-request.interface';
import { UserEditionRequest } from '../interfaces/user-edition-request.interface';
import { UserEditionResponse } from '../interfaces/user-edition-response.interface';

describe('UserService', () => {
  let service: UserService;
  let httpRequestService: HttpRequestService;

  const userInformationResponse: UserResponse = {
    id: 1,
    address: '',
    cell_phone: '',
    contact_number: '',
    date_joined: '',
    document_number: '',
    document_type: {
      id: 1,
      name: '',
      alias: ''
    },
    email: '',
    first_name: 'Prueba',
    gender: '',
    is_active: true,
    last_name: 'PruebaApellido',
    groups: [{
      id: 1,
      is_active: true,
      name: '',
    }]
  }

  const userCreationResponse: UserCreationResponse = {
    address: 'Calle 123',
    cell_phone: '1234567890',
    contact_number: '1234567890',
    document_number: '1234567890',
    document_type: 1,
    email: 'prueba@medellin.gov.co',
    first_name: 'Prueba',
    gender: 'M',
    id: 1,
    last_name: 'PruebaApellido'
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpRequestService = TestBed.inject(HttpRequestService);
    
    service = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the correct user name', () => {
    spyOn(httpRequestService, 'get').and.returnValue(of(userInformationResponse));

    service.getUserInformation()
    .pipe(take(1))
    .subscribe(() => {
      expect(service.userInformation.firstName).toEqual('Prueba');
    });
    
    service.getUserInformation()
    .pipe(take(1))
    .subscribe(() => {
      expect(service.userInformation.firstName).toEqual('Prueba');
    });
  });

  it('should get the correct user last name', () => {
    spyOn(httpRequestService, 'get').and.returnValue(of(userInformationResponse));
    
    service.getUserInformation()
    .pipe(take(1))
    .subscribe(() => {
      expect(service.userInformation.lastName).toEqual('PruebaApellido');
    });
  });

  it('should get the correct user complete name', () => {
    spyOn(httpRequestService, 'get').and.returnValue(of(userInformationResponse));
    
    service.getUserInformation()
    .pipe(take(1))
    .subscribe(() => {
      expect(service.userInformation.completeName).toEqual('Prueba PruebaApellido');
    });
  });

  it('should not get any user if the method is executed with bad request', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));
    

    service.getUserInformation()
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('');
      }
    );
  });

  it('should get the username when the creation user method is executed', () => {
    spyOn(httpRequestService, 'post').and.returnValue(of(userCreationResponse));

    const userCreationRequest: UserCreationRequest = {
      address: 'Calle 123',
      cell_phone: '1234567890',
      contact_number: '1234567890',
      document_number: '1234567890',
      document_type: 1,
      email: 'prueba@medellin.gov.co',
      first_name: 'Prueba',
      gender: 'M',
      last_name: 'PruebaApellido',
      password: '1234567890',
      send_email: true,
      groups: [1]
    }
    
    service.createUser(userCreationRequest)
    .pipe(take(1))
    .subscribe((userName) => {
      expect(userName).toEqual(`${userCreationRequest.first_name} ${userCreationRequest.last_name}`);
    });
  });

  it('should an error when the creation user method is executed with bad request', () => {
    const error = '¡El usuario ya se encuentra registrado en el sistema!';

    spyOn(httpRequestService, 'post').and.returnValue(throwError(() => {}));
    
    const userCreationRequest: UserCreationRequest = {
      address: 'Calle 123',
      cell_phone: '1234567890',
      contact_number: '1234567890',
      document_number: '1234567890',
      document_type: 1,
      email: 'prueba@medellin.gov.co',
      first_name: 'Prueba',
      gender: 'M',
      last_name: 'PruebaApellido',
      password: '1234567890',
      send_email: true,
      groups: [1]
    }

    service.createUser(userCreationRequest)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual(error);
      }
    );
  });

  it('should change password when method is executed', () => {
    spyOn(httpRequestService, 'put').and.returnValue(of({}));

    const changePasswordRequest: ChangePasswordRequest = {
      email: 'test@medellin.gov.co',
      old_password: '1234567890',
      new_password: '1234567890',
      confirm_new_password: '1234567890'
    }
    
    service.changePassword(changePasswordRequest)
    .subscribe((response) => {
      expect(response).toEqual(true);
    });
  });

  it('should not change password when method is executed with bad request', () => {
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => {}));

    const changePasswordRequest: ChangePasswordRequest = {
      email: 'test@medellin.gov.co',
      old_password: '1234567890',
      new_password: '1234567890',
      confirm_new_password: '1234567890'
    }

    service.changePassword(changePasswordRequest)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('');
      }
    );
  });

  it('should get users when method is executed', () => {
    const usersSearchResponse: UsersSearchResponse = {
      count: 1,
      next: '',
      previous: '',
      results: [{
        address: 'Calle falsa 123',
        cell_phone: '1234567890',
        contact_number: '1234567890',
        document_number: '1234567890',
        document_type: {
          id: 1,
          alias: 'CC',
          name: 'Cédula de ciudadanía'
        },
        email: 'prueba@mail.com',
        first_name: 'Prueba',
        gender: 'M',
        id: 1,
        is_active: true,
        last_name: 'PruebaApellido',
        groups: []
      }]
    }
    spyOn(httpRequestService, 'get').and.returnValue(of(usersSearchResponse));

    const usersSearchRequest: UsersSearchRequest = {
      document_number: '1234567890',
      document_type: 1,
      email: 'prueba@mail.com',
      first_name: 'Prueba',
      last_name: 'PruebaApellido',
      page: 1,
      page_size: 10
    }
    
    service.searchUsers(usersSearchRequest)
    .subscribe((response) => {
      expect(response).toEqual(usersSearchResponse);
    });
  });

  it('should an error when the users search method is executed with bad request', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));
    
    const usersSearchRequest: UsersSearchRequest = {
      document_number: '1234567890',
      document_type: 1,
      email: 'prueba@mail.com',
      first_name: 'Prueba',
      last_name: 'PruebaApellido',
      page: 1,
      page_size: 10
    }

    service.searchUsers(usersSearchRequest)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('');
      }
    );
  });

  it('should get a specific user when method is executed', () => {
    const getUserResponse: GetUserResponse = {
      address: 'Calle falsa 123',
      cell_phone: '1234567890',
      contact_number: '1234567890',
      document_number: '1234567890',
      document_type: {
        id: 1,
        alias: 'CC',
        name: 'Cédula de ciudadanía'
      },
      email: 'prueba@mail.com',
      first_name: 'Prueba',
      gender: 'M',
      id: 1,
      is_active: true,
      last_name: 'PruebaApellido',
      groups: [{
        id: 1,
        is_active: true,
        name: 'Prueba',
      }],
    }
    const userExpected: User = {
      address: 'Calle falsa 123',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '1234567890',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: [{
        id: 1,
        isActive: true,
        name: 'Prueba',
      }]
    }
    spyOn(httpRequestService, 'get').and.returnValue(of(getUserResponse));
    
    service.getUser(1)
    .subscribe((response) => {
      expect(response).toEqual(userExpected);
    });
  });

  it('should get a specific user when method is executed without address and contact number', () => {
    const getUserResponse: GetUserResponse = {
      address: '',
      cell_phone: '1234567890',
      contact_number: '',
      document_number: '1234567890',
      document_type: {
        id: 1,
        alias: 'CC',
        name: 'Cédula de ciudadanía'
      },
      email: 'prueba@mail.com',
      first_name: 'Prueba',
      gender: 'M',
      id: 1,
      is_active: true,
      last_name: 'PruebaApellido',
      groups: [],
    }
    const userExpected: User = {
      address: '',
      cellPhone: '1234567890',
      completeName: 'Prueba PruebaApellido',
      contactNumber: '',
      documentNumber: '1234567890',
      documentType: {
        id: 1,
        name: 'Cédula de ciudadanía',
        alias: 'CC'
      },
      email: 'prueba@mail.com',
      firstName: 'Prueba',
      gender: 'M',
      id: 1,
      isActive: true,
      lastName: 'PruebaApellido',
      roles: []
    }
    spyOn(httpRequestService, 'get').and.returnValue(of(getUserResponse));
    
    service.getUser(1)
    .subscribe((response) => {
      expect(response).toEqual(userExpected);
    });
  });

  it('should an error when the get user method is executed with bad request', () => {
    spyOn(httpRequestService, 'get').and.returnValue(throwError(() => {}));

    service.getUser(1)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('');
      }
    );
  });

  it('should activate a specific user when method is executed', () => {
    const userActivateRequest: UserActivateRequest = {
      id: 1,
      is_active: true
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(true));
    
    service.activateUser(userActivateRequest)
    .subscribe((response) => {
      expect(response).toBeTrue();
    });
  });

  it('should not activate any  user when method is executed with bad request', () => {
    const userActivateRequest: UserActivateRequest = {
      id: 1,
      is_active: true
    }
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => {}));

    service.activateUser(userActivateRequest)
    .pipe(take(1))
    .subscribe(
      (err) => {
        expect(err).toEqual(false);
      },
    );
  });

  it('should edit a specific user when method is executed and return complete name', () => {
    const userEditionRequest: UserEditionRequest = {
      address: 'Calle falsa 123',
      cell_phone: '1234567890',
      contact_number: '1234567890',
      email: 'prueba@medellin.gov.co',
      first_name: 'Prueba',
      gender: 'M',
      id: 1,
      last_name: 'PruebaApellido',
      groups: [1],
      send_email: true,
    }
    const userEditionResponse: UserEditionResponse = {
      address: 'Calle falsa 123',
      cell_phone: '1234567890',
      contact_number: '1234567890',
      document_number: '1234567890',
      document_type: {
        id: 1,
        name: 'Test',
        alias: 'CC'
      },
      email: 'prueba@medellin.gov.co',
      first_name: 'Prueba',
      gender: 'M',
      id: 1,
      last_name: 'PruebaApellido',
      groups: [{
        id: 1,
        is_active: true,
        name: 'Administrador'
      }],
      is_active: true,
      last_login: 'Prueba'
    }
    spyOn(httpRequestService, 'put').and.returnValue(of(userEditionResponse));
    
    service.editUser(userEditionRequest)
    .subscribe((response) => {
      expect(response.completeName).toEqual(`${userCreationResponse.first_name} ${userCreationResponse.last_name}`);
    });
  });

  it('should not edit any user when method is executed with bad request', () => {
    const userEditionRequest: UserEditionRequest = {
      address: 'Calle falsa 123',
      cell_phone: '1234567890',
      contact_number: '1234567890',
      email: 'prueba@medellin.gov.co',
      first_name: 'Prueba',
      gender: 'M',
      id: 1,
      last_name: 'PruebaApellido',
      groups: [1],
      send_email: true,
    }
    spyOn(httpRequestService, 'put').and.returnValue(throwError(() => {}));

    service.editUser(userEditionRequest)
    .pipe(take(1))
    .subscribe(
      () => {},
      (err) => {
        expect(err).toEqual('');
      }
    );
  });
});
