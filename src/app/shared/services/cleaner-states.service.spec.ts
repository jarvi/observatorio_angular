import { TestBed } from '@angular/core/testing';

import { CleanerStatesService } from './cleaner-states.service';
import { RoleService } from './role.service';
import { HttpRequestService } from './http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CleanerStatesService', () => {
  let service: CleanerStatesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        RoleService,
        HttpRequestService,
      ]
    });
    service = TestBed.inject(CleanerStatesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
