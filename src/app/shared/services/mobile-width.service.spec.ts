import { TestBed } from '@angular/core/testing';

import { MobileWidthService } from './mobile-width.service';

describe('WidthScreenService', () => {
  let service: MobileWidthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MobileWidthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
