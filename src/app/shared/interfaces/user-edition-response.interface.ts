export interface UserEditionResponse {
    id: number;
    document_type: DocumentType;
    groups: Group[];
    last_login: string;
    first_name: string;
    last_name: string;
    is_active: boolean;
    email: string;
    document_number: string;
    gender: string;
    cell_phone: string;
    contact_number: string;
    address: string;
}

export interface DocumentType {
    id: number;
    name: string;
    alias: string;
}

export interface Group {
    id: number;
    name: string;
    is_active: boolean;
}
