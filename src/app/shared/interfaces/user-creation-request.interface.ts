export interface UserCreationRequest {
    address: string;
    cell_phone: string;
    contact_number: string;
    document_number: string;
    document_type: number;
    email: string;
    first_name: string;
    gender: string;
    last_name: string;
    password: string;
    send_email: boolean;
    groups: number[];
}