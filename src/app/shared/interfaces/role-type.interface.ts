export interface RoleType {
    id: number;
    name: string;
    isActive: boolean;
}
