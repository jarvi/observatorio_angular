export interface UserActivateRequest {
    id: number;
    is_active: boolean;
}