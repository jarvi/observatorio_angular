export interface ModalDetails {
    label: string;
    valid?: boolean;
    value?: string;
    iterableValue?: string[];
}