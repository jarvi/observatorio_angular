export interface RoleEditionRequest {
    id: number;
    name: string;
    permissions?: string[];
}