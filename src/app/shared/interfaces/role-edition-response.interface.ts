export interface RoleEditionResponse {
    result: Result;
    message: string;
}

export interface Result {
    id: number;
    name: string;
    is_active: boolean;
    permissions?: Permission[];
}

export interface Permission {
    id: number;
    codename: string;
}
