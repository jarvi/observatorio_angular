export interface InputRadiobutton<T> {
    id: string;
    name: string;
    value: T;
}