export interface ChipButton {
    id: number;
    name: string;
    selected: boolean;
    blocked: boolean;
}