import { IdentificationTypeResponse } from "./identification-type-response.interface";

export interface UsersSearchResponse {
    count: number;
    next: string | null;
    previous: string | null;
    results: UserSearchResult[];
}

interface UserSearchResult {
    address: string | null;
    cell_phone: string;
    contact_number: string | null;
    document_number: string;
    document_type: IdentificationTypeResponse;
    email: string;
    first_name: string;
    gender: string;
    groups: GroupResponse[];
    id: number;
    is_active: true;
    last_name: string;
}

interface GroupResponse {
    id: number;
    is_active: boolean;
    name: string;
}