export interface UserCreationResponse {
    address: string;
    cell_phone: string;
    contact_number: string;
    document_number: string;
    document_type: number;
    email: string;
    first_name: string;
    gender: string;
    id: number;
    last_name: string;
}
