export interface GetPermissionsResponse {
    id: number;
    codename: string;
}