export interface PermissionType {
    id: number;
    codename: string;
}