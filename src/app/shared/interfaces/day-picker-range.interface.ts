export interface DatePickerRange {
    from: number | null;
    to: number | null;
}