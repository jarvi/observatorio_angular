export interface RoleCreationResponse {
    id: number;
    name: string;
    is_active: boolean;
    permissions: string[];
}