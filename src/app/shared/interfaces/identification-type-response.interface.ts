export interface IdentificationTypeResponse {
    id: number;
    name: string;
    alias: string;
}