export interface UserEditionRequest {
    address: string;
    cell_phone: string;
    contact_number: string;
    email: string;
    first_name: string;
    gender: string;
    id: number;
    last_name: string
    groups: number[];
    send_email: boolean;
}
