import { INFO_MESSAGE_TYPE } from "../enums/info-message-type.enum";

export interface InfoMessage {
    message: string;
    type: INFO_MESSAGE_TYPE;
}