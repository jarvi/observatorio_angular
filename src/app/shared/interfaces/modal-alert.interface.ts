import { MODAL_ALERT_TYPE } from "../enums/modal-alert-type.enum";

export interface ModalAlert {
    title: string;
    type: MODAL_ALERT_TYPE;
    message?: string;
    highlightText?: string;
    confirmButton: ModalAlertButton;
    cancelButton?: ModalAlertButton;
}

interface ModalAlertButton {
    label: string;
    action?: () => void;
    primary?: boolean;
}