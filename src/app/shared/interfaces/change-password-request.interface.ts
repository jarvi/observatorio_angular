export interface ChangePasswordRequest {
    email: string;
    old_password: string;
    new_password: string;
    confirm_new_password: string;
}