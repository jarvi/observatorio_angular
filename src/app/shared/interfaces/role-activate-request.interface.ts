export interface RoleActivateRequest {
    id: number;
    is_active: boolean;
}