export interface GetRoleResponse {
    id: number;
    name: string;
    is_active: boolean;
    permissions: PermissionResponse[];
}

interface PermissionResponse {
    id: number;
    codename: string;
}