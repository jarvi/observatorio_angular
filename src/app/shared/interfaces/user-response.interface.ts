import { IdentificationTypeResponse } from "./identification-type-response.interface";

export interface UserResponse {
    address: string;
    cell_phone: string;
    contact_number: string;
    date_joined: string;
    document_number: string;
    document_type: IdentificationTypeResponse;
    email: string;
    first_name: string;
    gender: string;
    groups: GroupResponse[];
    id: number;
    is_active: boolean;
    last_name: string;
}

interface GroupResponse {
    id: number;
    is_active: boolean;
    name: string;
}