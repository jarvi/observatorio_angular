export interface RolesSearchResponse {
    count: number;
    next: string | null;
    previous: string | null;
    results: RoleSearchResult[];
}

interface RoleSearchResult {
    id: number;
    name: string;
    is_active: boolean;
    permissions: PermissionResponse[];
}

interface PermissionResponse {
    id: number;
    codename: string;
}