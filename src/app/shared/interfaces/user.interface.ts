import { IdentificationType } from "./identification-type.interface";
import { Role } from "./role.interface";

export interface User {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    completeName: string;
    isActive: boolean;
    documentType: IdentificationType;
    documentNumber: string;
    gender: string;
    cellPhone: string;
    contactNumber: string;
    address: string;
    roles: Role[];
}
