export interface IdentificationType {
    id: number;
    name: string;
    alias: string;
}