export interface DayPicker {
    name: number;
    selected: boolean;
    focused: boolean;
    blocked: boolean;
}