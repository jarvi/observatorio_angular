export interface RoleCreationRequest {
    name: string;
    permissions: string[];
}