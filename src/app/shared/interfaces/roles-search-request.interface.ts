export interface RolesSearchRequest {
    page?: number;
    page_size?: number;
    active: boolean | '';
    id?: number | '';
    permissions?: string;
}