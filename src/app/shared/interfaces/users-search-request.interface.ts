export interface UsersSearchRequest {
    document_number: string;
    document_type: number | string;
    email: string;
    first_name: string;
    last_name: string;
    page_size: number;
    page: number;
}