export interface Gender {
    id: number;
    name: string;
    alias: string;
}