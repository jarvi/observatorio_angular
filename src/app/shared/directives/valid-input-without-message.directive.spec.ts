import { Component, DebugElement } from '@angular/core';
import { ValidInputWithoutMessageDirective } from './valid-input-without-message.directive';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: `
    <div>
      <input type="text" [formControl]="controlText" validInputWithoutMessage>
    </div>
  `
})
class TestComponent {
  controlText: FormControl = new FormControl('', [Validators.required, Validators.maxLength(3)]);
}

describe('ValidInputWithoutMessageDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let inputTextElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestComponent, ValidInputWithoutMessageDirective],
      imports: [FormsModule, ReactiveFormsModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    inputTextElement = fixture.debugElement.query(By.css('input'));
    fixture.detectChanges();
  });

  it('should add ng-dirty and ng-invalid classes to controlText when is invalid', () => {
    component.controlText.setValue('');
    fixture.detectChanges();

    const errorMessageElement = inputTextElement.nativeElement;
    expect(errorMessageElement).toBeTruthy();
    expect(errorMessageElement.classList).toContain('ng-dirty');
    expect(errorMessageElement.classList).toContain('ng-invalid');
  });
});
