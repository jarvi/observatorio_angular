import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[DropZone]'
})
export class DropZoneDirective {

  constructor(private _ref: ElementRef) {}

  @Output() onFileDropped = new EventEmitter<File[]>();

  @HostListener('dragover', ['$event']) public onDragOver(evt: DragEvent): void {
    evt.preventDefault();
    evt.stopPropagation();
    this._ref.nativeElement.classList.add('dragover');
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt: DragEvent): void {
    evt.preventDefault();
    evt.stopPropagation();
    this._ref.nativeElement.classList.remove('dragover');
  }

  @HostListener('drop', ['$event']) public ondrop(evt: DragEvent): void {
    evt.preventDefault();
    evt.stopPropagation();
    this._ref.nativeElement.classList.remove('dragover');
    const files: File[] = Array.from(evt.dataTransfer?.files ?? []);
    if (files.length > 0) {
      this.onFileDropped.emit(files);
    }
  }
}
