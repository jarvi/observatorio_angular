import { Directive, ElementRef, Input, OnDestroy, OnInit, Renderer2, inject } from '@angular/core';
import { UserPermissionsService } from '../services/user-permissions.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[checkPermissionToRender]'
})
export class CheckPermissionToRenderDirective implements OnInit, OnDestroy {

  @Input() alternativeElement?: HTMLElement;
  @Input() checkPermissionToRender: string = '';

  private _userPermissionsService = inject(UserPermissionsService);

  private _subscription: Subscription = new Subscription();

  constructor(private _elementRef: ElementRef, private _renderer: Renderer2) {}

  ngOnInit() {
    this.checkAccess();
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  private renderElement(hasAccess: boolean): void {
    const parentNode = this._elementRef.nativeElement.parentNode;
  
    if (!hasAccess) {
      this._renderer.removeChild(parentNode, this._elementRef.nativeElement);
      this.alternativeElement && this._renderer.appendChild(parentNode, this.alternativeElement);
      return;
    }

    this.alternativeElement && this._renderer.removeChild(parentNode, this.alternativeElement);
  }

  private checkAccess() {
    if (!this.checkPermissionToRender) return;
    
    this._subscription = this._userPermissionsService.permissions$.subscribe((permissions) => {
      const hasAccess = permissions.some((permission) => permission.codename === this.checkPermissionToRender);
      this.renderElement(hasAccess);
      this._subscription.unsubscribe();
    });
  }
}
