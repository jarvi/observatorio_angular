import { Component } from '@angular/core';
import { CheckPermissionToRenderDirective } from './check-permission-to-render.directive';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PERMISSION_TYPE } from '../enums/permission-type.enum';
import { UserPermissionsService } from '../services/user-permissions.service';
import { HttpRequestService } from '../services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

@Component({
  template: `
    <div [checkPermissionToRender]="permission" [alternativeElement]="alternative">
      <span>ELEMENT TEST</span>
    </div>
    <div #alternative>
      <span>ALTERNATIVE ELEMENT</span>
    </div>
  `
})
class TestComponent {
  public permission = PERMISSION_TYPE.addUser.name
}

describe('CheckPermissionToRenderDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let userPermissionsService: UserPermissionsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestComponent, CheckPermissionToRenderDirective],
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        UserPermissionsService,
        HttpRequestService
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    userPermissionsService = TestBed.inject(UserPermissionsService);

    spyOnProperty(userPermissionsService, 'permissions$', 'get').and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.addUser.name
    }]));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should valid the user permissions and allow access to element', () => {
    const spanElement: HTMLElement = fixture.nativeElement.querySelector('span');
    expect(spanElement).toBeTruthy();
    expect(spanElement.innerText).toContain('ELEMENT TEST');
  });
});

describe('CheckPermissionToRenderDirective whitout access', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let userPermissionsService: UserPermissionsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestComponent, CheckPermissionToRenderDirective],
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        UserPermissionsService,
        HttpRequestService
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    userPermissionsService = TestBed.inject(UserPermissionsService);

    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.changeUser.name
    }]));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should valid the user permissions and deny access to element', () => {
    const spanElement: HTMLElement = fixture.nativeElement.querySelector('span');
    expect(spanElement).toBeTruthy();
    expect(spanElement.innerText).toContain('ALTERNATIVE ELEMENT');
  });
});