import { Component, DebugElement } from '@angular/core';
import { ValidInputErrorsDirective } from './valid-input-errors.directive';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { INPUT_TYPE_ERROR } from '../enums/input-error-type.enum';

@Component({
  template: `
    <div>
      <input type="text" [formControl]="controlText" validInputErrors>
    </div>
    <div>
      <span class="input-password-show-hide">
        <input type="password" [formControl]="controlPassword" validInputErrors>
      </span>
    </div>
  `
})
class TestComponent {
  controlText: FormControl = new FormControl('', [Validators.required, Validators.maxLength(3), Validators.pattern('^[a-zA-Z]+$')]);
  controlPassword: FormControl = new FormControl('', [Validators.required]);
}

describe('ValidInputErrorsDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let inputTextElement: DebugElement;
  let inputPasswordElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestComponent, ValidInputErrorsDirective],
      imports: [FormsModule, ReactiveFormsModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    inputTextElement = fixture.debugElement.query(By.css('input'));
    inputPasswordElement = fixture.debugElement.query(By.css('.input-password-show-hide'));
    fixture.detectChanges();
  });

  it('should create an error message element when controlText is invalid', () => {
    component.controlText.setValue('');
    fixture.detectChanges();

    const errorMessageElement = inputTextElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeTruthy();
    expect(errorMessageElement.innerText).toContain(INPUT_TYPE_ERROR.required);
  });

  it('should remove the error message element when controlText becomes valid', () => {
    component.controlText.setValue('');
    fixture.detectChanges();

    let errorMessageElement = inputTextElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeTruthy();

    component.controlText.setValue('Val');
    fixture.detectChanges();

    errorMessageElement = inputTextElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeFalsy();
  });

  it('should create an error message element when controlPassword is invalid', () => {
    component.controlPassword.setValue('');
    fixture.detectChanges();
    
    const errorMessageElement = inputPasswordElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeTruthy();
    expect(errorMessageElement.innerText).toContain(INPUT_TYPE_ERROR.required);
  });

  it('should remove the error message element when controlPassword becomes valid', () => {
    component.controlPassword.setValue('');
    fixture.detectChanges();

    let errorMessageElement = inputPasswordElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeTruthy();

    component.controlPassword.setValue('Valid value');
    fixture.detectChanges();

    errorMessageElement = inputPasswordElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeFalsy();
  });

  it('should create an error message element when controlText value has a length greater than 3', () => {
    component.controlText.setValue('123456');
    fixture.detectChanges();

    const errorMessageElement = inputTextElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeTruthy();
    expect(errorMessageElement.innerText).toContain(INPUT_TYPE_ERROR.maxlength);
  });

  it('should create an error message element when controlText value has a different type of error', () => {
    component.controlText.setValue(123456);
    fixture.detectChanges();

    const errorMessageElement = inputTextElement.nativeElement.parentNode.querySelector('.error-input-message');
    expect(errorMessageElement).toBeTruthy();
    expect(errorMessageElement.innerText).toEqual('');
  });
});
