import { Component } from '@angular/core';
import { DropZoneDirective } from './drop-zone.directive';
import { ComponentFixture, TestBed } from '@angular/core/testing';


@Component({
  template: `
    <div>
      <span DropZone>ELEMENT TEST</span>
    </div>
  `
})
class TestComponent {
  
}

describe('DropZoneDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestComponent, DropZoneDirective],
      imports: [
        
      ],
      providers: [
        
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should drag over span element and add it the dragover class', () => {
    const spanElement: HTMLElement = fixture.nativeElement.querySelector('span');
    spanElement.dispatchEvent(new DragEvent('dragover'));
    expect(spanElement.classList.contains('dragover')).toBeTruthy();
  });

  it('should drag leave of span element and revome the dragover class', () => {
    const spanElement: HTMLElement = fixture.nativeElement.querySelector('span');
    spanElement.dispatchEvent(new DragEvent('dragover'));
    expect(spanElement.classList.contains('dragover')).toBeTruthy();

    spanElement.dispatchEvent(new DragEvent('dragleave'));
    expect(spanElement.classList.contains('dragover')).toBeFalsy();
  });

  it('should drop nothing file to span element and before revome the dragover class', () => {
    const spanElement: HTMLElement = fixture.nativeElement.querySelector('span');
    spanElement.dispatchEvent(new DragEvent('dragover'));
    expect(spanElement.classList.contains('dragover')).toBeTruthy();

    spanElement.dispatchEvent(new DragEvent('drop'));
    expect(spanElement.classList.contains('dragover')).toBeFalsy();
  });

  it('should drop a file to span element and before revome the dragover class', () => {
    const spanElement: HTMLElement = fixture.nativeElement.querySelector('span');
    spanElement.dispatchEvent(new DragEvent('dragover'));
    expect(spanElement.classList.contains('dragover')).toBeTruthy();

    const dataTransfer = new DataTransfer();
    dataTransfer.items.add(new File([''], 'test.txt'));
    const dropEvent = new DragEvent('drop', {
      dataTransfer: dataTransfer
    });

    spanElement.dispatchEvent(dropEvent);
    expect(spanElement.classList.contains('dragover')).toBeFalsy();
  });
});
