import { Directive, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { NgControl, ValidationErrors } from '@angular/forms';
import { Subscription } from 'rxjs';
import { INPUT_TYPE_ERROR } from '../enums/input-error-type.enum';

@Directive({
  selector: '[validInputErrors]'
})
export class ValidInputErrorsDirective implements OnInit, OnDestroy {

  constructor(private _el: ElementRef, private _control: NgControl) { }

  private _subscription: Subscription = new Subscription();

  ngOnInit() {
    this._subscription = this._control.statusChanges!.subscribe(() => {
      this.updateErrorMessage();
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private updateErrorMessage() {
    const originalElement = this._el.nativeElement;
    const parentElement = this._el.nativeElement.parentNode as HTMLElement;
    const currentElement = parentElement.classList.contains('input-password-show-hide')
      ? parentElement.parentNode as HTMLElement
      : parentElement;

    this.createErrorMessageElement(currentElement, originalElement);
  }

  private getErrorMessage(errors: ValidationErrors): string {
    const inputTypeErrorKeys = Object.keys(INPUT_TYPE_ERROR);
    const keyFound = Object.keys(errors).find((key) => inputTypeErrorKeys.includes(key));
    const errorType = keyFound ? INPUT_TYPE_ERROR[keyFound as keyof typeof INPUT_TYPE_ERROR] : '';
  
    if (errorType === INPUT_TYPE_ERROR.maxlength || errorType === INPUT_TYPE_ERROR.minlength) {
      const requiredLength = errors[keyFound as keyof typeof INPUT_TYPE_ERROR]?.requiredLength;
      return `${errorType} ${requiredLength}`;
    }
  
    return errorType;
  }

  private createErrorMessageElement(parentElement: HTMLElement, originalElement: HTMLElement) {
    const existingErrorElement = parentElement.querySelector('.error-input-message');
    if (existingErrorElement) {
      existingErrorElement.remove();
    }

    if (this._control.errors) {
      const spanError = document.createElement('span');
      spanError.classList.add('error-input-message');
      spanError.innerText = this.getErrorMessage(this._control.errors);
      parentElement.appendChild(spanError);
      originalElement.classList.add('ng-dirty');
    }
  }
}
