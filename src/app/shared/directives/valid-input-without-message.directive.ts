import { Directive, ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[validInputWithoutMessage]'
})
export class ValidInputWithoutMessageDirective {

  constructor(private el: ElementRef, private _control: NgControl) { }

  private _subscription: Subscription = new Subscription();

  ngOnInit() {
    this._subscription = this._control.statusChanges!.subscribe(() => {
      this.updateErrorMessage();
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private updateErrorMessage() {
    const originalElement = this.el.nativeElement;

    this.createErrorMessageElement(originalElement);
  }

  private createErrorMessageElement(originalElement: HTMLElement) {
    if (this._control.errors) {
      originalElement.classList.add('ng-dirty');
    }
  }

}
