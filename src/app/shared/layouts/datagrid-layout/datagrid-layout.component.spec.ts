import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatagridLayoutComponent } from './datagrid-layout.component';
import { SharedModule } from '../../shared.module';

describe('DatagridLayoutComponent', () => {
  let component: DatagridLayoutComponent;
  let fixture: ComponentFixture<DatagridLayoutComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatagridLayoutComponent],
      imports: [
        SharedModule
      ],
    });
    fixture = TestBed.createComponent(DatagridLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit changePerPage value when onChangePerPage is executed', () => {
    spyOn(component.changePerPage, 'emit');
    const event: any = {
      value: '10'
    }

    component.onChangePerPage(event)
    expect(component.changePerPage.emit).toHaveBeenCalledWith(event.value);
  });
});
