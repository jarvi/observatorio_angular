import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DropdownChangeEvent } from 'primeng/dropdown';
import { perPageOptions } from 'src/app/shared/constants/perpage-options';

@Component({
  selector: 'shared-datagrid-layout',
  templateUrl: './datagrid-layout.component.html',
  styleUrls: ['./datagrid-layout.component.scss'],
})
export class DatagridLayoutComponent {

  @Input() public isEmpty: boolean = true;
  @Input() public totalResults: number = 0;
  @Input() public perPage: string = '10';
  @Output() public changePerPage: EventEmitter<string> = new EventEmitter<string>();

  public perpageResultsOptions: string[] = perPageOptions.map((option) => option.toString());

  public onChangePerPage(event: DropdownChangeEvent): void {
    this.changePerPage.emit(event.value);
  }
}
