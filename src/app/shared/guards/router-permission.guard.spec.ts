import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, CanActivateFn, Router } from '@angular/router';

import { routerPermissionGuard } from './router-permission.guard';
import { UserPermissionsService } from '../services/user-permissions.service';
import { HttpRequestService } from '../services/http-request.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PERMISSION_TYPE } from '../enums/permission-type.enum';
import { Observable, of, take } from 'rxjs';

describe('routerPermissionGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => routerPermissionGuard(...guardParameters));

  let router: Router;
  let userPermissionsService: UserPermissionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        UserPermissionsService,
        HttpRequestService
      ]
    });
    router = TestBed.inject(Router);
    userPermissionsService = TestBed.inject(UserPermissionsService);
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });

  it('should deny access to route if the user does not have the correctly permission', () => {
    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.changeUser.name,
    }]));

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    route.data = {
      permissions: [PERMISSION_TYPE.addUser.name]
    };
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const executeGuardResult: any = executeGuard(route, state);

    executeGuardResult.pipe(take(1))
    .subscribe((result: boolean) => {
      expect(result).toBeFalsy();
    });
  });

  it('should deny access to route if the route has undefined permissions', () => {
    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.changeUser.name,
    }]));

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    route.data = {
      permissions: undefined
    };
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const executeGuardResult: any = executeGuard(route, state);

    executeGuardResult.pipe(take(1))
    .subscribe((result: boolean) => {
      expect(result).toBeFalsy();
    });
  });

  it('should access to route if the user has the correctly permission', () => {
    spyOn(userPermissionsService, 'getPermissions').and.returnValue(of([{
      id: 1,
      codename: PERMISSION_TYPE.addUser.name,
    }]));

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    route.data = {
      permissions: [PERMISSION_TYPE.addUser.name]
    };
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const executeGuardResult: any = executeGuard(route, state);

    executeGuardResult.pipe(take(1))
    .subscribe((result: boolean) => {
      expect(result).toBeTruthy();
    });
  });
});
