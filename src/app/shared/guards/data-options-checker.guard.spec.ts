import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, CanActivateFn, Router } from '@angular/router';

import { dataOptionsCheckerGuard } from './data-options-checker.guard';
import { DATA_OPTIONS_TYPES } from '../enums/data-options-types';

describe('dataOptionsCheckerGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => dataOptionsCheckerGuard(...guardParameters));

  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    router = TestBed.inject(Router);
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });

  it('should allow go to target route if option is valid', () => {
    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    route.params = {
      type: DATA_OPTIONS_TYPES.simat
    };
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const accessDeny = executeGuard(route, state);

    expect(accessDeny).toBeTruthy();
  });

  it('should deny go to target route if option is not valid', () => {
    spyOn(router, 'navigateByUrl');

    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    route.params = {
      type: 'not valid'
    };
    const state = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    const accessDeny = executeGuard(route, state);

    expect(accessDeny).toBeFalsy();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/');
  });
});
