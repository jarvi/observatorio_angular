import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { DATA_OPTIONS_TYPES } from '../enums/data-options-types';

export const dataOptionsCheckerGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const option = route.params['type'];
  const optionValids = Object.values(DATA_OPTIONS_TYPES);
  
  if (!optionValids.includes(option)) {
    router.navigateByUrl('/');
    return false;
  }

  return true;
};
