import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { map } from 'rxjs';
import { UserPermissionsService } from '../services/user-permissions.service';
import { PermissionType } from '../interfaces/permission-type.interface';

export const routerPermissionGuard: CanActivateFn = (route, state) => {

  const router = inject(Router);
  const userPermissionsService = inject(UserPermissionsService);

  const routePermissions = route.data['permissions'] || [] as string[];

  const verifyPermissions = (permissions: PermissionType[]): boolean => {
    const userPermissionsList = permissions.map((permission) => permission.codename);
    const permissionsMatch: boolean = routePermissions.some((val: string) => userPermissionsList.includes(val));

    if (!permissionsMatch) {
      router.navigateByUrl('/');
    }

    return permissionsMatch;
  }

  return userPermissionsService.getPermissions().pipe(
    map((permissions) => {
      return verifyPermissions(permissions);
    })
  );
};
