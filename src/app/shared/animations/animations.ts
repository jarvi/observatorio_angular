import { style, animate, trigger, transition, query } from '@angular/animations';

export const slideInOutAnimation = trigger('slideInOut', [
    transition(':enter', [
        style({ opacity: '0' }),
        animate('.15s ease-out', style({ opacity: '1' }))
    ]),
    transition(':leave', [
        animate('.15s ease-out', style({ opacity: '0' }))
    ])
]);

export const fadeAnimation = trigger('fadeAnimation', [
    transition('* => *', [
        query(':enter', [style({ opacity: 0 })], {
            optional: true,
        }),
        query(
            ':leave',
            [
                style({ opacity: 1 }),
                animate('.2s', style({ opacity: 0 })),
            ],
            { optional: true }
        ),
    ]),
]);