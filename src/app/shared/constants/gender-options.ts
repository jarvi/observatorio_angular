import { Gender } from "../interfaces/gender.interface";

export const genderOptions: Gender[] = [
    { id: 1, name: 'Masculino', alias: 'M' },
    { id: 2, name: 'Feminino', alias: 'F' },
]