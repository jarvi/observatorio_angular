FROM node:18 as builder
WORKDIR /app
COPY package.json /app
COPY scripts /app/scripts
RUN yarn install
COPY . .
RUN cp -f .env.dev .env
RUN node ./scripts/set-envs.js
RUN yarn build

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/dist/frontend-observatorio /usr/share/nginx/html
EXPOSE 80