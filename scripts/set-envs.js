const { writeFileSync, mkdirSync } = require('fs');
require('dotenv').config();

const targetPath = `./src/environments/environment.ts`;

const envConfigFile = `
export const environment = {
    appVersion: '${process.env.APP_VERSION}',
    poweredBy: '${process.env.POWERED_BY}',
    urlBase: '${process.env.URL_BASE}',
    urlPowerBIDinamicReports: '${process.env.URL_POWER_BI_DINAMIC_REPORTS}',
    pathUser: '${process.env.PATH_USER}',
    pathRole: '${process.env.PATH_ROLE}',
    pathBoard: '${process.env.PATH_BOARD}',
    pathReport: '${process.env.PATH_REPORT}',
    pathNotifications: '${process.env.PATH_NOTIFICATIONS}',
};
`;

mkdirSync('./src/environments', { recursive: true });

writeFileSync(targetPath, envConfigFile);